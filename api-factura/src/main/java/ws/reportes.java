package ws;

import Auth.JWT;
import Entities.FE.EmisorToken;
import Entities.MensajeWs.MensajeBase;
import Entities.reports.ReportParameter;
import Entities.reports.ReporteEmitidos;
import Entities.reports.ReporteRecibidos;
import Methods.IMetodos;
import Methods.Metodos;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Path("/reportesws")
public class reportes {

    @POST
    @Path("/reporteEmitidos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reporteEmitidos(@HeaderParam("pToken") String pToken,
                                    List<ReportParameter> params) {
        StreamingOutput zipFile = null;
        MensajeBase res = new MensajeBase();
        EmisorToken mClaims;
        String fileName = "Documentos.zip";
        List<ReporteEmitidos> reporte = null;

        try {


            if (pToken == null || pToken.equals("")) {
                return Response.ok().header("status", "ERROR").header("mensaje", "El token es requerido").build();
            }

            mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                return Response.ok().header("status", "ERROR").header("mensaje", "Token invalido o expirado").build();
            }


            if (params.size() == 0 ) {
                return Response.ok()
                        .header("status", "ERROR")
                        .header("mensaje", "El tipo de documento, el estado y las fecha de inicio y fin son requeridos")
                        .build();
            }

            ReportParameter test = new ReportParameter("fechaInicio");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "La fecha de inicio es requerida").build();

            }
            test.setName("fechaFinal");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "La fecha final es requerida").build();

            }
            test.setName("tipoDocumento");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "El tipo documento es requerido").build();

            }
            test.setName("estado");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "El estado es requerido").build();

            }

            IMetodos metodos = new Metodos();

            reporte = metodos.getReportEmitidos(mClaims.getIdEmpresaFe(),
                    test.findParamByName(params, "fechaInicio"),
                    test.findParamByName(params, "fechaFinal"),
                    test.findParamByName(params, "estado"),
                    test.findParamByName(params, "tipoDocumento"));

            String[] columns = { "Documento", "Clave", "Consecutivo",
                    "Cliente", "Identificación", "Correo Electrónico", "Condición Venta", "Plazo Crédito",
                    "Tipo Cambio", "Total Servicios Gravados", "total Servicios Exentos",
                    "Total de Mercancias Exentas",
                    "Total de Mercancias Gravadas", "Total Gravado", "Total Exento", "Total Descuentos",
                    "Total Impuestos",  "Total", "Moneda", "Estado", "Fecha", "Motivo Rechazo"};

            final Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("documentosEmitidos");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            // Create Other rows and cells with contacts data
            int rowNum = 1;

            for (ReporteEmitidos r : reporte) {
                Row row = sheet.createRow(rowNum++);

                row.createCell(0).setCellValue(r.getTipoDescDocumento());
                row.createCell(1).setCellValue(r.getClave());
                row.createCell(2).setCellValue(r.getConsecutivo());
                row.createCell(3).setCellValue(r.getCliente());
                row.createCell(4).setCellValue(r.getIdentificacion());
                row.createCell(5).setCellValue(r.getCorreo());
                row.createCell(6).setCellValue(r.getCondicionVenta());
                row.createCell(7).setCellValue(r.getPlazoCredito());
                row.createCell(8).setCellValue(r.getTipoCambio().toPlainString());
                row.createCell(9).setCellValue(r.getTotalServiciosGrav().toPlainString());
                row.createCell(10).setCellValue(r.getTotalServiciosExen().toPlainString());
                row.createCell(11).setCellValue(r.getTotalMercanciasExen().toPlainString());
                row.createCell(12).setCellValue(r.getTotalMercanciasGrav().toPlainString());
                row.createCell(13).setCellValue(r.getTotalGravado().toPlainString());
                row.createCell(14).setCellValue(r.getTotalExento().toPlainString());
                row.createCell(15).setCellValue(r.getTotalDescuento().toPlainString());
                row.createCell(16).setCellValue(r.getTotalImpuestos().toPlainString());
                row.createCell(17).setCellValue(r.getTotalComprobante().toPlainString());
                row.createCell(18).setCellValue(r.getMoneda());
                row.createCell(19).setCellValue(r.getDescEstado());
                row.createCell(20).setCellValue(r.getFechaEmision());
                row.createCell(21).setCellValue(r.getMotivoRechazo());
            }
            // Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream("documentosemitidos.xlsx");


            StreamingOutput output = new StreamingOutput() {
                @Override
                public void write(OutputStream out)
                        throws IOException, WebApplicationException {
                    workbook.write(out);
                    out.close();
                }
            };

            return Response.ok( output, MediaType.APPLICATION_OCTET_STREAM).header("status", "SUCCESS").header("mensaje", "Reporte generado correctamente")
                    .header("Content-Disposition", "attachment; filename=documentosemitidos.xlsx").build();

        } catch (Exception e) {
            res.setStatus("ERROR");
            res.setMensaje( e.getMessage());
            return Response.ok(res, MediaType.APPLICATION_JSON).header("status", "ERROR").header("mensaje", e.getMessage()).build();
        }

    }


    @POST
    @Path("/reporteRecibidos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reporteRecibidos(@HeaderParam("pToken") String pToken,
                                    List<ReportParameter> params) {
        StreamingOutput zipFile = null;
        MensajeBase res = new MensajeBase();
        EmisorToken mClaims;
        String fileName = "Documentos.zip";
        List<ReporteRecibidos> reporte = null;

        try {


            if (pToken == null || pToken.equals("")) {
                return Response.ok().header("status", "ERROR").header("mensaje", "El token es requerido").build();
            }

            mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                return Response.ok().header("status", "ERROR").header("mensaje", "Token invalido o expirado").build();
            }


            if (params.size() == 0 ) {
                return Response.ok()
                        .header("status", "ERROR")
                        .header("mensaje", "El tipo de documento, el estado y las fecha de inicio y fin son requeridos")
                        .build();
            }

            ReportParameter test = new ReportParameter("fechaInicio");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "La fecha de inicio es requerida").build();

            }
            test.setName("fechaFinal");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "La fecha final es requerida").build();

            }
            test.setName("tipoDocumento");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "El tipo documento es requerido").build();

            }
            test.setName("estado");
            if( !params.contains(test) ){
                return Response.ok().header("status", "ERROR").header("mensaje", "El estado es requerido").build();

            }

            IMetodos metodos = new Metodos();

            reporte = metodos.getReportRecibidos(mClaims.getIdEmpresaFe(),
                    test.findParamByName(params, "fechaInicio"),
                    test.findParamByName(params, "fechaFinal"),
                    test.findParamByName(params, "estado"),
                    test.findParamByName(params, "tipoDocumento"));

            String[] columns = { "Documento", "Consecutivo",
                    "Identificación Emisor", "Emisor", "Nombre Comercial", "Correo Electrónico Emisor", "Moneda",
                    "Total Comprobante", "Fecha Emisión",
                    "Fecha de Aceptación", "Aceptación", "Estado", "Motivo Rechazo"};

            final Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("documentosRecibidos");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            // Create Other rows and cells with contacts data
            int rowNum = 1;

            for (ReporteRecibidos r : reporte) {
                Row row = sheet.createRow(rowNum++);

                row.createCell(0).setCellValue(r.getDescTipoDocumento());
                row.createCell(1).setCellValue(r.getConsecutivo());
                row.createCell(2).setCellValue(r.getIdentificacionEmisor());
                row.createCell(3).setCellValue(r.getRazonSocial());
                row.createCell(4).setCellValue(r.getNombreComercial());
                row.createCell(5).setCellValue(r.getCorreoElectronico());
                row.createCell(6).setCellValue(r.getMoneda());
                row.createCell(7).setCellValue(r.getTotalComprobante().toPlainString());
                row.createCell(8).setCellValue(r.getFechaEmision());
                row.createCell(9).setCellValue(r.getFechaAceptacion());
                row.createCell(10).setCellValue(r.getDescAceptacion());
                row.createCell(11).setCellValue(r.getDescEstado());
                row.createCell(12).setCellValue(r.getMotivoRechazo());
            }
            // Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream("documentosrecibidos.xlsx");


            StreamingOutput output = new StreamingOutput() {
                @Override
                public void write(OutputStream out)
                        throws IOException, WebApplicationException {
                    workbook.write(out);
                    out.close();
                }
            };

            return Response.ok( output, MediaType.APPLICATION_OCTET_STREAM).header("status", "SUCCESS").header("mensaje", "Reporte generado correctamente")
                    .header("Content-Disposition", "attachment; filename=documentosrecibidos.xlsx").build();

        } catch (Exception e) {
            res.setStatus("ERROR");
            res.setMensaje( e.getMessage());
            return Response.ok(res, MediaType.APPLICATION_JSON).header("status", "ERROR").header("mensaje", e.getMessage()).build();
        }

    }

}
