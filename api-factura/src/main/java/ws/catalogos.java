package ws;

import Auth.JWT;
import Constants.Constantes;
import Entities.Catalogos.CodDescripcion;
import Entities.FE.ProcesarCatalogoEmp;
import Entities.FE.AddTipoDocEmp;
import Entities.FE.AgregarExoArticulo;
import Entities.FE.ArticuloAdd;
import Entities.FE.EmisorToken;
import Entities.FE.ExoneracionArt;
import Entities.FE.GetBarrios;
import Entities.FE.GetCantones;
import Entities.FE.GetDistritos;
import Entities.MensajeWs.*;
import Methods.IMetodos;
import Methods.Metodos;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Administrador
 */
@Path("/catalogosws")
public class catalogos {

    @GET
    @Path("/unidadesMedida")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeUnidadesMedida unidadesMedida(@HeaderParam("pToken") String pToken) {
        MensajeUnidadesMedida mensajeConsulta = new MensajeUnidadesMedida();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getUnidadesMedida(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addUnidadMedidaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addUnidadMedidaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addUnidadMedidaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteUnidadMedidaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteUnidadMedidaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La unidad de medida es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La unidad de medida de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteUnidadMedidaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/condicionesVenta")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCondicionesVenta condicionesVenta(@HeaderParam("pToken") String pToken) {
        MensajeCondicionesVenta mensajeConsulta = new MensajeCondicionesVenta();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getCondicionesVenta(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addCondicionVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addCondicionVentaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addCondicionVentaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteCondicionVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteCondicionVentaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La condicion de venta es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La condicion de venta de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteCondicionVentaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/distGeografica")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDistGeografica distGeografica(@HeaderParam("pToken") String pToken) {
        List<CodDescripcion> niveles = new ArrayList<>();
        CodDescripcion nivel;
        MensajeDistGeografica mensajeConsulta = new MensajeDistGeografica();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getDistGeografica(mClaims);

            nivel = new CodDescripcion();
            nivel.setCodigo("0");
            nivel.setDescripcion("Pais");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("1");
            nivel.setDescripcion("Provincia");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("2");
            nivel.setDescripcion("Canton");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("3");
            nivel.setDescripcion("Distrito");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("4");
            nivel.setDescripcion("Barrio");

            niveles.add(nivel);

            mensajeConsulta.setNiveles(niveles);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addDistGeoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addDistGeoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addDistGeoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteDistGeoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteDistGeoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La distribucion geografica es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La distribucion geografica de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteDistGeoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/mediosPago")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeMediosPago mediosPago(@HeaderParam("pToken") String pToken) {
        MensajeMediosPago mensajeConsulta = new MensajeMediosPago();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getMediosPago(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addMedioPagoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addMedioPagoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addMedioPagoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteMedioPagoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteMedioPagoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El medio de pago es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El medio de pago de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteMedioPagoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/impuestos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeImpuestos impuestos(@HeaderParam("pToken") String pToken) {
        MensajeImpuestos mensajeConsulta = new MensajeImpuestos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getImpuestos(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addImpuestoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addImpuestoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addImpuestoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteImpuestoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteImpuestoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El impuesto es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El impuesto de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteImpuestoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/monedas")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeMonedas monedas(@HeaderParam("pToken") String pToken) {
        MensajeMonedas mensajeConsulta = new MensajeMonedas();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getMonedas(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addMonedaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addMonedaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addMonedaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteMonedaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteMonedaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La moneda es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La moneda de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteMonedaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposDocumento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposDocumento(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposDocumento(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoDocumentoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoDocumentoEmp(@HeaderParam("pToken") String pToken, AddTipoDocEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoDocumentoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp(), data.getpDescripcion());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoDocumentoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoDocumentoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoDocumentoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposExoneracion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposExoneracion(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposExoneracion(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoExoneracionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoExoneracionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoExoneracionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoExoneracionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoExoneracionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de exoneracion es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de exoneracion de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoExoneracionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposIdentificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipoIdentificacion tiposIdentificacion(@HeaderParam("pToken") String pToken) {
        MensajeTipoIdentificacion mensajeConsulta = new MensajeTipoIdentificacion();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposIdentificacion(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoIdentificacionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoIdentificacionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoIdentificacionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoIdentificacionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoIdentificacionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de identificacion es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de identificacion de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoIdentificacionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposCodigoArt")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposCodigoArt(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposCodigoArt(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoCodigoArtEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoCodigoArtEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoCodigoArtEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoCodigoArtEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoCodigoArtEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de codigo de articulo es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de codigo de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoCodigoArtEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/articulos/{pIdArticulo}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeArticulos articulos(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado, @PathParam("pIdArticulo") String pIdArticulo, @HeaderParam("pIdCliente") String pIdCliente) { 
        MensajeArticulos mensajeArticulo = new MensajeArticulos();

        try {

            if (pToken == null || pToken.equals("")) {
                mensajeArticulo.setStatus(Constantes.statusError);
                mensajeArticulo.setMensaje("El token es requerido");
                return mensajeArticulo;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeArticulo.setStatus(Constantes.statusError);
                mensajeArticulo.setMensaje("Token invalido o expirado");
                return mensajeArticulo;
            }

            if (pIdCliente == null || pIdCliente.equals("")) {
                pIdCliente = "0";
            }
            
            IMetodos metodos = new Metodos();

            mensajeArticulo = metodos.getArticulos(pIdArticulo, mClaims.getIdEmpresaFe(), pEstado,pIdCliente);

            return mensajeArticulo;

        } catch (Exception ex) {
            mensajeArticulo.setStatus(Constantes.statusError);
            mensajeArticulo.setMensaje(ex.getMessage());
            return mensajeArticulo;
        }

    }

    @POST
    @Path("/addArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeArticulos addArticuloEmp(@HeaderParam("pToken") String pToken, ArticuloAdd data) {
        MensajeArticulos mensajeInsert = new MensajeArticulos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addArticuloEmp(mClaims.getIdEmpresaFe(), data);
            
     
            mensajeInsert  = metodos.getArticulos(mensajeInsert.getId(), mClaims.getIdEmpresaFe(),"","0");

           
            return mensajeInsert;


        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/addArticuloListEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeArticulos addArticuloEmp(@HeaderParam("pToken") String pToken, List<ArticuloAdd> data) {
        MensajeArticulos mensajeInsert = new MensajeArticulos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();
            
            for(ArticuloAdd row: data){
               mensajeInsert = metodos.addArticuloEmp(mClaims.getIdEmpresaFe(), row);    
            }

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateArticuloEmp(@HeaderParam("pToken") String pToken, ArticuloAdd data) {
        MensajeBase mensajeUpdate = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("El token es requerido");
                return mensajeUpdate;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("Token invalido o expirado");
                return mensajeUpdate;
            }

            IMetodos metodos = new Metodos();

            mensajeUpdate = metodos.updateArticuloEmp(mClaims.getIdEmpresaFe(), data);

            return mensajeUpdate;

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());
            return mensajeUpdate;
        }
    }

    @POST
    @Path("/deleteArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteArticuloEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            mensajeDelete = metodos.deleteArticuloEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogoEmp());

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/exoneraciones/{pIdExoneracion}/{pIdCliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeExoneraciones exoneraciones(@HeaderParam("pToken") String pToken,
            @PathParam("pIdExoneracion") String pIdExoneracion,
            @PathParam("pIdCliente") String pIdCliente) {
        MensajeExoneraciones mensajeExoneracion = new MensajeExoneraciones();

        try {

            if (pToken == null || pToken.equals("")) {
                mensajeExoneracion.setStatus(Constantes.statusError);
                mensajeExoneracion.setMensaje("El token es requerido");
                return mensajeExoneracion;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            IMetodos metodos = new Metodos();

            mensajeExoneracion = metodos.getExoneraciones(pIdExoneracion, mClaims.getIdEmpresaFe(), pIdCliente);

            return mensajeExoneracion;

        } catch (Exception ex) {
            mensajeExoneracion.setStatus(Constantes.statusError);
            mensajeExoneracion.setMensaje(ex.getMessage());
            return mensajeExoneracion;
        }
    }

    @POST
    @Path("/agregarExoneracionCliente")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase agregarExoneracionCliente(@HeaderParam("pToken") String pToken, ExoneracionArt data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getCodigoTipoExo() == null || data.getCodigoTipoExo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El Codigo Tipo Exoneracion es requerido");
                return mensajeDelete;
            }

            if (data.getNumeroDocumento() == null || data.getNumeroDocumento().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El Numero Documento es requerido");
                return mensajeDelete;
            }

            if (data.getNombreInstitucion() == null || data.getNombreInstitucion().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El nombre de la institucion es requerido");
                return mensajeDelete;
            }

            if (data.getFechaEmision() == null || data.getFechaEmision().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La fecha de emision es requerida");
                return mensajeDelete;
            }

            if (data.getMontoImpuesto() == null || data.getMontoImpuesto().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            if (data.getPorcentajeCompra() == null || data.getPorcentajeCompra().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El porcentaje de compra es requerido");
                return mensajeDelete;
            }

            if (data.getIdCliente() == null || data.getIdCliente().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID cliente requerido");
                return mensajeDelete;
            }

            if (data.getFechaHasta() == null || data.getFechaHasta().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La Fecha hasta  es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            mensajeDelete = metodos.addExoneracionClienteEmpresa(mClaims.getIdEmpresaFe(), data);

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @POST
    @Path("/DeleteExoneracionCliente")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase DeleteExoneracionCliente(@HeaderParam("pToken") String pToken, ExoneracionArt data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getIdExoneracion() == null || data.getIdExoneracion().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El id exoneracion es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteExoneracionCliente(data.getIdExoneracion(), mClaims.getIdEmpresaFe())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Exoneracion eliminada correctamente");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("No se encontro la exoneracion por eliminar");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @POST
    @Path("/UpdateExoneracionCliente")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase UpdateExoneracionCliente(@HeaderParam("pToken") String pToken, ExoneracionArt data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getIdExoneracion() == null || data.getIdExoneracion().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El id exoneracion es requerido");
                return mensajeDelete;
            }

            if (data.getCodigoTipoExo() == null || data.getCodigoTipoExo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El Codigo Tipo Exoneracion es requerido");
                return mensajeDelete;
            }

            if (data.getNumeroDocumento() == null || data.getNumeroDocumento().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El Numero Documento es requerido");
                return mensajeDelete;
            }

            if (data.getNombreInstitucion() == null || data.getNombreInstitucion().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El nombre de la institucion es requerido");
                return mensajeDelete;
            }

            if (data.getFechaEmision() == null || data.getFechaEmision().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La fecha de emision es requerida");
                return mensajeDelete;
            }

            if (data.getMontoImpuesto() == null || data.getMontoImpuesto().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            if (data.getPorcentajeCompra() == null || data.getPorcentajeCompra().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El porcentaje de compra es requerido");
                return mensajeDelete;
            }

            if (data.getIdCliente() == null || data.getIdCliente().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID cliente requerido");
                return mensajeDelete;
            }

            if (data.getFechaHasta() == null || data.getFechaHasta().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La fecha hasta es requerida");
                return mensajeDelete;
            }

            if (data.getEstado() == null || data.getEstado().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El estado es requerido  (0-1)");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.updateExoneracionCliente(mClaims.getIdEmpresaFe(), data)) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Exoneracion actualizada correctamente");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("No se encontro la exoneracion por eliminar");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/clientesExoneracion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeReceptor clientesExoneracion(@HeaderParam("pToken") String pToken) {
        MensajeReceptor mensajeReceptor = new MensajeReceptor();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeReceptor.setStatus(Constantes.statusError);
                mensajeReceptor.setMensaje("El token es requerido");
                return mensajeReceptor;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeReceptor.setStatus(Constantes.statusError);
                mensajeReceptor.setMensaje("Token invalido o expirado");
                return mensajeReceptor;
            }

            IMetodos metodos = new Metodos();

            mensajeReceptor = metodos.getReceptoresExoneracion(mClaims.getIdEmpresaFe(), "1");

            return mensajeReceptor;

        } catch (Exception e) {
            mensajeReceptor.setStatus(Constantes.statusError);
            mensajeReceptor.setMensaje(e.getMessage());
            return mensajeReceptor;
        }

    }

    @GET
    @Path("/articulosAplicaExo")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeArticulosAplicaExo articulosAplicaExonerados(@HeaderParam("pToken") String pToken, @HeaderParam("idReceptor") String idReceptor) {
        MensajeArticulosAplicaExo mensajeArticulosAplicaExo = new MensajeArticulosAplicaExo();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeArticulosAplicaExo.setStatus(Constantes.statusError);
                mensajeArticulosAplicaExo.setMensaje("El token es requerido");
                return mensajeArticulosAplicaExo;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeArticulosAplicaExo.setStatus(Constantes.statusError);
                mensajeArticulosAplicaExo.setMensaje("Token invalido o expirado");
                return mensajeArticulosAplicaExo;
            }

            if (idReceptor == null || idReceptor.equals("")) {
                mensajeArticulosAplicaExo.setStatus(Constantes.statusError);
                mensajeArticulosAplicaExo.setMensaje("El idReceptor es requerido");
                return mensajeArticulosAplicaExo;
            }

            IMetodos metodos = new Metodos();

            mensajeArticulosAplicaExo = metodos.getArticulosAplicaExo(mClaims.getIdEmpresaFe(), idReceptor);

            return mensajeArticulosAplicaExo;

        } catch (Exception e) {
            mensajeArticulosAplicaExo.setStatus(Constantes.statusError);
            mensajeArticulosAplicaExo.setMensaje(e.getMessage());
            return mensajeArticulosAplicaExo;
        }

    }

    @POST
    @Path("/agregaExoArticulo")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase agregarExoArticulo(@HeaderParam("pToken") String pToken, AgregarExoArticulo data) {
        MensajeBase mensajeBase = new MensajeBase();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El token es requerido");
                return mensajeBase;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("Token invalido o expirado");
                return mensajeBase;
            }

            if (data.getIdArticulo() == null || data.getIdArticulo().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El IdArticulo es requerido");
                return mensajeBase;
            }

            if (data.getIdExoneracion() == null || data.getIdExoneracion().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El IdExoneracion es requerido");
                return mensajeBase;
            }

            if (data.getIdImpuesto() == null || data.getIdImpuesto().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El IdImpuesto es requerido");
                return mensajeBase;
            }

            if (data.getIdCliente() == null || data.getIdCliente().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El IdCliente es requerido");
                return mensajeBase;
            }

            IMetodos metodos = new Metodos();

            mensajeBase = metodos.agregarExoArticuloEmp(mClaims.getIdEmpresaFe(), data);

            return mensajeBase;

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());
            return mensajeBase;
        }

    }

    @GET
    @Path("/listadoExoArticulos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeExoneracionesArticulos listadoExoneracionesPorArticuloCliente(@HeaderParam("pToken") String pToken) {
        MensajeExoneracionesArticulos mensajeBase = new MensajeExoneracionesArticulos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El token es requerido");
                return mensajeBase;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("Token invalido o expirado");
                return mensajeBase;
            }

            IMetodos metodos = new Metodos();

            mensajeBase = metodos.getListadoExoArticulo(mClaims.getIdEmpresaFe());

            return mensajeBase;

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());
            return mensajeBase;
        }

    }

    @GET
    @Path("/DeleteExoClienteArticulo")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase DeleteExoClienteArticulo(@HeaderParam("pToken") String pToken, @HeaderParam("pId") String id, @HeaderParam("pIdArticulo") String idArticulo, @HeaderParam("pIdImpuesto") String idImpuesto) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (id == null || id.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El id  es requerido");
                return mensajeDelete;
            }

            if (idArticulo == null || idArticulo.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El idArticulo  es requerido");
                return mensajeDelete;
            }

            if (idImpuesto == null || idImpuesto.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El idImpuesto  es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteExoArticulo(id, idArticulo, idImpuesto)) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Exoneracion eliminada correctamente");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("No se encontro la exoneracion por eliminar");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposDocumentoRef")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposDocumentoRef(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTipoDocRef(mClaims);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/addTipoDocRefEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoDocRefEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoDocRefEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoDocRefEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoDocRefEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de referencia es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de referencia de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoDocRefEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/provincias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura provincias(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getProvincias(mClaims.getIdEmpresaFe());

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/cantones")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura cantones(@HeaderParam("pToken") String pToken, GetCantones data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getCantones(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/distritos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura distritos(@HeaderParam("pToken") String pToken, GetDistritos data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdCantonEmp() == null || data.getpIdCantonEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del canton es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getDistritos(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp(), data.getpIdCantonEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/barrios")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura barrios(@HeaderParam("pToken") String pToken, GetBarrios data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdCantonEmp() == null || data.getpIdCantonEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del canton es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdDistritoEmp() == null || data.getpIdDistritoEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del distrito es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getBarrios(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp(), data.getpIdCantonEmp(), data.getpIdDistritoEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }



    @GET
    @Path("/listaPlantillas/{idPlantilla}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePlantillas listaPlantillas(@HeaderParam("pToken") String pToken,
            @PathParam("idPlantilla") String idPlantilla) {
        MensajePlantillas mensajeBasePlantilla = new MensajePlantillas();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeBasePlantilla.setStatus(Constantes.statusError);
                mensajeBasePlantilla.setMensaje("El token es requerido");
                return mensajeBasePlantilla;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeBasePlantilla.setStatus(Constantes.statusError);
                mensajeBasePlantilla.setMensaje("Token invalido o expirado");
                return mensajeBasePlantilla;
            }

            IMetodos metodos = new Metodos();

            mensajeBasePlantilla = metodos.getPlantillas(idPlantilla);

            return mensajeBasePlantilla;

        } catch (Exception e) {
            mensajeBasePlantilla.setStatus(Constantes.statusError);
            mensajeBasePlantilla.setMensaje(e.getMessage());
            return mensajeBasePlantilla;
        }
    }
    
    @GET
    @Path("/listaPlantillasCliente/{idPlantilla}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePlantillas listaPlantillasCliente(@HeaderParam("pToken") String pToken,
            @PathParam("idPlantilla") String idPlantilla, 
            @HeaderParam("pIdCliente") String pIdCliente,
            @HeaderParam("pIDocumento") String pIdDocumento) {
        
        MensajePlantillas mensajeBasePlantilla = new MensajePlantillas();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeBasePlantilla.setStatus(Constantes.statusError);
                mensajeBasePlantilla.setMensaje("El token es requerido");
                return mensajeBasePlantilla;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeBasePlantilla.setStatus(Constantes.statusError);
                mensajeBasePlantilla.setMensaje("Token invalido o expirado");
                return mensajeBasePlantilla;
            }

            IMetodos metodos = new Metodos();

            mensajeBasePlantilla = metodos.getPlantillasCliente(idPlantilla, pIdCliente, pIdDocumento);

            return mensajeBasePlantilla;

        } catch (Exception e) {
            mensajeBasePlantilla.setStatus(Constantes.statusError);
            mensajeBasePlantilla.setMensaje(e.getMessage());
            return mensajeBasePlantilla;
        }
    }
    
    


    @GET
    @Path("/listaPlantillasTags/{idTag}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTags listaPlantillasTags(@HeaderParam("pToken") String pToken, @PathParam("idTag") String idTag, @HeaderParam("isPlantilla") String idPlantilla) {
        MensajeTags mensajeTags = new MensajeTags();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeTags.setStatus(Constantes.statusError);
                mensajeTags.setMensaje("El token es requerido");
                return mensajeTags;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeTags.setStatus(Constantes.statusError);
                mensajeTags.setMensaje("Token invalido o expirado");
                return mensajeTags;
            }

            IMetodos metodos = new Metodos();

            mensajeTags = metodos.getPlantillasTags(idTag, idPlantilla);

            return mensajeTags;

        } catch (Exception e) {
            mensajeTags.setStatus(Constantes.statusError);
            mensajeTags.setMensaje(e.getMessage());
            return mensajeTags;
        }
    }
    
    
    @GET
    @Path("/cargaCatalogos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCargaCatalogos cargaCatalogo(@HeaderParam("pToken") String pToken) {
        MensajeCargaCatalogos mensajeCargaCatalogos = new MensajeCargaCatalogos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeCargaCatalogos.setStatus(Constantes.statusError);
                mensajeCargaCatalogos.setMensaje("El token es requerido");
                return mensajeCargaCatalogos;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeCargaCatalogos.setStatus(Constantes.statusError);
                mensajeCargaCatalogos.setMensaje("Token invalido o expirado");
                return mensajeCargaCatalogos;
            }

            List<CodDescripcion> niveles = new ArrayList<>();
            CodDescripcion nivel;
        
            IMetodos metodos = new Metodos();
            mensajeCargaCatalogos.setMediosPago(metodos.getMediosPago(mClaims).getMediosPago());
            mensajeCargaCatalogos.setImpuestos(metodos.getImpuestos(mClaims).getImpuestos()); ;
            mensajeCargaCatalogos.setMonedas(metodos.getMonedas(mClaims).getMonedas());
            mensajeCargaCatalogos.setUnidadesMedida(metodos.getUnidadesMedida(mClaims).getUnidadesMedida());
            mensajeCargaCatalogos.setCondicionesVenta(metodos.getCondicionesVenta(mClaims).getCondicionesVenta());
            mensajeCargaCatalogos.setDistGeografica(metodos.getDistGeografica(mClaims).getDistGeografica());
            mensajeCargaCatalogos.setTipos(metodos.getTiposDocumento(mClaims).getTipos());
            mensajeCargaCatalogos.setTiposIden(metodos.getTiposIdentificacion(mClaims).getTipos());
            mensajeCargaCatalogos.setTipoCodigoArticulo(metodos.getTiposCodigoArt(mClaims).getTipos());
            mensajeCargaCatalogos.setTiposDocRef(metodos.getTipoDocRef(mClaims).getTipos());
            mensajeCargaCatalogos.setEstadoDocumento(metodos.getEstadosDocumento().getpLista());
            mensajeCargaCatalogos.setMensajeHacienda(metodos.getCodigoMensajeHacienda().getpLista());
            mensajeCargaCatalogos.setSituacion(metodos.getSituacionDocumento().getpLista());
            mensajeCargaCatalogos.setTipoDocumentoDetalle(metodos.getTipoDocumentosDet().getpLista());
            mensajeCargaCatalogos.setCodDocumentoRef(metodos.getCodigoDocumentoRef().getpLista());
            
            nivel = new CodDescripcion();
            nivel.setCodigo("0");
            nivel.setDescripcion("Pais");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("1");
            nivel.setDescripcion("Provincia");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("2");
            nivel.setDescripcion("Canton");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("3");
            nivel.setDescripcion("Distrito");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("4");
            nivel.setDescripcion("Barrio");

            niveles.add(nivel);

            mensajeCargaCatalogos.setNiveles(niveles);
            mensajeCargaCatalogos.setStatus(Constantes.statusSuccess);
            mensajeCargaCatalogos.setMensaje("Completada!");
            return mensajeCargaCatalogos;

        } catch (Exception e) {
            mensajeCargaCatalogos.setStatus(Constantes.statusError);
            mensajeCargaCatalogos.setMensaje(e.getMessage());
            return mensajeCargaCatalogos;
        }
    }
}
