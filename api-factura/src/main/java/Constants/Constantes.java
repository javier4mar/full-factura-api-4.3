/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 *
 * @author msalasch
 */
public class Constantes {
    public static final String statusError = "ERROR"; 
    public static final String statusSuccess = "SUCCESS"; 
    public static final String tipoDocFactura = "01"; 
    public static final String tipoDocNotaDebido = "02"; 
    public static final String tipoDocNotaCredito = "03"; 
    public static final String tipoDocTiquete = "04"; 
    public static final String tipoDocNotaDespacho = "05"; 
    public static final String tipoDocContrato = "06"; 
    public static final String tipoDocProcedimiento = "07"; 
    public static final String tipoDocCompContingencia = "08"; 
    public static final String tipoDocOtros = "99"; 
    
    public static final String apiHaciendaToken = "https://idp.comprobanteselectronicos.go.cr/auth/realms/rut/protocol/openid-connect/token"; 
    public static final String apiHaciendaSend = "https://api.comprobanteselectronicos.go.cr/recepcion/v1/recepcion"; 
    public static final String clientId = "api-prod";
    
    public static final String KEY_STORE = "PKCS12"; // KeychainStore en MAC  Windows-MY En Windows
    
    public static final String ID_PARAMETRO_XML_BASE = "18";
    public static final String ID_PARAMETRO_CARACTER_CARPETA = "3";
    public static final String ID_PARAMETRO_LOGO_DEFECTO = "4";
    public static final String ID_PARAMETRO_CORREO_FULL_FACTURA = "6";
    public static final String ID_PARAMETRO_IP_SERVIDOR = "15";
    public static final String ID_PARAMETRO_DIAS_ANTES_VENCE = "10";
    public static final String ID_PARAMETRO_DIAS_DESPUES_VENCE = "11";
    public static final String ID_PARAMETRO_DIAS_VENCE_CERT = "12";
    
    public static final String ESTADO_DOC_PROCESO_REG = "PR";
    public static final String ESTADO_DOC_APROBO_VALIDACION = "VA";
    public static final String ESTADO_DOC_EN_COLA = "SE";
    public static final String ESTADO_DOC_ENVIADO_TRIBUTACION = "EN";
    public static final String ESTADO_DOC_APROBADO = "AP";
    public static final String ESTADO_DOC_RECHAZADO = "RE";
    
    public static final String TIPO_ARCHIVO_LOGO = "1";
    
    public static final String ID_TIPO_IDENTIFICACION_EXT = "87";
    

}
