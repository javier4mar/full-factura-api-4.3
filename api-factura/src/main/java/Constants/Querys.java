/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 *
 * @author Administrador
 */
public class Querys {
    
    public static final String DATOS_TOKEN = "SELECT  " +
                "	ID_EMPRESA, " +
                "	EMPRESA, " +
                "	RAZON_SOCIAL, " +
                "	NOMBRE_COMERCIAL, " +
                "	CORREO_ADM_FE, " +
                "	IDEN_INGRESO, " +
                "	CLAVE_INGRESO, " +
                "	TIENE_HOMOGEN, " +
                "	TOKEN_WS_EXP, " +
                "	RUTA_CERTIFICADO, " +
                "	PIN_CERTIFICADO, " +
                "	URL_CALLBACK, " +
                "	TIPO_IDENTIFICACION, " +
                "	NUM_IDENTIFICACION   " +
                "FROM  " +
                "	EFE_EMPRESAS  " +
                "WHERE  " +
                "	ID_EMPRESA = ? ";

    
    public static final String PR_ADD_EMPRESA = "{CALL PR_ADD_EMPRESA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_EMPRESA = "{CALL PR_UPDATE_EMPRESA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_EMPRESA_REF = "{CALL PR_UPDATE_EMPRESA_REF(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_RECEPTOR = "{CALL PR_ADD_RECEPTOR( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    public static final String PR_RECEPTOR_TERCERO = "{CALL PR_RECEPTOR_TERCERO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    public static final String PR_UPDATE_RECEPTOR = "{CALL PR_UPDATE_RECEPTOR(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    public static final String PR_DEVUELVE_HISTORICO = "{CALL PR_DEVUELVE_HISTORICO(?, ?, ?)}";


    public static final String ACTUALIZA_RUTA_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET RUTA_XML = ? WHERE ID_DOCUMENTO = ? ";
    
    public static final String ACTUALIZA_EXONERACION_CLIENTE_EMP = "UPDATE EFE_EXONERACIONES_EMP SET CODIGOTIPOEXO = ?, "
            + "NUMERODOCUMENTO = ?, NOMBREINSTITUCION = ?, FECHAEMISION = ?, MONTOIMPUESTO = ?, PORCENTAJECOMPRA = ?, FECHAHASTA = ?, ESTADO = ? "
            + "WHERE ID_EXONERACION = ? AND ID_EMPRESA = ? AND ID_CLIENTE = ?";

    
    public static final String CLAVE_DOCUMENTO = "SELECT  " +
                "	CLAVENUMERICA  " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND ID_DOCUMENTO = ? ";
    
    public static final String TIPO_DOCUMENTO = "SELECT  " +
                "	CODIGOTIPODOC  " +
                "FROM  " +
                "	EFE_TIPOS_DOCUMENTOS_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOTIPODOC_EMPRESA = ? ";
    
    public static final String RUTA_XML = "SELECT  " +
                "	RUTA_XML, " +
                "	CLAVENUMERICA  " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND ID_DOCUMENTO = ? ";
    
    public static final String TOKEN_HACIENDA = "SELECT  " +
                "	ULT_TOKEN,  " +
                "	FECHA_HORA_TOKEN  " +
                "FROM  " +
                "	EFE_EMPRESAS  " +
                "WHERE  " +
                "	ID_EMPRESA = ? ";
    
    public static final String UPDATE_TOKEN = "UPDATE EFE_EMPRESAS SET ULT_TOKEN = ?, FECHA_HORA_TOKEN = ? WHERE ID_EMPRESA = ?";

    public static final String GET_XML = "{CALL PR_GENERA_XML_FACTURA(?, ?)}";
    
    public static final String GET_VALOR1_PARAMETROS = "SELECT  " +
                "	VALOR_1  " +
                "FROM  " +
                "	EFE_PARAMETROS  " +
                "WHERE  " +
                "	ID_PARAMETRO = ?";
    
    public static final String GET_DOCUMENTO_X_CLAVE = "SELECT ID_DOCUMENTO "
             + " FROM `EFE_DOCUMENTOS_ENC` "
             + " WHERE `CLAVENUMERICA` = ?";
    
    public static final String GET_UNIDADES_MEDIDA = "SELECT  " +
                "	CODIGOMEDIDA, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOMEDIDA_EMPRESA) FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIDA = EFE_CODIGOSMEDIDA.CODIGOMEDIDA)  " +
                "FROM  " +
                "	EFE_CODIGOSMEDIDA ";
    
    public static final String GET_RESUMEN_VENTA = "SELECT " +
            "(SELECT EFE_TIPOS_DOCUMENTOS.DESCRIPCION FROM EFE_TIPOS_DOCUMENTOS WHERE EFE_TIPOS_DOCUMENTOS.CODIGOTIPODOC =  FE.VIEW_DOCUMENTOS_ENC.TIPO_COMPROBANTE) as TIPO_COMPROBANTE, " +
            "SUM(FE.VIEW_DOCUMENTOS_ENC.TOTALCOMPROBANTE) as TOTAL, " +
            "count(*) CANTIDAD " +
            "FROM " +
            "FE.VIEW_DOCUMENTOS_ENC " +
            "WHERE " +
            "`ID_EMPRESA` = ?  AND DATE(FE.VIEW_DOCUMENTOS_ENC.FECHAEMISION)  BETWEEN ? AND ? " +
            "AND  EMITIDA ='1' " +
            "GROUP BY  VIEW_DOCUMENTOS_ENC.TIPO_COMPROBANTE";
    
    public static final String GET_CONDICIONES_VENTA = "SELECT  " +
                "	CODIGOCONDICION, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOCONDICION_EMPRESA) FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_EMPRESA = ? AND CODIGOCONDICION = EFE_CONDICIONESVENTAS.CODIGOCONDICION)  " +
                "FROM  " +
                "	EFE_CONDICIONESVENTAS ";
    
    public static final String GET_DIST_GEO = "SELECT  " +
                "	ID_DIST_GEO, NIVEL, NOMBRE, CODIGO, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = EFE_DIST_GEOGRAFICA.ID_DIST_GEO_PERTENECE),  " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = EFE_DIST_GEOGRAFICA.ID_DIST_GEO),  " +
                "	ID_DIST_GEO_PERTENECE  " +
                "FROM  " +
                "	EFE_DIST_GEOGRAFICA ";

    public static final String GET_MEDIOS_PAGO = "SELECT  " +
                "	CODIGOMEDIOPAGO, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOMEDIOPAGO_EMP) FROM EFE_MEDIOSPAGO_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIOPAGO = EFE_MEDIOSPAGO.CODIGOMEDIOPAGO)," +
                "       COMPROBANTE_REQUERIDO  " +
                "FROM  " +
                "	EFE_MEDIOSPAGO ";
    
    public static final String GET_IMPUESTOS = "SELECT  " +
                "	CODIGOIMPUESTO, DESCRIPCION, PORCENTAJE_IMP, EXCEPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOIMPUESTO_EMPRESA) FROM EFE_IMPUESTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOIMPUESTO = EFE_IMPUESTOS.CODIGOIMPUESTO)  " +
                "FROM  " +
                "	EFE_IMPUESTOS ";
    
    public static final String GET_MONEDAS = "SELECT  " +
                "	CODIGOMONEDA, DESCRIPCION, SIMBOLO, " +
                "	(SELECT GROUP_CONCAT(COD_MONEDA_EMPRESA) FROM EFE_MONEDAS_EMP WHERE ID_EMPRESA = ? AND CODIGOMONEDA = EFE_MONEDAS.CODIGOMONEDA)  " +
                "FROM  " +
                "	EFE_MONEDAS ";
    
    public static final String GET_TIPOS_DOCUMENTO = "SELECT  " +
                "	CODIGOTIPODOC, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOTIPODOC_EMPRESA) FROM EFE_TIPOS_DOCUMENTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPODOC = EFE_TIPOS_DOCUMENTOS.CODIGOTIPODOC)  " +
                "FROM  " +
                "	EFE_TIPOS_DOCUMENTOS ";
    
    public static final String GET_TIPOS_EXONERACION = "SELECT  " +
                "	CODIGOTIPOEXO, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(CODIGOTIPOEXO_EMPRESA) FROM EFE_TIPOS_EXON_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPOEXO = EFE_TIPOS_EXON.CODIGOTIPOEXO)  " +
                "FROM  " +
                "	EFE_TIPOS_EXON ";
    
    public static final String GET_TIPOS_IDENTIFICACION = "SELECT  " +
                "	TIPO_IDENT, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE ID_EMPRESA = ? AND TIPO_IDEN = EFE_TIPOS_IDEN.TIPO_IDENT),  " +
                "	LONGITUD  " +
                "FROM  " +
                "	EFE_TIPOS_IDEN ";
    
    public static final String GET_TIPOS_CODIGO_ART = "SELECT  " +
                "	TIPOCODIGO, DESCRIPCION, " +
                "	(SELECT GROUP_CONCAT(TIPOCODIGO_EMPRESA) FROM EFE_TIPOSCODIGOART_EMP WHERE ID_EMPRESA = ? AND TIPOCODIGO = EFE_TIPOSCODIGOART.TIPOCODIGO)  " +
                "FROM  " +
                "	EFE_TIPOSCODIGOART ";
    
    public static final String GET_EMPRESAS = "SELECT  " +
                "	ID_EMPRESA, " +
                "	RAZON_SOCIAL  " +
                "FROM  " +
                "	EFE_EMPRESAS ";
    
    public static final String PR_ADD_UNIDAD_MEDIDA_EMP = "{CALL PR_ADD_UNIDAD_MEDIDA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_CONDICION_VENTA_EMP = "{CALL PR_ADD_CONDICION_VENTA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_DIST_GEO_EMP = "{CALL PR_ADD_DIST_GEO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_IMPUESTO_EMP = "{CALL PR_ADD_IMPUESTO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_MEDIO_PAGO_EMP = "{CALL PR_ADD_MEDIO_PAGO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_MONEDA_EMP = "{CALL PR_ADD_MONEDA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_DOCUMENTO_EMP = "{CALL PR_ADD_TIPO_DOCUMENTO_EMP(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_EXONERACION_EMP = "{CALL PR_ADD_TIPO_EXONERACION_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_IDENTIFICACION_EMP = "{CALL PR_ADD_TIPO_IDENTIFICACION_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_COD_ARTICULO_EMP = "{CALL PR_ADD_TIPO_COD_ARTICULO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_DOC_REF_EMP = "{CALL PR_ADD_TIPO_DOC_REF_EMP(?, ?, ?, ?, ?)}";
    
    public static final String DELETE_TIPO_DOC_REF_EMP = "DELETE FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND CODIGO = ? AND "
                + " CODIGO_EMP = ?  ";
    
    public static final String DELETE_UNIDAD_MEDIDA_EMP = "DELETE FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIDA = ? AND "
                + " CODIGOMEDIDA_EMPRESA = ?  ";
    
    public static final String DELETE_CONDICION_VENTA_EMP = "DELETE FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_EMPRESA = ? AND CODIGOCONDICION = ? AND "
                + " CODIGOCONDICION_EMPRESA = ?  ";
    
    public static final String DELETE_DIST_GEO_EMP = "DELETE FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = ? AND "
                + " CODIGO_LUGAR_EMP = ? ";
    
    public static final String DELETE_IMPUESTO_EMP = "DELETE FROM EFE_IMPUESTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOIMPUESTO = ? AND "
                + " CODIGOIMPUESTO_EMPRESA = ? ";
    
    public static final String DELETE_MEDIO_PAGO_EMP = "DELETE FROM EFE_MEDIOSPAGO_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIOPAGO = ? AND "
                + " CODIGOMEDIOPAGO_EMP = ? ";
    
    public static final String DELETE_MONEDA_EMP = "DELETE FROM EFE_MONEDAS_EMP WHERE ID_EMPRESA = ? AND CODIGOMONEDA = ? AND "
                + " COD_MONEDA_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_DOCUMENTO_EMP = "DELETE FROM EFE_TIPOS_DOCUMENTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPODOC = ? AND "
                + " CODIGOTIPODOC_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_EXONERACION_EMP = "DELETE FROM EFE_TIPOS_EXON_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPOEXO = ? AND "
                + " CODIGOTIPOEXO_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_IDENTIFICACION_EMP = "DELETE FROM EFE_TIPOS_IDEN_EMP WHERE ID_EMPRESA = ? AND TIPO_IDEN = ? AND "
                + " TIPO_IDEN_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_COD_ARTICULO_EMP = "DELETE FROM EFE_TIPOSCODIGOART_EMP WHERE ID_EMPRESA = ? AND TIPOCODIGO = ? AND "
                + " TIPOCODIGO_EMPRESA = ? ";
    
    public static final String LOGIN = "SELECT  " +
                "	SEG_USUARIOS.ID_USUARIO, " +
                "	SEG_USUARIOS.DS_DESCRIPCION, " +
                "	SEG_USUARIOS.ID_EMPRESA_DEFAULT, " +
                "	IFNULL((SELECT 1 FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = SEG_USUARIOS.ID_USUARIO AND ID_GRUPO_SEGURIDAD = 1),'0'),  " +   
                "	SEG_USUARIOS.ST_USUARIO  " +
                "FROM  " +
                "	SEG_USUARIOS WHERE SEG_USUARIOS.DS_USUARIO = ? AND SEG_USUARIOS.DS_USUARIO_PASSWORD = ? ";
    
    public static final String ACTUALIZA_EMPRESA_DEFAULT = "UPDATE SEG_USUARIOS SET ID_EMPRESA_DEFAULT = ? WHERE ID_USUARIO = ? AND ? IN (SELECT ID_EMPRESA FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ?)";
    
    public static final String ACTUALIZA_ESTADO_DOCUMENTO_PLANTILLA = "UPDATE `EFE_DOCUMENTOS_ENC` "
            + " SET  `ESTADO` = ? , `FECHA_ENVIO` = now() WHERE `ID_DOCUMENTO` = ?;";
    
    public static final String ACTUALIZA_GRUPO_DEFAULT = "UPDATE SEG_USUARIOS SET ID_GRUPO_SEGURIDAD_DEFAULT = ? WHERE ID_USUARIO = ? AND ? IN (SELECT ID_GRUPO_SEGURIDAD FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_EMPRESA = ?)";
    
    public static final String GRUPOS_USUARIO = "SELECT  " +
                "	SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD,  " +
                "	(SELECT SEG_GRUPOS.DS_GRUPO_SEGURIDAD FROM SEG_GRUPOS WHERE SEG_GRUPOS.ID_GRUPO_SEGURIDAD = SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD) " +
                "FROM  " +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE SEG_USUARIOS_EMP_GRUPOS.ID_USUARIO = ? AND SEG_USUARIOS_EMP_GRUPOS.ID_EMPRESA = ?";
    
    public static final String GET_PAGINAS_USUARIO = "SELECT  " +
                "	SEG_GRUPOS_PAGINAS.ID_PAGINA,  " +
                "	(SELECT SEG_PAGINAS.DS_PAGINA FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA)," +
                "	(SELECT SEG_PAGINAS.URL FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA) " +
                "FROM  " +
                "	SEG_GRUPOS_PAGINAS WHERE SEG_GRUPOS_PAGINAS.ID_GRUPO_SEGURIDAD = ?";
    
    public static final String GET_PAGINAS_PREFERENCIAS = "SELECT  " +
                "	SEG_GRUPOS_PAGINAS.ID_PAGINA,  " +
                "	(SELECT CONCAT((SELECT DS_PAGINA FROM SEG_PAGINAS PAGS WHERE SEG_PAGINAS.ID_PADRE = PAGS.ID_PAGINA), ' - ', SEG_PAGINAS.DS_PAGINA) FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA), " +
                "	(SELECT SEG_PAGINAS.URL FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA) " +
                "FROM  " +
                "	SEG_GRUPOS_PAGINAS WHERE SEG_GRUPOS_PAGINAS.ID_GRUPO_SEGURIDAD = ? AND 1 = (SELECT SEG_PAGINAS.PREFERENCIA FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA) " +
                "ORDER BY 2 ASC";
    
    public static final String GET_MENU_LATERAL = "SELECT  CONCAT(REPEAT('    ', level - 1), CAST(hi.id AS CHAR)) AS treeitem,  " +
"hi.id as pagina, hi.DS_PAGINA, hi.URL, parent, hi.level, hi.ICON, hi.COLOR, hi.SECUENCIA " +
"FROM    ( " +
"        SELECT  hierarchy_connect_by_parent_eq_prior_id(id) AS id, @level  " +
"        FROM    ( " +
"                SELECT  @start_with := 0, " +
"                        @id := @start_with, " +
"                        @level := 0 " +
"                ) vars, t_hierarchy  " +
"        WHERE   @id IS NOT NULL " +
"	 " +
"        ) ho  " +
"JOIN    t_hierarchy hi  " +
"ON      hi.id = ho.id AND hi.id in (SELECT  sgp.ID_PAGINA FROM SEG_GRUPOS_PAGINAS AS sgp WHERE sgp.ID_GRUPO_SEGURIDAD = ?)";

    public static final String GET_ID_PLANTILLA = "SELECT ID_PLANTILLA FROM EFE_CLIENTES WHERE ID_EMPRESA = ? AND ID_CLIENTE = ?;";

    public static final String GET_PLANTILLAS = "SELECT ID_PLANTILLA, CODIGOTIPODOC_EMPRESA, NOMBRE_PLANTILLA, `XML`, ESTADO, ARCHIVOS, OTRO_TEXTO, OTRO_CONTENIDO, COD_TIPO_ARTICULO FROM EFE_XML_PLANTILLAS WHERE ID_PLANTILLA = IF( ? = 0, ID_PLANTILLA, ?);";

    public static final String GET_PLANTILLAS_CLIENTE = "SELECT " +
"EFE_XML_PLANTILLAS.ID_PLANTILLA, " +
"EFE_XML_PLANTILLAS.CODIGOTIPODOC_EMPRESA,  " +
"EFE_XML_PLANTILLAS.NOMBRE_PLANTILLA,  " +
"EFE_XML_PLANTILLAS.`XML`,  " +
"EFE_XML_PLANTILLAS.ESTADO,  " +
"EFE_XML_PLANTILLAS.ARCHIVOS,  " +
"EFE_XML_PLANTILLAS.OTRO_TEXTO,  " +
"EFE_XML_PLANTILLAS.OTRO_CONTENIDO,  " +
"EFE_XML_PLANTILLAS.COD_TIPO_ARTICULO  " +
"FROM " +
"EFE_XML_PLANTILLAS, " +
"EFE_XML_PLANTILLA_CLIENTE " +
"WHERE " +
" EFE_XML_PLANTILLA_CLIENTE.ID_CLIENTE = ? AND  " +
"EFE_XML_PLANTILLAS.ID_PLANTILLA = EFE_XML_PLANTILLA_CLIENTE.ID_PLANTILLA AND  " +
"EFE_XML_PLANTILLAS.ID_PLANTILLA = IF( ? = 0, EFE_XML_PLANTILLAS.ID_PLANTILLA, ?) AND  " +
"EFE_XML_PLANTILLAS.CODIGOTIPODOC_EMPRESA = IF( ? = 0, EFE_XML_PLANTILLAS.CODIGOTIPODOC_EMPRESA, ?);";

   
    public static final String GET_PLANTILLA_TAGS = "SELECT ID_TAG, ID_PLANTILLA, DESCRIPCION, COD_TAG_REPLACE, KEY_VALUE_TAG, TYPE_TAG, OPCIONAL_TAG, LONGITUD_TAG, ES_AJDUNTO, ESTADO,ID_TAG_PADRE, SECUENCIA, ID_TIPO_TAG, HTML, CD_HTML, VISIBLE, AUTO_INCREMENT, END_TAG_XML,ES_EXTENSION FROM EFE_XML_PLANTILLA_TAGS WHERE IF( ? = 0, ID_TAG, ?) AND ID_PLANTILLA  = ? AND ESTADO = 1;";

    public static final String GET_USUARIOS = "select ID_USUARIO, DS_USUARIO, DS_DESCRIPCION, " +
"ID_EMPRESA_DEFAULT, ST_USUARIO FROM SEG_USUARIOS WHERE ID_USUARIO = IF(? = 0, ID_USUARIO, ?)";
    
    public static final String GET_RECEPTORES = "SELECT  " +

                "	EFE_CLIENTES.RAZON_SOCIAL, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_CLIENTES.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.NUM_IDENTIFICACION, " +
                "	EFE_CLIENTES.IDENTIFICACION_EXTRANJERO, " +
                "	EFE_CLIENTES.NOMBRE_COMERCIAL, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_PROVINCIA AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_CANTON AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_DISTRITO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_BARRIO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.OTRAS_SENAS, " +
                "	EFE_CLIENTES.COD_PAIS_TELEFONO, " +
                "	EFE_CLIENTES.NUM_TELEFONO, " +
                "	EFE_CLIENTES.COD_PAIS_FAX, " +
                "	EFE_CLIENTES.NUM_FAX, " +
                "	EFE_CLIENTES.CORREO_ELECTRONICO, " +
                "	EFE_CLIENTES.COD_CLIENTE_REF, " +
                "	EFE_CLIENTES.TIPO_TRIBUTARIO,  " +
                "	EFE_CLIENTES.ESTADO,  " +
                "	EFE_CLIENTES.ID_PLANTILLA,  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	(SELECT DESCRIPCION FROM EFE_TIPOS_IDEN WHERE TIPO_IDENT = EFE_CLIENTES.TIPO_IDENTIFICACION),  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'E' THEN  " +
                "                               'Emisor / Receptor'  " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'R' THEN  " +
                "                               'Receptor no emisor'  " +
                "                       ELSE " +
                "                               'No aplica'  " +
                "                       END  " +
                "FROM  " +
                "	EFE_CLIENTES WHERE ID_EMPRESA = ? AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
     public static final String GET_RECEPTORES_EXO = "SELECT  " +
                "	EFE_CLIENTES.RAZON_SOCIAL, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_CLIENTES.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.NUM_IDENTIFICACION, " +
                "	EFE_CLIENTES.IDENTIFICACION_EXTRANJERO, " +
                "	EFE_CLIENTES.NOMBRE_COMERCIAL, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_PROVINCIA AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_CANTON AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_DISTRITO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_BARRIO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.OTRAS_SENAS, " +
                "	EFE_CLIENTES.COD_PAIS_TELEFONO, " +
                "	EFE_CLIENTES.NUM_TELEFONO, " +
                "	EFE_CLIENTES.COD_PAIS_FAX, " +
                "	EFE_CLIENTES.NUM_FAX, " +
                "	EFE_CLIENTES.CORREO_ELECTRONICO, " +
                "	EFE_CLIENTES.COD_CLIENTE_REF, " +
                "	EFE_CLIENTES.TIPO_TRIBUTARIO,  " +
                "	EFE_CLIENTES.ESTADO,  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	(SELECT DESCRIPCION FROM EFE_TIPOS_IDEN WHERE TIPO_IDENT = EFE_CLIENTES.TIPO_IDENTIFICACION),  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'E' THEN  " +
                "                               'Emisor / Receptor'  " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'R' THEN  " +
                "                               'Receptor no emisor'  " +
                "                       ELSE " +
                "                               'No aplica'  " +
                "                       END  " +
                " FROM  " +
                "	EFE_CLIENTES , EFE_EXONERACIONES_EMP " +
                " WHERE EFE_CLIENTES.ID_CLIENTE = EFE_EXONERACIONES_EMP.ID_CLIENTE AND  EFE_CLIENTES.ID_EMPRESA = ? AND EFE_CLIENTES.ESTADO = IF(? = 'NA', EFE_CLIENTES.ESTADO, ?) GROUP BY EFE_CLIENTES.ID_CLIENTE";
    
    public static final String DELETE_RECEPTOR = "DELETE FROM EFE_CLIENTES WHERE ID_EMPRESA = ? AND COD_CLIENTE_REF = ?  ";
    
    public static final String ID_DOC_EMPRESA_FE = "SELECT  " +
                "	ID_EMPRESA, ID_DOCUMENTO, EMITIDA, DATE_FORMAT(FECHAEMISION , \"%Y-%m-%d\") as FECHAEMISION   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	CLAVENUMERICA = ? ";
    
    public static final String ACTUALIZA_ESTADO_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET ESTADO = ?, MOTIVO_RECHAZO = ?, FECHA_ULT_CONSULTA = NOW() WHERE ID_DOCUMENTO = ? ";
    
    public static final String PR_REGISTRA_DOCUMENTO = "{CALL PR_REGISTRA_DOCUMENTO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DOCUMENTO_REFERENCIA = "SELECT  " +
                "	NUMEROCONSECUTIVO, " +
                "	FECHAEMISION  " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE = ?";
    
    public static final String GET_IMPUESTO_FACTURA = "SELECT  " +
                "	CODIGOIMPUESTO  " +
                "FROM  " +
                "	EFE_IMPUESTOS_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOIMPUESTO_EMPRESA = ?";
    
    public static final String GET_ARTICULO_TERCERO = "SELECT ID_ARTICULO, ID_ARTICULO_EMP , CODIGO "
            + " FROM EFE_ARTICULOS_EMP  WHERE ID_ARTICULO_TERCERO = ? AND ID_EMPRESA = ?";
    
    public static final String GET_CLIENTE_TERCERO = "SELECT ID_CLIENTE, RAZON_SOCIAL, NOMBRE_COMERCIAL, TIPO_IDENTIFICACION , CORREO_ELECTRONICO "
            + " FROM EFE_CLIENTES WHERE NUM_IDENTIFICACION = ? AND ID_EMPRESA = ?  ";
    
    public static final String GET_CODIGO_ART_DOC = "SELECT  " +
                "	TIPOCODIGO  " +
                "FROM  " +
                "	EFE_TIPOSCODIGOART_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND TIPOCODIGO_EMPRESA = ?";
    
    public static final String GET_EXONERACION_DOCUMENTO = "SELECT  " +
                "	CODIGOTIPOEXO  " +
                "FROM  " +
                "	EFE_TIPOS_EXON_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOTIPOEXO_EMPRESA = ?";
    
    public static final String GET_UNIDAD_MEDIDA_DOC = "SELECT  " +
                "	CODIGOMEDIDA  " +
                "FROM  " +
                "	EFE_CODIGOSMEDIDA_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOMEDIDA_EMPRESA = ?";
    
    public static final String ADD_INFORMACION_REFERENCIA = "INSERT INTO EFE_DOCUMENTOS_REF (TIPO_COMPROBANTE_REF, DOCUMENTO_REF,"
                + " FECHAEMISION, CODIGO, RAZON, ID_DOCUMENTO) VALUES (?,?,?,?,?,?) ";
    
    public static final String ADD_INTERESADO_CORREO = "INSERT INTO EFE_INTERESADO_DOCUMENTO (CORREO, ID_DOCUMENTO) VALUES (?,?) ";

    public static final String ADD_INTERESADO_CORREO_TERCEROS = "call pr_insert_interesados_email_terceros(?,?,?,?)";


    public static final String ADD_INTERESADO_CORREO_PROCEDURE = "call pr_insert_interesados_email(?,?,?,?,?,?,?,?,?,?,?,?) ";

    public static final String ADD_XML_VALORES_PLANTILLA = "INSERT INTO `EFE_XML_VALORES_DOCUMENTOS`"
            + "(`ID_DOCUMENTO`, `NOMBRE`, `VALOR`, `TIPO_VALOR`, `TAG`, `ESTADO`, `FECHA_CREACION`) "
            + "VALUES ( ?, ?, ?, ?, ?, ?, now()); ";
    
    public static final String ADD_DETALLE_DOCUMENTO = "insert into EFE_DOCUMENTOS_DET ( ID_DOCUMENTO, LINEA, TIPOCODIGO, CODIGO, "
                + "CANTIDAD, CODIGOMEDIDA, UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, "
                + "NATURALEZADESCUENTO, SUBTOTAL, MONTOTOTALLINEA, CODIGOMEDIDA_EMP, TIPOCODIGO_EMP, MERC_O_SERV, CANTIDAD_RESTANTE, MONTO_RESTANTE, ID_DOCUMENTO_DET_PADRE) "
                + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    
    public static final String ADD_IMPUESTO_DOCUMENTO = "insert into EFE_DOCUMENTOS_DET_IMP ( ID_DOCUMENTO_DET, CODIGOIMPUESTO, TARIFA, "
                + "MONTO, CODIGOIMPUESTO_EMPRESA, CODIGOTIPOEXO, NUMERODOCUMENTO, NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, "
                + "PORCENTAJECOMPRA, TIPO_EXON_EMPRESA) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
    
    public static final String ADD_MEDIO_PAGO_DOCUMENTO = "insert into EFE_DOCUMENTOS_MEDIO_PAGO ( ID_DOCUMENTO, CODIGOMEDIOPAGO, CODIGOMEDIOPAGO_EMP, "
                + "MONTO) values (?,?,?,?) ";
    
    public static final String GET_ARTICULOS = "SELECT ID_ARTICULO_EMP, ID_CATEGORIA, DESCRIPCION, ID_UNIDAD_MEDIDA, UNIDAD_MEDIDA_COMERCIAL, " +
            "TIPO_ARTICULO, PRECIO, PORCENTAJE, DESCUENTO, RECARGO, ESTADO,  " +
            "(SELECT GROUP_CONCAT(ID_ARTICULO_RECARGO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "(SELECT GROUP_CONCAT(ID_IMPUESTO) FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "(SELECT GROUP_CONCAT(CODIGOMEDIDA_EMPRESA) FROM EFE_CODIGOSMEDIDA_EMP WHERE CODIGOMEDIDA = EFE_ARTICULOS_EMP.ID_UNIDAD_MEDIDA AND ID_EMPRESA = ?),  " +
            "(SELECT GROUP_CONCAT(ID_ARTICULO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO_RECARGO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "CODIGO,  " +
            "CASE WHEN CODIGO IS NULL THEN DESCRIPCION ELSE CONCAT(CODIGO, ' - ', DESCRIPCION) END,  " +
            "COSTO  " +
            "FROM EFE_ARTICULOS_EMP WHERE ID_EMPRESA = ? AND ID_ARTICULO = IF (? = 0, ID_ARTICULO, ?) AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String GET_ARTICULOS_APLICA_EXO = "SELECT " +
"       EFE_ARTICULOS_EMP_IMPUESTOS.ID_IMPUESTO, " +
"	EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA, " +
"	EFE_IMPUESTOS.DESCRIPCION as DESCRIPCION_IMPUESTO, " +
"	EFE_IMPUESTOS.PORCENTAJE_IMP, " +
"	EFE_IMPUESTOS.EXCEPCION, " +
"	EFE_ARTICULOS_EMP.ID_ARTICULO_EMP, " +
"	EFE_ARTICULOS_EMP.ID_CATEGORIA, " +
"	EFE_ARTICULOS_EMP.DESCRIPCION AS DESCRIPCION_PRODUCTO, " +
"	EFE_ARTICULOS_EMP.ID_UNIDAD_MEDIDA, " +
"	EFE_ARTICULOS_EMP.UNIDAD_MEDIDA_COMERCIAL, " +
"	EFE_ARTICULOS_EMP.TIPO_ARTICULO, " +
"	EFE_ARTICULOS_EMP.PRECIO, " +
"	EFE_ARTICULOS_EMP.PORCENTAJE, " +
"	EFE_ARTICULOS_EMP.DESCUENTO, " +
"	EFE_ARTICULOS_EMP.RECARGO, " +
"	EFE_ARTICULOS_EMP.ESTADO, " +
"	EFE_ARTICULOS_EMP.CODIGO, " +
"	(CASE WHEN CODIGO IS NULL THEN " +
"			EFE_ARTICULOS_EMP.DESCRIPCION ELSE CONCAT( EFE_ARTICULOS_EMP.CODIGO, ' - ', EFE_ARTICULOS_EMP.DESCRIPCION )  " +
"	 END) NOMBRECOMPLETO, " +
"	EFE_ARTICULOS_EMP.COSTO  " +
"	FROM " +
"		EFE_ARTICULOS_EMP, " +
"		EFE_ARTICULOS_EMP_IMPUESTOS ," +
"		EFE_IMPUESTOS " +
"	WHERE " +
"		EFE_ARTICULOS_EMP.ID_ARTICULO = EFE_ARTICULOS_EMP_IMPUESTOS.ID_ARTICULO AND " +
"		EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA = EFE_IMPUESTOS.CODIGOIMPUESTO AND " +
"		EFE_ARTICULOS_EMP_IMPUESTOS.ID_IMPUESTO NOT IN (SELECT EFE_ARTICULOS_EMP_IMP_EXO.ID_IMPUESTO " +
"			FROM EFE_ARTICULOS_EMP_IMP_EXO " +
"			WHERE EFE_ARTICULOS_EMP_IMP_EXO.ID_ARTICULO =  EFE_ARTICULOS_EMP.ID_ARTICULO " +
"			AND EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE = ? ) AND " +
"		EFE_ARTICULOS_EMP.ID_EMPRESA = ? AND" +
"		ESTADO = '1'";
    
    public static final String GET_IMPUESTOS_ART = "SELECT  " +
            "COD_IMPUESTO_HACIENDA,  " +
            "(SELECT DESCRIPCION FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA),  " +
            "(SELECT PORCENTAJE_IMP FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA),  " +
            "(SELECT EXCEPCION FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA),  " +
            "(SELECT GROUP_CONCAT(ID_EXONERACION) FROM EFE_ARTICULOS_EMP_IMP_EXO WHERE ID_IMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.ID_IMPUESTO AND  " +
"		EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE = ?) , " +
            "(SELECT GROUP_CONCAT(CODIGOIMPUESTO_EMPRESA) FROM EFE_IMPUESTOS_EMP WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA AND ID_EMPRESA = ?) , ID_IMPUESTO  " +
            "FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_IMPUESTO = ?;";
    
    public static final String GET_EXONERACIONES = "SELECT  " +
            " EFE_EXONERACIONES_EMP.CODIGOTIPOEXO, EFE_EXONERACIONES_EMP.NUMERODOCUMENTO, EFE_EXONERACIONES_EMP.NOMBREINSTITUCION, EFE_EXONERACIONES_EMP.FECHAEMISION, EFE_EXONERACIONES_EMP.MONTOIMPUESTO, EFE_EXONERACIONES_EMP.PORCENTAJECOMPRA, EFE_EXONERACIONES_EMP.ID_CLIENTE , EFE_EXONERACIONES_EMP.FECHAHASTA ,EFE_EXONERACIONES_EMP.ID_EXONERACION, EFE_TIPOS_EXON.DESCRIPCION,  " +
            " EFE_EXONERACIONES_EMP.ESTADO," +
            " (SELECT  IF (count(*) = 0 ,'1','0')  FROM EFE_ARTICULOS_EMP_IMP_EXO WHERE  EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE =EFE_EXONERACIONES_EMP.ID_CLIENTE LIMIT 1)" +
            " FROM EFE_EXONERACIONES_EMP ,EFE_TIPOS_EXON WHERE EFE_EXONERACIONES_EMP.CODIGOTIPOEXO = EFE_TIPOS_EXON.CODIGOTIPOEXO AND EFE_EXONERACIONES_EMP.ID_EMPRESA = ? AND EFE_EXONERACIONES_EMP.ID_EXONERACION = IF (? = 0, EFE_EXONERACIONES_EMP.ID_EXONERACION, ?) AND EFE_EXONERACIONES_EMP.ID_CLIENTE = ? ";
     
     public static final String GET_EXONERACIONES_ARTICULO = "SELECT  " +
            " EFE_EXONERACIONES_EMP.CODIGOTIPOEXO, EFE_EXONERACIONES_EMP.NUMERODOCUMENTO, EFE_EXONERACIONES_EMP.NOMBREINSTITUCION, EFE_EXONERACIONES_EMP.FECHAEMISION, EFE_EXONERACIONES_EMP.MONTOIMPUESTO, EFE_EXONERACIONES_EMP.PORCENTAJECOMPRA, EFE_EXONERACIONES_EMP.ID_CLIENTE , EFE_EXONERACIONES_EMP.FECHAHASTA ,EFE_EXONERACIONES_EMP.ID_EXONERACION, EFE_TIPOS_EXON.DESCRIPCION,  " +
            " EFE_EXONERACIONES_EMP.ESTADO," +
            " (SELECT (CASE WHEN count(*)= 0 THEN 1 " +
            " WHEN count(*) = 1 THEN 0 END ) FROM EFE_ARTICULOS_EMP_IMP_EXO WHERE  EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE =EFE_EXONERACIONES_EMP.ID_CLIENTE LIMIT 1) as EXONERACION_GLOBAL "+
            " FROM " +
            " EFE_EXONERACIONES_EMP ,EFE_TIPOS_EXON , EFE_ARTICULOS_EMP_IMP_EXO " +
            " WHERE EFE_EXONERACIONES_EMP.CODIGOTIPOEXO = EFE_TIPOS_EXON.CODIGOTIPOEXO AND " +
            " EFE_EXONERACIONES_EMP.ID_EXONERACION = EFE_ARTICULOS_EMP_IMP_EXO.ID_EXONERACION AND " +
            " EFE_EXONERACIONES_EMP.ID_EMPRESA = ? AND EFE_EXONERACIONES_EMP.ID_EXONERACION = IF (? = 0, EFE_EXONERACIONES_EMP.ID_EXONERACION, ?) AND EFE_EXONERACIONES_EMP.ID_CLIENTE = ? AND EFE_ARTICULOS_EMP_IMP_EXO.ID_ARTICULO = ? " ;
    
     
   public static final String ADD_EXONERACION_CLIENTE_EMPRESA = "INSERT INTO EFE_EXONERACIONES_EMP "
           + "(ID_EXONERACION, ID_EMPRESA, ID_CLIENTE, CODIGOTIPOEXO, NUMERODOCUMENTO, "
           + " NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA, FECHAHASTA) "
           + " VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);"; 
    
    public static final String DELETE_EXONERACION_CLIENTE_EMPRESA = "DELETE FROM EFE_EXONERACIONES_EMP WHERE ID_EXONERACION = ? AND ID_EMPRESA = ?";

    public static final String DELETE_EXO_CLIENTE_ARTICULO = "DELETE FROM EFE_ARTICULOS_EMP_IMP_EXO WHERE ID = ? AND ID_ARTICULO = ? AND ID_IMPUESTO = ?";
    
    public static final String PR_ADD_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_ARTICULO_EMP_TERCERO = "{CALL PR_ADD_ARTICULO_TER(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    public static final String PR_UPDATE_ARTICULO_EMP = "{CALL PR_UPDATE_ARTICULO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_RECARGO_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_RECARGO(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_IMPUESTO_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_IMPUESTO(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_EXO_IMP_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_IMP_EXO(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_DELETE_ARTICULO_EMP = "{CALL PR_DELETE_ARTICULO(?, ?, ?, ?)}";
    
    public static final String GET_SUCURSALES = "SELECT  " +
                "	COD_SUCURSAL_EMP, NUM_SUCURSAL, DESC_SUCURSAL_EMP,  " +
                "	ESTADO,  " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM  " +
                "	EFE_SUCURSALES  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    
    public static final String GET_LISTADO_EXO_PRODUCTO ="SELECT " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID, " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_IMPUESTO, " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_EXONERACION, " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_ARTICULO, " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE, " +
                " EFE_ARTICULOS_EMP.CODIGO, " +
                " EFE_ARTICULOS_EMP.DESCRIPCION AS DESCRIPCION_PRODUCTO, " +
                " EFE_ARTICULOS_EMP.TIPO_ARTICULO, " +
                " EFE_ARTICULOS_EMP.COSTO, " +
                " EFE_ARTICULOS_EMP.PRECIO, " +
                " EFE_ARTICULOS_EMP.PORCENTAJE, " +
                " EFE_ARTICULOS_EMP.DESCUENTO, " +
                " EFE_IMPUESTOS.DESCRIPCION AS DESCRIPCION_IMPUESTO, " +
                " EFE_IMPUESTOS.PORCENTAJE_IMP, " +
                " EFE_EXONERACIONES_EMP.CODIGOTIPOEXO, " +
                " EFE_EXONERACIONES_EMP.NUMERODOCUMENTO, " +
                " EFE_EXONERACIONES_EMP.NOMBREINSTITUCION, " +
                " EFE_EXONERACIONES_EMP.FECHAEMISION, " +
                " EFE_EXONERACIONES_EMP.MONTOIMPUESTO, " +
                " EFE_EXONERACIONES_EMP.PORCENTAJECOMPRA, " +
                " EFE_EXONERACIONES_EMP.FECHAHASTA, " +
                " EFE_EXONERACIONES_EMP.ESTADO, " +
                " EFE_CLIENTES.NOMBRE_COMERCIAL, " +
                " EFE_CLIENTES.TIPO_IDENTIFICACION, " +
                " EFE_CLIENTES.NUM_IDENTIFICACION, " +
                " EFE_CLIENTES.IDENTIFICACION_EXTRANJERO, " +
                " EFE_CLIENTES.RAZON_SOCIAL, " +
                " (SELECT DESCRIPCION FROM EFE_TIPOS_IDEN WHERE EFE_TIPOS_IDEN.TIPO_IDENT = EFE_CLIENTES.TIPO_IDENTIFICACION) as DESCTIPOIDENT " +
                " FROM " +
                " EFE_ARTICULOS_EMP, " +
                " EFE_ARTICULOS_EMP_IMP_EXO, " +
                " EFE_ARTICULOS_EMP_IMPUESTOS, " +
                " EFE_IMPUESTOS, " +
                " EFE_CLIENTES, " +
                " EFE_EXONERACIONES_EMP " +
                " WHERE " +
                " EFE_ARTICULOS_EMP.ID_ARTICULO = EFE_ARTICULOS_EMP_IMP_EXO.ID_ARTICULO AND " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_IMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.ID_IMPUESTO AND " +
                " EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA = EFE_IMPUESTOS.CODIGOIMPUESTO AND " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_CLIENTE = EFE_CLIENTES.ID_CLIENTE AND " +
                " EFE_ARTICULOS_EMP_IMP_EXO.ID_EXONERACION = EFE_EXONERACIONES_EMP.ID_EXONERACION AND " +
                " EFE_ARTICULOS_EMP.ID_EMPRESA= ?";
    
    public static final String GET_PUNTOS_VENTA = "SELECT  " +
                "	COD_PUNTO_VENTA, NUM_PUNTO_VENTA, DESC_PUNTO_VENTA,  " +
                "	ESTADO,  " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	CONSECUTIVO_FACTURA, CONSECUTIVO_TIQUETE, CONSECUTIVO_NOTA_CREDITO, CONSECUTIVO_NOTA_DEBITO, CONSECUTIVO_ACEPTA, CONSECUTIVO_ACEPTA_PARCIAL, CONSECUTIVO_RECHAZA  " +
                "FROM  " +
                "	EFE_PUNTOS_DE_VENTA  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND ID_SUCURSALES = (SELECT ID_SUCURSALES FROM EFE_SUCURSALES WHERE ID_EMPRESA = ? AND COD_SUCURSAL_EMP = ?) AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String PR_ADD_SUCURSAL_EMP = "{CALL PR_ADD_SUCURSAL(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_PUNTO_VENTA_EMP = "{CALL PR_ADD_PDV(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_SUCURSAL_EMP = "{CALL PR_UPDATE_SUCURSAL(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PUNTO_VENTA_EMP = "{CALL PR_UPDATE_PDV(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DOCUMENTO = "SELECT  " +
                "	ID_DOCUMENTO , " +
                "	NO_DOCUMENTO_REFE , " +
                "	COD_TIPO_DOC_EMP , " +
                "	CLAVENUMERICA , " +
                "	NUMEROCONSECUTIVO , " +
                "	CODSUCURSAL_EMP , " +
                "	PUNTO_DE_VENTA , " +
                "	CONDICIONVENTA_EMP , " +
                "	PLAZOCREDITO , " +
                "	MEDIOS_PAGO , " +
                "	CODIGOMONEDA_EMP , " +
                "	TIPOCAMBIO , " +
                "	ESTADO , " +
                "	SITUACION , " +
                "	ULT_NOVEDAD , " +
                "	COD_CLIENTE , " +
                "	TIPO_IDEN_RECEPTOR_EMP , " +
                "	IDEN_RECEPTOR , " +
                "	NOMBRE_RECEPTOR , " +
                "	CORREO_RECEPTOR, " +
                "	ES_RECEPTOR , " +
                "	EMITIDA , " +
                "	COD_MENSAJE_HACIENDA , " +
                "	TOTALVENTANETA , " +
                "	TOTALIMPUESTOS , " +
                "	TOTALCOMPROBANTE , " +
                "	DESC_ESTADO , " +
                "	DESC_TIPO_DOCUMENTO , " +
                "	FECHA_EMISION, " +
                "	CORREO_EMPRESA , " +
                "	NOMBRE_EMPRESA , " +
                "	IDEN_EMPRESA,  " +
                "	TELEFONO_EMPRESA,  " +
                "	RUTA_XML,  " +	
                "	SIMBOLO_MONEDA,  " +
                "	RUTA_LOGO , " +
                "	SENAS_EMPRESA,  " +
                "	SENAS_RECEPTOR,  " +
                "	OBSERVACION_FACTURA,  " +
                "	FECHA_PROCESO,  " +
                "	DESC_MENSAJE_HAC,  " +
                "	CONDICIONVENTA_DESC,  " +
                "	NOMBRE_EMPRESA_COM,  " +
                "	FAX_EMPRESA,  " +
                "	RAZON_SOCIAL_EMP as RAZON_SOCIAL_EMP_RECEPCION,  " +
                "	NUM_IDENTIFICACION_EMP as NUM_IDENTIFICACION_EMP_RECEPCION,  " +
                "	CORREO_ELECTRONICO_EMP as CORREO_ELECTRONICO_EMP_RECEPCION,  " +
              "	TOTALSERVGRAVADOS,  " +
              "	TOTALSERVEXENTOS,  " +
             "	TOTALMERCANCIASGRAVADOS,  " +
             "	TOTALMERCANCIASEXENTOS,  " +
             "	TOTALGRAVADO,  " +
             "	TOTALEXENTO,  " +
             "	TOTALVENTA,  " +
             "	TOTALDESCUENTOS,  " +
            "	RECEPTOR_NOMBRE_COMERCIAL,  " +
            "	TIPO_IDEN_RECEPTOR_DESC,  " +
            "	EMITIDA,  " +
              "	ID_FACT_REF,  " +
              "	ORDEN_COMPRA  " +
                "FROM " +
                "	VIEW_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE <=> IF(? = 'NA', NO_DOCUMENTO_REFE, ?) AND FIND_IN_SET (ESTADO,IF(? = 'NA', ESTADO, ?)) " +
                "       AND EMITIDA = IF(? = 'NA', EMITIDA, ?) AND FECHAEMISION >=  IF(? = 'NA', FECHAEMISION, ?) AND FECHAEMISION <= IF(? = 'NA', FECHAEMISION, ?) " +
                "       AND TIPO_COMPROBANTE = IF(? = 'NA', TIPO_COMPROBANTE, ?) AND IDEN_RECEPTOR = IF(? = 'NA', IDEN_RECEPTOR, ?) AND NOMBRE_RECEPTOR LIKE IF(? = 'NA', NOMBRE_RECEPTOR, ?) " +
                "       AND CLAVENUMERICA = IF(? = 'NA', CLAVENUMERICA, ?) AND NUMEROCONSECUTIVO = IF(? = 'NA', NUMEROCONSECUTIVO, ?) AND TOTALCOMPROBANTE = IF(? = 'NA', TOTALCOMPROBANTE, ?)";  
    
    public static final String GET_DOCUMENTO_DET_EMIT = "SELECT\n" +
            "\tID_DOCUMENTO,\n" +
            "\tNO_DOCUMENTO_REFE,\n" +
            "\tCOD_TIPO_DOC_EMP,\n" +
            "\tCLAVENUMERICA,\n" +
            "\tNUMEROCONSECUTIVO,\n" +
            "\tCODSUCURSAL_EMP,\n" +
            "\tPUNTO_DE_VENTA,\n" +
            "\tCONDICIONVENTA_EMP,\n" +
            "\tPLAZOCREDITO,\n" +
            "\t( SELECT group_concat( `EFE_DOCUMENTOS_MEDIO_PAGO`.`CODIGOMEDIOPAGO` SEPARATOR ',' ) FROM `EFE_DOCUMENTOS_MEDIO_PAGO` WHERE ( `EFE_DOCUMENTOS_MEDIO_PAGO`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` ) ) AS `MEDIOS_PAGO`,\n" +
            "\tCODIGOMONEDA_EMP,\n" +
            "\tTIPOCAMBIO,\n" +
            "\tESTADO,\n" +
            "\tSITUACION,\n" +
            "\tULT_NOVEDAD,\n" +
            "\tCOD_CLIENTE,\n" +
            "\tTIPO_IDEN_RECEPTOR_EMP,\n" +
            "\tIDEN_RECEPTOR,\n" +
            "\tNOMBRE_RECEPTOR,\n" +
            "\tCORREO_RECEPTOR,\n" +
            "\tES_RECEPTOR,\n" +
            "\tEMITIDA,\n" +
            "\tCOD_MENSAJE_HACIENDA,\n" +
            "\tTOTALVENTANETA,\n" +
            "\tTOTALIMPUESTOS,\n" +
            "\tTOTALCOMPROBANTE,\n" +
            "\tDESC_ESTADO,\n" +
            "\tDESC_TIPO_DOCUMENTO,\n" +
            "\tFECHA_EMISION,\n" +
            "\tCORREO_EMPRESA,\n" +
            "\tNOMBRE_EMPRESA,\n" +
            "\tIDEN_EMPRESA,\n" +
            "\tTELEFONO_EMPRESA,\n" +
            "\tRUTA_XML,\n" +
            "\tSIMBOLO_MONEDA,\n" +
            "\t(\n" +
            "\tCASE\n" +
            "\t\t\t\n" +
            "\t\t\tWHEN (\n" +
            "\t\t\t\t(\n" +
            "\t\t\t\tSELECT\n" +
            "\t\t\t\t\t`EFE_ARCHIVOS`.`RUTA_ARCHIVO` \n" +
            "\t\t\t\tFROM\n" +
            "\t\t\t\t\t`EFE_ARCHIVOS` \n" +
            "\t\t\t\tWHERE\n" +
            "\t\t\t\t\t( ( `EFE_ARCHIVOS`.`ID_EMPRESA` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_EMPRESA` ) AND ( `EFE_ARCHIVOS`.`ID_TIPO_ARCHIVO` = 1 ) AND ( `EFE_ARCHIVOS`.`ESTADO` = 1 ) ) \n" +
            "\t\t\t\t) IS NOT NULL \n" +
            "\t\t\t\t) THEN\n" +
            "\t\t\t\tCONVERT (\n" +
            "\t\t\t\t\t(\n" +
            "\t\t\t\t\tSELECT\n" +
            "\t\t\t\t\t\t`EFE_ARCHIVOS`.`RUTA_ARCHIVO` \n" +
            "\t\t\t\t\tFROM\n" +
            "\t\t\t\t\t\t`EFE_ARCHIVOS` \n" +
            "\t\t\t\t\tWHERE\n" +
            "\t\t\t\t\t\t( ( `EFE_ARCHIVOS`.`ID_EMPRESA` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_EMPRESA` ) AND ( `EFE_ARCHIVOS`.`ID_TIPO_ARCHIVO` = 1 ) AND ( `EFE_ARCHIVOS`.`ESTADO` = 1 ) ) \n" +
            "\t\t\t\t\t) USING utf8 \n" +
            "\t\t\t\t) ELSE '' \n" +
            "\t\t\tEND \n" +
            "\t\t\t) AS `RUTA_LOGO`,\n" +
            "\t\t\tSENAS_EMPRESA,\n" +
            "\t\t\tSENAS_RECEPTOR,\n" +
            "\t\t\t(\n" +
            "\t\t\tCASE\n" +
            "\t\t\t\t\t\n" +
            "\t\t\t\t\tWHEN ( ( VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_COMPROBANTE` = 2 ) OR ( VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_COMPROBANTE` = 3 ) ) THEN\n" +
            "\t\t\t\t\t( SELECT `EFE_DOCUMENTOS_REF`.`RAZON` FROM `EFE_DOCUMENTOS_REF` WHERE ( `EFE_DOCUMENTOS_REF`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` ) LIMIT 1 ) ELSE CONVERT ( ( SELECT `EFE_DOCUMENTOS_OTROS`.`TEXTO` FROM `EFE_DOCUMENTOS_OTROS` WHERE ( `EFE_DOCUMENTOS_OTROS`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` ) LIMIT 1 ) USING utf8 ) \n" +
            "\t\t\t\tEND \n" +
            "\t\t\t\t) AS `OBSERVACION_FACTURA`,\n" +
            "\t\t\t\tFECHA_PROCESO,\n" +
            "\t\t\t\t( SELECT `EFE_MENSAJE_HACIENDA_REC`.`DESCRIPCION` FROM `EFE_MENSAJE_HACIENDA_REC` WHERE ( `EFE_MENSAJE_HACIENDA_REC`.`COD_MENSAJE_HACIENDA` = `VIEW_DOCUMENTOS_ENC_EMITIDO`.`COD_MENSAJE_HACIENDA` ) ) AS `DESC_MENSAJE_HAC`,\n" +
            "\t\t\t\t( SELECT `EFE_CONDICIONESVENTAS`.`DESCRIPCION` FROM `EFE_CONDICIONESVENTAS` WHERE ( `EFE_CONDICIONESVENTAS`.`CODIGOCONDICION` = VIEW_DOCUMENTOS_ENC_EMITIDO.`CONDICIONVENTA_EMP` ) ) AS `CONDICIONVENTA_DESC`,\n" +
            "\t\t\t\tNOMBRE_EMPRESA_COM,\n" +
            "\t\t\t\tFAX_EMPRESA,\n" +
            "\t\t\t\tRAZON_SOCIAL_EMP AS RAZON_SOCIAL_EMP_RECEPCION,\n" +
            "\t\t\t\tNUM_IDENTIFICACION_EMP AS NUM_IDENTIFICACION_EMP_RECEPCION,\n" +
            "\t\t\t\tCORREO_ELECTRONICO_EMP AS CORREO_ELECTRONICO_EMP_RECEPCION,\n" +
            "\t\t\t\tTOTALSERVGRAVADOS,\n" +
            "\t\t\t\tTOTALSERVEXENTOS,\n" +
            "\t\t\t\tTOTALMERCANCIASGRAVADOS,\n" +
            "\t\t\t\tTOTALMERCANCIASEXENTOS,\n" +
            "\t\t\t\tTOTALGRAVADO,\n" +
            "\t\t\t\tTOTALEXENTO,\n" +
            "\t\t\t\tTOTALVENTA,\n" +
            "\t\t\t\tTOTALDESCUENTOS,\n" +
            "\t\t\t\tRECEPTOR_NOMBRE_COMERCIAL,\n" +
            "\t\t\t\t( SELECT `EFE_TIPOS_IDEN`.`DESCRIPCION` FROM `EFE_TIPOS_IDEN` WHERE ( `EFE_TIPOS_IDEN`.`TIPO_IDENT` = VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_IDEN_RECEPTOR_EMP` ) ) AS `TIPO_IDEN_RECEPTOR_DESC`,\n" +
            "\t\t\t\tEMITIDA,\n" +
            "\t\t\t\tID_FACT_REF,\n" +
            "\t\t\t\tORDEN_COMPRA \n" +
            " FROM VIEW_DOCUMENTOS_ENC_EMITIDO WHERE ID_EMPRESA = ? AND ID_DOCUMENTO = ?";
    
    
    
    /* Fix Or en cantidad restante para notas de creditos parciales*/
    public static final String GET_DOCUMENTO_NC = "SELECT  " +
                "	ID_DOCUMENTO , " +
                "	NO_DOCUMENTO_REFE , " +
                "	COD_TIPO_DOC_EMP , " +
                "	CLAVENUMERICA , " +
                "	NUMEROCONSECUTIVO , " +
                "	CODSUCURSAL_EMP , " +
                "	PUNTO_DE_VENTA , " +
                "	CONDICIONVENTA_EMP , " +
                "	PLAZOCREDITO , " +
                "	MEDIOS_PAGO , " +
                "	CODIGOMONEDA_EMP , " +
                "	TIPOCAMBIO , " +
                "	ESTADO , " +
                "	SITUACION , " +
                "	ULT_NOVEDAD , " +
                "	COD_CLIENTE , " +
                "	TIPO_IDEN_RECEPTOR_EMP , " +
                "	IDEN_RECEPTOR , " +
                "	NOMBRE_RECEPTOR , " +
                "	CORREO_RECEPTOR, " +
                "	ES_RECEPTOR , " +
                "	EMITIDA , " +
                "	COD_MENSAJE_HACIENDA , " +
                "	TOTALVENTANETA , " +
                "	TOTALIMPUESTOS , " +
                "	TOTALCOMPROBANTE , " +
                "	DESC_ESTADO , " +
                "	DESC_TIPO_DOCUMENTO , " +
                "	FECHA_EMISION, " +
                "	CORREO_EMPRESA , " +
                "	NOMBRE_EMPRESA , " +
                "	IDEN_EMPRESA,  " +
                "	TELEFONO_EMPRESA,  " +
                "	RUTA_XML,  " +	
                "	SIMBOLO_MONEDA,  " +
                "	RUTA_LOGO , " +
                "	SENAS_EMPRESA,  " +
                "	SENAS_RECEPTOR,  " +
                "	OBSERVACION_FACTURA,  " +
                "	CONDICIONVENTA_DESC,  " +
                "	NOMBRE_EMPRESA_COM,  " +
                "	FAX_EMPRESA  " +
                "FROM " +
                "	VIEW_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE <=> IF(? = 'NA', NO_DOCUMENTO_REFE, ?) AND FIND_IN_SET (ESTADO,'AP,RE,EN') " +
                "       AND EMITIDA = 1 AND FECHAEMISION >=  IF(? = 'NA', FECHAEMISION, ?) AND FECHAEMISION <= IF(? = 'NA', FECHAEMISION, ?) " +
                "       AND IDEN_RECEPTOR = IF(? = 'NA', IDEN_RECEPTOR, ?) AND NOMBRE_RECEPTOR LIKE IF(? = 'NA', NOMBRE_RECEPTOR, ?) " +
                "       AND CLAVENUMERICA = IF(? = 'NA', CLAVENUMERICA, ?) AND NUMEROCONSECUTIVO = IF(? = 'NA', NUMEROCONSECUTIVO, ?) " +
                "       AND TOTALCOMPROBANTE = IF(? = 'NA', TOTALCOMPROBANTE, ?) AND 0 < (SELECT COUNT(ID_DOCUMENTO_DET) FROM EFE_DOCUMENTOS_DET WHERE ID_DOCUMENTO = VIEW_DOCUMENTOS_ENC.ID_DOCUMENTO AND (CANTIDAD_RESTANTE > 0 OR MONTO_RESTANTE > 0)) " +
                "       AND TIPO_COMPROBANTE <> '03'";  

            /* Fix Or en cantidad restante para notas de creditos parciales*/
    public static final String GET_DOCUMENTO_NC_LIKE = "SELECT ID_DOCUMENTO," +
                                      "  NO_DOCUMENTO_REFE," +
                                      "  COD_TIPO_DOC_EMP," +
                                      "  CLAVENUMERICA," +
                                      "  NUMEROCONSECUTIVO," +
                                      "  CODSUCURSAL_EMP," +
                                      "  PUNTO_DE_VENTA," +
                                      "  CONDICIONVENTA_EMP," +
                                      "  PLAZOCREDITO," +
                                      "  null MEDIOS_PAGO," +
                                      "  CODIGOMONEDA_EMP," +
                                     "   TIPOCAMBIO," +
                                     "   ESTADO," +
                                     "   SITUACION," +
                                     "   ULT_NOVEDAD," +
                                     "   COD_CLIENTE," +
                                     "   TIPO_IDEN_RECEPTOR_EMP," +
                                     "   IDEN_RECEPTOR," +
                                     "   NOMBRE_RECEPTOR," +
                                     "   CORREO_RECEPTOR," +
                                    "    ES_RECEPTOR," +
                                    "    EMITIDA," +
                                    "    COD_MENSAJE_HACIENDA," +
                                    "    TOTALVENTANETA," +
                                    "    TOTALIMPUESTOS," +
                                    "    TOTALCOMPROBANTE," +
                                    "    DESC_ESTADO," +
                                    "    DESC_TIPO_DOCUMENTO," +
                                    "    FECHA_EMISION," +
                                    "    CORREO_EMPRESA," +
                                    "    NOMBRE_EMPRESA," +
                                    "    IDEN_EMPRESA," +
                                    "    TELEFONO_EMPRESA," +
                                    "    null RUTA_XML," +
                                    "    SIMBOLO_MONEDA," +
                                    "    null RUTA_LOGO," +
                                    "    SENAS_EMPRESA," +
                                    "    null SENAS_RECEPTOR," +
                                   "     (SELECT TEXTO FROM" +
                                   "     VIEW_EFE_DOCUMENTOS_OTROS" +
                                   "     WHERE ID_DOCUMENTO=EMI.ID_DOCUMENTO ) OBSERVACION_FACTURA," +
                                   "     (CASE " +
                                   "   WHEN  CONDICIONVENTA_EMP = '01' THEN 'Contado'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '02' THEN 'Crédito'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '03' THEN 'Consignación'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '04' THEN 'Apartado'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '05' THEN 'Arrendamiento con opción compra'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '06' THEN 'Arrendamiento en función financiera'" +
                                   "   WHEN  CONDICIONVENTA_EMP = '99' THEN 'Contado'" +
                                   "     END)   AS CONDICIONVENTA_DESC," +
                                   "     NOMBRE_EMPRESA_COM," +
                                   "     FAX_EMPRESA " +
                                  "  FROM VIEW_DOCUMENTOS_ENC_EMITIDO EMI" +
                                  "  WHERE ID_EMPRESA = ? " +
                                  "  AND  NUMEROCONSECUTIVO =? " +
                                  "  AND TIPO_COMPROBANTE <> '03' "+
                                  "  AND EMITIDA = 1 ";
                                 // "  AND 0 < (SELECT COUNT(ID_DOCUMENTO_DET) FROM EFE_DOCUMENTOS_DET WHERE ID_DOCUMENTO = EMI.ID_DOCUMENTO AND (CANTIDAD_RESTANTE > 0 OR MONTO_RESTANTE > 0))"; 

    public static final String GET_DOCUMENTO_CORREO = "SELECT " +
"	ID_DOCUMENTO, " +
"	NO_DOCUMENTO_REFE, " +
"	COD_TIPO_DOC_EMP, " +
"	CLAVENUMERICA, " +
"	NUMEROCONSECUTIVO, " +
"	CODSUCURSAL_EMP, " +
"	PUNTO_DE_VENTA, " +
"	CONDICIONVENTA_EMP, " +
"	PLAZOCREDITO, " +
"	( " +
"	SELECT " +
"		group_concat( `EFE_DOCUMENTOS_MEDIO_PAGO`.`CODIGOMEDIOPAGO` SEPARATOR ',' )  " +
"	FROM " +
"		`EFE_DOCUMENTOS_MEDIO_PAGO`  " +
"	WHERE " +
"		( `EFE_DOCUMENTOS_MEDIO_PAGO`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` )  " +
"	) AS `MEDIOS_PAGO`, " +
"	CODIGOMONEDA_EMP, " +
"	TIPOCAMBIO, " +
"	ESTADO, " +
"	SITUACION, " +
"	ULT_NOVEDAD, " +
"	COD_CLIENTE, " +
"	TIPO_IDEN_RECEPTOR_EMP, " +
"	IDEN_RECEPTOR, " +
"	NOMBRE_RECEPTOR, " +
"	CORREO_RECEPTOR, " +
"	ES_RECEPTOR, " +
"	EMITIDA, " +
"	COD_MENSAJE_HACIENDA, " +
"	TOTALVENTANETA, " +
"	TOTALIMPUESTOS, " +
"	TOTALCOMPROBANTE, " +
"	DESC_ESTADO, " +
"	DESC_TIPO_DOCUMENTO, " +
"	FECHA_EMISION, " +
"	CORREO_EMPRESA, " +
"	NOMBRE_EMPRESA, " +
"	IDEN_EMPRESA, " +
"	TELEFONO_EMPRESA, " +
"	RUTA_XML, " +
"	SIMBOLO_MONEDA, " +
"	( " +
"	CASE " +
"			 " +
"			WHEN ( " +
"				( " +
"				SELECT " +
"					`EFE_ARCHIVOS`.`RUTA_ARCHIVO`  " +
"				FROM " +
"					`EFE_ARCHIVOS`  " +
"				WHERE " +
"					( " +
"						( `EFE_ARCHIVOS`.`ID_EMPRESA` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_EMPRESA` )  " +
"						AND ( `EFE_ARCHIVOS`.`ID_TIPO_ARCHIVO` = 1 )  " +
"						AND ( `EFE_ARCHIVOS`.`ESTADO` = 1 )  " +
"					)  " +
"				) IS NOT NULL  " +
"				) THEN " +
"				CONVERT ( " +
"					( " +
"					SELECT " +
"						`EFE_ARCHIVOS`.`RUTA_ARCHIVO`  " +
"					FROM " +
"						`EFE_ARCHIVOS`  " +
"					WHERE " +
"						( " +
"							( `EFE_ARCHIVOS`.`ID_EMPRESA` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_EMPRESA` )  " +
"							AND ( `EFE_ARCHIVOS`.`ID_TIPO_ARCHIVO` = 1 )  " +
"							AND ( `EFE_ARCHIVOS`.`ESTADO` = 1 )  " +
"						)  " +
"					) USING utf8  " +
"				) ELSE ''  " +
"			END  " +
"			) AS `RUTA_LOGO`, " +
"			SENAS_EMPRESA, " +
"			SENAS_RECEPTOR, " +
"			( " +
"			CASE " +
"					 " +
"					WHEN ( " +
"						( VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_COMPROBANTE` = 2 )  " +
"						OR ( VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_COMPROBANTE` = 3 )  " +
"						) THEN " +
"						( " +
"						SELECT " +
"							`EFE_DOCUMENTOS_REF`.`RAZON`  " +
"						FROM " +
"							`EFE_DOCUMENTOS_REF`  " +
"						WHERE " +
"							( `EFE_DOCUMENTOS_REF`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` )  " +
"							LIMIT 1  " +
"							) ELSE CONVERT ( " +
"							( " +
"							SELECT " +
"								`EFE_DOCUMENTOS_OTROS`.`TEXTO`  " +
"							FROM " +
"								`EFE_DOCUMENTOS_OTROS`  " +
"							WHERE " +
"								( `EFE_DOCUMENTOS_OTROS`.`ID_DOCUMENTO` = VIEW_DOCUMENTOS_ENC_EMITIDO.`ID_DOCUMENTO` )  " +
"								LIMIT 1  " +
"							) USING utf8  " +
"						)  " +
"					END  " +
"					) AS `OBSERVACION_FACTURA`, " +
"					( " +
"					SELECT " +
"						`EFE_CONDICIONESVENTAS`.`DESCRIPCION`  " +
"					FROM " +
"						`EFE_CONDICIONESVENTAS`  " +
"					WHERE " +
"						( `EFE_CONDICIONESVENTAS`.`CODIGOCONDICION` = VIEW_DOCUMENTOS_ENC_EMITIDO.`CONDICIONVENTA_EMP` )  " +
"					) AS `CONDICIONVENTA_DESC`, " +
"					NOMBRE_EMPRESA_COM, " +
"					FAX_EMPRESA, " +
"					TOTALSERVGRAVADOS, " +
"					TOTALSERVEXENTOS, " +
"					TOTALMERCANCIASGRAVADOS, " +
"					TOTALMERCANCIASEXENTOS, " +
"					TOTALGRAVADO, " +
"					TOTALEXENTO, " +
"					TOTALVENTA, " +
"					TOTALDESCUENTOS, " +
"					RECEPTOR_NOMBRE_COMERCIAL, " +
"					( " +
"					SELECT " +
"						`EFE_TIPOS_IDEN`.`DESCRIPCION`  " +
"					FROM " +
"						`EFE_TIPOS_IDEN`  " +
"					WHERE " +
"						( `EFE_TIPOS_IDEN`.`TIPO_IDENT` = VIEW_DOCUMENTOS_ENC_EMITIDO.`TIPO_IDEN_RECEPTOR_EMP` )  " +
"					) AS `TIPO_IDEN_RECEPTOR_DESC`, " +
"					EMITIDA, " +
"					ID_FACT_REF, " +
"					ORDEN_COMPRA, " +
"					ID_EMPRESA  " +
"				FROM " +
"					VIEW_DOCUMENTOS_ENC_EMITIDO  " +
"				WHERE " +
"					ESTADO IN ( 'AP', 'VA' )  AND ERROR ='0' " +
"					AND 0 < ( " +
"					SELECT " +
"						COUNT( ID_INTERESADO )  " +
"					FROM " +
"						EFE_INTERESADO_DOCUMENTO  " +
"					WHERE " +
"						ID_DOCUMENTO = VIEW_DOCUMENTOS_ENC_EMITIDO.ID_DOCUMENTO  " +
"						AND ENVIADO = 0 " +
"					)  " +
"				ORDER BY " +
"				FECHA_EMISION ASC  " +
"	LIMIT 100";  
    
    
    
    public static final String GET_DOCUMENTO_SYNC = "SELECT  " +
                "	ID_DOCUMENTO , " +
                "	NO_DOCUMENTO_REFE , " +
                "	COD_TIPO_DOC_EMP , " +
                "	CLAVENUMERICA , " +
                "	NUMEROCONSECUTIVO , " +
                "	CODSUCURSAL_EMP , " +
                "	PUNTO_DE_VENTA , " +
                "	CONDICIONVENTA_EMP , " +
                "	PLAZOCREDITO , " +
                "	MEDIOS_PAGO , " +
                "	CODIGOMONEDA_EMP , " +
                "	TIPOCAMBIO , " +
                "	ESTADO , " +
                "	SITUACION , " +
                "	ULT_NOVEDAD , " +
                "	COD_CLIENTE , " +
                "	TIPO_IDEN_RECEPTOR_EMP , " +
                "	IDEN_RECEPTOR , " +
                "	NOMBRE_RECEPTOR , " +
                "	CORREO_RECEPTOR, " +
                "	ES_RECEPTOR , " +
                "	EMITIDA , " +
                "	COD_MENSAJE_HACIENDA , " +
                "	TOTALVENTANETA , " +
                "	TOTALIMPUESTOS , " +
                "	TOTALCOMPROBANTE , " +
                "	DESC_ESTADO , " +
                "	DESC_TIPO_DOCUMENTO , " +
                "	FECHA_EMISION, " +
                "	CORREO_EMPRESA , " +
                "	NOMBRE_EMPRESA , " +
                "	IDEN_EMPRESA,  " +
                "	TELEFONO_EMPRESA,  " +
                "	RUTA_XML,  " +	
                "	SIMBOLO_MONEDA,  " +
                "	RUTA_LOGO , " +
                "	SENAS_EMPRESA,  " +
                "	SENAS_RECEPTOR,  " +
                "	OBSERVACION_FACTURA,  " +
                "	CONDICIONVENTA_DESC,  " +
                "	NOMBRE_EMPRESA_COM,  " +
                "	FAX_EMPRESA,  " +
             "	TOTALSERVGRAVADOS,  " +
              "	TOTALSERVEXENTOS,  " +
             "	TOTALMERCANCIASGRAVADOS,  " +
             "	TOTALMERCANCIASEXENTOS,  " +
             "	TOTALGRAVADO,  " +
             "	TOTALEXENTO,  " +
             "	TOTALVENTA,  " +
             "	TOTALDESCUENTOS,  " +
            "	RECEPTOR_NOMBRE_COMERCIAL,  " +
            "	TIPO_IDEN_RECEPTOR_DESC,  " +
            "	EMITIDA,  " +
              "	ID_FACT_REF,  " +
              "	ORDEN_COMPRA,  " +
            "	ID_EMPRESA  " +
                "FROM " +
                "	VIEW_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ESTADO IN ('AP','VA') AND ID_DOCUMENTO IN (SELECT ID_DOCUMENTO FROM `FE`.`EFE_DOCUMENTOS_ENC` WHERE `COD_CLIENTE` IN (  SELECT ID_CLIENTE FROM `FE`.`EFE_XML_PLANTILLA_CLIENTE`  " +
"WHERE `ID_PLANTILLA` = '2' OR `ID_PLANTILLA` = '4' ) AND EFE_DOCUMENTOS_ENC.SYNC_TERCEROS = '0' ) AND COD_TIPO_DOC_EMP ='01'";  
    
    
    
    public static final String GET_DETALLE_DOCUMENTO = "SELECT\n" +
            "\tID_DOCUMENTO,\n" +
            "\tID_DOCUMENTO_DET,\n" +
            "\tLINEA,\n" +
            "\tTIPOCODIGO_EMP,\n" +
            "\tCODIGO,\n" +
            "\tCANTIDAD,\n" +
            "\tCODIGOMEDIDA_EMP,\n" +
            "\tUNIDADMEDIDACOMERCIAL,\n" +
            "\tDETALLE,\n" +
            "\tPRECIOUNITARIO,\n" +
            "\tMONTOTOTAL,\n" +
            "\tMONTODESCUENTO,\n" +
            "\tNATURALEZADESCUENTO,\n" +
            "\tSUBTOTAL,\n" +
            "\tMONTOTOTALLINEA,\n" +
            "\tMERC_O_SERV,\n" +
            "\tCANTIDAD_RESTANTE,\n" +
            "\tMONTO_RESTANTE \n" +
            "FROM\n" +
            "\tEFE_DOCUMENTOS_DET \n" +
            "WHERE\n" +
            "\tID_DOCUMENTO = ?";
    
    //FIX para lista lineas de NC
    public static final String GET_DETALLE_DOCUMENTO_NC = "SELECT  " +
                "	ID_DOCUMENTO, ID_DOCUMENTO_DET, LINEA, TIPOCODIGO_EMP, CODIGO, CANTIDAD, CODIGOMEDIDA_EMP,   " +
                "	UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, NATURALEZADESCUENTO,  " +
                "	SUBTOTAL, MONTOTOTALLINEA, MERC_O_SERV, CANTIDAD_RESTANTE, MONTO_RESTANTE   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_DET  " +
                "WHERE  " +
                "	ID_DOCUMENTO = ? AND (CANTIDAD_RESTANTE > 0 OR MONTO_RESTANTE > 0 )";  
    
     public static final String GET_ENVIOS_DOCUMENTO = "SELECT  " +
                "       EFE_INTERESADO_DOCUMENTO.CORREO,  " +
                "	EFE_INTERESADO_DOCUMENTO.ENVIADO,  " +
                "	EFE_INTERESADO_DOCUMENTO.FECHAENVIO  " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC, " +
                "	EFE_INTERESADO_DOCUMENTO  " +
                "WHERE  " +
                "	EFE_DOCUMENTOS_ENC.CLAVENUMERICA = ? AND  " +
                "	EFE_DOCUMENTOS_ENC.ID_EMPRESA = ? AND  " +
                "	EFE_DOCUMENTOS_ENC.ID_DOCUMENTO = EFE_INTERESADO_DOCUMENTO.ID_DOCUMENTO";  
    
    public static final String GET_IMPUESTO_DETALLE = "SELECT  " +
                "	ID_DOCUMENTO_DET, ID_DOC_DETIMPUESTOS, CODIGOIMPUESTO_EMPRESA, TARIFA, MONTO, TIPO_EXON_EMPRESA, NUMERODOCUMENTO,   " +
                "	NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_DET_IMP  " +
                "WHERE  " +
                "	ID_DOCUMENTO_DET = ? "; 
    
    public static final String GET_MEDIO_PAGO = "SELECT  " +
                "	CODIGOMEDIOPAGO,   " +
                "	(SELECT COMPROBANTE_REQUERIDO FROM EFE_MEDIOSPAGO WHERE CODIGOMEDIOPAGO = EFE_MEDIOSPAGO_EMP.CODIGOMEDIOPAGO)    " +
                "FROM  " +
                "	EFE_MEDIOSPAGO_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOMEDIOPAGO_EMP = ? "; 
    
    public static final String VALIDA_DOCUMENTO_RECEPCION = "SELECT  " +
                "	COD_MENSAJE_HACIENDA   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ENC  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CLAVENUMERICA = ? AND EMITIDA = 0"; 
    
    public static final String ADD_DOCUMENTO_RECEPCION = "insert into EFE_DOCUMENTOS_ENC ( TIPO_COMPROBANTE, CLAVENUMERICA, NUMEROCONSECUTIVO, "
            + "CONDICIONVENTA, PLAZOCREDITO, CODIGOMONEDA, TIPOCAMBIO, TOTALSERVGRAVADOS, TOTALSERVEXENTOS, TOTALMERCANCIASGRAVADOS, "
            + "TOTALMERCANCIASEXENTOS, TOTALGRAVADO, TOTALEXENTO, TOTALVENTA, TOTALDESCUENTOS, TOTALVENTANETA, TOTALIMPUESTOS, "
            + "TOTALCOMPROBANTE, ESTADO, NUMERORESOLUCION, FECHARESOLUCION, ULT_NOVEDAD, TIPO_IDEN_RECEPTOR, NUMERO_IDEN_RECEPTOR, "
            + "RAZON_SOCIAL_RECEPTOR, EMAIL_RECEPTOR, ES_RECEPTOR, FECHAEMISION, TIPO_IDENTIFICACION_EMP, NUM_IDENTIFICACION_EMP, "
            + "RAZON_SOCIAL_EMP, NOMBRE_COMERCIAL_EMP, CORREO_ELECTRONICO_EMP, EMITIDA, COD_MENSAJE_HACIENDA, ID_EMPRESA, EMPRESA, TAG_IMPUESTO) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
        
    public static final String ADD_MEDIO_PAGO_RECEPCION = "insert into EFE_DOCUMENTOS_MEDIO_PAGO ( CODIGOMEDIOPAGO, ID_DOCUMENTO) "
            + "VALUES (?,?)"; 
    
    public static final String ADD_REFERENCIA_RECEPCION = "insert into EFE_DOCUMENTOS_REF ( ID_DOCUMENTO, TIPO_COMPROBANTE_REF, DOCUMENTO_REF,"
            + "FECHAEMISION, CODIGO, RAZON) "
            + "VALUES (?,?,?,?,?,?)"; 
    
    public static final String ADD_DETALLE_RECEPCION = "insert into EFE_DOCUMENTOS_DET ( ID_DOCUMENTO, LINEA, TIPOCODIGO, CODIGO, CANTIDAD,"
            + "CODIGOMEDIDA, UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, NATURALEZADESCUENTO, SUBTOTAL, MONTOTOTALLINEA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
    
    public static final String ADD_IMPUESTO_RECEPCION = "insert into EFE_DOCUMENTOS_DET_IMP ( ID_DOCUMENTO_DET, CODIGOIMPUESTO, TARIFA, MONTO, CODIGOTIPOEXO,"
            + "NUMERODOCUMENTO, NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?)"; 
    
    public static final String GET_ESTADOS_DOCUMENTO = "SELECT  " +
                "	ESTADO, DESCRIPCION   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_ESTADO ORDER BY ORDEN ASC"; 
    
    public static final String GET_CODIGO_MENSAJE_HACIENDA = "SELECT  " +
                "	COD_MENSAJE_HACIENDA, DESCRIPCION   " +
                "FROM  " +
                "	EFE_MENSAJE_HACIENDA_REC "; 
    
    public static final String GET_CODIGO_DOC_REF = "SELECT  " +
                "	CODIGO, DESCRIPCION, (SELECT GROUP_CONCAT(CODIGO_EMP) FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND CODIGO = EFE_TIPOS_DOCUMENTOS_REF.CODIGO)   " +
                "FROM  " +
                "	EFE_TIPOS_DOCUMENTOS_REF "; 
    
    public static final String GET_SITUACION_DOCUMENTO = "SELECT  " +
                "	SITUACION, DESCRIPCION   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_SITUACION "; 
    
    public static final String GET_TIPO_DOCUMENTOS_DET = "SELECT  " +
                "	MERC_O_SERV, DESCRIPCION   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_DET_TIPO "; 
    
    public static final String GET_TIPO_DOCUMENTO_REFERENCIA = "SELECT CODIGO FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND "
                + " CODIGO_EMP = ?  ";
    
    public static final String GET_CODIGO_DOCUMENTO_REF = "SELECT  " +
                "	CODIGO, DESCRIPCION   " +
                "FROM  " +
                "	EFE_DOCUMENTOS_REF_COD "; 
    
    public static final String VERIFICA_DOC_RECEPCION = "SELECT  " +
                "	ID_DOCUMENTO , DATE(FECHAEMISION) AS FECHAEMISION  " +
                " FROM  " +
                "	EFE_DOCUMENTOS_ENC WHERE ID_EMPRESA = ? AND CLAVENUMERICA = ? AND EMITIDA = 0"; 
    
    public static final String GET_INTERESADO_DOCUMENTO = "SELECT  " +
                "	ID_DOCUMENTO, CORREO, ENVIADO, ID_INTERESADO   " +
                "FROM  " +
                "	EFE_INTERESADO_DOCUMENTO WHERE ID_DOCUMENTO = ?"; 
    
    public static final String GET_PREFERENCIAS = "SELECT  " +
                "	(SELECT COD_SUCURSAL_EMP FROM EFE_SUCURSALES WHERE ID_SUCURSALES = EFE_PREFERENCIAS_USUARIO.ID_SUCURSALES AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	(SELECT COD_PUNTO_VENTA FROM EFE_PUNTOS_DE_VENTA WHERE ID_PUNTOS_DE_VENTA = EFE_PREFERENCIAS_USUARIO.ID_PUNTOS_DE_VENTA AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	ID_PAGINA,   " +
                "	(SELECT COD_CLIENTE_REF FROM EFE_CLIENTES WHERE ID_CLIENTE = EFE_PREFERENCIAS_USUARIO.ID_CLIENTE AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	(SELECT CODIGOMEDIOPAGO_EMP FROM EFE_MEDIOSPAGO_EMP WHERE ID_MEDIOPAGO_EMP = EFE_PREFERENCIAS_USUARIO.ID_MEDIOPAGO_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	(SELECT CODIGOCONDICION_EMPRESA FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_CONDICIONVENTA_EMP = EFE_PREFERENCIAS_USUARIO.ID_CONDICIONVENTA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  " +
                "	(SELECT COD_MONEDA_EMPRESA FROM EFE_MONEDAS_EMP WHERE ID_MONEDA_EMP = EFE_PREFERENCIAS_USUARIO.ID_MONEDA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	(SELECT CODIGOMEDIDA_EMPRESA FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_CODIGOMEDIDA_EMP = EFE_PREFERENCIAS_USUARIO.ID_CODIGOMEDIDA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),   " +
                "	ID_PREFERENCIA, MOSTRAR_FACTURA, ACTUALIZAR_PRECIOS, NOTIFICAR_EMISOR  " +
                "FROM  " +
                "	EFE_PREFERENCIAS_USUARIO WHERE ID_EMPRESA = ? AND ID_USUARIO = ? "; 
    
    public static final String ACTUALIZA_STATUS_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET ACEPTADO_CLIENTE = ?, FECHA_ACEPTADO = NOW() WHERE CLAVENUMERICA = ? AND EMITIDA = '1' AND ESTADO IN ('AP', 'RE') AND ACEPTADO_CLIENTE IS NULL ";
    
    public static final String PR_ADD_PREFERENCIAS = "{CALL PR_ADD_PREFERENCIAS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PREFERENCIAS = "{CALL PR_UPDATE_PREFERENCIAS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String ADD_LOGO_EMPRESA = "insert into EFE_ARCHIVOS ( ID_EMPRESA, NOMBRE, RUTA_ARCHIVO,"
            + "MIME_TYPE, DOC_SIZE, ID_TIPO_ARCHIVO) "
            + "VALUES (?,'Logo empresa',?,?,?,?)"; 
    
    public static final String ACTUALIZA_STATUS_INTERESADO_CORREO = "UPDATE EFE_INTERESADO_DOCUMENTO SET ENVIADO = ? WHERE ID_INTERESADO = ? ";
    
    public static final String ACTUALIZA_STATUS_SYNC_TERCEROS = "UPDATE EFE_DOCUMENTOS_ENC SET SYNC_TERCEROS = ? , RESPONSE_TERCERO = ? WHERE ID_DOCUMENTO = ? ";

    public static final String GET_DATOS_LOGIN_EMPRESA = "SELECT  " +
                "	(SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EMP.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1),   " +
                "       EMP.FECHA_PROXIMO_PAGO,  " +
                "       EMP.ID_PLAN,  " +
                "       EMP.ID_TIPO_PLAN,  " +
                "       (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_TIPO_ARCHIVO = 1 AND ESTADO = 1 AND ID_EMPRESA = (SELECT ID_EMPRESA FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE AND ES_DISTRIBUIDOR = 1)),  " +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?), " +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?),  " +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?),  " +
                "       EMP.FECHA_EXPIRA_CERTIFICADO  " +
                " FROM EFE_EMPRESAS EMP WHERE EMP.ID_EMPRESA = ? " ;
    
    public static final String ACTUALIZA_CLAVE_USUARIO = "UPDATE SEG_USUARIOS SET DS_USUARIO_PASSWORD = ? WHERE ID_USUARIO = ? AND DS_USUARIO_PASSWORD = ?";
    
    public static final String PR_VALIDA_OLVIDE_CLAVE = "{CALL PR_VALIDA_OLVIDE_CLAVE(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String RESETEA_CLAVE_USUARIO = "UPDATE SEG_USUARIOS SET DS_USUARIO_PASSWORD = ? WHERE ID_USUARIO = ?";
    
    public static final String PR_ADD_LOGO_EMPRESA = "{CALL PR_ADD_LOGO_EMPRESA(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_TIPO_DOC = "SELECT  " +
                "	CODIGOTIPODOC  " +
                "FROM  " +
                "	EFE_TIPOS_DOCUMENTOS_EMP  " +
                "WHERE  " +
                "	ID_EMPRESA = ? AND CODIGOTIPODOC_EMPRESA = ?";
    
    public static final String GET_EMPRESA = "SELECT  " +
                "	EFE_EMPRESAS.RAZON_SOCIAL, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_EMPRESAS.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	EFE_EMPRESAS.NUM_IDENTIFICACION, " +
                "	EFE_EMPRESAS.NOMBRE_COMERCIAL, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_PROVINCIA AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_CANTON AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_DISTRITO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_BARRIO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	EFE_EMPRESAS.OTRAS_SENAS, " +
                "	EFE_EMPRESAS.COD_PAIS_TELEFONO, " +
                "	EFE_EMPRESAS.NUM_TELEFONO, " +
                "	EFE_EMPRESAS.COD_PAIS_FAX, " +
                "	EFE_EMPRESAS.NUM_FAX, " +
                "	EFE_EMPRESAS.CORREO_ELECTRONICO, " +
                "	EFE_EMPRESAS.EMPRESA, " +
                "	EFE_EMPRESAS.PIN_CERTIFICADO, " +
                "	EFE_EMPRESAS.IDEN_INGRESO, " +
                "	EFE_EMPRESAS.CLAVE_INGRESO, " +
                "	CASE   " +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN  " +
                "                               'pruebas.p12' " +
                "                       ELSE " +
                "                               'produccion.p12' " +
                "                       END,  " +
                "	CASE   " +
                "                       WHEN (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) IS NOT NULL THEN  " +
                "                               (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) " +
                "                       ELSE " +
                "                               ''  " +
                "                       END,  " +
                "	CASE   " +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN  " +
                "                               'Pruebas'  " +
                "                       ELSE " +
                "                               'Producción'  " +
                "                       END,  " +
                "	EFE_EMPRESAS.CORREO_ADM_FE  " +
                "FROM  " +
                "	EFE_EMPRESAS WHERE ID_EMPRESA = ?";
    
    public static final String PR_GENERA_CLAVE_CONSECUTIVO = "{CALL PR_GENERA_CLAVE_CONSECUTIVO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_GENERA_CLAVE_CONSECUTIVO_TERCERO = "{CALL PR_GENERA_CLAVE_CONSECUTIVO_TERCERO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

    public static final String PR_VALIDA_DOC_ANTERIORES = "{CALL PR_VALIDA_DOC_ANTERIORES_PR(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_PROVINCIAS = "SELECT " +
                "	CODIGO_LUGAR_EMP , " +
                "	( SELECT NOMBRE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO) " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA_EMP " +
                "WHERE " +
                "	ID_EMPRESA = ? " +
                "AND 1 = (SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO)";
    
    public static final String GET_CANTONES = "SELECT " +
                "	CODIGO_LUGAR_EMP, " +
                "	NOMBRE " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA_EMP GEO, EFE_DIST_GEOGRAFICA GEO_EMP " +
                "WHERE " +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO " +
                "	AND ID_EMPRESA = ? " +
                "	AND NIVEL = 2 " +
                "	AND ID_DIST_GEO_PERTENECE = (SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 1 = (SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO))";
    
    public static final String GET_DISTRITOS = "SELECT " +
                "	CODIGO_LUGAR_EMP , " +
                "	NOMBRE " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA_EMP GEO , " +
                "	EFE_DIST_GEOGRAFICA GEO_EMP " +
                "WHERE " +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO " +
                "AND ID_EMPRESA = ?  " +
                "AND NIVEL = 3 " +
                "AND ID_DIST_GEO_PERTENECE =( " +
                "	SELECT " +
                "		ID_DIST_GEO " +
                "	FROM " +
                "		EFE_DIST_GEOGRAFICA_EMP " +
                "	WHERE " +
                "		CODIGO_LUGAR_EMP = ? " +
                "	AND ID_EMPRESA = ? " +
                "	AND 2 =( " +
                "		SELECT " +
                "			NIVEL " +
                "		FROM " +
                "			EFE_DIST_GEOGRAFICA " +
                "		WHERE " +
                "			ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "	) " +
                "	AND( " +
                "		SELECT " +
                "			ID_DIST_GEO_PERTENECE " +
                "		FROM " +
                "			EFE_DIST_GEOGRAFICA " +
                "		WHERE " +
                "			ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "	) =( " +
                "		SELECT " +
                "			ID_DIST_GEO " +
                "		FROM " +
                "			EFE_DIST_GEOGRAFICA_EMP " +
                "		WHERE " +
                "			CODIGO_LUGAR_EMP = ? " +
                "		AND ID_EMPRESA = ? " +
                "		AND 1 =( " +
                "			SELECT " +
                "				NIVEL " +
                "			FROM " +
                "				EFE_DIST_GEOGRAFICA " +
                "			WHERE " +
                "				ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "		) " +
                "	) " +
                ")";
    
    public static final String GET_BARRIOS = "SELECT " +
                "	CODIGO_LUGAR_EMP , " +
                "	NOMBRE " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA_EMP GEO , " +
                "	EFE_DIST_GEOGRAFICA GEO_EMP " +
                "WHERE " +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO " +
                "AND ID_EMPRESA = ? " +
                "AND NIVEL = 4 " +
                "AND ID_DIST_GEO_PERTENECE =( " +
                "	SELECT " +
                "		ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 3 =( " +
                "			SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "		) AND( " +
                "			SELECT ID_DIST_GEO_PERTENECE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "		) =( " +
                "			SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 2 =( " +
                "				SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "			) AND( " +
                "				SELECT ID_DIST_GEO_PERTENECE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "			) =( " +
                "				SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 1 =( " +
                "					SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO " +
                "				) " +
                "			) " +
                "		) " +
                ")";
    
    public static final String DELETE_RECARGOS_ARTICULOS = "DELETE FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO_RECARGO = ? ";
    
    public static final String DELETE_IMPUESTOS_ARTICULO = "DELETE FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_ARTICULO = ? ";
    
    public static final String GET_PROVINCIAS_REG = "SELECT " +
                "	ID_DIST_GEO , " +
                "	NOMBRE  " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA  " +
                "WHERE " +
                "	NIVEL = 1";
    
    public static final String GET_CANTONES_REG = "SELECT " +
                "	ID_DIST_GEO , " +
                "	NOMBRE  " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA  " +
                "WHERE " +
                "	NIVEL = 2 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_DISTRITOS_REG = "SELECT " +
                "	ID_DIST_GEO , " +
                "	NOMBRE  " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA  " +
                "WHERE " +
                "	NIVEL = 3 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_BARRIOS_REG = "SELECT " +
                "	ID_DIST_GEO , " +
                "	NOMBRE  " +
                "FROM " +
                "	EFE_DIST_GEOGRAFICA  " +
                "WHERE " +
                "	NIVEL = 4 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_TIPOS_IDEN_REG = "SELECT " +
                "	TIPO_IDENT , " +
                "	DESCRIPCION  " +
                "FROM " +
                "	EFE_TIPOS_IDEN";
    
    public static final String GET_GRUPOS = "SELECT " +
                "	ID_GRUPO_SEGURIDAD,  " +
                "	DS_GRUPO_SEGURIDAD,  " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_GRUPOS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) AND FIND_IN_SET (ID_GRUPO_SEGURIDAD,IF(? = 'NA', ID_GRUPO_SEGURIDAD, ?))";
    
    public static final String GET_PAGS = "SELECT " +
                "	ID_PAGINA , " +
                "	URL,  " +
                "	DS_PAGINA,  " +
                "	ID_PADRE,  " +
                "	NIVEL,  " +
                "	MENU,  " +
                "	SECUENCIA,  " +
                "	ICON,  " +
                "	COLOR,  " +
                "	PREFERENCIA,  " +
                "	ESTADO  " +
                "FROM " +
                "	SEG_PAGINAS";
    
    public static final String GET_USERS = "SELECT " +
                "	ID_USUARIO , " +
                "	DS_USUARIO,  " +
                "	DS_USUARIO_PASSWORD,  " +
                "	DS_DESCRIPCION,  " +
                "	ID_EMPRESA_DEFAULT,  " +
                "	ST_USUARIO,  " +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS.ID_EMPRESA_DEFAULT),  " +
                "	CASE   " +
                "                       WHEN ST_USUARIO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_USUARIOS";
    
    public static final String GET_USERS_FILTRADOS = "SELECT " +
                "	ID_USUARIO , " +
                "	DS_USUARIO,  " +
                "	DS_USUARIO_PASSWORD,  " +
                "	DS_DESCRIPCION,  " +
                "	ID_EMPRESA_DEFAULT,  " +
                "	ST_USUARIO,  " +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS.ID_EMPRESA_DEFAULT),  " +
                "	CASE   " +
                "                       WHEN ST_USUARIO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_USUARIOS WHERE ID_USUARIO IN (SELECT ID_USUARIO FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_GRUPO_SEGURIDAD <> 1 AND ID_GRUPO_SEGURIDAD <> 2 AND ID_EMPRESA = ?)";
    
    public static final String VERIFICA_SUPER_USUARIO = "SELECT IFNULL((SELECT 1 FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_GRUPO_SEGURIDAD = 1), 0)";
    
    public static final String GET_EMPRESA_USUARIO = "SELECT " +
                "	ID_EMPRESA , " +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS_EMP_GRUPOS.ID_EMPRESA)   " +
                "FROM " +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? GROUP BY ID_EMPRESA";
    
    public static final String GET_GRUPOS_EMPRESA = "SELECT " +
                "	ID_GRUPO_SEGURIDAD , " +
                "	(SELECT DS_GRUPO_SEGURIDAD FROM SEG_GRUPOS WHERE ID_GRUPO_SEGURIDAD = SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD)   " +
                "FROM " +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_EMPRESA = ? GROUP BY ID_GRUPO_SEGURIDAD";
    
    public static final String GET_KPI_TOTAL_VENTAS = "SELECT " +
                "	VISTA.FAC_EMITIDAS, " +
                "	CASE WHEN VISTA.FAC_EMITIDAS <> 0 THEN FORMAT(((VISTA.FAC_EMITIDAS * 100) / (VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) ),2) ELSE 0 END AS FAC_EMITIDAS_PORC, " +
                "	VISTA.FAC_RECIBIDAS, " +
                "	CASE WHEN FAC_RECIBIDAS <> 0 THEN FORMAT(((VISTA.FAC_RECIBIDAS * 100) / (VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) ),2) ELSE 0 END AS FAC_RECIBIDAS_PORC, " +
                "	(VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) AS TOTAL_FACTURAS, " +
                "	CASE WHEN VISTA.TOTAL_VENTAS <> 0 THEN FORMAT(VISTA.TOTAL_VENTAS,2)  ELSE 0 END  " +
                "FROM " +
                "	( SELECT  " +
                "		( " +
                "			SELECT " +
                "				COUNT(V1.ID_DOCUMENTO) AS FAC_EMITIDAS " +
                "			FROM " +
                "				VIEW_DOCUMENTOS_KPI V1 " +
                "			WHERE " +
                "				V1.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA  " +
                "			AND V1.EMITIDA = 1 " +
                "			AND ( " +
                "				(V1.ESTADO = 'AP') " +
                "				OR (V1.ESTADO = 'RE')) " +
                "			 AND DATE(V1.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V1.FECHA_ULT_CONSULTA), ?) AND DATE(V1.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V1.FECHA_ULT_CONSULTA), ?) " +
                "		) AS FAC_EMITIDAS, " +
                "		( " +
                "			SELECT " +
                "				COUNT(V2.ID_DOCUMENTO) AS FAC_RECIBIDAS " +
                "			FROM " +
                "				VIEW_DOCUMENTOS_KPI V2 " +
                "			WHERE " +
                "				V2.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA  " +
                "			AND V2.EMITIDA = 0 " +
                "			AND ( " +
                "				(V2.ESTADO = 'AP') " +
                "				OR (V2.ESTADO = 'RE')) " +
                "			AND DATE(V2.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V2.FECHA_ULT_CONSULTA), ?) AND DATE(V2.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V2.FECHA_ULT_CONSULTA), ?) " +
                "		) AS FAC_RECIBIDAS, " +
                "		(IFNULL(( " +
                "			SELECT " +
                "				SUM(TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTAL_VENTAS " +
                "			FROM " +
                "				VIEW_DOCUMENTOS_KPI V3 " +
                "			WHERE " +
                "				V3.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA  " +
                "			AND V3.EMITIDA = 1 " +
                "			AND ( " +
                "				(V3.ESTADO = 'AP') " +
                "				OR (V3.ESTADO = 'RE')) " +
                "			AND DATE(V3.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) AND DATE(V3.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) " +
                "			AND V3.TIPO_COMPROBANTE <> '03'), 0) -  " +
                "		IFNULL(( " +
                "			SELECT " +
                "				SUM(TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTAL_VENTAS " +
                "			FROM " +
                "				VIEW_DOCUMENTOS_KPI V3 " +
                "			WHERE " +
                "				V3.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA  " +
                "			AND V3.EMITIDA = 1 " +
                "			AND ( " +
                "				(V3.ESTADO = 'AP') " +
                "				OR (V3.ESTADO = 'RE')) " +
                "			AND DATE(V3.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) AND DATE(V3.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) " +
                "			AND V3.TIPO_COMPROBANTE = '03'), 0)) AS TOTAL_VENTAS  " +
                "	FROM EFE_EMPRESAS WHERE ID_EMPRESA = ?) VISTA ";
    
    public static final String GET_KPI_ARTICULOS_FAC = "SELECT " +
                "	CODIGO, " +
                "	SUM(CANTIDAD) AS CANTIDAD, " +
                "	DETALLE " +
                "FROM " +
                "	VIEW_DOCUMENTO_ALL " +
                "WHERE ID_EMPRESA = ? AND EMITIDA = 1 AND (ESTADO = 'AP' OR ESTADO = 'RE')  " +
                "	AND DATE(FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) AND DATE(FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) " +
                "GROUP BY CODIGO ORDER BY CANTIDAD DESC LIMIT ? ";
    
    public static final String GET_KPI_CLIENTES_FAC = "SELECT " +
                "	 NOMBRE_RECEPTOR, " +
                "	 SUM( TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTALCOMPROBANTE " +
                "FROM " +
                "	VIEW_DOCUMENTOS_ENC  " +
                "WHERE ID_EMPRESA = ? AND EMITIDA  = 1 AND (ESTADO = 'AP' OR ESTADO = 'RE')  " +
                "	AND DATE(FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) AND DATE(FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) " +
                "GROUP BY COD_CLIENTE ORDER BY TOTALCOMPROBANTE DESC LIMIT ? ";
    
    public static final String PR_ADD_USUARIO = "{CALL PR_ADD_USUARIO(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_USUARIO_RELACION = "{CALL PR_ADD_USUARIO_RELACION(?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_USUARIO = "{CALL PR_UPDATE_USUARIO(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String DELETE_USUARIO_RELACIONES = "DELETE FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? ";
    
    public static final String GET_PLANES = "SELECT " +
                "	ID_PLAN, " +
                "	DESCRIPCION, " +
                "	PRECIO_MENSUAL, " +
                "	CANTIDAD_USUARIOS, " +
                "	CODIGOMONEDA, " +
                "	(SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_PLANES.CODIGOMONEDA), " +
                "	(SELECT SIMBOLO FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_PLANES.CODIGOMONEDA), " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_PLANES WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String GET_TIPOS_PLAN = "SELECT " +
                "	ID_TIPO_PLAN, " +
                "	DESCRIPCION, " +
                "	CANTIDAD_MESES, " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_TIPO_PLANES WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String PR_ADD_PLAN = "{CALL PR_ADD_PLAN(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PLAN = "{CALL PR_UPDATE_PLAN(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_PLAN = "{CALL PR_ADD_TIPO_PLAN(?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_TIPO_PLAN = "{CALL PR_UPDATE_TIPO_PLAN(?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DISTRIBUIDORES = "SELECT  " +
                "	EFE_EMPRESAS.RAZON_SOCIAL, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_EMPRESAS.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	EFE_EMPRESAS.NUM_IDENTIFICACION, " +
                "	EFE_EMPRESAS.NOMBRE_COMERCIAL, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_PROVINCIA AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_CANTON AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_DISTRITO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_BARRIO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA), " +
                "	EFE_EMPRESAS.OTRAS_SENAS, " +
                "	EFE_EMPRESAS.COD_PAIS_TELEFONO, " +
                "	EFE_EMPRESAS.NUM_TELEFONO, " +
                "	EFE_EMPRESAS.COD_PAIS_FAX, " +
                "	EFE_EMPRESAS.NUM_FAX, " +
                "	EFE_EMPRESAS.CORREO_ELECTRONICO, " +
                "	EFE_EMPRESAS.EMPRESA, " +
                "	EFE_EMPRESAS.PIN_CERTIFICADO, " +
                "	EFE_EMPRESAS.IDEN_INGRESO, " +
                "	EFE_EMPRESAS.CLAVE_INGRESO, " +
                "	CASE   " +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN  " +
                "                               'pruebas.p12' " +
                "                       ELSE " +
                "                               'produccion.p12' " +
                "                       END,  " +
                "	CASE   " +
                "                       WHEN (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) IS NOT NULL THEN  " +
                "                               (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) " +
                "                       ELSE " +
                "                               ''  " +
                "                       END,  " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	ADMINISTRA_COBROS, " +
                "	CASE   " +
                "                       WHEN ADMINISTRA_COBROS = 1 THEN  " +
                "                               'Sí'  " +
                "                       ELSE " +
                "                               'No'  " +
                "                       END  " +
                "FROM  " +
                "	EFE_EMPRESAS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) AND ES_DISTRIBUIDOR = 1";
    
    public static final String DELETE_GRUPO_PAGINAS = "DELETE FROM SEG_GRUPOS_PAGINAS WHERE ID_GRUPO_SEGURIDAD = ? ";
    
    public static final String PR_ADD_GRUPO_USUARIO = "{CALL PR_ADD_GRUPO_USUARIO(?, ?, ?, ?)}";
    
    public static final String PR_ADD_GRUPO_PAGINA = "{CALL PR_ADD_GRUPO_PAGINA(?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_GRUPO_USUARIO = "{CALL PR_UPDATE_GRUPO_USUARIO(?, ?, ?, ?, ?)}";
    
    public static final String GET_EMPRESAS_LISTADO = "SELECT  " +
                "	EMP.RAZON_SOCIAL, " +
                "	EMP.TIPO_IDENTIFICACION, " +
                "	EMP.NUM_IDENTIFICACION, " +
                "	EMP.NOMBRE_COMERCIAL, " +
                "	EMP.ID_PROVINCIA, " +
                "	EMP.ID_CANTON, " +
                "	EMP.ID_DISTRITO, " +
                "	EMP.ID_BARRIO, " +
                "	EMP.OTRAS_SENAS, " +
                "	EMP.COD_PAIS_TELEFONO, " +
                "	EMP.NUM_TELEFONO, " +
                "	EMP.COD_PAIS_FAX, " +
                "	EMP.NUM_FAX, " +
                "	EMP.CORREO_ELECTRONICO, " +
                "	EMP.ID_EMPRESA, " +
                "	EMP.PIN_CERTIFICADO, " +
                "	EMP.IDEN_INGRESO, " +
                "	EMP.CLAVE_INGRESO, " +
                "	EMP.ID_PLAN,  " +
                "	(SELECT DESCRIPCION FROM SEG_PLANES WHERE ID_PLAN = EMP.ID_PLAN),  " +
                "	EMP.ID_TIPO_PLAN,  " +
                "	(SELECT DESCRIPCION FROM SEG_TIPO_PLANES WHERE ID_TIPO_PLAN = EMP.ID_TIPO_PLAN),  " +
                "	EMP.ID_EMPRESA_PADRE,  " +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE),  " +
                "	DATE_FORMAT(EMP.FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'),  " +
                "	EMP.ES_DISTRIBUIDOR,  " +
                "	EMP.ADMINISTRA_COBROS,  " +
                "	EMP.MANEJA_HACIENDA,  " +
                "	DATE_FORMAT(EMP.FECHA_PROXIMO_PAGO, '%Y-%m-%d %H:%i:%s'),  " +
                "	EMP.CORREO_ADM_FE  " +
                "FROM  " +
                "	EFE_EMPRESAS EMP WHERE EMP.ES_DISTRIBUIDOR = IF(? = 'NA', EMP.ES_DISTRIBUIDOR, ?) AND EMP.ADMINISTRA_COBROS = IF(? = 'NA', EMP.ADMINISTRA_COBROS, ?)";
    
    public static final String GET_REFERIDOS = "SELECT  " +
                "	EMP.RAZON_SOCIAL, " +
                "	EMP.TIPO_IDENTIFICACION, " +
                "	EMP.NUM_IDENTIFICACION, " +
                "	EMP.NOMBRE_COMERCIAL, " +
                "	EMP.ID_PROVINCIA, " +
                "	EMP.ID_CANTON, " +
                "	EMP.ID_DISTRITO, " +
                "	EMP.ID_BARRIO, " +
                "	EMP.OTRAS_SENAS, " +
                "	EMP.COD_PAIS_TELEFONO, " +
                "	EMP.NUM_TELEFONO, " +
                "	EMP.COD_PAIS_FAX, " +
                "	EMP.NUM_FAX, " +
                "	EMP.CORREO_ELECTRONICO, " +
                "	EMP.ID_EMPRESA, " +
                "	EMP.PIN_CERTIFICADO, " +
                "	EMP.IDEN_INGRESO, " +
                "	EMP.CLAVE_INGRESO, " +
                "	EMP.ID_PLAN,  " +
                "	(SELECT DESCRIPCION FROM SEG_PLANES WHERE ID_PLAN = EMP.ID_PLAN),  " +
                "	EMP.ID_TIPO_PLAN,  " +
                "	(SELECT DESCRIPCION FROM SEG_TIPO_PLANES WHERE ID_TIPO_PLAN = EMP.ID_TIPO_PLAN),  " +
                "	EMP.ID_EMPRESA_PADRE,  " +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE),  " +
                "	DATE_FORMAT(EMP.FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'),  " +
                "	EMP.ES_DISTRIBUIDOR,  " +
                "	EMP.ADMINISTRA_COBROS,  " +
                "	EMP.MANEJA_HACIENDA,  " +
                "	DATE_FORMAT(EMP.FECHA_PROXIMO_PAGO, '%Y-%m-%d %H:%i:%s'),  " +
                "	EMP.CORREO_ADM_FE  " +
                "FROM  " +
                "	EFE_EMPRESAS EMP WHERE EMP.ID_EMPRESA_PADRE = ? AND (SELECT ES_DISTRIBUIDOR FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE) = 1";
    
    public static final String GET_BANCOS = "SELECT " +
                "	ID_BANCO, " +
                "	DESCRIPCION, " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_BANCOS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String GET_CUENTAS_BANCO = "SELECT " +
                "	ID_CUENTA, " +
                "	ID_BANCO, " +
                "	CUENTA_CORRIENTE, " +
                "	CUENTA_CLIENTE, " +
                "	CUENTA_IBAN, " +
                "	CODIGOMONEDA, " +
                "	(SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_CUENTAS_BANCO.CODIGOMONEDA), " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	(SELECT SIMBOLO FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_CUENTAS_BANCO.CODIGOMONEDA)  " +
                "FROM " +
                "	SEG_CUENTAS_BANCO WHERE ID_BANCO = ? AND ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String PR_ADD_DEPOSITO_TRANSF = "{CALL PR_ADD_DEPOSITO_TRANSF(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DEPOSITOS_TRANSF = "SELECT " +
                "	ID_CUENTA, " +
                "	NUMERO_COMPROBANTE, " +
                "	MONTO, " +
                "	DATE_FORMAT(FECHA_DEPOSITO, '%Y-%m-%d %H:%i:%s'), " +
                "	DATE_FORMAT(FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'), " +
                "	DATE_FORMAT(FECHA_VALIDA, '%Y-%m-%d %H:%i:%s'), " +
                "	ESTADO_MOVIMIENTO, " +
                "	CASE   " +
                "                       WHEN ESTADO_MOVIMIENTO = 1 THEN  " +
                "                               'Registrado'  " +
                "                       WHEN ESTADO_MOVIMIENTO = 2 THEN  " +
                "                               'Aprobado'  " +
                "                       ELSE " +
                "                               'Rechazado'  " +
                "                       END,  " +
                "	(SELECT CUENTA_CORRIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT CUENTA_CLIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT CUENTA_IBAN FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT DESCRIPCION FROM SEG_BANCOS WHERE ID_BANCO = (SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)),  " +
                "       IFNULL(OBSERVACIONES,''),  " +
                "       ID_MOVIMIENTO,  " +
                "       (SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = (SELECT CODIGOMONEDA FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)),  " +
                "       (SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_EMPRESA)  " +
                "FROM " +
                "	SEG_DEPOSITOS_TRANSFERENCIAS WHERE ID_EMPRESA = ? ";
    
    public static final String GET_DEPOSITOS_CHECK = "SELECT " +
                "	ID_CUENTA, " +
                "	NUMERO_COMPROBANTE, " +
                "	MONTO, " +
                "	DATE_FORMAT(FECHA_DEPOSITO, '%Y-%m-%d %H:%i:%s'), " +
                "	DATE_FORMAT(FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'), " +
                "	DATE_FORMAT(FECHA_VALIDA, '%Y-%m-%d %H:%i:%s'), " +
                "	ESTADO_MOVIMIENTO, " +
                "	CASE   " +
                "                       WHEN ESTADO_MOVIMIENTO = 1 THEN  " +
                "                               'Registrado'  " +
                "                       WHEN ESTADO_MOVIMIENTO = 2 THEN  " +
                "                               'Aprobado'  " +
                "                       ELSE " +
                "                               'Rechazado'  " +
                "                       END,  " +
                "	(SELECT CUENTA_CORRIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT CUENTA_CLIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT CUENTA_IBAN FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA), " +
                "	(SELECT DESCRIPCION FROM SEG_BANCOS WHERE ID_BANCO = (SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)),  " +
                "       IFNULL(OBSERVACIONES,''),  " +
                "       ID_MOVIMIENTO,  " +
                "       (SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = (SELECT CODIGOMONEDA FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)),  " +
                "       (SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_EMPRESA)  " +
                "FROM " +
                "	SEG_DEPOSITOS_TRANSFERENCIAS";
    
    public static final String GET_SALDOS_EMPRESA = "SELECT " +
                "	EMP.ID_EMPRESA, " +
                "	IFNULL((SELECT SUM(MONTO) FROM SEG_DEPOSITOS_TRANSFERENCIAS WHERE ID_EMPRESA = EMP.ID_EMPRESA AND ESTADO_MOVIMIENTO = 2),0) PAGOS, " +
                "	IFNULL((SELECT SUM(MONTO) FROM SEG_COBROS_EMPRESA WHERE ID_EMPRESA = EMP.ID_EMPRESA),0) TOTAL_COBROS  " +
                "FROM " +
                "	EFE_EMPRESAS EMP " +
                "WHERE " +
                "	EMP.MANEJA_HACIENDA = 1 AND (EMP.ID_EMPRESA_PADRE IS NULL OR 1 = (SELECT 1 FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE AND (ES_DISTRIBUIDOR = 1 AND ADMINISTRA_COBROS = 0))) AND EMP.ID_EMPRESA = ? ";
    
    public static final String PR_VALIDA_DEPOSITO = "{CALL PR_VALIDA_DEPOSITO(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_GENERA_COBROS = "{CALL PR_GENERA_COBROS(?, ?)}";
    
    public static final String UPDATE_EXPIRA_TOKEN = "UPDATE EFE_EMPRESAS SET FECHA_EXPIRA_CERTIFICADO = ? WHERE ID_EMPRESA = ?";

    public static final String GET_GRUPOS_LISTA = "SELECT " +
                "	ID_GRUPO_SEGURIDAD,  " +
                "	DS_GRUPO_SEGURIDAD,  " +
                "	ESTADO, " +
                "	CASE   " +
                "                       WHEN ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END  " +
                "FROM " +
                "	SEG_GRUPOS ";
    
    public static final String GET_RUTAS_XML = "SELECT CONCAT(RUTA_XML, CLAVENUMERICA, '.xml') archivo  " +
                "FROM EFE_DOCUMENTOS_ENC  WHERE ID_EMPRESA = ? AND FIND_IN_SET (ID_DOCUMENTO,?) UNION  " +
                "SELECT CONCAT(RUTA_XML, CLAVENUMERICA, 'R.xml') archivo FROM EFE_DOCUMENTOS_ENC  WHERE ID_EMPRESA = ? AND FIND_IN_SET (ID_DOCUMENTO,?) "; 
    
    public static final String ACTUALIZA_INTERESADO_NOTIF = "UPDATE EFE_INTERESADO_DOCUMENTO SET ENVIADO = 0 WHERE ID_DOCUMENTO = ? ";
    
    public static final String ACTUALIZA_PRECIO_ARTICULO = "UPDATE EFE_ARTICULOS_EMP SET PRECIO = ? WHERE ID_ARTICULO = ? AND ID_EMPRESA = ? AND 1 = (SELECT ACTUALIZAR_PRECIOS FROM EFE_PREFERENCIAS_USUARIO WHERE ID_EMPRESA = ? LIMIT 1) ";
    
    
    public static final String GET_URL_DOCUMENTO = "SELECT EFE_DOCUMENTOS_ENC.RUTA_XML," +
                                                   " FE.EFE_DOCUMENTOS_ENC.FECHA_PROCESO, " +
                                                   " FE.EFE_DOCUMENTOS_ENC.FECHAEMISION " +
                                                   " FROM `FE`.`EFE_DOCUMENTOS_ENC` " +
                                                   " WHERE `ID_DOCUMENTO` = ? OR `CLAVENUMERICA` = ? ";
    
    
     public static final String GET_RECEPTOR_INFO = "SELECT  " +

                "	EFE_CLIENTES.RAZON_SOCIAL, " +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_CLIENTES.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.NUM_IDENTIFICACION, " +
                "	EFE_CLIENTES.IDENTIFICACION_EXTRANJERO, " +
                "	EFE_CLIENTES.NOMBRE_COMERCIAL, " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_PROVINCIA AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_CANTON AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_DISTRITO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_BARRIO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA), " +
                "	EFE_CLIENTES.OTRAS_SENAS, " +
                "	EFE_CLIENTES.COD_PAIS_TELEFONO, " +
                "	EFE_CLIENTES.NUM_TELEFONO, " +
                "	EFE_CLIENTES.COD_PAIS_FAX, " +
                "	EFE_CLIENTES.NUM_FAX, " +
                "	EFE_CLIENTES.CORREO_ELECTRONICO, " +
                "	EFE_CLIENTES.COD_CLIENTE_REF, " +
                "	EFE_CLIENTES.TIPO_TRIBUTARIO,  " +
                "	EFE_CLIENTES.ESTADO,  " +
                "	EFE_CLIENTES.ID_PLANTILLA,  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.ESTADO = 1 THEN  " +
                "                               'Activo'  " +
                "                       ELSE " +
                "                               'Inactivo'  " +
                "                       END,  " +
                "	(SELECT DESCRIPCION FROM EFE_TIPOS_IDEN WHERE TIPO_IDENT = EFE_CLIENTES.TIPO_IDENTIFICACION),  " +
                "	CASE   " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'E' THEN  " +
                "                               'Emisor / Receptor'  " +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'R' THEN  " +
                "                               'Receptor no emisor'  " +
                "                       ELSE " +
                "                               'No aplica'  " +
                "                       END  " +
                "FROM  " +
                "	EFE_CLIENTES WHERE ID_EMPRESA = ? AND  RAZON_SOCIAL LIKE IF(? = 'NA', RAZON_SOCIAL, ?) AND EFE_CLIENTES.TIPO_IDENTIFICACION LIKE IF(? = 'NA', TIPO_IDENTIFICACION, ?)";
     
     
      public static final String GET_ARTICULOS_INFO = "SELECT ID_ARTICULO_EMP, ID_CATEGORIA, DESCRIPCION, ID_UNIDAD_MEDIDA, UNIDAD_MEDIDA_COMERCIAL, " +
            "TIPO_ARTICULO, PRECIO, PORCENTAJE, DESCUENTO, RECARGO, ESTADO,  " +
            "(SELECT GROUP_CONCAT(ID_ARTICULO_RECARGO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "(SELECT GROUP_CONCAT(ID_IMPUESTO) FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "(SELECT GROUP_CONCAT(CODIGOMEDIDA_EMPRESA) FROM EFE_CODIGOSMEDIDA_EMP WHERE CODIGOMEDIDA = EFE_ARTICULOS_EMP.ID_UNIDAD_MEDIDA AND ID_EMPRESA = ?),  " +
            "(SELECT GROUP_CONCAT(ID_ARTICULO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO_RECARGO = EFE_ARTICULOS_EMP.ID_ARTICULO),  " +
            "CODIGO,  " +
            "CASE WHEN CODIGO IS NULL THEN DESCRIPCION ELSE CONCAT(CODIGO, ' - ', DESCRIPCION) END,  " +
            "COSTO  " +
            "FROM EFE_ARTICULOS_EMP WHERE ID_EMPRESA = ? AND "
              + " DESCRIPCION LIKE IF (? = 'NA', DESCRIPCION, ?) AND CODIGO = IF(? = 'NA', CODIGO, ?)";
     
      
         public static final String UPDATE_DOCUMENTO_ENC_ERROR = "UPDATE EFE_DOCUMENTOS_ENC SET ERROR = ? , MESSAGE_ERROR = ? WHERE ID_DOCUMENTO = ? ";

    public static final String REPORT_EMITIDOS = "SELECT\n" +
            "\tVDEE.DESC_TIPO_DOCUMENTO,\n" +
            "\tVDEE.CLAVENUMERICA,\n" +
            "\tVDEE.NUMEROCONSECUTIVO,\n" +
            "\tVDEE.NOMBRE_RECEPTOR,\n" +
            "\tVDEE.IDEN_RECEPTOR,\n" +
            "\tVDEE.CORREO_RECEPTOR,\n" +
            "\tVDEE.CONDICIONVENTA_EMP,\n"+
            "\tVDEE.PLAZOCREDITO,\n" +
            "\tVDEE.TIPOCAMBIO,\n" +
            "\tVDEE.TOTALSERVGRAVADOS,\n" +
            "\tVDEE.TOTALSERVEXENTOS,\n" +
            "\tVDEE.TOTALMERCANCIASEXENTOS,\n" +
            "\tVDEE.TOTALMERCANCIASGRAVADOS,\n" +
            "\tVDEE.TOTALGRAVADO,\n" +
            "\tVDEE.TOTALEXENTO,\n" +
            "\tVDEE.TOTALDESCUENTOS,\n" +
            "\tVDEE.TOTALIMPUESTOS,\n" +
            "\tVDEE.TOTALCOMPROBANTE,\n" +
            "\tVDEE.SIMBOLO_MONEDA,\n" +
            "\tVDEE.DESC_ESTADO,\n" +
            "\tVDEE.FECHA_EMISION, \n" +
            "\tVDEE.MOTIVO_RECHAZO \n" +
            "FROM\n" +
            "\tVIEW_DOCUMENTOS_ENC_EMITIDO VDEE \n" +
            "WHERE\n" +
            "\tID_EMPRESA = ? \n" +
            "\tAND DATE( FECHA_EMISION ) >= DATE( ? ) \n" +
            "\tAND DATE( FECHA_EMISION ) <= DATE( ? ) \n" +
            "\tAND ESTADO = ? \n" +
            "\tAND TIPO_COMPROBANTE = ? ";


    public static final String REPORT_RECIBIDOS = "SELECT\n" +
            "\tID_DOCUMENTO,\n" +
            "\tDESC_TIPO_DOCUMENTO,\n" +
            "\tNUMEROCONSECUTIVO,\n" +
            "\tNUM_IDENTIFICACION_EMP,\n" +
            "\tRAZON_SOCIAL_EMP,\n" +
            "\tNOMBRE_COMERCIAL_EMP,\n" +
            "\tCORREO_ELECTRONICO_EMP,\n" +
            "\tTOTALCOMPROBANTE,\n" +
            "\tFECHAEMISION,\n" +
            "\tFECHA_PROCESO,\n" +
            "\tDESC_ME_HACIENDA,\n" +
            "\tDESC_ESTADO,\n" +
            "\tSIMBOLO_MONEDA, \n" +
            "\tMOTIVO_RECHAZO \n" +
            "FROM\n" +
            "\tVIEW_DOCUMENTOS_ENC_RECIB \n" +
            "WHERE\n" +
            "\tID_EMPRESA = ? \n AND" +
            "\tdate(FECHAEMISION) >= date( ? ) AND \n" +
            "\tdate(FECHA_EMISION)  <= date( ? )\n" +
            "\tAND ESTADO = ? " +
            "\tAND TIPO_COMPROBANTE = ? ;";
}
