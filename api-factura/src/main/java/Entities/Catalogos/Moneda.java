/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Catalogos;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class Moneda {
    private String codigo;
    private String descripcion;
    private String simbolo;
    private List<String> codigosEmp;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public List<String> getCodigosEmp() {
        return codigosEmp;
    }

    public void setCodigosEmp(List<String> codigosEmp) {
        this.codigosEmp = codigosEmp;
    }
    
    
}
