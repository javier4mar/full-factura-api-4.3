/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Catalogos;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class CodDescripcion {
    private String codigo;
    private String descripcion;
    private List<String> codigosEmp;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<String> getCodigosEmp() {
        return codigosEmp;
    }

    public void setCodigosEmp(List<String> codigosEmp) {
        this.codigosEmp = codigosEmp;
    }
    
    
}
