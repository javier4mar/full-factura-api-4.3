package Entities.Catalogos;

import Entities.FE.PlantillaTags;
import java.util.List;

public class Plantilla {
    private String idPlantilla;
    private String codTipoDocEmp; //Es el tipo de documento notacredito, factura, notadebito, tiquete
    private String nombrePlantilla;
    private String XML;
    private int archivos;
    private int otroTexto;
    private int otroContenido;
    private String estado;
    private List<PlantillaTags> tags;
    private String tipoCodiogArticulo;

    public String getTipoCodiogArticulo() {
        return tipoCodiogArticulo;
    }

    public void setTipoCodiogArticulo(String tipoCodiogArticulo) {
        this.tipoCodiogArticulo = tipoCodiogArticulo;
    }
    
    

    public int getOtroTexto() {
        return otroTexto;
    }

    public void setOtroTexto(int otroTexto) {
        this.otroTexto = otroTexto;
    }

    public int getOtroContenido() {
        return otroContenido;
    }

    public void setOtroContenido(int otroContenido) {
        this.otroContenido = otroContenido;
    }
    
    
    
    

    public List<PlantillaTags> getTags() {
        return tags;
    }

    public void setTags(List<PlantillaTags> tags) {
        this.tags = tags;
    }

    public String getIdPlantilla() {
        return idPlantilla;
    }

    public int getArchivos() {
        return archivos;
    }

    public void setArchivos(int archivos) {
        this.archivos = archivos;
    }

    public void setIdPlantilla(String idPlantilla) {
        this.idPlantilla = idPlantilla;
    }

    public String getCodTipoDocEmp() {
        return codTipoDocEmp;
    }

    public void setCodTipoDocEmp(String codTipoDocEmp) {
        this.codTipoDocEmp = codTipoDocEmp;
    }

    public String getNombrePlantilla() {
        return nombrePlantilla;
    }

    public void setNombrePlantilla(String nombrePlantilla) {
        this.nombrePlantilla = nombrePlantilla;
    }

    public String getXML() {
        return XML;
    }

    public void setXML(String XML) {
        this.XML = XML;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
