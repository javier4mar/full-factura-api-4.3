/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author Administrador
 */
public class ProcesarCatalogoEmp {
    private String pIdCatalogo;
    private String pIdCatalogoEmp;

    public String getpIdCatalogo() {
        return pIdCatalogo;
    }

    public void setpIdCatalogo(String pIdCatalogo) {
        this.pIdCatalogo = pIdCatalogo;
    }

    public String getpIdCatalogoEmp() {
        return pIdCatalogoEmp;
    }

    public void setpIdCatalogoEmp(String pIdCatalogoEmp) {
        this.pIdCatalogoEmp = pIdCatalogoEmp;
    }
    
}
