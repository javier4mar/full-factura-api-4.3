/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class ImpuestoExoClienteAdd {
    private String idImpuestoEmp;
    private List<String> idsExoneracion;

    public String getIdImpuestoEmp() {
        return idImpuestoEmp;
    }

    public void setIdImpuestoEmp(String idImpuestoEmp) {
        this.idImpuestoEmp = idImpuestoEmp;
    }

    public List<String> getIdsExoneracion() {
        return idsExoneracion;
    }

    public void setIdsExoneracion(List<String> idsExoneracion) {
        this.idsExoneracion = idsExoneracion;
    }

}
