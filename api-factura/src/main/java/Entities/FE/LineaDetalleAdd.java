/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class LineaDetalleAdd {
    
    private String pIdArticuloTercero;
    private String pNumeroLinea;
    private String pCodigo;
    private String pCodigoArtEmp;
    private String pCodigoArt;
    private String pCantidad;
    private String pUnidadMedida;
    private String pUnidadMedidaComercial;
    private String pDetalle;
    private String pPrecioUnitario;
    private String pMontoTotal;
    private String pMontoDescuento;
    private String pNaturalezaDescuento;
    private String pSubTotal;
    private List<ImpuestoAdd> pImpuesto;
    private String pMercServ;
    private String pMontoTotalLinea;
    private String pUnidadMedidaEmp;
    private String pAplicaDescuento;
    private String pIdDocumentoDet;

    public String getpUnidadMedidaEmp() {
        return pUnidadMedidaEmp;
    }

    public void setpUnidadMedidaEmp(String pUnidadMedidaEmp) {
        this.pUnidadMedidaEmp = pUnidadMedidaEmp;
    }

    public String getpCodigoArt() {
        return pCodigoArt;
    }

    public void setpCodigoArt(String pCodigoArt) {
        this.pCodigoArt = pCodigoArt;
    }

    public String getpNumeroLinea() {
        return pNumeroLinea;
    }

    public void setpNumeroLinea(String pNumeroLinea) {
        this.pNumeroLinea = pNumeroLinea;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpCodigoArtEmp() {
        return pCodigoArtEmp;
    }

    public void setpCodigoArtEmp(String pCodigoArtEmp) {
        this.pCodigoArtEmp = pCodigoArtEmp;
    }

    public String getpCantidad() {
        return pCantidad;
    }

    public void setpCantidad(String pCantidad) {
        this.pCantidad = pCantidad;
    }

    public String getpUnidadMedida() {
        return pUnidadMedida;
    }

    public void setpUnidadMedida(String pUnidadMedida) {
        this.pUnidadMedida = pUnidadMedida;
    }

    public String getpUnidadMedidaComercial() {
        return pUnidadMedidaComercial;
    }

    public void setpUnidadMedidaComercial(String pUnidadMedidaComercial) {
        this.pUnidadMedidaComercial = pUnidadMedidaComercial;
    }

    public String getpDetalle() {
        return pDetalle;
    }

    public void setpDetalle(String pDetalle) {
        this.pDetalle = pDetalle;
    }

    public String getpPrecioUnitario() {
        return pPrecioUnitario;
    }

    public void setpPrecioUnitario(String pPrecioUnitario) {
        this.pPrecioUnitario = pPrecioUnitario;
    }

    public String getpMontoDescuento() {
        return pMontoDescuento;
    }

    public void setpMontoDescuento(String pMontoDescuento) {
        this.pMontoDescuento = pMontoDescuento;
    }

    public String getpNaturalezaDescuento() {
        return pNaturalezaDescuento;
    }

    public void setpNaturalezaDescuento(String pNaturalezaDescuento) {
        this.pNaturalezaDescuento = pNaturalezaDescuento;
    }

    public List<ImpuestoAdd> getpImpuesto() {
        return pImpuesto;
    }

    public void setpImpuesto(List<ImpuestoAdd> pImpuesto) {
        this.pImpuesto = pImpuesto;
    }

    public String getpMercServ() {
        return pMercServ;
    }

    public void setpMercServ(String pMercServ) {
        this.pMercServ = pMercServ;
    }

    public String getpMontoTotal() {
        return pMontoTotal;
    }

    public void setpMontoTotal(String pMontoTotal) {
        this.pMontoTotal = pMontoTotal;
    }

    public String getpSubTotal() {
        return pSubTotal;
    }

    public void setpSubTotal(String pSubTotal) {
        this.pSubTotal = pSubTotal;
    }

    public String getpMontoTotalLinea() {
        return pMontoTotalLinea;
    }

    public void setpMontoTotalLinea(String pMontoTotalLinea) {
        this.pMontoTotalLinea = pMontoTotalLinea;
    }

    public String getpAplicaDescuento() {
        return pAplicaDescuento;
    }

    public void setpAplicaDescuento(String pAplicaDescuento) {
        this.pAplicaDescuento = pAplicaDescuento;
    }

    public String getpIdDocumentoDet() {
        return pIdDocumentoDet;
    }

    public void setpIdDocumentoDet(String pIdDocumentoDet) {
        this.pIdDocumentoDet = pIdDocumentoDet;
    }

    public String getpIdArticuloTercero() {
        return pIdArticuloTercero;
    }

    public void setpIdArticuloTercero(String pIdArticuloTercero) {
        this.pIdArticuloTercero = pIdArticuloTercero;
    }

}
