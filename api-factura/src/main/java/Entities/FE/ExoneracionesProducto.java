package Entities.FE;

/**
 *
 * @author javierhernandez
 */
public class ExoneracionesProducto {
   
    String id;
    String idImpuesto;
    String idExoneracion;
    String idArticulo;
    String idCliente;
    String codigoArticulo;
    String descripcionArticulo;
    String tipoArticulo;
    String costo;
    String precio;
    String porcentaje;
    String descuento;
    String descripcionDescuento;
    String porcImpuesto;
    String codigoTipoExo;
    String numeroDocumento;
    String nombreInstitucion;
    String fechaEmision;
    String montoImpuesto;
    String porcCompra;
    String fechaHasta;
    String estadoExoneracion;
    String nombreComercialReceptor;
    String tipoIdentificacionReceptor;
    String numeroIdentificacionReceptor;
    String identificacionExtranjeroReceptor;
    String razonSocialReceptor;
    String tipoIdentDesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getIdExoneracion() {
        return idExoneracion;
    }

    public void setIdExoneracion(String idExoneracion) {
        this.idExoneracion = idExoneracion;
    }

    public String getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getDescripcionArticulo() {
        return descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getDescripcionDescuento() {
        return descripcionDescuento;
    }

    public void setDescripcionDescuento(String descripcionDescuento) {
        this.descripcionDescuento = descripcionDescuento;
    }

    public String getPorcImpuesto() {
        return porcImpuesto;
    }

    public void setPorcImpuesto(String porcImpuesto) {
        this.porcImpuesto = porcImpuesto;
    }

    public String getCodigoTipoExo() {
        return codigoTipoExo;
    }

    public void setCodigoTipoExo(String codigoTipoExo) {
        this.codigoTipoExo = codigoTipoExo;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(String montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public String getPorcCompra() {
        return porcCompra;
    }

    public void setPorcCompra(String porcCompra) {
        this.porcCompra = porcCompra;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getEstadoExoneracion() {
        return estadoExoneracion;
    }

    public void setEstadoExoneracion(String estadoExoneracion) {
        this.estadoExoneracion = estadoExoneracion;
    }

    public String getNombreComercialReceptor() {
        return nombreComercialReceptor;
    }

    public void setNombreComercialReceptor(String nombreComercialReceptor) {
        this.nombreComercialReceptor = nombreComercialReceptor;
    }

    public String getTipoIdentificacionReceptor() {
        return tipoIdentificacionReceptor;
    }

    public void setTipoIdentificacionReceptor(String tipoIdentificacionReceptor) {
        this.tipoIdentificacionReceptor = tipoIdentificacionReceptor;
    }

    public String getNumeroIdentificacionReceptor() {
        return numeroIdentificacionReceptor;
    }

    public void setNumeroIdentificacionReceptor(String numeroIdentificacionReceptor) {
        this.numeroIdentificacionReceptor = numeroIdentificacionReceptor;
    }

    public String getIdentificacionExtranjeroReceptor() {
        return identificacionExtranjeroReceptor;
    }

    public void setIdentificacionExtranjeroReceptor(String identificacionExtranjeroReceptor) {
        this.identificacionExtranjeroReceptor = identificacionExtranjeroReceptor;
    }

    public String getRazonSocialReceptor() {
        return razonSocialReceptor;
    }

    public void setRazonSocialReceptor(String razonSocialReceptor) {
        this.razonSocialReceptor = razonSocialReceptor;
    }

    public String getTipoIdentDesc() {
        return tipoIdentDesc;
    }

    public void setTipoIdentDesc(String tipoIdentDesc) {
        this.tipoIdentDesc = tipoIdentDesc;
    } 
    
}
