/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class ProcesaCorreo {
    private String pIdInteresado;
    private String pEstado;

    public String getpIdInteresado() {
        return pIdInteresado;
    }

    public void setpIdInteresado(String pIdInteresado) {
        this.pIdInteresado = pIdInteresado;
    }

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }
    
}
