package Entities.FE;

import java.util.List;

/**
 *
 * @author javierhernandez
 */
public class UpdateDataDocumentoPlantilla {
    
    private String pClaveDocumento;
    private String pEstado;
    private String pOtrosTextoPlantilla;
    private String pIdPlantillaXml;
    private List<DatosTextoPlantilla> pDatosTextoPlantilla;

    public String getpClaveDocumento() {
        return pClaveDocumento;
    }

    public void setpClaveDocumento(String pClaveDocumento) {
        this.pClaveDocumento = pClaveDocumento;
    }

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }

    public String getpOtrosTextoPlantilla() {
        return pOtrosTextoPlantilla;
    }

    public void setpOtrosTextoPlantilla(String pOtrosTextoPlantilla) {
        this.pOtrosTextoPlantilla = pOtrosTextoPlantilla;
    }

    public String getpIdPlantillaXml() {
        return pIdPlantillaXml;
    }

    public void setpIdPlantillaXml(String pIdPlantillaXml) {
        this.pIdPlantillaXml = pIdPlantillaXml;
    }

    public List<DatosTextoPlantilla> getpDatosTextoPlantilla() {
        return pDatosTextoPlantilla;
    }

    public void setpDatosTextoPlantilla(List<DatosTextoPlantilla> pDatosTextoPlantilla) {
        this.pDatosTextoPlantilla = pDatosTextoPlantilla;
    }
    
    
}
