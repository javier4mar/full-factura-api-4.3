/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author javierhernandez
 */
public class BuscarReceptor {
    
    private String pCedula;
    private String pNombre;

    public String getpCedula() {
        return pCedula;
    }

    public void setpCedula(String pCedula) {
        this.pCedula = pCedula;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

}
