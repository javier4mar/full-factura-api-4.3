/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author Administrador
 */
public class ExoneracionArt {
    private String codigoTipoExo;
    private String descTipoExo;
    private String numeroDocumento;
    private String nombreInstitucion;
    private String fechaEmision;
    private String montoImpuesto;
    private String porcentajeCompra;
    private String idExoneracion;
    private String idCliente;
    private String fechaHasta;
    private String estado;
    private String exoGlobal;

    public String getCodigoTipoExo() {
        return codigoTipoExo;
    }

    public void setCodigoTipoExo(String codigoTipoExo) {
        this.codigoTipoExo = codigoTipoExo;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(String montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public String getPorcentajeCompra() {
        return porcentajeCompra;
    }

    public void setPorcentajeCompra(String porcentajeCompra) {
        this.porcentajeCompra = porcentajeCompra;
    }

    public String getIdExoneracion() {
        return idExoneracion;
    }

    public void setIdExoneracion(String idExoneracion) {
        this.idExoneracion = idExoneracion;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getFechaHasta() {
        return fechaHasta; 
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescTipoExo() {
        return descTipoExo;
    }

    public void setDescTipoExo(String descTipoExo) {
        this.descTipoExo = descTipoExo;
    }

    public String getExoGlobal() {
        return exoGlobal;
    }

    public void setExoGlobal(String exoGlobal) {
        this.exoGlobal = exoGlobal;
    }
    
    
}
