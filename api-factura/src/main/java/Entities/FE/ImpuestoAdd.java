/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class ImpuestoAdd {
    private String pCodigo;
    private String pTarifa;
    private Exoneracion pExoneracion;
    private String pIdImpuestoEmp;
    private String pMonto;

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpTarifa() {
        return pTarifa;
    }

    public void setpTarifa(String pTarifa) {
        this.pTarifa = pTarifa;
    }

    public Exoneracion getpExoneracion() {
        return pExoneracion;
    }

    public void setpExoneracion(Exoneracion pExoneracion) {
        this.pExoneracion = pExoneracion;
    }

    public String getpMonto() {
        return pMonto;
    }

    public void setpMonto(String pMonto) {
        this.pMonto = pMonto;
    }

    public String getpIdImpuestoEmp() {
        return pIdImpuestoEmp;
    }

    public void setpIdImpuestoEmp(String pIdImpuestoEmp) {
        this.pIdImpuestoEmp = pIdImpuestoEmp;
    }

}
