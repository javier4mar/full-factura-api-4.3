/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class Usr {
    private String idUsuario;
    private String dsUsuario;
    private String dsClave;
    private String dsDescripcion;
    private String idEmpresaDefault;
    private String nombreEmpresaDefault;
    private String stUsuario;
    private String stUsuarioDescripcion;
    private List<EmpresaUsuario> empresas;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDsUsuario() {
        return dsUsuario;
    }

    public void setDsUsuario(String dsUsuario) {
        this.dsUsuario = dsUsuario;
    }

    public String getDsDescripcion() {
        return dsDescripcion;
    }

    public void setDsDescripcion(String dsDescripcion) {
        this.dsDescripcion = dsDescripcion;
    }

    public String getIdEmpresaDefault() {
        return idEmpresaDefault;
    }

    public void setIdEmpresaDefault(String idEmpresaDefault) {
        this.idEmpresaDefault = idEmpresaDefault;
    }

    public String getStUsuario() {
        return stUsuario;
    }

    public void setStUsuario(String stUsuario) {
        this.stUsuario = stUsuario;
    }

    public String getDsClave() {
        return dsClave;
    }

    public void setDsClave(String dsClave) {
        this.dsClave = dsClave;
    }

    public String getNombreEmpresaDefault() {
        return nombreEmpresaDefault;
    }

    public void setNombreEmpresaDefault(String nombreEmpresaDefault) {
        this.nombreEmpresaDefault = nombreEmpresaDefault;
    }

    public List<EmpresaUsuario> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<EmpresaUsuario> empresas) {
        this.empresas = empresas;
    }

    public String getStUsuarioDescripcion() {
        return stUsuarioDescripcion;
    }

    public void setStUsuarioDescripcion(String stUsuarioDescripcion) {
        this.stUsuarioDescripcion = stUsuarioDescripcion;
    }
    
}
