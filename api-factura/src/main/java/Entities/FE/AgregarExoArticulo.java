package Entities.FE;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class AgregarExoArticulo {
    
    private String idImpuesto;
    private String idArticulo;
    private String idExoneracion;
    private String idCliente;

    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getIdExoneracion() {
        return idExoneracion;
    }

    public void setIdExoneracion(String idExoneracion) {
        this.idExoneracion = idExoneracion;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

}
