/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import Entities.Catalogos.Plantilla;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class Receptor {
    private String nombre;
    private List<String> tiposIdentificacionEmp;
    private String numIdentificacion;
    private String identificacionExtranjero;
    private String nombreComercial;
    private List<String> provinciaEmp;
    private List<String> cantonEmp;
    private List<String> distritoEmp;
    private List<String> barrioEmp;
    private String otrasSenas;
    private String codPaisTel;
    private String numTel;
    private String codPaisFax;
    private String fax;
    private String correoElectronico;
    private String idReceptorEmp;
    private String tipoTributario;
    private String estado;
    private String estadoDesc;
    private String tipoIdentificacionDesc;
    private String tipoTributarioDesc;
    private List<ExoneracionArt> exoneraciones;
    private String exoGLobal;
    private String idPlantilla;
    private Plantilla plantilla;
    private List<Plantilla> listaPlantillas;

    public List<Plantilla> getListaPlantillas() {
        return listaPlantillas;
    }

    public void setListaPlantillas(List<Plantilla> listaPlantillas) {
        this.listaPlantillas = listaPlantillas;
    }

    public String getIdPlantilla() {
        return idPlantilla;
    }

    public Plantilla getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(Plantilla plantilla) {
        this.plantilla = plantilla;
    }

    public void setIdPlantilla(String idPlantilla) {
        this.idPlantilla = idPlantilla;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getTiposIdentificacionEmp() {
        return tiposIdentificacionEmp;
    }

    public void setTiposIdentificacionEmp(List<String> tiposIdentificacionEmp) {
        this.tiposIdentificacionEmp = tiposIdentificacionEmp;
    }

    public String getNumIdentificacion() {
        return numIdentificacion;
    }

    public void setNumIdentificacion(String numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

    public String getIdentificacionExtranjero() {
        return identificacionExtranjero;
    }

    public void setIdentificacionExtranjero(String identificacionExtranjero) {
        this.identificacionExtranjero = identificacionExtranjero;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public List<String> getProvinciaEmp() {
        return provinciaEmp;
    }

    public void setProvinciaEmp(List<String> provinciaEmp) {
        this.provinciaEmp = provinciaEmp;
    }

    public List<String> getCantonEmp() {
        return cantonEmp;
    }

    public void setCantonEmp(List<String> cantonEmp) {
        this.cantonEmp = cantonEmp;
    }

    public List<String> getDistritoEmp() {
        return distritoEmp;
    }

    public void setDistritoEmp(List<String> distritoEmp) {
        this.distritoEmp = distritoEmp;
    }

    public List<String> getBarrioEmp() {
        return barrioEmp;
    }

    public void setBarrioEmp(List<String> barrioEmp) {
        this.barrioEmp = barrioEmp;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getCodPaisTel() {
        return codPaisTel;
    }

    public void setCodPaisTel(String codPaisTel) {
        this.codPaisTel = codPaisTel;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getCodPaisFax() {
        return codPaisFax;
    }

    public void setCodPaisFax(String codPaisFax) {
        this.codPaisFax = codPaisFax;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getIdReceptorEmp() {
        return idReceptorEmp;
    }

    public void setIdReceptorEmp(String idReceptorEmp) {
        this.idReceptorEmp = idReceptorEmp;
    }

    public String getTipoTributario() {
        return tipoTributario;
    }

    public void setTipoTributario(String tipoTributario) {
        this.tipoTributario = tipoTributario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getTipoIdentificacionDesc() {
        return tipoIdentificacionDesc;
    }

    public void setTipoIdentificacionDesc(String tipoIdentificacionDesc) {
        this.tipoIdentificacionDesc = tipoIdentificacionDesc;
    }

    public String getTipoTributarioDesc() {
        return tipoTributarioDesc;
    }

    public void setTipoTributarioDesc(String tipoTributarioDesc) {
        this.tipoTributarioDesc = tipoTributarioDesc;
    }

    public List<ExoneracionArt> getExoneraciones() {
        return exoneraciones;
    }

    public void setExoneraciones(List<ExoneracionArt> exoneraciones) {
        this.exoneraciones = exoneraciones;
    }

    public String getExoGLobal() {
        return exoGLobal;
    }

    public void setExoGLobal(String exoGLobal) {
        this.exoGLobal = exoGLobal;
    }
   
}
