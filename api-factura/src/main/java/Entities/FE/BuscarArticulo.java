/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author javierhernandez
 */
public class BuscarArticulo {
    
    private String pIdCliente;
    private String pCodigoArticulo;
    private String pNombreArticulo;

    public String getpIdCliente() {
        return pIdCliente;
    }

    public void setpIdCliente(String pIdCliente) {
        this.pIdCliente = pIdCliente;
    }

    public String getpCodigoArticulo() {
        return pCodigoArticulo;
    }

    public void setpCodigoArticulo(String pCodigoArticulo) {
        this.pCodigoArticulo = pCodigoArticulo;
    }

    public String getpNombreArticulo() {
        return pNombreArticulo;
    }

    public void setpNombreArticulo(String pNombreArticulo) {
        this.pNombreArticulo = pNombreArticulo;
    }

 
}
