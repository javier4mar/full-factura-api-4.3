/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class Exoneracion {
    private String pTipoDocumentoEmp;
    private String pTipoDocumento;
    private String pNumeroDocumento;
    private String pNombreInstitucion;
    private String pFechaEmision;
    private String pMontoImpuesto;
    private String pPorcentajeCompra;

    public String getpTipoDocumento() {
        return pTipoDocumento;
    }

    public void setpTipoDocumento(String pTipoDocumento) {
        this.pTipoDocumento = pTipoDocumento;
    }

    public String getpTipoDocumentoEmp() {
        return pTipoDocumentoEmp;
    }

    public void setpTipoDocumentoEmp(String pTipoDocumentoEmp) {
        this.pTipoDocumentoEmp = pTipoDocumentoEmp;
    }

    public String getpNumeroDocumento() {
        return pNumeroDocumento;
    }

    public void setpNumeroDocumento(String pNumeroDocumento) {
        this.pNumeroDocumento = pNumeroDocumento;
    }

    public String getpNombreInstitucion() {
        return pNombreInstitucion;
    }

    public void setpNombreInstitucion(String pNombreInstitucion) {
        this.pNombreInstitucion = pNombreInstitucion;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpMontoImpuesto() {
        return pMontoImpuesto;
    }

    public void setpMontoImpuesto(String pMontoImpuesto) {
        this.pMontoImpuesto = pMontoImpuesto;
    }

    public String getpPorcentajeCompra() {
        return pPorcentajeCompra;
    }

    public void setpPorcentajeCompra(String pPorcentajeCompra) {
        this.pPorcentajeCompra = pPorcentajeCompra;
    }

}
