/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author javierhernandez
 */
public class DatosTextoPlantilla {

    String pNombre;
    String pValor;
    String pTipo;

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpValor() {
        return pValor;
    }

    public void setpValor(String pValor) {
        this.pValor = pValor;
    }

    public String getpTipo() {
        return pTipo;
    }

    public void setpTipo(String pTipo) {
        this.pTipo = pTipo;
    }

}
