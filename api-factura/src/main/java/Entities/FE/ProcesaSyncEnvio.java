/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class ProcesaSyncEnvio {
    private String pIdDocumento;
    private String pResponse;
    private String pEstado;

    public String getpIdDocumento() {
        return pIdDocumento;
    }

    public void setpIdDocumento(String pIdDocumento) {
        this.pIdDocumento = pIdDocumento;
    }

    public String getpResponse() {
        return pResponse;
    }

    public void setpResponse(String pResponse) {
        this.pResponse = pResponse;
    }

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }
    
}
