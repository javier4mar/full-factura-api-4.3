/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class PuntoVenta {
    private String idSucursalEmp;
    private String idPuntoVentaEmp;
    private String numPuntoVenta;
    private String descripcion;
    private String estado;
    private String estadoDesc;
    private String conFactura;
    private String conTiquete;
    private String conNotaCredito;
    private String conNotaDebito;
    private String conAcepta;
    private String conAceptaParcial;
    private String conRechaza;

    public String getIdSucursalEmp() {
        return idSucursalEmp;
    }

    public void setIdSucursalEmp(String idSucursalEmp) {
        this.idSucursalEmp = idSucursalEmp;
    }

    public String getIdPuntoVentaEmp() {
        return idPuntoVentaEmp;
    }

    public void setIdPuntoVentaEmp(String idPuntoVentaEmp) {
        this.idPuntoVentaEmp = idPuntoVentaEmp;
    }

    public String getNumPuntoVenta() {
        return numPuntoVenta;
    }

    public void setNumPuntoVenta(String numPuntoVenta) {
        this.numPuntoVenta = numPuntoVenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getConFactura() {
        return conFactura;
    }

    public void setConFactura(String conFactura) {
        this.conFactura = conFactura;
    }

    public String getConTiquete() {
        return conTiquete;
    }

    public void setConTiquete(String conTiquete) {
        this.conTiquete = conTiquete;
    }

    public String getConNotaCredito() {
        return conNotaCredito;
    }

    public void setConNotaCredito(String conNotaCredito) {
        this.conNotaCredito = conNotaCredito;
    }

    public String getConNotaDebito() {
        return conNotaDebito;
    }

    public void setConNotaDebito(String conNotaDebito) {
        this.conNotaDebito = conNotaDebito;
    }

    public String getConAcepta() {
        return conAcepta;
    }

    public void setConAcepta(String conAcepta) {
        this.conAcepta = conAcepta;
    }

    public String getConAceptaParcial() {
        return conAceptaParcial;
    }

    public void setConAceptaParcial(String conAceptaParcial) {
        this.conAceptaParcial = conAceptaParcial;
    }

    public String getConRechaza() {
        return conRechaza;
    }

    public void setConRechaza(String conRechaza) {
        this.conRechaza = conRechaza;
    }
    
}
