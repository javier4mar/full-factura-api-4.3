/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author Administrador
 */
public class Usuario {
    
    private String idUsuario;
    private String dsUsuario;
    private String dsDescripcion;
    private String idEmpresaDefault;
    private String stUsuario;

    public String getDsUsuario() {
        return dsUsuario;
    }

    public void setDsUsuario(String dsUsuario) {
        this.dsUsuario = dsUsuario;
    }
    
    public String getDsDescripcion() {
        return dsDescripcion;
    }

    public void setDsDescripcion(String dsDescripcion) {
        this.dsDescripcion = dsDescripcion;
    }
    
    public String getStUsuario() {
        return stUsuario;
    }

    public void setStUsuario(String stUsuario) {
        this.stUsuario = stUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdEmpresaDefault() {
        return idEmpresaDefault;
    }

    public void setIdEmpresaDefault(String idEmpresaDefault) {
        this.idEmpresaDefault = idEmpresaDefault;
    }
    
    
    
}
