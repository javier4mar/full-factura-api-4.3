/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class InformacionReferenciaAdd {
    private String pTipoDocRefEmp;
    private String pTipoDocRef;
    private String pNumero;
    private String pFechaEmision;
    private String pCodigo;
    private String pRazon;
    private String pIdDocumentoEmp;
    private String pExisteDb;

    public String getpTipoDocRef() {
        return pTipoDocRef;
    }

    public void setpTipoDocRef(String pTipoDocRef) {
        this.pTipoDocRef = pTipoDocRef;
    }

    public String getpNumero() {
        return pNumero;
    }

    public void setpNumero(String pNumero) {
        this.pNumero = pNumero;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpRazon() {
        return pRazon;
    }

    public void setpRazon(String pRazon) {
        this.pRazon = pRazon;
    }

    public String getpTipoDocRefEmp() {
        return pTipoDocRefEmp;
    }

    public void setpTipoDocRefEmp(String pTipoDocRefEmp) {
        this.pTipoDocRefEmp = pTipoDocRefEmp;
    }

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public String getpExisteDb() {
        return pExisteDb;
    }

    public void setpExisteDb(String pExisteDb) {
        this.pExisteDb = pExisteDb;
    }


}
