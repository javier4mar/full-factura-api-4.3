package Entities.FE;

public class PlantillaTags {
    private String idTag;
    private int idTipoTag;
    private String idTagPadre;
    private String secuencia;
    private String idPlantilla;
    private String descripcion;
    private String codTagReplace;
    private String keyValueTag;
    private String typeTag;
    private String opcionalTag;
    private String longitudTag;
    private String esAdjunto;
    private int html;
    private String estado;
    private String cdHtml;
    private int visible;
    private int autoIncrement;
    private String endTagXml;
    private int esExtension;

    public String getEndTagXml() {
        return endTagXml;
    }

    public void setEndTagXml(String endTagXml) {
        this.endTagXml = endTagXml;
    }

    public int getEsExtension() {
        return esExtension;
    }

    public void setEsExtension(int esExtension) {
        this.esExtension = esExtension;
    }

    public int getAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(int autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public String getCdHtml() {
        return cdHtml;
    }

    public void setCdHtml(String cdHtml) {
        this.cdHtml = cdHtml;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }
    public int getHtml() {
        return html;
    }

    public void setHtml(int html) {
        this.html = html;
    }

    public int getIdTipoTag() {
        return idTipoTag;
    }

    public void setIdTipoTag(int idTipoTag) {
        this.idTipoTag = idTipoTag;
    }

    public String getIdTag() {
        return idTag;
    }

    public String getIdTagPadre() {
        return idTagPadre;
    }

    public void setIdTagPadre(String idTagPadre) {
        this.idTagPadre = idTagPadre;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public void setIdTag(String idTag) {
        this.idTag = idTag;
    }

    public String getIdPlantilla() {
        return idPlantilla;
    }

    public void setIdPlantilla(String idPlantilla) {
        this.idPlantilla = idPlantilla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodTagReplace() {
        return codTagReplace;
    }

    public void setCodTagReplace(String codTagReplace) {
        this.codTagReplace = codTagReplace;
    }

    public String getKeyValueTag() {
        return keyValueTag;
    }

    public void setKeyValueTag(String keyValueTag) {
        this.keyValueTag = keyValueTag;
    }

    public String getTypeTag() {
        return typeTag;
    }

    public void setTypeTag(String typeTag) {
        this.typeTag = typeTag;
    }

    public String getOpcionalTag() {
        return opcionalTag;
    }

    public void setOpcionalTag(String opcionalTag) {
        this.opcionalTag = opcionalTag;
    }

    public String getLongitudTag() {
        return longitudTag;
    }

    public void setLongitudTag(String longitudTag) {
        this.longitudTag = longitudTag;
    }

    public String getEsAdjunto() {
        return esAdjunto;
    }

    public void setEsAdjunto(String esAdjunto) {
        this.esAdjunto = esAdjunto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
