/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class DocUrl {
    private String pIdDocumentoEmp;
    private String pClave;

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public String getpClave() {
        return pClave;
    }

    public void setpClave(String pClave) {
        this.pClave = pClave;
    }

    
    
}
