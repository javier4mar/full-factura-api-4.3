/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import Entities.Catalogos.Plantilla;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class Documento {
    private String pIdEmpresa;
    private String pIdDocumento;
    private String pIdDocumentoEmp;
    private String pTipoDocumentoEmp;
    private String pClave;
    private String pConsecutivo;
    private String pIdSucursalEmp;
    private String pIdPuntoVentaEmp;
    private String pCondicionVentaEmp;
    private String pPlazoCredito;
    private List<String> pMedioPagoEmp;
    private String pIdMonedaEmp;
    private String pSimboloMoneda;
    private String pTipoCambio;
    private String pEstado;
    private String pEstadoDescripcion;
    private String pSituacion;
    private String pUltNovedad;
    private String pIdClienteEmp;
    private String pTipoIdClienteEmp;
    private String pNumeroIdCliente;
    private String pNombreCliente;
    private String pCorreoCliente;
    private String pEsReceptor;
    private String pEmitida;
    private String pCodMensajeHacienda;
    private String pTotalVentaNeta;
    private String pTotalImpuestos;
    private String pTotalComprobante;
    private String pTipoDocumentoDesc;
    private String pFechaEmision;
    private String pCorreoEmpresa;
    private String pTelefonoEmpresa;
    private String pFaxEmpresa;
    private String pNombreEmpresa;
    private String pNombreEmpresaCom;
    private String pIdentificacionEmpresa;
    private String pRutaXml;
    private String pXmlFirmado;
    private String pXmlRespuesta;
    private String pRutaLogo;
    private String pOtrasSenasEmpresa;
    private String pOtrasSenasCliente;
    private String pObservaciones;
    private String pEstadoAceptacion;
    private String pFechaRegistro;
    private String pCondicionVentaDesc;
    private String pRazonSocialEmpEmitido;
    private String pNumIdentificacionEmitido;
    private String pCorreoElectronicoEmitido;
    private Plantilla plantilla;
    private String pTotalSerGravados;
    private String pTotalSerExentos;
    private String pTotalMercanciasGravados;
    private String pTotalMercanciasExentos;
    private String pTotalGravado;
    private String pTotalExento;
    private String pTotalVenta;
    private String pTotalDescuentos; 
    private String pReceptorNombreComercial;
    private String pTipoIdenReceptorDesc;
    private String Emitida;
    private String pConsecutivoReceptor;
    private List<Plantilla> listaPlantillas;
    private String idFacRef;
    private String ordenCompra;

    public String getIdFacRef() {
        return idFacRef;
    }

    public String getpIdEmpresa() {
        return pIdEmpresa;
    }

    public void setpIdEmpresa(String pIdEmpresa) {
        this.pIdEmpresa = pIdEmpresa;
    }

    public void setIdFacRef(String idFacRef) {
        this.idFacRef = idFacRef;
    }

    public String getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(String ordenCompra) {
        this.ordenCompra = ordenCompra;
    }
    

    public List<Plantilla> getListaPlantillas() {
        return listaPlantillas;
    }

    public void setListaPlantillas(List<Plantilla> listaPlantillas) {
        this.listaPlantillas = listaPlantillas;
    }

    public String getpTotalSerGravados() {
        return pTotalSerGravados;
    }

    public void setpTotalSerGravados(String pTotalSerGravados) {
        this.pTotalSerGravados = pTotalSerGravados;
    }

    public String getpTotalSerExentos() {
        return pTotalSerExentos;
    }

    public void setpTotalSerExentos(String pTotalSerExentos) {
        this.pTotalSerExentos = pTotalSerExentos;
    }

    public String getpTotalMercanciasGravados() {
        return pTotalMercanciasGravados;
    }

    public void setpTotalMercanciasGravados(String pTotalMercanciasGravados) {
        this.pTotalMercanciasGravados = pTotalMercanciasGravados;
    }

    public String getpTotalMercanciasExentos() {
        return pTotalMercanciasExentos;
    }

    public void setpTotalMercanciasExentos(String pTotalMercanciasExentos) {
        this.pTotalMercanciasExentos = pTotalMercanciasExentos;
    }

    public String getpTotalGravado() {
        return pTotalGravado;
    }

    public void setpTotalGravado(String pTotalGravado) {
        this.pTotalGravado = pTotalGravado;
    }

    public String getpTotalExento() {
        return pTotalExento;
    }

    public void setpTotalExento(String pTotalExento) {
        this.pTotalExento = pTotalExento;
    }

    public String getpTotalVenta() {
        return pTotalVenta;
    }

    public void setpTotalVenta(String pTotalVenta) {
        this.pTotalVenta = pTotalVenta;
    }

    public String getpTotalDescuentos() {
        return pTotalDescuentos;
    }

    public void setpTotalDescuentos(String pTotalDescuentos) {
        this.pTotalDescuentos = pTotalDescuentos;
    }

    public String getpReceptorNombreComercial() {
        return pReceptorNombreComercial;
    }

    public void setpReceptorNombreComercial(String pReceptorNombreComercial) {
        this.pReceptorNombreComercial = pReceptorNombreComercial;
    }

    public String getpTipoIdenReceptorDesc() {
        return pTipoIdenReceptorDesc;
    }

    public void setpTipoIdenReceptorDesc(String pTipoIdenReceptorDesc) {
        this.pTipoIdenReceptorDesc = pTipoIdenReceptorDesc;
    }

    public String getEmitida() {
        return Emitida;
    }

    public void setEmitida(String Emitida) {
        this.Emitida = Emitida;
    }

    public String getpConsecutivoReceptor() {
        return pConsecutivoReceptor;
    }

    public void setpConsecutivoReceptor(String pConsecutivoReceptor) {
        this.pConsecutivoReceptor = pConsecutivoReceptor;
    }
    
    
    public Plantilla getPlantilla() {
        
        return plantilla;
    }

    public void setPlantilla(Plantilla plantilla) {
        this.plantilla = plantilla;
    }
    
    private List<LineaDetalle> pLineasDetalle;
    private List<InteresadoDocumento> pInteresadosDoc;

    public String getpIdDocumento() {
        return pIdDocumento;
    }

    public void setpIdDocumento(String pIdDocumento) {
        this.pIdDocumento = pIdDocumento;
    }

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public String getpTipoDocumentoEmp() {
        return pTipoDocumentoEmp;
    }

    public void setpTipoDocumentoEmp(String pTipoDocumentoEmp) {
        this.pTipoDocumentoEmp = pTipoDocumentoEmp;
    }

    public String getpClave() {
        return pClave;
    }

    public void setpClave(String pClave) {
        this.pClave = pClave;
    }

    public String getpConsecutivo() {
        return pConsecutivo;
    }

    public void setpConsecutivo(String pConsecutivo) {
        this.pConsecutivo = pConsecutivo;
    }

    public String getpIdSucursalEmp() {
        return pIdSucursalEmp;
    }

    public void setpIdSucursalEmp(String pIdSucursalEmp) {
        this.pIdSucursalEmp = pIdSucursalEmp;
    }

    public String getpIdPuntoVentaEmp() {
        return pIdPuntoVentaEmp;
    }

    public void setpIdPuntoVentaEmp(String pIdPuntoVentaEmp) {
        this.pIdPuntoVentaEmp = pIdPuntoVentaEmp;
    }

    public String getpCondicionVentaEmp() {
        return pCondicionVentaEmp;
    }

    public void setpCondicionVentaEmp(String pCondicionVentaEmp) {
        this.pCondicionVentaEmp = pCondicionVentaEmp;
    }

    public String getpPlazoCredito() {
        return pPlazoCredito;
    }

    public void setpPlazoCredito(String pPlazoCredito) {
        this.pPlazoCredito = pPlazoCredito;
    }

    public List<String> getpMedioPagoEmp() {
        return pMedioPagoEmp;
    }

    public void setpMedioPagoEmp(List<String> pMedioPagoEmp) {
        this.pMedioPagoEmp = pMedioPagoEmp;
    }

    public String getpIdMonedaEmp() {
        return pIdMonedaEmp;
    }

    public void setpIdMonedaEmp(String pIdMonedaEmp) {
        this.pIdMonedaEmp = pIdMonedaEmp;
    }

    public String getpTipoCambio() {
        return pTipoCambio;
    }

    public void setpTipoCambio(String pTipoCambio) {
        this.pTipoCambio = pTipoCambio;
    }

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }

    public String getpSituacion() {
        return pSituacion;
    }

    public void setpSituacion(String pSituacion) {
        this.pSituacion = pSituacion;
    }

    public String getpUltNovedad() {
        return pUltNovedad;
    }

    public void setpUltNovedad(String pUltNovedad) {
        this.pUltNovedad = pUltNovedad;
    }

    public String getpIdClienteEmp() {
        return pIdClienteEmp;
    }

    public void setpIdClienteEmp(String pIdClienteEmp) {
        this.pIdClienteEmp = pIdClienteEmp;
    }

    public String getpTipoIdClienteEmp() {
        return pTipoIdClienteEmp;
    }

    public void setpTipoIdClienteEmp(String pTipoIdClienteEmp) {
        this.pTipoIdClienteEmp = pTipoIdClienteEmp;
    }

    public String getpNumeroIdCliente() {
        return pNumeroIdCliente;
    }

    public void setpNumeroIdCliente(String pNumeroIdCliente) {
        this.pNumeroIdCliente = pNumeroIdCliente;
    }

    public String getpNombreCliente() {
        return pNombreCliente;
    }

    public void setpNombreCliente(String pNombreCliente) {
        this.pNombreCliente = pNombreCliente;
    }

    public String getpCorreoCliente() {
        return pCorreoCliente;
    }

    public void setpCorreoCliente(String pCorreoCliente) {
        this.pCorreoCliente = pCorreoCliente;
    }

    public String getpEsReceptor() {
        return pEsReceptor;
    }

    public void setpEsReceptor(String pEsReceptor) {
        this.pEsReceptor = pEsReceptor;
    }

    public List<LineaDetalle> getpLineasDetalle() {
        return pLineasDetalle;
    }

    public void setpLineasDetalle(List<LineaDetalle> pLineasDetalle) {
        this.pLineasDetalle = pLineasDetalle;
    }

    public String getpEmitida() {
        return pEmitida;
    }

    public void setpEmitida(String pEmitida) {
        this.pEmitida = pEmitida;
    }

    public String getpCodMensajeHacienda() {
        return pCodMensajeHacienda;
    }

    public void setpCodMensajeHacienda(String pCodMensajeHacienda) {
        this.pCodMensajeHacienda = pCodMensajeHacienda;
    }

    public String getpTotalVentaNeta() {
        return pTotalVentaNeta;
    }

    public void setpTotalVentaNeta(String pTotalVentaNeta) {
        this.pTotalVentaNeta = pTotalVentaNeta;
    }

    public String getpTotalImpuestos() {
        return pTotalImpuestos;
    }

    public void setpTotalImpuestos(String pTotalImpuestos) {
        this.pTotalImpuestos = pTotalImpuestos;
    }

    public String getpTotalComprobante() {
        return pTotalComprobante;
    }

    public void setpTotalComprobante(String pTotalComprobante) {
        this.pTotalComprobante = pTotalComprobante;
    }

    public String getpEstadoDescripcion() {
        return pEstadoDescripcion;
    }

    public void setpEstadoDescripcion(String pEstadoDescripcion) {
        this.pEstadoDescripcion = pEstadoDescripcion;
    }

    public String getpTipoDocumentoDesc() {
        return pTipoDocumentoDesc;
    }

    public void setpTipoDocumentoDesc(String pTipoDocumentoDesc) {
        this.pTipoDocumentoDesc = pTipoDocumentoDesc;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public List<InteresadoDocumento> getpInteresadosDoc() {
        return pInteresadosDoc;
    }

    public void setpInteresadosDoc(List<InteresadoDocumento> pInteresadosDoc) {
        this.pInteresadosDoc = pInteresadosDoc;
    }

    public String getpCorreoEmpresa() {
        return pCorreoEmpresa;
    }

    public void setpCorreoEmpresa(String pCorreoEmpresa) {
        this.pCorreoEmpresa = pCorreoEmpresa;
    }

    public String getpTelefonoEmpresa() {
        return pTelefonoEmpresa;
    }

    public void setpTelefonoEmpresa(String pTelefonoEmpresa) {
        this.pTelefonoEmpresa = pTelefonoEmpresa;
    }

    public String getpNombreEmpresa() {
        return pNombreEmpresa;
    }

    public void setpNombreEmpresa(String pNombreEmpresa) {
        this.pNombreEmpresa = pNombreEmpresa;
    }

    public String getpIdentificacionEmpresa() {
        return pIdentificacionEmpresa;
    }

    public void setpIdentificacionEmpresa(String pIdentificacionEmpresa) {
        this.pIdentificacionEmpresa = pIdentificacionEmpresa;
    }

    public String getpRutaXml() {
        return pRutaXml;
    }

    public void setpRutaXml(String pRutaXml) {
        this.pRutaXml = pRutaXml;
    }

    public String getpXmlFirmado() {
        return pXmlFirmado;
    }

    public void setpXmlFirmado(String pXmlFirmado) {
        this.pXmlFirmado = pXmlFirmado;
    }

    public String getpXmlRespuesta() {
        return pXmlRespuesta;
    }

    public void setpXmlRespuesta(String pXmlRespuesta) {
        this.pXmlRespuesta = pXmlRespuesta;
    }

    public String getpSimboloMoneda() {
        return pSimboloMoneda;
    }

    public void setpSimboloMoneda(String pSimboloMoneda) {
        this.pSimboloMoneda = pSimboloMoneda;
    }

    public String getpRutaLogo() {
        return pRutaLogo;
    }

    public void setpRutaLogo(String pRutaLogo) {
        this.pRutaLogo = pRutaLogo;
    }

    public String getpOtrasSenasEmpresa() {
        return pOtrasSenasEmpresa;
    }

    public void setpOtrasSenasEmpresa(String pOtrasSenasEmpresa) {
        this.pOtrasSenasEmpresa = pOtrasSenasEmpresa;
    }

    public String getpOtrasSenasCliente() {
        return pOtrasSenasCliente;
    }

    public void setpOtrasSenasCliente(String pOtrasSenasCliente) {
        this.pOtrasSenasCliente = pOtrasSenasCliente;
    }

    public String getpObservaciones() {
        return pObservaciones;
    }

    public void setpObservaciones(String pObservaciones) {
        this.pObservaciones = pObservaciones;
    }

    public String getpEstadoAceptacion() {
        return pEstadoAceptacion;
    }

    public void setpEstadoAceptacion(String pEstadoAceptacion) {
        this.pEstadoAceptacion = pEstadoAceptacion;
    }

    public String getpFechaRegistro() {
        return pFechaRegistro;
    }

    public void setpFechaRegistro(String pFechaRegistro) {
        this.pFechaRegistro = pFechaRegistro;
    }

    public String getpCondicionVentaDesc() {
        return pCondicionVentaDesc;
    }

    public void setpCondicionVentaDesc(String pCondicionVentaDesc) {
        this.pCondicionVentaDesc = pCondicionVentaDesc;
    }

    public String getpNombreEmpresaCom() {
        return pNombreEmpresaCom;
    }

    public void setpNombreEmpresaCom(String pNombreEmpresaCom) {
        this.pNombreEmpresaCom = pNombreEmpresaCom;
    }

    public String getpFaxEmpresa() {
        return pFaxEmpresa;
    }

    public void setpFaxEmpresa(String pFaxEmpresa) {
        this.pFaxEmpresa = pFaxEmpresa;
    }

    public String getpRazonSocialEmpEmitido() {
        return pRazonSocialEmpEmitido;
    }

    public void setpRazonSocialEmpEmitido(String pRazonSocialEmpEmitido) {
        this.pRazonSocialEmpEmitido = pRazonSocialEmpEmitido;
    }

    public String getpNumIdentificacionEmitido() {
        return pNumIdentificacionEmitido;
    }

    public void setpNumIdentificacionEmitido(String pNumIdentificacionEmitido) {
        this.pNumIdentificacionEmitido = pNumIdentificacionEmitido;
    }

    public String getpCorreoElectronicoEmitido() {
        return pCorreoElectronicoEmitido;
    }

    public void setpCorreoElectronicoEmitido(String pCorreoElectronicoEmitido) {
        this.pCorreoElectronicoEmitido = pCorreoElectronicoEmitido;
    }
    
}
