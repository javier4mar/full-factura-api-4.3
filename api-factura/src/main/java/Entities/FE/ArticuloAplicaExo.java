package Entities.FE;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class ArticuloAplicaExo {

    private String idImpuesto;
    private String codImpuestoHacienda;
    private String descImpuesto;
    private String porcentajeImpuesto;
    private String excepcionImp;
    private String idArticuloEmp;
    private String idCategorias;
    private String descProducto;
    private String idUnidadMedidaHacienda;
    private String unidadMedidaComercial;
    private String tipoArticulo;
    private String precio;
    private String porcentaje;
    private String descuento;
    private String recargo;
    private String estado;

    
    private String codigo;
    private String descCompleta;
    private String costo;

    public String getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(String idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getCodImpuestoHacienda() {
        return codImpuestoHacienda;
    }

    public void setCodImpuestoHacienda(String codImpuestoHacienda) {
        this.codImpuestoHacienda = codImpuestoHacienda;
    }

    public String getDescImpuesto() {
        return descImpuesto;
    }

    public void setDescImpuesto(String descImpuesto) {
        this.descImpuesto = descImpuesto;
    }

    public String getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(String porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public String getExcepcionImp() {
        return excepcionImp;
    }

    public void setExcepcionImp(String excepcionImp) {
        this.excepcionImp = excepcionImp;
    }

    public String getIdArticuloEmp() {
        return idArticuloEmp;
    }

    public void setIdArticuloEmp(String idArticuloEmp) {
        this.idArticuloEmp = idArticuloEmp;
    }

    public String getIdCategorias() {
        return idCategorias;
    }

    public void setIdCategorias(String idCategorias) {
        this.idCategorias = idCategorias;
    }

    public String getDescProducto() {
        return descProducto;
    }

    public void setDescProducto(String descProducto) {
        this.descProducto = descProducto;
    }

    public String getIdUnidadMedidaHacienda() {
        return idUnidadMedidaHacienda;
    }

    public void setIdUnidadMedidaHacienda(String idUnidadMedidaHacienda) {
        this.idUnidadMedidaHacienda = idUnidadMedidaHacienda;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getRecargo() {
        return recargo;
    }

    public void setRecargo(String recargo) {
        this.recargo = recargo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescCompleta() {
        return descCompleta;
    }

    public void setDescCompleta(String descCompleta) {
        this.descCompleta = descCompleta;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getUnidadMedidaComercial() {
        return unidadMedidaComercial;
    }

    public void setUnidadMedidaComercial(String unidadMedidaComercial) {
        this.unidadMedidaComercial = unidadMedidaComercial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

   
}
