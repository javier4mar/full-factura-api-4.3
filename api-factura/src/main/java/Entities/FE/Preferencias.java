/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class Preferencias {
    private String idSucursalEmp;
    private String idPdvEmp;
    private String idPagina;
    private String idClienteEmp;
    private String idMedioPagoEmp;
    private String idMonedaEmp;
    private String idCondicionVentaEmp;
    private String idCodigoMedidaEmp;
    private String idPreferencia;
    private String actualizarPrecios;
    private String mostrarFactura;
    private String notificarEmisor;

    public String getIdSucursalEmp() {
        return idSucursalEmp;
    }

    public void setIdSucursalEmp(String idSucursalEmp) {
        this.idSucursalEmp = idSucursalEmp;
    }

    public String getIdPdvEmp() {
        return idPdvEmp;
    }

    public void setIdPdvEmp(String idPdvEmp) {
        this.idPdvEmp = idPdvEmp;
    }

    public String getIdPagina() {
        return idPagina;
    }

    public void setIdPagina(String idPagina) {
        this.idPagina = idPagina;
    }

    public String getIdClienteEmp() {
        return idClienteEmp;
    }

    public void setIdClienteEmp(String idClienteEmp) {
        this.idClienteEmp = idClienteEmp;
    }

    public String getIdMedioPagoEmp() {
        return idMedioPagoEmp;
    }

    public void setIdMedioPagoEmp(String idMedioPagoEmp) {
        this.idMedioPagoEmp = idMedioPagoEmp;
    }

    public String getIdMonedaEmp() {
        return idMonedaEmp;
    }

    public void setIdMonedaEmp(String idMonedaEmp) {
        this.idMonedaEmp = idMonedaEmp;
    }

    public String getIdCondicionVentaEmp() {
        return idCondicionVentaEmp;
    }

    public void setIdCondicionVentaEmp(String idCondicionVentaEmp) {
        this.idCondicionVentaEmp = idCondicionVentaEmp;
    }

    public String getIdPreferencia() {
        return idPreferencia;
    }

    public void setIdPreferencia(String idPreferencia) {
        this.idPreferencia = idPreferencia;
    }

    public String getIdCodigoMedidaEmp() {
        return idCodigoMedidaEmp;
    }

    public void setIdCodigoMedidaEmp(String idCodigoMedidaEmp) {
        this.idCodigoMedidaEmp = idCodigoMedidaEmp;
    }

    public String getActualizarPrecios() {
        return actualizarPrecios;
    }

    public void setActualizarPrecios(String actualizarPrecios) {
        this.actualizarPrecios = actualizarPrecios;
    }

    public String getMostrarFactura() {
        return mostrarFactura;
    }

    public void setMostrarFactura(String mostrarFactura) {
        this.mostrarFactura = mostrarFactura;
    }

    public String getNotificarEmisor() {
        return notificarEmisor;
    }

    public void setNotificarEmisor(String notificarEmisor) {
        this.notificarEmisor = notificarEmisor;
    }
    
}
