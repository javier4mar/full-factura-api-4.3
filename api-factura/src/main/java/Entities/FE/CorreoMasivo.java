package Entities.FE;

import java.util.List;

public class CorreoMasivo {

    private String tipoBusqueda;
    private String clave;
    private String consecutivo;
    private String fechaInicio;
    private String fechaFin;
    private String tipoComprobante;
    private String estado;
    private String identificacion;
    private String razonSocial;
    private List<String> correos;
    private List<String> consecutivos;

    public CorreoMasivo() {
    }

    public List<String> getConsecutivos() {
        return consecutivos;
    }

    public void setConsecutivos(List<String> consecutivos) {
        this.consecutivos = consecutivos;
    }

    public CorreoMasivo(String tipoBusqueda, String clave, String consecutivo, String fechaInicio, String fechaFin, String tipoComprobante, String estado, String identificacion, String razonSocial, List<Correo> correos) {
        this.tipoBusqueda = tipoBusqueda;
        this.clave = clave;
        this.consecutivo = consecutivo;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.tipoComprobante = tipoComprobante;
        this.estado = estado;
        this.identificacion = identificacion;
        this.razonSocial = razonSocial;

    }

    public String getTipoBusqueda() {
        return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
        this.tipoBusqueda = tipoBusqueda;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public List<String> getCorreos() {
        return correos;
    }

    public void setCorreos(List<String> correos) {
        this.correos = correos;
    }
}
