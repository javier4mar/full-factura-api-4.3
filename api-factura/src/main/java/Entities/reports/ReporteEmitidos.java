package Entities.reports;

import java.math.BigDecimal;

public class ReporteEmitidos {

    private String tipoDescDocumento;
    private String clave;
    private String consecutivo;
    private String cliente;
    private String tipoDescIdentificacion;
    private String identificacion;
    private String correo;
    private String condicionVenta;
    private String plazoCredito;
    private BigDecimal tipoCambio;
    private BigDecimal TotalServiciosGrav;
    private BigDecimal TotalServiciosExen;
    private BigDecimal TotalMercanciasExen;
    private BigDecimal TotalMercanciasGrav;
    private BigDecimal TotalGravado;
    private BigDecimal TotalExento;
    private BigDecimal TotalDescuento;
    private BigDecimal TotalImpuestos;
    private BigDecimal totalComprobante;
    private String moneda;
    private String descEstado;
    private String fechaEmision;
    private String motivoRechazo;


    public ReporteEmitidos() { }


    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    public BigDecimal getTotalServiciosGrav() {
        return TotalServiciosGrav;
    }

    public BigDecimal getTotalServiciosExen() {
        return TotalServiciosExen;
    }

    public BigDecimal getTotalMercanciasExen() {
        return TotalMercanciasExen;
    }

    public BigDecimal getTotalMercanciasGrav() {
        return TotalMercanciasGrav;
    }

    public BigDecimal getTotalGravado() {
        return TotalGravado;
    }

    public BigDecimal getTotalExento() {
        return TotalExento;
    }

    public BigDecimal getTotalDescuento() {
        return TotalDescuento;
    }

    public BigDecimal getTotalImpuestos() {
        return TotalImpuestos;
    }

    public void setTotalServiciosGrav(BigDecimal totalServiciosGrav) {
        TotalServiciosGrav = totalServiciosGrav;
    }

    public void setTotalServiciosExen(BigDecimal totalServiciosExen) {
        TotalServiciosExen = totalServiciosExen;
    }

    public void setTotalMercanciasExen(BigDecimal totalMercanciasExen) {
        TotalMercanciasExen = totalMercanciasExen;
    }

    public void setTotalMercanciasGrav(BigDecimal totalMercanciasGrav) {
        TotalMercanciasGrav = totalMercanciasGrav;
    }

    public void setTotalGravado(BigDecimal totalGravado) {
        TotalGravado = totalGravado;
    }

    public void setTotalExento(BigDecimal totalExento) {
        TotalExento = totalExento;
    }

    public void setTotalDescuento(BigDecimal totalDescuento) {
        TotalDescuento = totalDescuento;
    }

    public void setTotalImpuestos(BigDecimal totalImpuestos) {
        TotalImpuestos = totalImpuestos;
    }

    public BigDecimal getTotalComprobante() {
        return totalComprobante;
    }

    public void setTotalComprobante(BigDecimal totalComprobante) {
        this.totalComprobante = totalComprobante;
    }

    public String getTipoDescDocumento() {
        return tipoDescDocumento;
    }

    public void setTipoDescDocumento(String tipoDescDocumento) {
        this.tipoDescDocumento = tipoDescDocumento;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipoDescIdentificacion() {
        return tipoDescIdentificacion;
    }

    public void setTipoDescIdentificacion(String tipoDescIdentificacion) {
        this.tipoDescIdentificacion = tipoDescIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCondicionVenta() {
        return condicionVenta;
    }

    public void setCondicionVenta(String condicionVenta) {
        this.condicionVenta = condicionVenta;
    }

    public String getPlazoCredito() {
        return plazoCredito;
    }

    public void setPlazoCredito(String plazoCredito) {
        this.plazoCredito = plazoCredito;
    }

    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getDescEstado() {
        return descEstado;
    }

    public void setDescEstado(String descEstado) {
        this.descEstado = descEstado;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}


