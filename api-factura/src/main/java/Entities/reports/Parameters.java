package Entities.reports;

import java.util.List;

public class Parameters {


    private String reportUnitUri;
    private String outputFormat;
    private List<ReportParameter> reportParameter;

    public Parameters(){}

    public Parameters(List<ReportParameter> reportParameter) {
        this.reportParameter = reportParameter;
    }

    public List<ReportParameter> getReportParameter() {
        return reportParameter;
    }

    public void setReportParameter(List<ReportParameter> reportParameter) {
        this.reportParameter = reportParameter;
    }

    public String getReportUnitUri() {
        return reportUnitUri;
    }

    public void setReportUnitUri(String reportUnitUri) {
        this.reportUnitUri = reportUnitUri;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }
}
