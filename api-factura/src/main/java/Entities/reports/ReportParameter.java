package Entities.reports;

import java.util.List;

public class ReportParameter {
    private String name;
    private String value;

    public ReportParameter() {}


    public ReportParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public ReportParameter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != getClass()) {
            result = false;

        }else {
            ReportParameter param = (ReportParameter) obj;
            if (this.name.equals(param.getName())) {
                result = true;
            }
        }
        return result;
    }

    public String findParamByName(List<ReportParameter> params, String search) {
        for(ReportParameter p : params) {
            if(p.getName().equals(search)) {
                return p.getValue();
            }
        }
        return null;
    }


}
