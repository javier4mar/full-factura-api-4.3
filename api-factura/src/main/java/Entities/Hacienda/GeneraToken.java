/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Hacienda;

/**
 *
 * @author msalasch
 */
public class GeneraToken {
    private String pIdEmpresaFe;
    private String pIdUsuario;

    public String getpIdEmpresaFe() {
        return pIdEmpresaFe;
    }

    public void setpIdEmpresaFe(String pIdEmpresaFe) {
        this.pIdEmpresaFe = pIdEmpresaFe;
    }

    public String getpIdUsuario() {
        return pIdUsuario;
    }

    public void setpIdUsuario(String pIdUsuario) {
        this.pIdUsuario = pIdUsuario;
    }
    
}
