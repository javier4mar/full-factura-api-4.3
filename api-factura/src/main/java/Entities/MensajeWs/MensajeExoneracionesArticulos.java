package Entities.MensajeWs;

import Entities.FE.ExoneracionesProducto;
import java.util.List;

/**
 *
 * @author javierhernandez
 */
public class MensajeExoneracionesArticulos extends MensajeBase  {
    private List<ExoneracionesProducto> articulos;

    public List<ExoneracionesProducto> getArticulos() {
        return articulos;
    }

    public void setArticulos(List<ExoneracionesProducto> articulos) {
        this.articulos = articulos;
    }
}
