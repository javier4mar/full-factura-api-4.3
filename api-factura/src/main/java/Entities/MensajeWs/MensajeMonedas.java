/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.Moneda;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeMonedas extends MensajeBase {
    List<Moneda> monedas;

    public List<Moneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<Moneda> monedas) {
        this.monedas = monedas;
    }
    
}
