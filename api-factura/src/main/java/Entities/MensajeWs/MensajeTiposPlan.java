/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.TipoPlan;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeTiposPlan extends MensajeBase {
    private List<TipoPlan> tiposPlan;

    public List<TipoPlan> getTiposPlan() {
        return tiposPlan;
    }

    public void setTiposPlan(List<TipoPlan> tiposPlan) {
        this.tiposPlan = tiposPlan;
    }
    
    
}
