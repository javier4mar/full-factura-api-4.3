package Entities.MensajeWs;

import Entities.Catalogos.Plantilla;

import java.util.List;

public class MensajePlantillas  extends MensajeBase {


    private List<Plantilla> plantilas;

    public List<Plantilla> getPlantilas() {
        return plantilas;
    }

    public void setPlantilas(List<Plantilla> plantilas) {
        this.plantilas = plantilas;
    }
}