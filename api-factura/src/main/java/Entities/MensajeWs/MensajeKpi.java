/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.KpiArtFacturado;
import Entities.FE.KpiClienteFacturado;
import Entities.FE.KpiResumen;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeKpi extends MensajeBase {
    private KpiResumen resumen;
    private List<KpiArtFacturado> articulosMasFac;
    private List<KpiClienteFacturado> clientesMasFac;

    public List<KpiArtFacturado> getArticulosMasFac() {
        return articulosMasFac;
    }

    public void setArticulosMasFac(List<KpiArtFacturado> articulosMasFac) {
        this.articulosMasFac = articulosMasFac;
    }

    public List<KpiClienteFacturado> getClientesMasFac() {
        return clientesMasFac;
    }

    public void setClientesMasFac(List<KpiClienteFacturado> clientesMasFac) {
        this.clientesMasFac = clientesMasFac;
    }

    public KpiResumen getResumen() {
        return resumen;
    }

    public void setResumen(KpiResumen resumen) {
        this.resumen = resumen;
    }

}
