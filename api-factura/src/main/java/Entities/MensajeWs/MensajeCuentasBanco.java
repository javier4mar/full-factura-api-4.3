/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.CuentaBanco;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeCuentasBanco extends MensajeBase {
    private List<CuentaBanco> cuentas;

    public List<CuentaBanco> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<CuentaBanco> cuentas) {
        this.cuentas = cuentas;
    }

}
