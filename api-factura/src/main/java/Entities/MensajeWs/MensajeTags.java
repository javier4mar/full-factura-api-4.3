package Entities.MensajeWs;

import Entities.FE.PlantillaTags;

import java.util.List;

public class MensajeTags extends MensajeBase {

    private List<PlantillaTags> tags;

    public List<PlantillaTags> getPlantilas() {
        return tags;
    }

    public void setPlantilas(List<PlantillaTags> plantilas) {
        this.tags = plantilas;
    }
}

