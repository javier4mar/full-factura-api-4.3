/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.DatosEmpresa;

/**
 *
 * @author msalasch
 */
public class MensajeDatosEmpresa extends MensajeBase  {
    private DatosEmpresa pDatosEmpresa;

    public DatosEmpresa getpDatosEmpresa() {
        return pDatosEmpresa;
    }

    public void setpDatosEmpresa(DatosEmpresa pDatosEmpresa) {
        this.pDatosEmpresa = pDatosEmpresa;
    }
    
}
