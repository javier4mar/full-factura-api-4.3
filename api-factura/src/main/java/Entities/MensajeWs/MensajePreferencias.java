/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Preferencias;

/**
 *
 * @author msalasch
 */
public class MensajePreferencias extends MensajeBase  {
    private Preferencias preferencias;

    public Preferencias getPreferencias() {
        return preferencias;
    }

    public void setPreferencias(Preferencias preferencias) {
        this.preferencias = preferencias;
    }
    
}
