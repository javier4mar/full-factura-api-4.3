/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Articulo;
import java.util.List;

/**
 *
 * @author javierhernandez
 */
public class MensajeInsertArticulo extends MensajeBase {
    
    private String id;
    private Articulo producto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Articulo getProducto() {
        return producto;
    }

    public void setProducto(Articulo producto) {
        this.producto = producto;
    }
    
    
    
}

