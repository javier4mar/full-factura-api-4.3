/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.ArticuloAplicaExo;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeArticulosAplicaExo extends MensajeBase  {
    private List<ArticuloAplicaExo> articulos;

    public List<ArticuloAplicaExo> getArticulos() {
        return articulos;
    }

    public void setArticulos(List<ArticuloAplicaExo> articulos) {
        this.articulos = articulos;
    }
}
