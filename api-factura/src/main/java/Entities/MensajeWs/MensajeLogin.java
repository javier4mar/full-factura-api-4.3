/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.EmpresasUsuario;
import Entities.FE.GruposUsuario;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeLogin  extends MensajeBase{
    private String nombre;
    private String token;
    private String superUsuario;
    private String logo;
    private String fechaVencePago;
    private String idPlan;
    private String idTipoPlan;
    private String logoDistribuidor;
    private String diasPosteriorVence;
    private String diasAntesVence;
    private String diasVenceCert;
    private String fechaVenceCert;
    private List<GruposUsuario> grupos;
    private List<EmpresasUsuario> empresas;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<GruposUsuario> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GruposUsuario> grupos) {
        this.grupos = grupos;
    }

    public List<EmpresasUsuario> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<EmpresasUsuario> empresas) {
        this.empresas = empresas;
    }

    public String getSuperUsuario() {
        return superUsuario;
    }

    public void setSuperUsuario(String superUsuario) {
        this.superUsuario = superUsuario;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFechaVencePago() {
        return fechaVencePago;
    }

    public void setFechaVencePago(String fechaVencePago) {
        this.fechaVencePago = fechaVencePago;
    }

    public String getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    public String getIdTipoPlan() {
        return idTipoPlan;
    }

    public void setIdTipoPlan(String idTipoPlan) {
        this.idTipoPlan = idTipoPlan;
    }

    public String getLogoDistribuidor() {
        return logoDistribuidor;
    }

    public void setLogoDistribuidor(String logoDistribuidor) {
        this.logoDistribuidor = logoDistribuidor;
    }

    public String getDiasPosteriorVence() {
        return diasPosteriorVence;
    }

    public void setDiasPosteriorVence(String diasPosteriorVence) {
        this.diasPosteriorVence = diasPosteriorVence;
    }

    public String getDiasAntesVence() {
        return diasAntesVence;
    }

    public void setDiasAntesVence(String diasAntesVence) {
        this.diasAntesVence = diasAntesVence;
    }

    public String getDiasVenceCert() {
        return diasVenceCert;
    }

    public void setDiasVenceCert(String diasVenceCert) {
        this.diasVenceCert = diasVenceCert;
    }

    public String getFechaVenceCert() {
        return fechaVenceCert;
    }

    public void setFechaVenceCert(String fechaVenceCert) {
        this.fechaVenceCert = fechaVenceCert;
    }

}
