/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.CodDescripcion;
import Entities.Catalogos.DistGeografica;
import Entities.Catalogos.Identificacion;
import Entities.Catalogos.ImpuestoCat;
import Entities.Catalogos.Moneda;
import Entities.FE.IdDescripcion;
import Entities.FE.MediosPago;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeCargaCatalogos extends MensajeBase  {

     List<MediosPago> mediosPago;
     List<ImpuestoCat> impuestos;
     List<Moneda> monedas;
     List<CodDescripcion> unidadesMedida;
     List<CodDescripcion> condicionesVenta;
     List<DistGeografica> distGeografica;
     List<CodDescripcion> niveles;
     List<CodDescripcion> tipoDocumentos;
     List<Identificacion> tiposIdentificacion;
     List<CodDescripcion> tiposCodArticulo;
     List<CodDescripcion> tiposDocRef;
     List<IdDescripcion> estadosDocumento;
     List<IdDescripcion> codMensajeHacienda;
     List<IdDescripcion> situacion;
     List<IdDescripcion> tipoDocumentoDetalle;
     List<IdDescripcion> codDocumentoRef;

    public List<IdDescripcion> getCodDocumentoRef() {
        return codDocumentoRef;
    }

    public void setCodDocumentoRef(List<IdDescripcion> pLista) {
        this.codDocumentoRef = pLista;
    }
    
    public List<IdDescripcion> getTipoDocumentoDetalle() {
        return tipoDocumentoDetalle;
    }

    public void setTipoDocumentoDetalle(List<IdDescripcion> pLista) {
        this.tipoDocumentoDetalle = pLista;
    }
    public List<IdDescripcion> getSituacion() {
        return situacion;
    }

    public void setSituacion(List<IdDescripcion> pLista) {
        this.situacion = pLista;
    }
    
    public List<IdDescripcion> getMensajeHacienda() {
        return codMensajeHacienda;
    }

    public void setMensajeHacienda(List<IdDescripcion> pLista) {
        this.codMensajeHacienda = pLista;
    }
    
    public List<IdDescripcion> getEstadoDocumento() {
        return estadosDocumento;
    }

    public void setEstadoDocumento(List<IdDescripcion> pLista) {
        this.estadosDocumento = pLista;
    }
    public List<CodDescripcion> getTiposDocRef() {
        return tiposDocRef;
    }

    public void setTiposDocRef(List<CodDescripcion> tipos) {
        this.tiposDocRef = tipos;
    }

    
    public List<CodDescripcion> getTipoCodigoArticulo() {
        return tiposCodArticulo;
    }

    public void setTipoCodigoArticulo(List<CodDescripcion> tipos) {
        this.tiposCodArticulo = tipos;
    }
    
    public List<Identificacion> getTiposIden() {
        return tiposIdentificacion;
    }

    public void setTiposIden(List<Identificacion> tipos) {
        this.tiposIdentificacion = tipos;
    }
     
    public List<CodDescripcion> getTipos() {
        return tipoDocumentos;
    }

    public void setTipos(List<CodDescripcion> tipos) {
        this.tipoDocumentos = tipos;
    }
    
    public List<DistGeografica> getDistGeografica() {
        return distGeografica;
    }

    public void setDistGeografica(List<DistGeografica> distGeografica) {
        this.distGeografica = distGeografica;
    }

    public List<CodDescripcion> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<CodDescripcion> niveles) {
        this.niveles = niveles;
    }
    
    public List<MediosPago> getMediosPago() {
        return mediosPago;
    }

    public void setMediosPago(List<MediosPago> mediosPago) {
        this.mediosPago = mediosPago;
    }
    

    public List<ImpuestoCat> getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(List<ImpuestoCat> impuestos) {
        this.impuestos = impuestos;
    }
    

    public List<Moneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<Moneda> monedas) {
        this.monedas = monedas;
    }
    

   

    public List<CodDescripcion> getUnidadesMedida() {
        return unidadesMedida;
    }

    public void setUnidadesMedida(List<CodDescripcion> unidadesMedida) {
        this.unidadesMedida = unidadesMedida;
    }
    

    public List<CodDescripcion> getCondicionesVenta() {
        return condicionesVenta;
    }

    public void setCondicionesVenta(List<CodDescripcion> condicionesVenta) {
        this.condicionesVenta = condicionesVenta;
    }
}
