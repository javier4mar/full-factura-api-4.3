/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

/**
 *
 * @author Administrador
 */
public class MensajeBase {
    private String Status;
    private String Mensaje;


    public MensajeBase( ){ }


    public MensajeBase(String status, String mensaje) {
        Status = status;
        Mensaje = mensaje;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String Mensaje) {
        this.Mensaje = Mensaje;
    }

    
}
