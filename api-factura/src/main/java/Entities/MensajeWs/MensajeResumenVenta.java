/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.ResumenVenta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeResumenVenta extends MensajeBase  {
    
    List<ResumenVenta> mVenta = new ArrayList<>();

    public List<ResumenVenta> getmVenta() {
        return mVenta;
    }

    public void setmVenta(List<ResumenVenta> mVenta) {
        this.mVenta = mVenta;
    }
    
    
}
