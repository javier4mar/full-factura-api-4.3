/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Usr;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeUsr extends MensajeBase {
    private List<Usr> usuarios; 

    public List<Usr> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usr> usuarios) {
        this.usuarios = usuarios;
    }
    
    
}
