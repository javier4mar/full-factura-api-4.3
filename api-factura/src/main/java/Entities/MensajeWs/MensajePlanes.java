/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Plan;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajePlanes extends MensajeBase {
    private List<Plan> planes;

    public List<Plan> getPlanes() {
        return planes;
    }

    public void setPlanes(List<Plan> planes) {
        this.planes = planes;
    }
    
    
    
}
