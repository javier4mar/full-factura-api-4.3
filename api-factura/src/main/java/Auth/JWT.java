package Auth;

import Entities.FE.DatosOlvideClave;
import Entities.FE.EmisorToken;
import Entities.FE.TokenCorreo;
import com.auth0.jwt.Algorithm;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

public class JWT {

    //Sha512($Web$Service$Recarga$) 
    public static String SECRET_UID_KEY = "da87df12090d915bf0d65d2c83f4669547898aaeb328e92bfe5d5fc794fa18e5ea3170f005fa55db3aad135208a13b3bd58fa2d6dc3d79982e49d419f2f3eb63";

    private static String SECRET_KEY = "q61HAH7SVLT3Tk8SuPhdegUH2blmC1mT3zU42/lI4F7SWY1CM6Y+6Ahqp+G1qOejEuVOcf+L5R8uCgXQyNGctQ==";
    
    private static String HASH_KEY = "2e42ddd86fb091a05ab1c99cccb7281e";

    public static String generateToken(String idEmpresaFe, String idEmpresa, String razonSocial, String nombreComercial, String correoAdmFe, String usuarioHacienda, String claveHacienda, String rutaCertificado, String claveCertificado, String tieneHomogen, String callBack, String tipoIdenEmisor, String idenEmisor, String idUsuario, int tokenExp) {
       
        final JWTSigner signer = new JWTSigner(SECRET_KEY);
        final JWTSigner.Options jwtOption = new JWTSigner.Options();
        //Time Out Token
        if(tokenExp != 0){
          jwtOption.setExpirySeconds(tokenExp*60*60);
        }
        jwtOption.setAlgorithm(Algorithm.HS512);

        final HashMap<String, Object> claims = new HashMap<>();
       
        claims.put("idEmpresaFe", idEmpresaFe);
        claims.put("idEmpresa", idEmpresa);
        claims.put("razonSocial", razonSocial);
        claims.put("nombreComercial", nombreComercial);
        claims.put("correoAdmFe", correoAdmFe);
        claims.put("usuarioHacienda", usuarioHacienda);
        claims.put("claveHacienda", claveHacienda);
        claims.put("rutaCertificado", rutaCertificado);
        claims.put("claveCertificado", claveCertificado);
        claims.put("tieneHomogen", tieneHomogen);
        claims.put("callBack", callBack);
        claims.put("tipoIdenEmisor", tipoIdenEmisor);
        claims.put("idenEmisor", idenEmisor);
        claims.put("idUsuario", idUsuario);
        claims.put("tokenExp", tokenExp);

        String jwt = "!" + signer.sign(claims, jwtOption);

        jwt = jwt.replace(".", "//");

        return jwt;
    }

    public static EmisorToken verifyToken(String jwt) {

        try {
          
            final JWTVerifier verifier = new JWTVerifier(SECRET_KEY);
            final Map<String, Object> claims = verifier.verify(cleanToken(jwt));

            EmisorToken jwtClaims = new EmisorToken();

            jwtClaims.setIdEmpresaFe((String) claims.get("idEmpresaFe"));
            jwtClaims.setIdEmpresa((String) claims.get("idEmpresa"));
            jwtClaims.setRazonSocial((String) claims.get("razonSocial"));
            jwtClaims.setNombreComercial((String) claims.get("nombreComercial"));
            jwtClaims.setCorreoAdmFe((String) claims.get("correoAdmFe"));
            jwtClaims.setUsuarioHacienda((String) claims.get("usuarioHacienda"));
            jwtClaims.setClaveHacienda((String) claims.get("claveHacienda"));
            jwtClaims.setRutaCertificado((String) claims.get("rutaCertificado"));
            jwtClaims.setPasswordCertificado((String) claims.get("claveCertificado"));
            jwtClaims.setTieneHomogen((String) claims.get("tieneHomogen"));
            jwtClaims.setCallBack((String) claims.get("callBack"));
            jwtClaims.setTipoIdentificacion((String) claims.get("tipoIdenEmisor"));
            jwtClaims.setIdentificacion((String) claims.get("idenEmisor"));
            jwtClaims.setIdUsuario((String) claims.get("idUsuario"));
            jwtClaims.setTokenExp((Integer) claims.get("tokenExp"));
            
            return jwtClaims;

        } catch (JWTVerifyException e) {
            // Invalid Token
            return null;

        } catch (NoSuchAlgorithmException | InvalidKeyException | IllegalStateException | IOException | SignatureException e) {
            // Invalid Token
            return null;

        }
    }
    
    public static String generateHashCorreo(String idUsuario, int tokenExp) {
       
        final JWTSigner signer = new JWTSigner(HASH_KEY);
        final JWTSigner.Options jwtOption = new JWTSigner.Options();
        //Time Out Token
        if(tokenExp != 0){
          jwtOption.setExpirySeconds(tokenExp*60*60);
        }
        jwtOption.setAlgorithm(Algorithm.HS256);

        final HashMap<String, Object> claims = new HashMap<>();
       
        claims.put("idUsuario", idUsuario);
        claims.put("tokenExp", tokenExp);

        //String jwt = "!" + signer.sign(claims, jwtOption);

        //jwt = jwt.replace(".", "//");

        String jwt = signer.sign(claims, jwtOption);
        
        return jwt;
    }

    public static TokenCorreo verifyHashCorreo(String jwt) {

        try {
          
            final JWTVerifier verifier = new JWTVerifier(HASH_KEY);
            final Map<String, Object> claims = verifier.verify(jwt);

            TokenCorreo jwtClaims = new TokenCorreo();

            jwtClaims.setpIdUsuario((String) claims.get("idUsuario"));
            jwtClaims.setpTokenExp((Integer) claims.get("tokenExp"));
            
            return jwtClaims;

        } catch (JWTVerifyException e) {
            // Invalid Token
            return null;

        } catch (NoSuchAlgorithmException | InvalidKeyException | IllegalStateException | IOException | SignatureException e) {
            // Invalid Token
            return null;

        }
    }

    public static String cleanToken(String jwt) {

        String filterToken = jwt.substring(1);
        filterToken = filterToken.replace("//", ".");
        return filterToken;
    }
}
