package DBConnection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author Javier Hernandez
 */
public class HikariConnection {

    private static final String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    private static final String DB_NAME = "FE";

    /////Produccion
//    private static final String DB_URL = "jdbc:mysql://104.236.180.177/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "remotefebd";
//    private static final String DB_PASSWORD = "Us3rR3moFeGt3Ccr@DB3*";
    ////// Desarrollo
//    private static final String DB_URL = "jdbc:mysql://138.197.14.30/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "febdtest";
//    private static final String DB_PASSWORD = "fA3L3CTRoNIC@TEST2*";
    ////// Desarrollo Terceros
//    private static final String DB_URL = "jdbc:mysql://localhost/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "remotefe";
//    private static final String DB_PASSWORD = "f3-p0s-r3m0t3-Bd*!";
    ////// Pollero FE
//    private static final String DB_URL = "jdbc:mysql://localhost:3306/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "febdtest";
//    private static final String DB_PASSWORD = "S1f@m-f]3-@?p)|DB*1";

    ////// Cafe FE
//    private static final String DB_URL = "jdbc:mysql://10.132.130.35:3306/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "sifamfebd";
//    private static final String DB_PASSWORD = "S1f@m-f]3-@?p)|DB*2";
    ////// VNET
// private static final String DB_URL = "jdbc:mysql://localhost:3306/" + DB_NAME + "?useSSL=false";
//    private static final String DB_USER = "febdtest";
//    private static final String DB_PASSWORD = "S1f@m-f]3-+n3t|DB1*";
    //DELTA
    private static final String DB_URL = "jdbc:mysql://192.168.0.53:3306/" + DB_NAME + "?useSSL=false";
    //private static final String DB_URL = "jdbc:mysql://201.224.73.233:3306/" + DB_NAME + "?useSSL=false";
    private static final String DB_USER = "deltaremotedb";
    private static final String DB_PASSWORD = "#Fu11_F4+My5Q|r";
    
    private static final int CONN_POOL_SIZE = 50;
    private static final int CONN_POOL_SIZE_MIN = 20;

    private static DataSource datasource;

    public static DataSource getDataSource() {

        if (datasource == null) {

            try {

                HikariConfig config = new HikariConfig();

                config.setDriverClassName(DRIVER_CLASS_NAME);
                config.setJdbcUrl(DB_URL);

                config.setUsername(DB_USER);
                config.setPassword(DB_PASSWORD);

                config.setMinimumIdle(CONN_POOL_SIZE_MIN);
                config.setMaximumPoolSize(CONN_POOL_SIZE);
                config.setAutoCommit(true);

                config.addDataSourceProperty("cachePrepStmts", "true");
                config.addDataSourceProperty("prepStmtCacheSize", "250");
                config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
                config.addDataSourceProperty("useServerPrepStmts", "true");
                config.addDataSourceProperty("rewriteBatchedStatements", "true");
                config.addDataSourceProperty("cacheResultSetMetadata", "true");
                config.addDataSourceProperty("cacheServerConfiguration", "true");
                config.addDataSourceProperty("maintainTimeStats", "false");
                config.addDataSourceProperty("characterEncoding", "UTF-8");
                config.addDataSourceProperty("useUnicode", "true");
                config.addDataSourceProperty("useSSL", "false");
                config.addDataSourceProperty("loglevel", "debug");

                config.setIdleTimeout(30000);

                config.setLeakDetectionThreshold(60000);

                datasource = new HikariDataSource(config);

            } catch (Exception e) {
                e.printStackTrace();
                // exception handling here
            }
        }

        return datasource;
    }
}
