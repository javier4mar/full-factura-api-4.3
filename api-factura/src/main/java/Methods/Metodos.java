package Methods;

import Auth.JWT;
import Constants.Constantes;
import Constants.Querys;
import DBConnection.HikariConnection;
import Entities.Catalogos.*;
import Entities.FE.*;
import Entities.Hacienda.Callback;
import Entities.Hacienda.CheckHacienda;
import Entities.Hacienda.GeneraToken;
import Entities.Hacienda.SendHacienda;
import Entities.Hacienda.Token;
import Entities.MensajeWs.*;
import Entities.reports.ReporteEmitidos;
import Entities.reports.ReporteRecibidos;
import Signature.FirmaDocumento;
import Signature.Pkcs12Util;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.mail.smtp.SMTPTransport;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import javax.sql.DataSource;
import java.util.Base64;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.zeroturnaround.zip.ZipUtil;

public class Metodos implements IMetodos {

    private final DataSource dataSource;

    public Metodos() {
        dataSource = HikariConnection.getDataSource();
    }

    @Override
    public String getToken(GeneraToken data) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        EmisorToken emisorToken = null;
        String token = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DATOS_TOKEN);
            pstmt.setInt(1, Integer.valueOf(data.getpIdEmpresaFe()));

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                emisorToken = new EmisorToken();

                emisorToken.setIdEmpresaFe(resultSet.getString(1));
                emisorToken.setIdEmpresa(resultSet.getString(2));
                emisorToken.setRazonSocial(resultSet.getString(3));
                emisorToken.setNombreComercial(resultSet.getString(4));
                emisorToken.setCorreoAdmFe(resultSet.getString(5));
                emisorToken.setUsuarioHacienda(resultSet.getString(6));
                emisorToken.setClaveHacienda(resultSet.getString(7));
                emisorToken.setTieneHomogen(resultSet.getString(8));
                emisorToken.setTokenExp(resultSet.getInt(9));
                emisorToken.setRutaCertificado(resultSet.getString(10));
                emisorToken.setPasswordCertificado(resultSet.getString(11));
                emisorToken.setCallBack(resultSet.getString(12));
                emisorToken.setTipoIdentificacion(resultSet.getString(13));
                emisorToken.setIdentificacion(resultSet.getString(14));
                emisorToken.setIdUsuario(data.getpIdUsuario());
            }

            if (emisorToken != null) {
                token = JWT.generateToken(emisorToken.getIdEmpresaFe(), emisorToken.getIdEmpresa(), emisorToken.getRazonSocial(), emisorToken.getNombreComercial(), emisorToken.getCorreoAdmFe(), emisorToken.getUsuarioHacienda(), emisorToken.getClaveHacienda(), emisorToken.getRutaCertificado(), emisorToken.getPasswordCertificado(), emisorToken.getTieneHomogen(), emisorToken.getCallBack(), emisorToken.getTipoIdentificacion(), emisorToken.getIdentificacion(), emisorToken.getIdUsuario(), emisorToken.getTokenExp());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return token;
    }

    public String getClave(String pIdEmpresa, String pIdDocumento) throws Exception {
        String clave = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.CLAVE_DOCUMENTO);
            pstmt.setString(1, pIdEmpresa);
            pstmt.setString(2, pIdDocumento);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                clave = resultSet.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return clave;
    }

    public String getTipoDocumento(String pIdEmpresa, String pTipoDocumentoEmp) throws Exception {
        String tipoDoc = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.TIPO_DOCUMENTO);
            pstmt.setString(1, pIdEmpresa);
            pstmt.setString(2, pTipoDocumentoEmp);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tipoDoc = resultSet.getString(1);
            }

        } catch (Exception e) {
            closeDaoResources(resultSet, pstmt, connection);

            return tipoDoc;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return tipoDoc;
    }

    public String getRutaXmlBase() throws Exception {
        String ruta = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_VALOR1_PARAMETROS);
            pstmt.setString(1, Constantes.ID_PARAMETRO_XML_BASE);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                ruta = resultSet.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return ruta;
    }

    @Override
    public MensajeBase enviarDocumentoHacienda(EmisorToken pEmisorToken, String pIdDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeEnvio = new MensajeBase();
        FirmaDocumento firma = new FirmaDocumento();
        String xml;
        String xmlFirmado = null;
        String clave = "";
        APIHacienda apiHacienda = new APIHacienda();

        try {

            String tokenHacienda = getTokenHaciendaValido(pEmisorToken.getIdEmpresaFe(), pEmisorToken.getUsuarioHacienda(), pEmisorToken.getClaveHacienda(), apiHacienda);

            if (tokenHacienda == null) {
                mensajeEnvio.setStatus(Constantes.statusError);
                mensajeEnvio.setMensaje("Error al obtener el token");
                return mensajeEnvio;
            }

            //Se obtiene el documento de DB
            xml = getXml(pIdDocumento);

            //Se firma el documento
            xmlFirmado = firma.firmarXML(pEmisorToken.getRutaCertificado(), pEmisorToken.getPasswordCertificado(), xml);

            if (xmlFirmado == null) {
                mensajeEnvio.setStatus(Constantes.statusError);
                mensajeEnvio.setMensaje("Error firmando el documento");
                return mensajeEnvio;
            }

            //Se convierte el XML a Base64 para enviar a Hacienda
            final byte[] authBytes = xmlFirmado.getBytes(StandardCharsets.UTF_8);
            final String encoded = Base64.getEncoder().encodeToString(authBytes);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

            //Metodo para obtener la clave, la cual se envia a hacienda
            String[] array;
            array = xml.split("<Clave>");
            array = array[1].split("</Clave>");
            clave = array[0];

            //Se guarda documento firmado en repositorio
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            String rutaArchivo = guardarDocumentoFisico(pEmisorToken.getIdEmpresaFe(), xmlFirmado, clave, "1", sdf2.format(new Date()));

            SendHacienda respuestaEnvio = apiHacienda.sendXML(tokenHacienda, clave, sdf.format(new Date()), pEmisorToken.getTipoIdentificacion(), pEmisorToken.getIdentificacion(), pEmisorToken.getCallBack(), encoded);
            if (respuestaEnvio.isDocumentoEnviado()) {
                mensajeEnvio.setStatus(Constantes.statusSuccess);

                actualizaRutaDocumento(pIdDocumento, rutaArchivo);
            } else {
                mensajeEnvio.setStatus(Constantes.statusError);
            }

            mensajeEnvio.setMensaje(respuestaEnvio.getMensaje());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return mensajeEnvio;
    }

    public boolean actualizaRutaDocumento(String pIdDocumento, String pRuta) throws SQLException {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ACTUALIZA_RUTA_DOC);
            pstmt.setString(1, pRuta);
            pstmt.setString(2, pIdDocumento);

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            closeDaoResources(resultSet, pstmt, connection);

            return resultado;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public MensajeConsultaHacienda consultarDocumentoHacienda(EmisorToken pEmisorToken, String pIdDocumento) throws Exception {
        MensajeConsultaHacienda mensajeConsulta = new MensajeConsultaHacienda();
        APIHacienda apiHacienda = new APIHacienda();

        try {
            String clave = getClave(pEmisorToken.getIdEmpresaFe(), pIdDocumento);
            IdDocIdEmpresa datos = getIdDocIdEmpresaFe(clave);

            if (clave == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("No se ha encontrado el documento");
                return mensajeConsulta;
            }

            String tokenHacienda = getTokenHaciendaValido(pEmisorToken.getIdEmpresaFe(), pEmisorToken.getUsuarioHacienda(), pEmisorToken.getClaveHacienda(), apiHacienda);

            if (tokenHacienda == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Error al obtener el token");
                return mensajeConsulta;
            }

            String apiConsulta = Constantes.apiHaciendaSend + "/" + clave;

            CheckHacienda checkHacienda = apiHacienda.checkXML(tokenHacienda, apiConsulta);

            if (checkHacienda == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Error al consultar el documento");
            } else if (checkHacienda.getInd_estado().equals("rechazado")) {
                byte[] decoded = Base64.getDecoder().decode(checkHacienda.getRespuesta_xml());
                String respuesta = new String(decoded);

                guardarDocumentoFisico(pEmisorToken.getIdEmpresaFe(), respuesta, clave + "R", "1", datos.getFechaEmision());

                //Metodo para obtener el detalle de error de hacienda
                String mensaje = "";
                String[] array;
                array = respuesta.split("<DetalleMensaje>");
                array = array[1].split("</DetalleMensaje>");
                mensaje = array[0];
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setIndicadorHacienda("1");
                mensajeConsulta.setMensaje(mensaje);

            } else if (checkHacienda.getInd_estado().equals("aceptado")) {
                byte[] decoded = Base64.getDecoder().decode(checkHacienda.getRespuesta_xml());
                String respuesta = new String(decoded);

                guardarDocumentoFisico(pEmisorToken.getIdEmpresaFe(), respuesta, clave + "R", "1", datos.getFechaEmision());

                mensajeConsulta.setStatus(Constantes.statusSuccess);
                mensajeConsulta.setIndicadorHacienda("1");
                mensajeConsulta.setMensaje("Comprobante tramitado exitosamente");
            }

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            return mensajeConsulta;
        }

        return mensajeConsulta;
    }

    @Override
    public MensajeConsultaXml consultarXml(EmisorToken pEmisorToken, String pIdDocumento, String pTipo) throws Exception {
        MensajeConsultaXml mensajeConsulta = new MensajeConsultaXml();
        String rutaArchivo;

        try {
            RutaXml datos = getRutaXml(pEmisorToken.getIdEmpresaFe(), pIdDocumento);

            if (datos == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("No se ha encontrado la ruta del documento");
                return mensajeConsulta;
            }

            if (pTipo.equals("0")) {
                rutaArchivo = datos.getRuta() + datos.getClave() + ".xml";
            } else {
                rutaArchivo = datos.getRuta() + datos.getClave() + "R.xml";
            }

            File fXmlFile = new File(rutaArchivo);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta exitosa");
            mensajeConsulta.setXml(sw.toString());

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            return mensajeConsulta;
        }

        return mensajeConsulta;
    }

    public RutaXml getRutaXml(String pIdEmpresa, String pIdDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        RutaXml datos = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.RUTA_XML);
            pstmt.setString(1, pIdEmpresa);
            pstmt.setString(2, pIdDocumento);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                datos = new RutaXml();

                datos.setRuta(resultSet.getString(1));
                datos.setClave(resultSet.getString(2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return datos;
    }

    @Override
    public void callBack(Callback pRespuesta) throws Exception {

        try {
            if (pRespuesta != null) {

                IdDocIdEmpresa datos = getIdDocIdEmpresaFe(pRespuesta.getClave());

                byte[] decoded = Base64.getDecoder().decode(pRespuesta.getRespuesta_xml());
                String respuesta = new String(decoded);

                String estado = "";
                String[] arrayEstado;
                arrayEstado = respuesta.split("<Mensaje>");
                arrayEstado = arrayEstado[1].split("</Mensaje>");
                estado = arrayEstado[0];

                String mensaje = "";
                String[] array;
                array = respuesta.split("<DetalleMensaje>");
                array = array[1].split("</DetalleMensaje>");
                mensaje = array[0];

                if (estado.equals("3")) {
                    //XML Rechazado
                    actualizaEstadoDocumento(datos.getIdDocumento(), Constantes.ESTADO_DOC_RECHAZADO, mensaje);
                } else if (estado.equals("1") || estado.equals("2")) {
                    //XML Aceptado - XML Aceptado Parcialmente
                    actualizaEstadoDocumento(datos.getIdDocumento(), Constantes.ESTADO_DOC_APROBADO, mensaje);
                } else {
                    //XML sin estado
                    actualizaEstadoDocumento(datos.getIdDocumento(), Constantes.ESTADO_DOC_ENVIADO_TRIBUTACION, mensaje);
                }

                guardarDocumentoFisico(datos.getIdEmpresaFe(), respuesta, pRespuesta.getClave() + "R", datos.getEmitida(), datos.getFechaEmision());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IdDocIdEmpresa getIdDocIdEmpresaFe(String pClave) throws Exception {
        IdDocIdEmpresa datos = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ID_DOC_EMPRESA_FE);
            pstmt.setString(1, pClave);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                datos = new IdDocIdEmpresa();
                datos.setIdEmpresaFe(resultSet.getString(1));
                datos.setIdDocumento(resultSet.getString(2));
                datos.setEmitida(resultSet.getString(3));
                datos.setFechaEmision(resultSet.getString(4));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return datos;
    }

    public boolean actualizaEstadoDocumento(String pIdDocumento, String pEstado, String pMotivoRechazo) throws SQLException {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ACTUALIZA_ESTADO_DOC);
            pstmt.setString(1, pEstado);
            pstmt.setString(2, pMotivoRechazo);
            pstmt.setString(3, pIdDocumento);

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            closeDaoResources(resultSet, pstmt, connection);

            return resultado;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    public String getTokenHaciendaValido(String pIdEmpresa, String pUsuarioHacienda, String pPasswordHacienda, APIHacienda apiHacienda) throws Exception {
        TokenHacienda tokenHaciendaDb = new TokenHacienda();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        String vtoken = null;
        Date fechaExpira;
        String segundosExpira;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.TOKEN_HACIENDA);
            pstmt.setString(1, pIdEmpresa);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tokenHaciendaDb.setToken(resultSet.getString(1));
                tokenHaciendaDb.setExpireDate(resultSet.getString(2));
            }

            if (tokenHaciendaDb.getExpireDate() == null || tokenHaciendaDb.getExpireDate().equals("")) {

                Token token = apiHacienda.getToken(pUsuarioHacienda, pPasswordHacienda);

                if (token != null) {
                    vtoken = token.getAccess_token();
                    segundosExpira = token.getExpires_in();

                    actualizaFechaToken(pIdEmpresa, vtoken, segundosExpira);
                }

            } else {
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                fechaExpira = inputFormat.parse(tokenHaciendaDb.getExpireDate());

                if (fechaExpira.before(new Date())) {

                    Token token = apiHacienda.getToken(pUsuarioHacienda, pPasswordHacienda);

                    if (token != null) {
                        vtoken = token.getAccess_token();

                        segundosExpira = token.getExpires_in();

                        actualizaFechaToken(pIdEmpresa, vtoken, segundosExpira);
                    }
                } else {
                    vtoken = tokenHaciendaDb.getToken();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return vtoken;
    }

    public String guardarDocumentoFisico(String pIdEmpresaFe, String pXml, String pNombreArchivo, String pEmite, String fechaEmision) throws Exception {
        String fileLocation = null;

        try {

            String rutaBase = getRutaXmlBase();
            String caracterSeparador = getCaracterSeparadorCarpeta();

            if (pEmite.equals("1")) {
                fileLocation = rutaBase + pIdEmpresaFe + caracterSeparador + "Emitidos" + caracterSeparador + fechaEmision + caracterSeparador;
            } else {
                fileLocation = rutaBase + pIdEmpresaFe + caracterSeparador + "Recibidos" + caracterSeparador + fechaEmision + caracterSeparador;
            }

            new File(fileLocation).mkdirs();

            final File file = new File(fileLocation + pNombreArchivo + ".xml");
            file.setReadable(true, false);
            file.setExecutable(true, false);
            file.setWritable(true, false);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(pXml.getBytes(StandardCharsets.UTF_8)));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(file);
            Source input = new DOMSource(doc);

            transformer.transform(input, output);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileLocation;
    }

    public void actualizaFechaToken(String pIdEmpresa, String pToken, String pSegundos) throws SQLException {
        Connection connection = null;
        PreparedStatement pstmt = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.SECOND, Integer.valueOf(pSegundos));

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.UPDATE_TOKEN);
            pstmt.setString(1, pToken);
            pstmt.setString(2, simpleDateFormat.format(calendar.getTime()));
            pstmt.setString(3, pIdEmpresa);

            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(null, pstmt, connection);
        }
    }

    public String getXml(String pIdDocumento) throws SQLException {

        Connection connection = null;
        CallableStatement cstmt = null;
        String vlc_Resultado = "";

        try {

            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.GET_XML);

            cstmt.setString(1, pIdDocumento);

            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);

            cstmt.execute();

            vlc_Resultado = cstmt.getString(2);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(null, cstmt, connection);
        }

        return vlc_Resultado;
    }

    public static void closeDaoResources(ResultSet rs, Statement stmt, Connection con) throws SQLException {
        try {
            try {
                if (rs != null) {
                    rs.close();
                }
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public MensajeUnidadesMedida getUnidadesMedida(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeUnidadesMedida mensajeConsulta = new MensajeUnidadesMedida();
        List<CodDescripcion> unidades = new ArrayList<>();
        CodDescripcion unidad = null;
        String unidadMedidaEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_UNIDADES_MEDIDA);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                unidad = new CodDescripcion();

                unidad.setCodigo(resultSet.getString(1));
                unidad.setDescripcion(resultSet.getString(2));
                unidadMedidaEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (unidadMedidaEmp != null) {
                    String[] parts = unidadMedidaEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                unidad.setCodigosEmp(codigosEmp);

                unidades.add(unidad);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setUnidadesMedida(unidades);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCondicionesVenta getCondicionesVenta(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCondicionesVenta mensajeConsulta = new MensajeCondicionesVenta();
        List<CodDescripcion> condiciones = new ArrayList<>();
        CodDescripcion condicion = null;
        String condicionVentaEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CONDICIONES_VENTA);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                condicion = new CodDescripcion();

                condicion.setCodigo(resultSet.getString(1));
                condicion.setDescripcion(resultSet.getString(2));
                condicionVentaEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (condicionVentaEmp != null) {
                    String[] parts = condicionVentaEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                condicion.setCodigosEmp(codigosEmp);

                condiciones.add(condicion);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setCondicionesVenta(condiciones);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeDistGeografica getDistGeografica(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<DistGeografica> distribuciones = new ArrayList<>();
        DistGeografica distribucion;
        MensajeDistGeografica mensajeConsulta = new MensajeDistGeografica();
        String distGeoEmp;
        String distGeoEmpPertenece;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DIST_GEO);

            pstmt.setString(1, pToken.getIdEmpresaFe());
            pstmt.setString(2, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                distribucion = new DistGeografica();

                distribucion.setIdDistGeo(resultSet.getString(1));
                distribucion.setNivel(resultSet.getString(2));
                distribucion.setNombre(resultSet.getString(3));
                distribucion.setCodigo(resultSet.getString(4));
                distGeoEmpPertenece = resultSet.getString(5);
                distGeoEmp = resultSet.getString(6);
                distribucion.setIdDistGeoPertenece(resultSet.getString(7));

                List<String> codigosEmp = new ArrayList<>();

                if (distGeoEmp != null) {
                    String[] parts = distGeoEmp.split(",");

                    codigosEmp = Arrays.asList(parts);
                }

                distribucion.setCodigosEmp(codigosEmp);

                List<String> codigosPerteneceEmp = new ArrayList<>();

                if (distGeoEmpPertenece != null) {
                    String[] parts = distGeoEmpPertenece.split(",");

                    codigosPerteneceEmp = Arrays.asList(parts);
                }

                distribucion.setCodigosPerteneceEmp(codigosPerteneceEmp);

                distribuciones.add(distribucion);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setDistGeografica(distribuciones);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeMediosPago getMediosPago(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<MediosPago> medios = new ArrayList<>();
        MediosPago medio;
        MensajeMediosPago mensajeConsulta = new MensajeMediosPago();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_MEDIOS_PAGO);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                medio = new MediosPago();

                medio.setCodigo(resultSet.getString(1));
                medio.setDescripcion(resultSet.getString(2));
                codigoEmp = resultSet.getString(3);
                medio.setSolicitaComprobante(resultSet.getString(4));

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                medio.setCodigosEmp(codigosEmp);

                medios.add(medio);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setMediosPago(medios);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeImpuestos getImpuestos(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<ImpuestoCat> impuestos = new ArrayList<>();
        ImpuestoCat impuesto = null;
        MensajeImpuestos mensajeConsulta = new MensajeImpuestos();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_IMPUESTOS);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                impuesto = new ImpuestoCat();

                impuesto.setCodigo(resultSet.getString(1));
                impuesto.setDescripcion(resultSet.getString(2));
                impuesto.setPorcentajeImp(resultSet.getString(3));
                impuesto.setExcepcion(resultSet.getString(4));
                codigoEmp = resultSet.getString(5);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                impuesto.setCodigosEmp(codigosEmp);

                impuestos.add(impuesto);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setImpuestos(impuestos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeMonedas getMonedas(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Moneda> monedas = new ArrayList<>();
        Moneda moneda;
        MensajeMonedas mensajeConsulta = new MensajeMonedas();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_MONEDAS);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                moneda = new Moneda();

                moneda.setCodigo(resultSet.getString(1));
                moneda.setDescripcion(resultSet.getString(2));
                moneda.setSimbolo(resultSet.getString(3));
                codigoEmp = resultSet.getString(4);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                moneda.setCodigosEmp(codigosEmp);

                monedas.add(moneda);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setMonedas(monedas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTipos getTiposDocumento(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<CodDescripcion> tipos = new ArrayList<>();
        CodDescripcion tipo;
        MensajeTipos mensajeConsulta = new MensajeTipos();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_DOCUMENTO);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tipo = new CodDescripcion();

                tipo.setCodigo(resultSet.getString(1));
                tipo.setDescripcion(resultSet.getString(2));
                codigoEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                tipo.setCodigosEmp(codigosEmp);

                tipos.add(tipo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTipos(tipos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTipos getTiposExoneracion(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<CodDescripcion> tipos = new ArrayList<>();
        CodDescripcion tipo;
        MensajeTipos mensajeConsulta = new MensajeTipos();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_EXONERACION);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tipo = new CodDescripcion();

                tipo.setCodigo(resultSet.getString(1));
                tipo.setDescripcion(resultSet.getString(2));
                codigoEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                tipo.setCodigosEmp(codigosEmp);

                tipos.add(tipo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTipos(tipos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTipoIdentificacion getTiposIdentificacion(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Identificacion> tipos = new ArrayList<>();
        Identificacion tipo;
        MensajeTipoIdentificacion mensajeConsulta = new MensajeTipoIdentificacion();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_IDENTIFICACION);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tipo = new Identificacion();

                tipo.setCodigo(resultSet.getString(1));
                tipo.setDescripcion(resultSet.getString(2));
                codigoEmp = resultSet.getString(3);
                tipo.setLongitud(resultSet.getString(4));

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                tipo.setCodigosEmp(codigosEmp);

                tipos.add(tipo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTipos(tipos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTipos getTiposCodigoArt(EmisorToken pToken) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<CodDescripcion> tipos = new ArrayList<>();
        CodDescripcion tipo;
        MensajeTipos mensajeConsulta = new MensajeTipos();
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_CODIGO_ART);

            pstmt.setString(1, pToken.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tipo = new CodDescripcion();

                tipo.setCodigo(resultSet.getString(1));
                tipo.setDescripcion(resultSet.getString(2));
                codigoEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    List<String> list = Arrays.asList(parts);

                    for (String i : list) {
                        codigosEmp.add(i);
                    }
                }

                tipo.setCodigosEmp(codigosEmp);

                tipos.add(tipo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTipos(tipos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addUnidadMedidaEmp(String pIdEmpresaFe, String pUnidadMedida, String pUnidadMedidaEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_UNIDAD_MEDIDA_EMP);

            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pUnidadMedida);
            cstmt.setString(3, pUnidadMedidaEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.close();
            }

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addCondicionVentaEmp(String pIdEmpresaFe, String pCondicionVenta, String pCondicionVentaEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_CONDICION_VENTA_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pCondicionVenta);
            cstmt.setString(3, pCondicionVentaEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addDistGeoEmp(String pIdEmpresaFe, String pDistGeo, String pDistGeoEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_DIST_GEO_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pDistGeo);
            cstmt.setString(3, pDistGeoEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addMedioPagoEmp(String pIdEmpresaFe, String pMedioPago, String pMedioPagoEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_MEDIO_PAGO_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pMedioPago);
            cstmt.setString(3, pMedioPagoEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addImpuestoEmp(String pIdEmpresaFe, String pImpuesto, String pImpuestoEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_IMPUESTO_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pImpuesto);
            cstmt.setString(3, pImpuestoEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addMonedaEmp(String pIdEmpresaFe, String pMoneda, String pMonedaEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_MONEDA_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pMoneda);
            cstmt.setString(3, pMonedaEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addTipoDocumentoEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp, String pDescripcion) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_DOCUMENTO_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pTipoDocumento);
            cstmt.setString(3, pTipoDocumentoEmp);
            cstmt.setString(4, pDescripcion);

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addTipoExoneracionEmp(String pIdEmpresaFe, String pTipoExoneracion, String pTipoExoneracionEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_EXONERACION_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pTipoExoneracion);
            cstmt.setString(3, pTipoExoneracionEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addTipoIdentificacionEmp(String pIdEmpresaFe, String pTipoIdentificacion, String pTipoIdentificacionEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_IDENTIFICACION_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pTipoIdentificacion);
            cstmt.setString(3, pTipoIdentificacionEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addTipoCodigoArtEmp(String pIdEmpresaFe, String pTipoCodigoArt, String pTipoCodigoArtEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_COD_ARTICULO_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pTipoCodigoArt);
            cstmt.setString(3, pTipoCodigoArtEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public boolean deleteUnidadMedidaEmp(String pIdEmpresaFe, String pUnidadMedida, String pUnidadMedidaEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_UNIDAD_MEDIDA_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pUnidadMedida);
            pstmt.setString(3, pUnidadMedidaEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteCondicionVentaEmp(String pIdEmpresaFe, String pCondicionVenta, String pCondicionVentaEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_CONDICION_VENTA_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pCondicionVenta);
            pstmt.setString(3, pCondicionVentaEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteDistGeoEmp(String pIdEmpresaFe, String pDistGeo, String pDistGeoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_DIST_GEO_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDistGeo);
            pstmt.setString(3, pDistGeoEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteMedioPagoEmp(String pIdEmpresaFe, String pMedioPago, String pMedioPagoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_MEDIO_PAGO_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pMedioPago);
            pstmt.setString(3, pMedioPagoEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteImpuestoEmp(String pIdEmpresaFe, String pImpuesto, String pImpuestoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_IMPUESTO_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pImpuesto);
            pstmt.setString(3, pImpuestoEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteMonedaEmp(String pIdEmpresaFe, String pMoneda, String pMonedaEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_MONEDA_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pMoneda);
            pstmt.setString(3, pMonedaEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteTipoDocumentoEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_TIPO_DOCUMENTO_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pTipoDocumento);
            pstmt.setString(3, pTipoDocumentoEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteTipoExoneracionEmp(String pIdEmpresaFe, String pTipoExoneracion, String pTipoExoneracionEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_TIPO_EXONERACION_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pTipoExoneracion);
            pstmt.setString(3, pTipoExoneracionEmp);

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteTipoIdentificacionEmp(String pIdEmpresaFe, String pTipoIdentificacion, String pTipoIdentificacionEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_TIPO_IDENTIFICACION_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pTipoIdentificacion);
            pstmt.setString(3, pTipoIdentificacionEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteTipoCodigoArtEmp(String pIdEmpresaFe, String pTipoCodigoArt, String pTipoCodigoArtEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_TIPO_COD_ARTICULO_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pTipoCodigoArt);
            pstmt.setString(3, pTipoCodigoArtEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public MensajeLogin login(String pUsuario, String pClave, String pIdEmpresaFe, String pActualizaDef) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        boolean verificado = false;
        boolean superUsuario = false;
        boolean activo = false;
        MensajeLogin mensaje = new MensajeLogin();
        String idUsuario = null;
        String nombreUsuario = null;
        String idEmpresaDefault = null;
        String fechaVencePago = null;
        String idPlan = null;
        String idTipoPlan = null;
        String logoDistribuidor = null;
        String diasAntesVence = null;
        String diasDespuesVence = null;
        String diasVenceCert = null;
        String fechaVenceCert = null;
        String emp;
        String token;
        String logoEmpresa = null;
        GruposUsuario grupo;
        List<GruposUsuario> grupos = new ArrayList<>();
        EmpresasUsuario empresa;
        List<EmpresasUsuario> empresas = new ArrayList<>();

        try {
            connection = dataSource.getConnection();

            pstmt = connection.prepareStatement(Querys.LOGIN);

            pstmt.setString(1, pUsuario);
            pstmt.setString(2, pClave);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                verificado = true;

                idUsuario = resultSet.getString(1);
                nombreUsuario = resultSet.getString(2);
                idEmpresaDefault = resultSet.getString(3);
                superUsuario = resultSet.getBoolean(4);
                activo = resultSet.getBoolean(5);
            }

            if (!verificado) {
                mensaje.setStatus(Constantes.statusError);
                mensaje.setMensaje("Usuario o clave incorrectos");
                return mensaje;
            }

            if (!activo) {
                mensaje.setStatus(Constantes.statusError);
                mensaje.setMensaje("Usuario inactivo");
                return mensaje;
            }

            emp = idEmpresaDefault;

            if (!superUsuario) {
                if (pIdEmpresaFe != null && !pIdEmpresaFe.equals("")) {
                    if (pActualizaDef != null && pActualizaDef.equals("1")) {
                        pstmt = connection.prepareStatement(Querys.ACTUALIZA_EMPRESA_DEFAULT);
                        pstmt.setString(1, pIdEmpresaFe);
                        pstmt.setString(2, idUsuario);
                        pstmt.setString(3, pIdEmpresaFe);
                        pstmt.setString(4, idUsuario);

                        int result = pstmt.executeUpdate();

                        if (result != 1) {
                            mensaje.setStatus(Constantes.statusError);
                            mensaje.setMensaje("No se pudo actualizar la empresa por defecto");

                            return mensaje;
                        }

                        emp = pIdEmpresaFe;
                    }

                    idEmpresaDefault = pIdEmpresaFe;
                }
            } else {
                if (pIdEmpresaFe != null && !pIdEmpresaFe.equals("")) {
                    idEmpresaDefault = pIdEmpresaFe;
                }
            }

            if (superUsuario) {
                grupo = new GruposUsuario();

                grupo.setIdGrupo("1");
                grupo.setNombre("Super Administrador");

                grupos.add(grupo);

                pstmt = connection.prepareStatement(Querys.GET_EMPRESAS);
                resultSet = pstmt.executeQuery();
            } else {
                pstmt = connection.prepareStatement(Querys.GRUPOS_USUARIO);

                pstmt.setString(1, idUsuario);
                pstmt.setString(2, idEmpresaDefault);

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    grupo = new GruposUsuario();

                    grupo.setIdGrupo(resultSet.getString(1));
                    grupo.setNombre(resultSet.getString(2));

                    grupos.add(grupo);
                }

                pstmt = connection.prepareStatement(Querys.GET_EMPRESA_USUARIO);
                pstmt.setString(1, idUsuario);
                resultSet = pstmt.executeQuery();
            }

            while (resultSet.next()) {
                empresa = new EmpresasUsuario();
                empresa.setIdEmpresa(resultSet.getString(1));
                empresa.setNombre(resultSet.getString(2));

                if (empresa.getIdEmpresa().equals(emp)) {
                    empresa.setDefecto("1");
                } else {
                    empresa.setDefecto("0");
                }
                empresas.add(empresa);
            }

            pstmt = connection.prepareStatement(Querys.GET_DATOS_LOGIN_EMPRESA);

            pstmt.setString(1, Constantes.ID_PARAMETRO_DIAS_ANTES_VENCE);
            pstmt.setString(2, Constantes.ID_PARAMETRO_DIAS_DESPUES_VENCE);
            pstmt.setString(3, Constantes.ID_PARAMETRO_DIAS_VENCE_CERT);
            pstmt.setString(4, idEmpresaDefault);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                logoEmpresa = resultSet.getString(1);
                fechaVencePago = resultSet.getString(2);
                idPlan = resultSet.getString(3);
                idTipoPlan = resultSet.getString(4);
                logoDistribuidor = resultSet.getString(5);
                diasAntesVence = resultSet.getString(6);
                diasDespuesVence = resultSet.getString(7);
                diasVenceCert = resultSet.getString(8);
                fechaVenceCert = resultSet.getString(9);
            }

            GeneraToken data = new GeneraToken();

            data.setpIdEmpresaFe(idEmpresaDefault);
            data.setpIdUsuario(idUsuario);

            token = getToken(data);

            if (token != null && !token.equals("")) {
                mensaje.setStatus(Constantes.statusSuccess);
                mensaje.setMensaje("Usuario verificado");
                mensaje.setNombre(nombreUsuario);
                mensaje.setGrupos(grupos);
                mensaje.setEmpresas(empresas);
                mensaje.setToken(token);
                mensaje.setLogo(logoEmpresa);
                mensaje.setDiasAntesVence(diasAntesVence);
                mensaje.setDiasPosteriorVence(diasDespuesVence);
                mensaje.setDiasVenceCert(diasVenceCert);
                mensaje.setFechaVenceCert(fechaVenceCert);
                mensaje.setFechaVencePago(fechaVencePago);
                mensaje.setIdPlan(idPlan);
                mensaje.setIdTipoPlan(idTipoPlan);
                mensaje.setLogoDistribuidor(logoDistribuidor);

                if (superUsuario) {
                    mensaje.setSuperUsuario("1");
                } else {
                    mensaje.setSuperUsuario("0");
                }
            } else {
                mensaje.setStatus(Constantes.statusSuccess);
                mensaje.setMensaje("Error generando el token");
            }

        } catch (Exception e) {
            mensaje.setStatus(Constantes.statusError);
            mensaje.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensaje;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensaje;
    }

    @Override
    public MensajePaginas getPaginas(String pIdGrupo, String pPreferencias) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Pagina> paginas = new ArrayList<>();
        Pagina pagina;
        MensajePaginas mensajeConsulta = new MensajePaginas();

        try {
            connection = dataSource.getConnection();

            if (pPreferencias == null || !pPreferencias.equals("1")) {
                pstmt = connection.prepareStatement(Querys.GET_PAGINAS_USUARIO);

                pstmt.setString(1, pIdGrupo);

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    pagina = new Pagina();

                    pagina.setIdPagina(resultSet.getString(1));
                    pagina.setDescripcion(resultSet.getString(2));
                    pagina.setUrl(resultSet.getString(3));

                    paginas.add(pagina);
                }
            } else {
                pstmt = connection.prepareStatement(Querys.GET_PAGINAS_PREFERENCIAS);

                pstmt.setString(1, pIdGrupo);

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    pagina = new Pagina();

                    pagina.setIdPagina(resultSet.getString(1));
                    pagina.setDescripcion(resultSet.getString(2));
                    pagina.setUrl(resultSet.getString(3));

                    paginas.add(pagina);
                }
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setPaginas(paginas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeMenuLateral getMenuLateral(String pIdGrupo) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<OpcionMenu> opciones = new ArrayList<>();
        OpcionMenu opcion;
        MensajeMenuLateral mensajeConsulta = new MensajeMenuLateral();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_MENU_LATERAL);

            pstmt.setString(1, pIdGrupo);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                opcion = new OpcionMenu();

                opcion.setTreeItem(resultSet.getString(1));
                opcion.setIdPagina(resultSet.getString(2));
                opcion.setNombre(resultSet.getString(3));
                opcion.setUrl(resultSet.getString(4));
                opcion.setIdPadre(resultSet.getString(5));
                opcion.setNivel(resultSet.getString(6));
                opcion.setIcon(resultSet.getString(7));
                opcion.setColor(resultSet.getString(8));
                opcion.setSecuencia(resultSet.getString(9));

                opciones.add(opcion);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setOpciones(opciones);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeEmpresa addEmpresa(String pIdHomEmpresa, String pRazonSocial, String pNombreComercial, String pTipoIdentificacion,
            String pNumIdentificacion, String pIdProvincia, String pIdCanton, String pIdDistrito, String pIdBarrio, String pOtrasSenas, String pCodPaisTel,
            String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail,
            String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario,
            InputStream pLogo, FormDataContentDisposition pLogoDetail, String pRutaGlassfish,
            String pIdGrupo, String pIdPlan, String pIdTipoPlan, String pEsDistribuidor, String pIdPadre, String pAdministraCobros, String pManejaHacienda) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeEmpresa mensajeInsert = new MensajeEmpresa();
        String rutaCertificado;
        String rutalogo;
        OutputStream out;

        try {

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_ADD_EMPRESA);

            cstmt.setString(1, pIdHomEmpresa);
            cstmt.setString(2, pRazonSocial);
            cstmt.setString(3, pNombreComercial);
            cstmt.setString(4, pTipoIdentificacion);
            cstmt.setString(5, pNumIdentificacion);
            cstmt.setString(6, pIdProvincia);
            cstmt.setString(7, pIdCanton);
            cstmt.setString(8, pIdDistrito);
            cstmt.setString(9, pIdBarrio);
            cstmt.setString(10, pOtrasSenas);
            cstmt.setString(11, pCodPaisTel);
            cstmt.setString(12, pTelefono);
            cstmt.setString(13, pCodPaisFax);
            cstmt.setString(14, pFax);
            cstmt.setString(15, pCorreoElectronico);
            cstmt.setString(16, pCorreoFe);
            cstmt.setString(17, pPinCertificado);
            cstmt.setString(18, pUsuarioHacienda);
            cstmt.setString(19, pClaveHacienda);
            cstmt.setString(20, pNombreUsuario);
            cstmt.setString(21, pIdGrupo);
            cstmt.setString(22, pIdPlan);
            cstmt.setString(23, pIdTipoPlan);
            cstmt.setString(24, pIdPadre);
            cstmt.setString(25, pAdministraCobros);
            cstmt.setString(26, pManejaHacienda);
            cstmt.setString(27, pEsDistribuidor);

            cstmt.registerOutParameter(28, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(29, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(30, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(31, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(32, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(33, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(34, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(35, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(36, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(28));
            mensajeInsert.setMensaje(cstmt.getString(29));
            mensajeInsert.setIdEmpresa(cstmt.getString(30));
            mensajeInsert.setIdSucursal(cstmt.getString(31));
            mensajeInsert.setIdPdv(cstmt.getString(32));
            mensajeInsert.setIdUsuario(cstmt.getString(33));
            mensajeInsert.setUsuario(cstmt.getString(34));
            mensajeInsert.setClave(cstmt.getString(35));
            rutaCertificado = cstmt.getString(36);

            if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                connection.rollback();

                return mensajeInsert;
            }

            if (pUsuarioHacienda != null && !pUsuarioHacienda.equals("") && pClaveHacienda != null && !pClaveHacienda.equals("")) {
                APIHacienda apiHacienda = new APIHacienda();

                Token token = apiHacienda.getToken(pUsuarioHacienda, pClaveHacienda);

                if (token == null) {
                    connection.rollback();

                    mensajeInsert = new MensajeEmpresa();

                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Error con los credenciales de hacienda");

                    return mensajeInsert;
                }
            }

            if (pLogo != null) {
                String nombreLogo = pLogoDetail.getFileName().toUpperCase();

                if (!nombreLogo.endsWith(".JPG") && !nombreLogo.endsWith(".JPEG") && !nombreLogo.endsWith(".PNG")) {
                    mensajeInsert = new MensajeEmpresa();

                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Solo se permiten logos de tipo .jpeg, .jpg y .png");

                    connection.rollback();

                    return mensajeInsert;
                } else {
                    String caracterSeparador = getCaracterSeparadorCarpeta();

                    String rutaTabla = "resources" + caracterSeparador + mensajeInsert.getIdEmpresa() + caracterSeparador;
                    String pRutaGfTemp = pRutaGlassfish;
                    pRutaGfTemp = pRutaGfTemp.replace("api-factura", "api-recursos");

                    rutalogo = pRutaGfTemp + rutaTabla;

                    int length = pLogo.available();

                    new File(rutalogo).mkdirs();

                    String mimeType = pLogoDetail.getFileName().substring(pLogoDetail.getFileName().indexOf(".") + 1);

                    rutalogo = rutalogo + "logoEmpresa." + mimeType;

                    String ipServidor = getIpServidor();

                    rutaTabla = "http://" + ipServidor + ":8080/api-recursos/resources/" + mensajeInsert.getIdEmpresa() + "/logoEmpresa." + mimeType;

                    out = new FileOutputStream(new File(rutalogo));
                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = pLogo.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    out.flush();
                    out.close();

                    pstmt = connection.prepareStatement(Querys.ADD_LOGO_EMPRESA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, mensajeInsert.getIdEmpresa());
                    pstmt.setString(2, rutaTabla);
                    pstmt.setString(3, mimeType);
                    pstmt.setString(4, String.valueOf(length));
                    pstmt.setString(5, Constantes.TIPO_ARCHIVO_LOGO);

                    pstmt.executeUpdate();
                }
            }

            if (pManejaHacienda.equals("1")) {
                if (pCertificado != null) {
                    if (rutaCertificado == null) {
                        connection.rollback();

                        mensajeInsert = new MensajeEmpresa();
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Error con la ruta del certificado");

                        return mensajeInsert;
                    }

                    new File(rutaCertificado).mkdirs();

                    rutaCertificado = rutaCertificado + "produccion.p12";

                    //Se guarda el certificado
                    out = new FileOutputStream(new File(rutaCertificado));
                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = pCertificado.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    out.flush();
                    out.close();

                    File file1 = new File(rutaCertificado);
                    Pkcs12Util util = new Pkcs12Util(file1, pPinCertificado);
                    if (util.init()) {
                        if (!util.isValidCert()) {
                            connection.rollback();

                            mensajeInsert = new MensajeEmpresa();

                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Error con el pin del certificado");

                            FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                            return mensajeInsert;
                        }

                        Date fechaExpira = util.getExpire();

                        if (fechaExpira.before(new Date())) {
                            connection.rollback();

                            mensajeInsert = new MensajeEmpresa();

                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El certificado se encuentra vencido, favor generar uno nuevo en ATV");

                            FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                            return mensajeInsert;
                        }

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

                        //Calendar calendar = Calendar.getInstance();
                        //calendar.setTime(fechaExpira);
                        pstmt = connection.prepareStatement(Querys.UPDATE_EXPIRA_TOKEN);
                        pstmt.setString(1, simpleDateFormat.format(fechaExpira)); //calendar.gettime
                        pstmt.setString(2, mensajeInsert.getIdEmpresa());

                        pstmt.executeUpdate();

                    } else {
                        connection.rollback();

                        mensajeInsert = new MensajeEmpresa();

                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Error con el pin del certificado");

                        FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                        return mensajeInsert;
                    }
                } else {
                    connection.rollback();

                    mensajeInsert = new MensajeEmpresa();
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El certificado es requerido cuando se manejan credenciales de hacienda");

                    return mensajeInsert;
                }
            }

            connection.commit();

        } catch (Exception e) {
            mensajeInsert = new MensajeEmpresa();

            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeAddLogo updateEmpresaRef(String pIdHomEmpresa, String pRazonSocial, String pNombreComercial, String pTipoIdentificacion,
            String pNumIdentificacion, String pIdProvincia, String pIdCanton, String pIdDistrito, String pIdBarrio, String pOtrasSenas, String pCodPaisTel,
            String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail,
            String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario,
            InputStream pLogo, FormDataContentDisposition pLogoDetail, String pRutaGlassfish,
            String pIdGrupo, String pEsDistribuidor, String pAdministraCobros, String pManejaHacienda, String pIdEmpresa) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeAddLogo mensajeInsert = new MensajeAddLogo();
        String rutaCertificado;
        String rutalogo;
        OutputStream out;
        String caracterSeparador;

        try {

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_UPDATE_EMPRESA_REF);

            cstmt.setString(1, pIdHomEmpresa);
            cstmt.setString(2, pRazonSocial);
            cstmt.setString(3, pNombreComercial);
            cstmt.setString(4, pTipoIdentificacion);
            cstmt.setString(5, pNumIdentificacion);
            cstmt.setString(6, pIdProvincia);
            cstmt.setString(7, pIdCanton);
            cstmt.setString(8, pIdDistrito);
            cstmt.setString(9, pIdBarrio);
            cstmt.setString(10, pOtrasSenas);
            cstmt.setString(11, pCodPaisTel);
            cstmt.setString(12, pTelefono);
            cstmt.setString(13, pCodPaisFax);
            cstmt.setString(14, pFax);
            cstmt.setString(15, pCorreoElectronico);
            cstmt.setString(16, pCorreoFe);
            cstmt.setString(17, pPinCertificado);
            cstmt.setString(18, pUsuarioHacienda);
            cstmt.setString(19, pClaveHacienda);
            cstmt.setString(20, pNombreUsuario);
            cstmt.setString(21, pIdGrupo);
            cstmt.setString(22, pEsDistribuidor);
            cstmt.setString(23, pAdministraCobros);
            cstmt.setString(24, pManejaHacienda);
            cstmt.setString(25, pIdEmpresa);

            cstmt.registerOutParameter(26, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(27, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(28, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(29, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(26));
            mensajeInsert.setMensaje(cstmt.getString(27));
            rutaCertificado = cstmt.getString(28);
            caracterSeparador = cstmt.getString(29);

            if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                connection.rollback();

                return mensajeInsert;
            }

            if (pUsuarioHacienda != null && !pUsuarioHacienda.equals("") && pClaveHacienda != null && !pClaveHacienda.equals("")) {
                APIHacienda apiHacienda = new APIHacienda();

                Token token = apiHacienda.getToken(pUsuarioHacienda, pClaveHacienda);

                if (token == null) {
                    connection.rollback();

                    mensajeInsert = new MensajeAddLogo();

                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Error con los credenciales de hacienda");

                    return mensajeInsert;
                }
            }

            if (pLogo != null) {
                String nombreLogo = pLogoDetail.getFileName().toUpperCase();

                if (!nombreLogo.endsWith(".JPG") && !nombreLogo.endsWith(".JPEG") && !nombreLogo.endsWith(".PNG")) {
                    mensajeInsert = new MensajeAddLogo();

                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Solo se permiten logos de tipo .jpeg, .jpg y .png");

                    connection.rollback();

                    return mensajeInsert;
                } else {

                    String rutaTabla = "resources" + caracterSeparador + pIdEmpresa + caracterSeparador;
                    String pRutaGfTemp = pRutaGlassfish;
                    pRutaGfTemp = pRutaGfTemp.replace("api-factura", "api-recursos");

                    rutalogo = pRutaGfTemp + rutaTabla;

                    int length = pLogo.available();

                    new File(rutalogo).mkdirs();

                    String mimeType = pLogoDetail.getFileName().substring(pLogoDetail.getFileName().indexOf(".") + 1);

                    rutalogo = rutalogo + pLogoDetail.getFileName();

                    String ipServidor = getIpServidor();

                    rutaTabla = "http://" + ipServidor + ":8080/api-recursos/resources/" + pIdEmpresa + "/" + pLogoDetail.getFileName();

                    out = new FileOutputStream(new File(rutalogo));
                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = pLogo.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    out.flush();
                    out.close();

                    pstmt = connection.prepareStatement(Querys.ADD_LOGO_EMPRESA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, pIdEmpresa);
                    pstmt.setString(2, rutaTabla);
                    pstmt.setString(3, mimeType);
                    pstmt.setString(4, String.valueOf(length));
                    pstmt.setString(5, Constantes.TIPO_ARCHIVO_LOGO);

                    pstmt.executeUpdate();
                }
            }

            if (pCertificado != null) {
                if (rutaCertificado == null) {
                    connection.rollback();

                    mensajeInsert = new MensajeAddLogo();
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Error con la ruta del certificado");

                    return mensajeInsert;
                }

                new File(rutaCertificado).mkdirs();

                rutaCertificado = rutaCertificado + "produccion.p12";

                //Se guarda el certificado
                out = new FileOutputStream(new File(rutaCertificado));
                int read = 0;
                byte[] bytes = new byte[1024];

                while ((read = pCertificado.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();

                File file1 = new File(rutaCertificado);
                Pkcs12Util util = new Pkcs12Util(file1, pPinCertificado);
                if (util.init()) {
                    if (!util.isValidCert()) {
                        connection.rollback();

                        mensajeInsert = new MensajeAddLogo();

                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Error con el pin del certificado");

                        FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                        return mensajeInsert;
                    }
                } else {
                    connection.rollback();

                    mensajeInsert = new MensajeAddLogo();

                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Error con el pin del certificado");

                    FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                    return mensajeInsert;
                }
            }

            connection.commit();

        } catch (Exception e) {
            mensajeInsert = new MensajeAddLogo();

            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeAddLogo updateEmpresa(String pRazonSocial, String pNombreComercial, String pTipoIdentificacionEmp,
            String pNumIdentificacion, String pIdProvinciaEmp, String pIdCantonEmp, String pIdDistritoEmp, String pIdBarrioEmp, String pOtrasSenas, String pCodPaisTel,
            String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail,
            String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario,
            InputStream pLogo, FormDataContentDisposition pLogoDetail, String pRutaGlassfish, EmisorToken pToken) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeAddLogo mensajeUpdate = new MensajeAddLogo();
        String rutaCertificado;
        String rutalogo;
        OutputStream out;
        String caracterSeparador;

        try {

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_UPDATE_EMPRESA);

            cstmt.setString(1, pRazonSocial);
            cstmt.setString(2, pNombreComercial);
            cstmt.setString(3, pTipoIdentificacionEmp);
            cstmt.setString(4, pNumIdentificacion);
            cstmt.setString(5, pIdProvinciaEmp);
            cstmt.setString(6, pIdCantonEmp);
            cstmt.setString(7, pIdDistritoEmp);
            cstmt.setString(8, pIdBarrioEmp);
            cstmt.setString(9, pOtrasSenas);
            cstmt.setString(10, pCodPaisTel);
            cstmt.setString(11, pTelefono);
            cstmt.setString(12, pCodPaisFax);
            cstmt.setString(13, pFax);
            cstmt.setString(14, pCorreoElectronico);
            cstmt.setString(15, pCorreoFe);
            cstmt.setString(16, pPinCertificado);
            cstmt.setString(17, pUsuarioHacienda);
            cstmt.setString(18, pClaveHacienda);
            cstmt.setString(19, pNombreUsuario);
            cstmt.setString(20, pToken.getIdEmpresaFe());
            cstmt.setString(21, pToken.getIdUsuario());

            cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(23, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(24, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(25, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeUpdate.setStatus(cstmt.getString(22));
            mensajeUpdate.setMensaje(cstmt.getString(23));
            rutaCertificado = cstmt.getString(24);
            caracterSeparador = cstmt.getString(25);

            if (mensajeUpdate.getStatus().equals(Constantes.statusError)) {
                connection.rollback();

                return mensajeUpdate;
            }

            if (pLogo != null) {
                String nombreLogo = pLogoDetail.getFileName().toUpperCase();

                if (!nombreLogo.endsWith(".JPG") && !nombreLogo.endsWith(".JPEG") && !nombreLogo.endsWith(".PNG")) {
                    mensajeUpdate = new MensajeAddLogo();

                    mensajeUpdate.setStatus(Constantes.statusError);
                    mensajeUpdate.setMensaje("Solo se permiten logos de tipo .jpeg, .jpg y .png");

                    connection.rollback();

                    return mensajeUpdate;
                } else {

                    String rutaTabla = "resources" + caracterSeparador + pToken.getIdEmpresaFe() + caracterSeparador;
                    String pRutaGfTemp = pRutaGlassfish;
                    pRutaGfTemp = pRutaGfTemp.replace("api-factura", "api-recursos");

                    rutalogo = pRutaGfTemp + rutaTabla;

                    int length = pLogo.available();

                    new File(rutalogo).mkdirs();

                    String mimeType = pLogoDetail.getFileName().substring(pLogoDetail.getFileName().indexOf(".") + 1);

                    rutalogo = rutalogo + pLogoDetail.getFileName();

                    String ipServidor = getIpServidor();

                    rutaTabla = "http://" + ipServidor + ":8080/api-recursos/resources/" + pToken.getIdEmpresaFe() + "/" + pLogoDetail.getFileName();

                    out = new FileOutputStream(new File(rutalogo));
                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = pLogo.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    out.flush();
                    out.close();

                    connection = dataSource.getConnection();
                    cstmt = connection.prepareCall(Querys.PR_ADD_LOGO_EMPRESA);

                    cstmt.setString(1, pToken.getIdEmpresaFe());
                    cstmt.setString(2, rutaTabla);
                    cstmt.setString(3, mimeType);
                    cstmt.setString(4, String.valueOf(length));
                    cstmt.setString(5, Constantes.TIPO_ARCHIVO_LOGO);

                    cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeUpdate.setStatus(cstmt.getString(6));
                    mensajeUpdate.setMensaje(cstmt.getString(7));
                    mensajeUpdate.setRutaLogo(rutaTabla);

                    if (mensajeUpdate.getStatus().equals(Constantes.statusError)) {
                        connection.rollback();

                        return mensajeUpdate;
                    }
                }
            }

            if (pCertificado != null) {
                new File(rutaCertificado).mkdirs();

                rutaCertificado = rutaCertificado + "produccion.p12";

                //Se guarda el certificado
                out = new FileOutputStream(new File(rutaCertificado));
                int read = 0;
                byte[] bytes = new byte[1024];

                while ((read = pCertificado.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();

                File file1 = new File(rutaCertificado);
                Pkcs12Util util = new Pkcs12Util(file1, pPinCertificado);
                if (util.init()) {
                    if (!util.isValidCert()) {
                        connection.rollback();

                        mensajeUpdate = new MensajeAddLogo();

                        mensajeUpdate.setStatus(Constantes.statusError);
                        mensajeUpdate.setMensaje("Error con el pin del certificado");

                        return mensajeUpdate;
                    }

                    Date fechaExpira = util.getExpire();

                    if (fechaExpira.before(new Date())) {
                        connection.rollback();

                        mensajeUpdate = new MensajeAddLogo();

                        mensajeUpdate.setStatus(Constantes.statusError);
                        mensajeUpdate.setMensaje("El certificado se encuentra vencido, favor generar uno nuevo en ATV");

                        return mensajeUpdate;
                    }

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

                    //Calendar calendar = Calendar.getInstance();
                    //calendar.setTime(fechaExpira);
                    pstmt = connection.prepareStatement(Querys.UPDATE_EXPIRA_TOKEN);
                    pstmt.setString(1, simpleDateFormat.format(fechaExpira)); //calendar.gettime
                    pstmt.setString(2, pToken.getIdEmpresaFe());

                    pstmt.executeUpdate();

                    pstmt.close();

                    pstmt = null;

                } else {
                    connection.rollback();

                    mensajeUpdate = new MensajeAddLogo();

                    mensajeUpdate.setStatus(Constantes.statusError);
                    mensajeUpdate.setMensaje("Error con el pin del certificado");

                    FileUtils.deleteDirectory(new File(rutaCertificado.replaceFirst("produccion.p12", "")));

                    return mensajeUpdate;
                }
            }

            connection.commit();

        } catch (Exception e) {
            mensajeUpdate = new MensajeAddLogo();

            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeUpdate;
    }

    @Override
    public MensajeInsert addReceptor(EmisorToken pToken, ReceptorAdd pReceptor) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_RECEPTOR);

            cstmt.setString(1, pReceptor.getNombre());
            cstmt.setString(2, pReceptor.getTipoIdentificacionEmp());
            cstmt.setString(3, pReceptor.getNumIdentificacion());
            cstmt.setString(4, pReceptor.getIdentificacionExtranjero());
            cstmt.setString(5, pReceptor.getNombreComercial());
            cstmt.setString(6, pReceptor.getProvinciaEmp());
            cstmt.setString(7, pReceptor.getCantonEmp());
            cstmt.setString(8, pReceptor.getDistritoEmp());
            cstmt.setString(9, pReceptor.getBarrioEmp());
            cstmt.setString(10, pReceptor.getOtrasSenas());
            cstmt.setString(11, pReceptor.getCodPaisTel());
            cstmt.setString(12, pReceptor.getNumTel());
            cstmt.setString(13, pReceptor.getCodPaisFax());
            cstmt.setString(14, pReceptor.getFax());
            cstmt.setString(15, pReceptor.getCorreoElectronico());
            cstmt.setString(16, pReceptor.getIdReceptorEmp());
            cstmt.setString(17, pReceptor.getTipoTributario());
            cstmt.setString(18, pToken.getIdEmpresaFe());
            cstmt.setString(19, pReceptor.getIdPlanilla());
           // cstmt.setString(20, pReceptor.getClienteCorrecto());

            cstmt.registerOutParameter(20, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(21, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(20));
            mensajeInsert.setMensaje(cstmt.getString(21));
            mensajeInsert.setId(cstmt.getString(22));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateReceptor(EmisorToken pToken, ReceptorAdd pReceptor) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeUpdate = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_UPDATE_RECEPTOR);

            cstmt.setString(1, pReceptor.getNombre());
            cstmt.setString(2, pReceptor.getTipoIdentificacionEmp());
            cstmt.setString(3, pReceptor.getNumIdentificacion());
            cstmt.setString(4, pReceptor.getIdentificacionExtranjero());
            cstmt.setString(5, pReceptor.getNombreComercial());
            cstmt.setString(6, pReceptor.getProvinciaEmp());
            cstmt.setString(7, pReceptor.getCantonEmp());
            cstmt.setString(8, pReceptor.getDistritoEmp());
            cstmt.setString(9, pReceptor.getBarrioEmp());
            cstmt.setString(10, pReceptor.getOtrasSenas());
            cstmt.setString(11, pReceptor.getCodPaisTel());
            cstmt.setString(12, pReceptor.getNumTel());
            cstmt.setString(13, pReceptor.getCodPaisFax());
            cstmt.setString(14, pReceptor.getFax());
            cstmt.setString(15, pReceptor.getCorreoElectronico());
            cstmt.setString(16, pReceptor.getIdReceptorEmp());
            cstmt.setString(17, pReceptor.getTipoTributario());
            cstmt.setString(18, pReceptor.getIdPlanilla());

            cstmt.setString(19, pToken.getIdEmpresaFe());
            cstmt.setString(20, pReceptor.getEstado());
            //cstmt.setString(21, pReceptor.getClienteCorrecto());

            cstmt.registerOutParameter(21, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeUpdate.setStatus(cstmt.getString(21));
            mensajeUpdate.setMensaje(cstmt.getString(22));

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeUpdate;
    }

    public String getCaracterSeparadorCarpeta() throws Exception {
        String ruta = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_VALOR1_PARAMETROS);
            pstmt.setString(1, Constantes.ID_PARAMETRO_CARACTER_CARPETA);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                ruta = resultSet.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return ruta;
    }

    @Override
    public MensajeUsuarios getUsuarios(String pIdUsuario) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Usuario> usuarios = new ArrayList<>();
        Usuario usu;
        MensajeUsuarios mensajeUsuarios = new MensajeUsuarios();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_USUARIOS);

            pstmt.setString(1, pIdUsuario);
            pstmt.setString(2, pIdUsuario);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                usu = new Usuario();
                usu.setIdUsuario(resultSet.getString(1));
                usu.setDsUsuario(resultSet.getString(2));
                usu.setDsDescripcion(resultSet.getString(3));
                usu.setIdEmpresaDefault(resultSet.getString(4));
                usu.setStUsuario(resultSet.getString(5));
                usuarios.add(usu);
            }

            mensajeUsuarios.setStatus(Constantes.statusSuccess);
            mensajeUsuarios.setMensaje("Consulta realizada con exito");
            mensajeUsuarios.setUsuarios(usuarios);

        } catch (Exception e) {
            mensajeUsuarios.setStatus(Constantes.statusError);
            mensajeUsuarios.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeUsuarios;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeUsuarios;
    }

    @Override
    public MensajeReceptor getReceptores(String pIdEmpresaFe, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Receptor> receptores = new ArrayList<>();
        List<Plantilla> listaPlantillas;
        Plantilla plantilla;
        Receptor receptor;
        MensajeReceptor mensajeReceptor = new MensajeReceptor();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_RECEPTORES);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pEstado);
            pstmt.setString(3, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                receptor = new Receptor();
                receptor.setNombre(resultSet.getString(1));
                String tiposIdEmpComa = resultSet.getString(2);
                receptor.setNumIdentificacion(resultSet.getString(3));
                receptor.setIdentificacionExtranjero(resultSet.getString(4));
                receptor.setNombreComercial(resultSet.getString(5));
                String provinciasEmpComa = resultSet.getString(6);
                String cantonEmpComa = resultSet.getString(7);
                String distritoEmpComa = resultSet.getString(8);
                String barrioEmpComa = resultSet.getString(9);
                receptor.setOtrasSenas(resultSet.getString(10));
                receptor.setCodPaisTel(resultSet.getString(11));
                receptor.setNumTel(resultSet.getString(12));
                receptor.setCodPaisFax(resultSet.getString(13));
                receptor.setFax(resultSet.getString(14));
                receptor.setCorreoElectronico(resultSet.getString(15));
                receptor.setIdReceptorEmp(resultSet.getString(16));
                receptor.setTipoTributario(resultSet.getString(17));
                receptor.setEstado(resultSet.getString(18));
                receptor.setIdPlantilla(resultSet.getString(19));
                receptor.setEstadoDesc(resultSet.getString(20));
                receptor.setTipoIdentificacionDesc(resultSet.getString(21));
                receptor.setTipoTributarioDesc(resultSet.getString(22));

                List<String> tiposIdEmp = new ArrayList<>();

                if (tiposIdEmpComa != null) {
                    String[] parts = tiposIdEmpComa.split(",");

                    tiposIdEmp = Arrays.asList(parts);
                }

                receptor.setTiposIdentificacionEmp(tiposIdEmp);

                List<String> provinciasEmp = new ArrayList<>();

                if (provinciasEmpComa != null) {
                    String[] parts = provinciasEmpComa.split(",");

                    provinciasEmp = Arrays.asList(parts);
                }

                receptor.setProvinciaEmp(provinciasEmp);

                List<String> cantonEmp = new ArrayList<>();

                if (cantonEmpComa != null) {
                    String[] parts = cantonEmpComa.split(",");

                    cantonEmp = Arrays.asList(parts);
                }

                receptor.setCantonEmp(cantonEmp);

                List<String> distritoEmp = new ArrayList<>();

                if (distritoEmpComa != null) {
                    String[] parts = distritoEmpComa.split(",");

                    distritoEmp = Arrays.asList(parts);
                }

                receptor.setDistritoEmp(distritoEmp);

                List<String> barrioEmp = new ArrayList<>();

                if (barrioEmpComa != null) {
                    String[] parts = barrioEmpComa.split(",");

                    barrioEmp = Arrays.asList(parts);
                }

                receptor.setBarrioEmp(barrioEmp);

                List<ExoneracionArt> ExoList = getExoneraciones("0", pIdEmpresaFe, receptor.getIdReceptorEmp()).getExoneraciones();
                if (ExoList != null && ExoList.size() > 0) {
                    receptor.setExoneraciones(ExoList);
                    receptor.setExoGLobal(ExoList.get(0).getExoGlobal());
                } else {
                    receptor.setExoGLobal("0");
                }

                
                  /*Busco las plantillas del cliente .*/
                listaPlantillas = getPlantillasCliente("0", receptor.getIdReceptorEmp(), "0").getPlantilas();
                if (listaPlantillas != null && !listaPlantillas.isEmpty()) { 
                    receptor.setListaPlantillas(listaPlantillas); 
                }
                
                receptores.add(receptor);
            }

            mensajeReceptor.setStatus(Constantes.statusSuccess);
            mensajeReceptor.setMensaje("Consulta realizada con exito");
            mensajeReceptor.setReceptores(receptores);

        } catch (Exception e) {
            mensajeReceptor.setStatus(Constantes.statusError);
            mensajeReceptor.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeReceptor;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeReceptor;
    }

    @Override
    public MensajeReceptor getReceptoresExoneracion(String pIdEmpresaFe, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Receptor> receptores = new ArrayList<>();
        Receptor receptor;
        MensajeReceptor mensajeReceptor = new MensajeReceptor();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_RECEPTORES_EXO);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pEstado);
            pstmt.setString(3, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                receptor = new Receptor();
                receptor.setNombre(resultSet.getString(1));
                String tiposIdEmpComa = resultSet.getString(2);
                receptor.setNumIdentificacion(resultSet.getString(3));
                receptor.setIdentificacionExtranjero(resultSet.getString(4));
                receptor.setNombreComercial(resultSet.getString(5));
                String provinciasEmpComa = resultSet.getString(6);
                String cantonEmpComa = resultSet.getString(7);
                String distritoEmpComa = resultSet.getString(8);
                String barrioEmpComa = resultSet.getString(9);
                receptor.setOtrasSenas(resultSet.getString(10));
                receptor.setCodPaisTel(resultSet.getString(11));
                receptor.setNumTel(resultSet.getString(12));
                receptor.setCodPaisFax(resultSet.getString(13));
                receptor.setFax(resultSet.getString(14));
                receptor.setCorreoElectronico(resultSet.getString(15));
                receptor.setIdReceptorEmp(resultSet.getString(16));
                receptor.setTipoTributario(resultSet.getString(17));
                receptor.setEstado(resultSet.getString(18));
                receptor.setEstadoDesc(resultSet.getString(19));
                receptor.setTipoIdentificacionDesc(resultSet.getString(20));
                receptor.setTipoTributarioDesc(resultSet.getString(21));

                List<String> tiposIdEmp = new ArrayList<>();

                if (tiposIdEmpComa != null) {
                    String[] parts = tiposIdEmpComa.split(",");

                    tiposIdEmp = Arrays.asList(parts);
                }

                receptor.setTiposIdentificacionEmp(tiposIdEmp);

                List<String> provinciasEmp = new ArrayList<>();

                if (provinciasEmpComa != null) {
                    String[] parts = provinciasEmpComa.split(",");

                    provinciasEmp = Arrays.asList(parts);
                }

                receptor.setProvinciaEmp(provinciasEmp);

                List<String> cantonEmp = new ArrayList<>();

                if (cantonEmpComa != null) {
                    String[] parts = cantonEmpComa.split(",");

                    cantonEmp = Arrays.asList(parts);
                }

                receptor.setCantonEmp(cantonEmp);

                List<String> distritoEmp = new ArrayList<>();

                if (distritoEmpComa != null) {
                    String[] parts = distritoEmpComa.split(",");

                    distritoEmp = Arrays.asList(parts);
                }

                receptor.setDistritoEmp(distritoEmp);

                List<String> barrioEmp = new ArrayList<>();

                if (barrioEmpComa != null) {
                    String[] parts = barrioEmpComa.split(",");

                    barrioEmp = Arrays.asList(parts);
                }
                receptor.setBarrioEmp(barrioEmp);
                receptor.setExoneraciones(getExoneraciones("0", pIdEmpresaFe, receptor.getIdReceptorEmp()).getExoneraciones());

                receptores.add(receptor);
            }

            mensajeReceptor.setStatus(Constantes.statusSuccess);
            mensajeReceptor.setMensaje("Consulta realizada con exito");
            mensajeReceptor.setReceptores(receptores);

        } catch (Exception e) {
            mensajeReceptor.setStatus(Constantes.statusError);
            mensajeReceptor.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeReceptor;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeReceptor;
    }

    @Override
    public boolean deleteReceptor(String pIdEmpresaFe, String pIdReceptorEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_RECEPTOR);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdReceptorEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public MensajeInsert registraDocumento(EmisorToken pToken, DocumentoAdd pDocumento) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();
        Double montoDescuento;
        Double totalServGrav;
        Double totalServExc;
        Double totalMercGrav;
        Double totalMercExc;
        Double totalGravado;
        Double totalExento;
        Double totalVentaResumen;
        Double totalDescuentoResumen;
        Double totalVentaNetaResumen;
        Double totalImpuestoResumen;
        Double totalComprobanteResumen;
        Double totalMediosPago;
        List<ImpuestoAdd> impuestos;
        List<InformacionReferenciaAdd> referencias;
        List<LineaDetalleAdd> lineas;
        List<LineaDetalleAdd> lineasFinales = new ArrayList<>();
        List<MedioPagoAdd> mediosPago;
        List<String> interesadosCorreo;
        List<DatosTextoPlantilla> datosPlantilla;
        Exoneracion exoneracion;
        boolean tieneImpuestos;
        int idLineaDetalle = 0;
        int idImpuesto;
        String tipoDocumento = null;
        boolean revisarMontos = false;
        String claveDocumentoRef = "";
        String correoReceptor = null;

        try {
            connection = dataSource.getConnection();

            connection.setAutoCommit(false);

            totalServGrav = Double.valueOf(0);
            totalServExc = Double.valueOf(0);
            totalMercGrav = Double.valueOf(0);
            totalMercExc = Double.valueOf(0);
            totalGravado = Double.valueOf(0);
            totalExento = Double.valueOf(0);
            totalVentaResumen = Double.valueOf(0);
            totalDescuentoResumen = Double.valueOf(0);
            totalVentaNetaResumen = Double.valueOf(0);
            totalImpuestoResumen = Double.valueOf(0);
            totalComprobanteResumen = Double.valueOf(0);
            totalMediosPago = Double.valueOf(0);

            if (pDocumento.getpTipoDocumentoEmp() == null || pDocumento.getpTipoDocumentoEmp().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El tipo de documento es requerido");
                return mensajeInsert;
            }

//            if (pDocumento.getpIdPlantillaXml() == null || pDocumento.getpTipoDocumentoEmp().equals("")) {
//                mensajeInsert.setStatus(Constantes.statusError);
//                mensajeInsert.setMensaje("El tipo de plantilla xml es requerida");
//                return mensajeInsert;
//            }
            
            if(pDocumento.getpIdPlantillaXml() != null && !pDocumento.getpIdPlantillaXml().equals("0")){
                if ( pDocumento.getpOtrosTextoPlantilla() == null || pDocumento.getpOtrosTextoPlantilla().equals("")) {
                    /* Valido variables plantilla vienen llenas*/
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El otro texto plantilla xml es requerido");
                        return mensajeInsert;
                }
                if (pDocumento.getpDatosTextoPlantilla().isEmpty()) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El listado de otro texto variables plantilla es requerido");
                        return mensajeInsert;
                }
            }

            tipoDocumento = getTipoDocumento(pToken.getIdEmpresaFe(), pDocumento.getpTipoDocumentoEmp());

            referencias = pDocumento.getpInformacionReferencia();

            mediosPago = pDocumento.getpMediosPagoEmp();

            if (null == tipoDocumento) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("No se ha encontrado el tipo de documento mediante el id indicado " + pDocumento.getpTipoDocumentoEmp());
                return mensajeInsert;


            } else {


                switch (tipoDocumento) {


                    case "02":


                    case "03":
                        if (referencias == null || referencias.isEmpty()) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Debe indicar al menos un documento de referencia");
                            return mensajeInsert;
                        }
                        break;


                    case "01":


                    case "04":


                        if (mediosPago == null || mediosPago.isEmpty()) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Debe indicar al menos un medio de pago");
                            return mensajeInsert;
                        }
                        break;
                    default:
                        break;
                }
            }

            interesadosCorreo = pDocumento.getpCorreoEnvioFac();

            if (pDocumento.getpEnviaFactura() == null || !pDocumento.getpEnviaFactura().equals("1")) {
                if (interesadosCorreo != null && !interesadosCorreo.isEmpty()) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar el parametro de enviar documento en 1 si desea notificar al cliente");
                    return mensajeInsert;
                }
            } else if (pDocumento.getpEnviaFactura().equals("1")) {
                if (interesadosCorreo == null || interesadosCorreo.isEmpty()) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar a cuales correos desea notificar al cliente");
                    return mensajeInsert;
                }
            }

            if (referencias != null) {

                for (InformacionReferenciaAdd infoRef : referencias) {

                    mensajeInsert.setStatus(Constantes.statusError);

                    if (infoRef.getpNumero() == null || infoRef.getpNumero().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El numero de documento de referencia es requerido");
                        return mensajeInsert;
                    }

                    if (infoRef.getpCodigo() == null || infoRef.getpCodigo().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El codigo en la informacion de referencia es requerido");
                        return mensajeInsert;
                    }

                    if (infoRef.getpExisteDb() == null || infoRef.getpExisteDb().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar si el documento de referencia existe en el sistema");
                        return mensajeInsert;

                    } else if (!infoRef.getpExisteDb().equals("1") && !infoRef.getpExisteDb().equals("0")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Solo se permiten los valores 1 y 0 para indicar si el documento referencia existe en la base de datos");
                        return mensajeInsert;
                    }

                    if (infoRef.getpExisteDb().equals("1")) {

                        if ((tipoDocumento.equals("03")) && (infoRef.getpCodigo().equals("01") || infoRef.getpCodigo().equals("03"))) { //tipoDocumento.equals("02") ||
                            revisarMontos = true;
                            claveDocumentoRef = infoRef.getpNumero();
                        }
                    }

                    if (infoRef.getpRazon() == null || infoRef.getpRazon().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("La razon en la informacion de referencia es requerido");
                        return mensajeInsert;
                    }

                    pstmt = connection.prepareStatement(Querys.GET_TIPO_DOCUMENTO_REFERENCIA);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, infoRef.getpTipoDocRefEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        infoRef.setpTipoDocRef(resultSet.getString(1));
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("No se ha encontrado el tipo de documento de referencia");
                        return mensajeInsert;
                    }
                }
            }

            if (mediosPago != null) {
                for (MedioPagoAdd medio : mediosPago) {
                    mensajeInsert.setStatus(Constantes.statusError);

                    if (medio.getpMedioPagoEmp() == null || medio.getpMedioPagoEmp().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El id del medio de pago de la empresa es requerido");
                        return mensajeInsert;
                    }

                    if (medio.getpMonto() == null || medio.getpMonto().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El monto que cubre el medio de pago es requerido");
                        return mensajeInsert;
                    }

                    String comprobanteRequerido = "";

                    pstmt = connection.prepareStatement(Querys.GET_MEDIO_PAGO);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, medio.getpMedioPagoEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        medio.setpMedioPago(resultSet.getString(1));

                        comprobanteRequerido = resultSet.getString(2);
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("No se ha encontrado el medio de pago mediante el id " + medio.getpMedioPagoEmp());
                        return mensajeInsert;
                    }

//                    Fix para no solicitar comprobante medio de pago
//                    if (comprobanteRequerido.equals("1")  && (medio.getpComprobante() == null || medio.getpComprobante().equals(""))) {
//                        mensajeInsert.setStatus(Constantes.statusError);
//                        mensajeInsert.setMensaje("El comprobante es requerido para el medio de pago id " + medio.getpMedioPagoEmp());
//                        return mensajeInsert;
//                    }
                    totalMediosPago = totalMediosPago + Double.valueOf(medio.getpMonto());
                }
            }

            lineas = pDocumento.getpLineasDetalle();

            if (lineas == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos una linea de detalle");
                return mensajeInsert;
            }

            for (LineaDetalleAdd lineaDet : lineas) {
                mensajeInsert.setStatus(Constantes.statusError);

                tieneImpuestos = false;

                if (lineaDet.getpMercServ() == null) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar si el detalle es un producto o servicio (M/S)");
                    return mensajeInsert;
                }

                if (!lineaDet.getpMercServ().equals("M") && !lineaDet.getpMercServ().equals("S")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar si el detalle es un producto o servicio (M/S)");
                    return mensajeInsert;
                }

                if (lineaDet.getpNumeroLinea() == null || lineaDet.getpNumeroLinea().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El numero de linea en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpCantidad() == null || lineaDet.getpCantidad().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("La cantidad en la linea de detalle es requerida");
                    return mensajeInsert;
                }

                if (lineaDet.getpUnidadMedidaEmp() == null || lineaDet.getpUnidadMedidaEmp().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("La unidad de medida en la linea de detalle es requerida");
                    return mensajeInsert;
                }

                mensajeInsert.setStatus(Constantes.statusError);

                pstmt = connection.prepareStatement(Querys.GET_UNIDAD_MEDIDA_DOC);

                pstmt.setString(1, pToken.getIdEmpresaFe());
                pstmt.setString(2, lineaDet.getpUnidadMedidaEmp());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    mensajeInsert.setStatus(Constantes.statusSuccess);

                    lineaDet.setpUnidadMedida(resultSet.getString(1));
                }

                if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                    mensajeInsert.setMensaje("No se ha encontrado la unidad de medida indicada para la linea de detalle");
                    return mensajeInsert;
                }

                if (lineaDet.getpDetalle() == null || lineaDet.getpDetalle().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El detalle en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpPrecioUnitario() == null || lineaDet.getpPrecioUnitario().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El precio unitario en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoDescuento() != null || lineaDet.getpNaturalezaDescuento() != null) {
                    if (!lineaDet.getpMontoDescuento().equals("") || !lineaDet.getpNaturalezaDescuento().equals("")) {
                        if (lineaDet.getpMontoDescuento() == null || lineaDet.getpMontoDescuento().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El monto de descuento es requerido si indica naturaleza descuento en la linea de detalle");
                            return mensajeInsert;
                        }

                        if (lineaDet.getpNaturalezaDescuento() == null || lineaDet.getpNaturalezaDescuento().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El naturaleza de descuento es requerido si indica monto descuento en la linea de detalle");
                            return mensajeInsert;
                        }
                    }
                }

                if (lineaDet.getpMontoTotal() == null || lineaDet.getpMontoTotal().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El monto total en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpSubTotal() == null || lineaDet.getpSubTotal().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El subtotal en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoTotalLinea() == null || lineaDet.getpMontoTotalLinea().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El monto total de la linea en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoDescuento() == null || lineaDet.getpMontoDescuento().equals("")) {
                    montoDescuento = Double.valueOf(0);
                    lineaDet.setpMontoDescuento(String.valueOf(montoDescuento));
                } else {
                    montoDescuento = Double.valueOf(lineaDet.getpMontoDescuento());
                }

                impuestos = lineaDet.getpImpuesto();

                if (impuestos != null) {
                    for (ImpuestoAdd imp : impuestos) {
                        mensajeInsert.setStatus(Constantes.statusError);

                        tieneImpuestos = true;

                        if (imp.getpIdImpuestoEmp() == null || imp.getpIdImpuestoEmp().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El id del impuesto es requerido");
                            return mensajeInsert;
                        }

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_FACTURA);

                        pstmt.setString(1, pToken.getIdEmpresaFe());
                        pstmt.setString(2, imp.getpIdImpuestoEmp());

                        resultSet = pstmt.executeQuery();

                        while (resultSet.next()) {
                            mensajeInsert.setStatus(Constantes.statusSuccess);

                            imp.setpCodigo(resultSet.getString(1));
                        }

                        if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                            mensajeInsert.setMensaje("No se ha encontrado el impuesto indicado");
                            return mensajeInsert;
                        }

                        if (imp.getpTarifa() == null || imp.getpTarifa().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El codigo del impuesto es requerido");
                            return mensajeInsert;
                        }

                        if (imp.getpMonto() == null || imp.getpMonto().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("El monto del impuesto es requerido");
                            return mensajeInsert;
                        }

                        totalImpuestoResumen = totalImpuestoResumen + Double.valueOf(imp.getpMonto());

                        exoneracion = imp.getpExoneracion();

                        if (exoneracion != null) {
                            if (exoneracion.getpTipoDocumentoEmp() == null || exoneracion.getpTipoDocumentoEmp().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("El tipo de documento de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            mensajeInsert.setStatus(Constantes.statusError);

                            pstmt = connection.prepareStatement(Querys.GET_EXONERACION_DOCUMENTO);

                            pstmt.setString(1, pToken.getIdEmpresaFe());
                            pstmt.setString(2, exoneracion.getpTipoDocumentoEmp());

                            resultSet = pstmt.executeQuery();

                            while (resultSet.next()) {
                                mensajeInsert.setStatus(Constantes.statusSuccess);

                                exoneracion.setpTipoDocumento(resultSet.getString(1));
                            }

                            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                                mensajeInsert.setMensaje("No se ha encontrado la exoneracion indicada");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpNumeroDocumento() == null || exoneracion.getpNumeroDocumento().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("El numero de documento de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpNombreInstitucion() == null || exoneracion.getpNombreInstitucion().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("El nombre de la institucion que emitio la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpFechaEmision() == null || exoneracion.getpFechaEmision().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("La fecha de emision de la exoneracion es requerida");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpMontoImpuesto() == null || exoneracion.getpMontoImpuesto().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("El monto del impuesto exonerado de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpPorcentajeCompra() == null || exoneracion.getpPorcentajeCompra().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("El porcentaje de compra exonerado de la exoneracion es requerido");
                                return mensajeInsert;
                            }
                        }
                    }
                }

                if (tieneImpuestos) {
                    if (lineaDet.getpMercServ().equals("M")) {
                        totalMercGrav = totalMercGrav + Double.valueOf(lineaDet.getpMontoTotal());
                    } else if (lineaDet.getpMercServ().equals("S")) {
                        totalServGrav = totalServGrav + Double.valueOf(lineaDet.getpMontoTotal());
                    }
                } else {
                    if (lineaDet.getpMercServ().equals("M")) {
                        totalMercExc = totalMercExc + Double.valueOf(lineaDet.getpMontoTotal());
                    } else if (lineaDet.getpMercServ().equals("S")) {
                        totalServExc = totalServExc + Double.valueOf(lineaDet.getpMontoTotal());
                    }
                }

                totalDescuentoResumen = totalDescuentoResumen + montoDescuento;

                if (revisarMontos) {
                    if (lineaDet.getpIdDocumentoDet() == null || lineaDet.getpIdDocumentoDet().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar a cual linea hace referencia para verificar montos");
                        return mensajeInsert;
                    }

                    cstmt = connection.prepareCall(Querys.PR_VALIDA_DOC_ANTERIORES);

                    cstmt.setString(1, lineaDet.getpIdDocumentoDet());
                    cstmt.setString(2, lineaDet.getpCantidad());
                    cstmt.setString(3, lineaDet.getpPrecioUnitario());

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeInsert.setStatus(cstmt.getString(4));
                    mensajeInsert.setMensaje(cstmt.getString(5));
                    String cantidadRestante = cstmt.getString(6);
                    String montoRestante = cstmt.getString(7);

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        mensajeInsert.setMensaje("No se puede agregar el producto/servicio " + lineaDet.getpDetalle() + ", ya que la cantidad o monto indicados superan al monto y cantidad restantes para aplicar Notas de Crédito, cantidad restante = " + cantidadRestante + ", monto restante = " + montoRestante);

                        return mensajeInsert;
                    }
                }

                if (lineaDet.getpCodigoArtEmp() == null || lineaDet.getpCodigoArtEmp().equals("")) {
                    if (lineaDet.getpCodigo() != null && !lineaDet.getpCodigo().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El tipo de articulo es requerido si indica código en la linea de detalle");
                        return mensajeInsert;
                    }
                } else {
                    mensajeInsert.setStatus(Constantes.statusError);

                    pstmt = connection.prepareStatement(Querys.GET_CODIGO_ART_DOC);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, lineaDet.getpCodigoArtEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        lineaDet.setpCodigoArt(resultSet.getString(1));
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("No se ha encontrado el codigo de articulo indicado");
                        return mensajeInsert;
                    }

                    if (lineaDet.getpCodigo() == null || lineaDet.getpCodigo().equals("")) {
                        MensajeInsert mensajeArticulo = new MensajeInsert();

                        cstmt = connection.prepareCall(Querys.PR_ADD_ARTICULO_EMP);

                        cstmt.setString(1, pToken.getIdEmpresaFe());
                        cstmt.setString(2, null);
                        cstmt.setString(3, null);
                        cstmt.setString(4, lineaDet.getpDetalle());
                        cstmt.setString(5, lineaDet.getpUnidadMedidaEmp());
                        cstmt.setString(6, lineaDet.getpUnidadMedidaComercial());
                        cstmt.setString(7, lineaDet.getpMercServ());
                        cstmt.setString(8, lineaDet.getpPrecioUnitario());
                        cstmt.setString(9, null);
                        cstmt.setString(10, lineaDet.getpMontoDescuento());
                        cstmt.setString(11, "0");
                        cstmt.setString(12, null);
                        cstmt.setString(13, null);

                        cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);
                        cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
                        cstmt.registerOutParameter(16, java.sql.Types.VARCHAR);

                        cstmt.execute();

                        mensajeArticulo.setStatus(cstmt.getString(14));
                        mensajeArticulo.setMensaje(cstmt.getString(15));
                        mensajeArticulo.setId(cstmt.getString(16));

                        if (!mensajeArticulo.getStatus().equals(Constantes.statusSuccess)) {
                            connection.rollback();
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("No se ha logrado agregar de forma automática el artículo");
                            return mensajeInsert;
                        } else {
                            lineaDet.setpCodigo(mensajeArticulo.getId());
                        }

                        if (impuestos != null) {
                            for (ImpuestoAdd imp : impuestos) {

                                Exoneracion Exo = imp.getpExoneracion();

                                cstmt = connection.prepareCall(Querys.PR_ADD_IMPUESTO_ARTICULO_EMP);
                                cstmt.setString(1, lineaDet.getpCodigo());
                                cstmt.setString(2, pToken.getIdEmpresaFe());
                                cstmt.setString(3, imp.getpIdImpuestoEmp());

                                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                                cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                                cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                                cstmt.execute();

                                mensajeArticulo.setStatus(cstmt.getString(4));
                                mensajeArticulo.setMensaje(cstmt.getString(5));
                                String idImpuestoArt = cstmt.getString(6);

                                if (!mensajeArticulo.getStatus().equals(Constantes.statusSuccess)) {
                                    connection.rollback();
                                    mensajeInsert.setStatus(Constantes.statusError);
                                    mensajeInsert.setMensaje("No se ha logrado agregar el impuesto de forma automática el artículo");

                                    return mensajeInsert;
                                }
                            }
                        }
                    }
                }
                lineasFinales.add(lineaDet);
            }

            totalGravado = totalMercGrav + totalServGrav;
            totalExento = totalMercExc + totalServExc;
            totalVentaResumen = totalGravado + totalExento;
            totalVentaNetaResumen = totalVentaResumen - totalDescuentoResumen;
            totalComprobanteResumen = totalVentaNetaResumen + totalImpuestoResumen;

            if (tipoDocumento.equals("01") || tipoDocumento.equals("04")) {
                if (Math.abs(totalMediosPago - totalComprobanteResumen) > Double.valueOf("1")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El monto de los medios de pago deben cubrir la totalidad del documento");
                    return mensajeInsert;
                }
            }

            if (pDocumento.getpIdMonedaEmp() != null || pDocumento.getpTipoCambio() != null) {
                if (!pDocumento.getpIdMonedaEmp().equals("") || !pDocumento.getpTipoCambio().equals("")) {
                    if (pDocumento.getpIdMonedaEmp() == null || pDocumento.getpIdMonedaEmp().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar el id de la moneda");
                        return mensajeInsert;
                    }

                    if (pDocumento.getpTipoCambio() == null || pDocumento.getpTipoCambio().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar el tipo de cambio de la moneda");
                        return mensajeInsert;
                    }
                }

            }

            if (pDocumento.getpIdReceptorEmp() == null || pDocumento.getpIdReceptorEmp().equals("")) {
                if (pDocumento.getpNombreReceptor() != null && !pDocumento.getpNombreReceptor().equals("")) {
                    MensajeInsert mensajeReceptor = new MensajeInsert();

                    if (pDocumento.getpCorreoEnvioFac() != null && !pDocumento.getpCorreoEnvioFac().isEmpty()) {
                        correoReceptor = pDocumento.getpCorreoEnvioFac().get(0);
                    }

                    cstmt = connection.prepareCall(Querys.PR_ADD_RECEPTOR);

                    cstmt.setString(1, pDocumento.getpNombreReceptor());

                    if (pDocumento.getpTipoIdenReceptorEmp().equals(Constantes.ID_TIPO_IDENTIFICACION_EXT)) {
                        cstmt.setString(2, Constantes.ID_TIPO_IDENTIFICACION_EXT);
                        cstmt.setString(3, pDocumento.getpNumeroIdenReceptor());
                        cstmt.setString(4, pDocumento.getpNumeroIdenReceptor());
                        pDocumento.setpTipoIdenReceptorEmp(null);
                        pDocumento.setpNumeroIdenReceptor(null);
                    } else {
                        cstmt.setString(2, pDocumento.getpTipoIdenReceptorEmp());
                        cstmt.setString(3, pDocumento.getpNumeroIdenReceptor());
                        cstmt.setString(4, null);
                    }

                    cstmt.setString(5, null);
                    cstmt.setString(6, null);
                    cstmt.setString(7, null);
                    cstmt.setString(8, null);
                    cstmt.setString(9, null);
                    cstmt.setString(10, null);
                    cstmt.setString(11, null);
                    cstmt.setString(12, null);
                    cstmt.setString(13, null);
                    cstmt.setString(14, null);
                    cstmt.setString(15, correoReceptor);
                    cstmt.setString(16, null);
                    cstmt.setString(17, "E");
                    cstmt.setString(18, pToken.getIdEmpresaFe());
                    cstmt.setString(19, null);

                    cstmt.registerOutParameter(20, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(21, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeReceptor.setStatus(cstmt.getString(20));
                    mensajeReceptor.setMensaje(cstmt.getString(21));
                    mensajeReceptor.setId(cstmt.getString(22));

                    if (!mensajeReceptor.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("No se ha logrado agregar el cliente de forma automática");

                        return mensajeInsert;
                    } else {
                        pDocumento.setpIdReceptorEmp(mensajeReceptor.getId());
                    }
                }
            }

            cstmt = connection.prepareCall(Querys.PR_REGISTRA_DOCUMENTO);

            cstmt.setString(1, pDocumento.getpConsecutivo());
            cstmt.setString(2, pDocumento.getpFechaEmision());
            cstmt.setString(3, pDocumento.getpIdReceptorEmp());
            cstmt.setString(4, pDocumento.getpNombreReceptor());
            cstmt.setString(5, pDocumento.getpTipoIdenReceptorEmp());
            cstmt.setString(6, pDocumento.getpNumeroIdenReceptor());
            cstmt.setString(7, correoReceptor);
            cstmt.setString(8, pDocumento.getpEsReceptor());
            cstmt.setString(9, pDocumento.getpCondicionVentaEmp());
            cstmt.setString(10, pDocumento.getpPlazoCredito());
            cstmt.setString(11, pDocumento.getpOtroTexto());
            cstmt.setString(12, pDocumento.getpOtroContenido());
            cstmt.setString(13, pDocumento.getpTipoDocumentoEmp());
            cstmt.setString(14, pDocumento.getpEstadoDocumento());
            cstmt.setString(15, pToken.getIdEmpresaFe());
            cstmt.setString(16, pToken.getIdEmpresa());
            cstmt.setString(17, pDocumento.getpIdSucursalEmp());
            cstmt.setString(18, pDocumento.getpIdPdvEmp());
            cstmt.setString(19, pDocumento.getpSituacionDoc());
            cstmt.setString(20, pDocumento.getpIdMonedaEmp());
            cstmt.setString(21, pDocumento.getpTipoCambio());
            cstmt.setString(22, String.valueOf(totalServGrav));
            cstmt.setString(23, String.valueOf(totalMercGrav));
            cstmt.setString(24, String.valueOf(totalServExc));
            cstmt.setString(25, String.valueOf(totalMercExc));
            cstmt.setString(26, String.valueOf(totalGravado));
            cstmt.setString(27, String.valueOf(totalExento));
            cstmt.setString(28, String.valueOf(totalVentaResumen));
            cstmt.setString(29, String.valueOf(totalDescuentoResumen));
            cstmt.setString(30, String.valueOf(totalVentaNetaResumen));
            cstmt.setString(31, String.valueOf(totalImpuestoResumen));
            cstmt.setString(32, String.valueOf(totalComprobanteResumen));
            cstmt.setString(33, pDocumento.getpIdDocumentoEmp());
            cstmt.setString(34, pDocumento.getpEnviaFactura());
            cstmt.setString(35, pToken.getIdUsuario());

            cstmt.registerOutParameter(36, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(37, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(38, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(36));
            mensajeInsert.setMensaje(cstmt.getString(37));
            mensajeInsert.setId(cstmt.getString(38));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeInsert;
            }

            if (interesadosCorreo != null) {
                for (String correo : interesadosCorreo) {
                    if (correo != null && !correo.equals("")) {
                        pstmt = connection.prepareStatement(Querys.ADD_INTERESADO_CORREO, Statement.RETURN_GENERATED_KEYS);
                        pstmt.setString(1, correo);
                        pstmt.setString(2, mensajeInsert.getId());

                        pstmt.executeUpdate();
                    }
                }
            }

            /* INICIO inserts Plantilla terceros */

 /*
             * Insert Otros Texto Plantilla
             */
            if (pDocumento.getpOtrosTextoPlantilla() != null ) {
                pstmt = connection.prepareStatement(Querys.ADD_XML_VALORES_PLANTILLA, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, mensajeInsert.getId());
                pstmt.setString(2, "OTROS");
                pstmt.setString(3, pDocumento.getpOtrosTextoPlantilla());
                pstmt.setString(4, "XML");
                pstmt.setString(5, "0");
                pstmt.setString(6, "1");

                pstmt.executeUpdate();
            }

            /*
             * Insert Plantilla tercero
             */
            if (pDocumento.getpDatosTextoPlantilla() != null) {
                for (DatosTextoPlantilla datosPlan : pDocumento.getpDatosTextoPlantilla()) {
                    pstmt = connection.prepareStatement(Querys.ADD_XML_VALORES_PLANTILLA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, mensajeInsert.getId());
                    pstmt.setString(2, datosPlan.getpNombre());
                    pstmt.setString(3, datosPlan.getpValor());
                    pstmt.setString(4, datosPlan.getpTipo());
                    pstmt.setString(5, "0");
                    pstmt.setString(6, "1");

                    pstmt.executeUpdate();
                }
            }

            /* FIN inserts Plantilla terceros */

            if (referencias != null) {
                for (InformacionReferenciaAdd infoRef : referencias) {

                    pstmt = connection.prepareStatement(Querys.ADD_INFORMACION_REFERENCIA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, infoRef.getpTipoDocRef());
                    pstmt.setString(2, infoRef.getpNumero());
                    pstmt.setString(3, infoRef.getpFechaEmision());
                    pstmt.setString(4, infoRef.getpCodigo());
                    pstmt.setString(5, infoRef.getpRazon());
                    pstmt.setString(6, mensajeInsert.getId());

                    pstmt.executeUpdate();
                }
            }

            if (mediosPago != null) {
                for (MedioPagoAdd medio : mediosPago) {
                    pstmt = connection.prepareStatement(Querys.ADD_MEDIO_PAGO_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, mensajeInsert.getId());
                    pstmt.setString(2, medio.getpMedioPago());
                    pstmt.setString(3, medio.getpMedioPagoEmp());
                    pstmt.setString(4, medio.getpMonto());

                    pstmt.executeUpdate();
                }
            }

            //Cambio a lineas finales por si hay creacion de producto desde documento
            for (LineaDetalleAdd linea : lineasFinales) {
                if (linea.getpIdDocumentoDet() == null || linea.getpIdDocumentoDet().equals("")) {
                    linea.setpIdDocumentoDet(null);
                }

                pstmt = connection.prepareStatement(Querys.ACTUALIZA_PRECIO_ARTICULO);
                pstmt.setString(1, linea.getpPrecioUnitario());
                pstmt.setString(2, linea.getpCodigo());
                pstmt.setString(3, pToken.getIdEmpresaFe());
                pstmt.setString(4, pToken.getIdEmpresaFe());

                pstmt.executeUpdate();

                pstmt = connection.prepareStatement(Querys.ADD_DETALLE_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, mensajeInsert.getId());
                pstmt.setString(2, linea.getpNumeroLinea());
                pstmt.setString(3, linea.getpCodigoArt());
                pstmt.setString(4, linea.getpCodigo());
                pstmt.setString(5, linea.getpCantidad());
                pstmt.setString(6, linea.getpUnidadMedida());
                pstmt.setString(7, linea.getpUnidadMedidaComercial());
                pstmt.setString(8, linea.getpDetalle());
                pstmt.setString(9, linea.getpPrecioUnitario());
                pstmt.setString(10, linea.getpMontoTotal());
                pstmt.setString(11, linea.getpMontoDescuento());
                pstmt.setString(12, linea.getpNaturalezaDescuento());
                pstmt.setString(13, linea.getpSubTotal());
                pstmt.setString(14, linea.getpMontoTotalLinea());
                pstmt.setString(15, linea.getpUnidadMedidaEmp());
                pstmt.setString(16, linea.getpCodigoArtEmp());
                pstmt.setString(17, linea.getpMercServ());
                pstmt.setString(18, linea.getpCantidad());
                pstmt.setString(19, linea.getpMontoTotal());
                pstmt.setString(20, linea.getpIdDocumentoDet());

                pstmt.executeUpdate();

                resultSet = pstmt.getGeneratedKeys();

                if (resultSet.next()) {
                    idLineaDetalle = resultSet.getInt(1);
                }

                impuestos = linea.getpImpuesto();

                if (impuestos != null) {
                    for (ImpuestoAdd imp : impuestos) {
                        exoneracion = imp.getpExoneracion();

                        if (exoneracion == null) {
                            exoneracion = new Exoneracion();
                        }

                        pstmt = connection.prepareStatement(Querys.ADD_IMPUESTO_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                        pstmt.setString(1, String.valueOf(idLineaDetalle));
                        pstmt.setString(2, imp.getpCodigo());
                        pstmt.setString(3, imp.getpTarifa());
                        pstmt.setString(4, imp.getpMonto());
                        pstmt.setString(5, imp.getpIdImpuestoEmp());
                        pstmt.setString(6, exoneracion.getpTipoDocumento());
                        pstmt.setString(7, exoneracion.getpNumeroDocumento());
                        pstmt.setString(8, exoneracion.getpNombreInstitucion());
                        pstmt.setString(9, exoneracion.getpFechaEmision());
                        pstmt.setString(10, exoneracion.getpMontoImpuesto());
                        pstmt.setString(11, exoneracion.getpPorcentajeCompra());
                        pstmt.setString(12, exoneracion.getpTipoDocumentoEmp());

                        pstmt.executeUpdate();

                        resultSet = pstmt.getGeneratedKeys();

                        if (resultSet.next()) {
                            idImpuesto = resultSet.getInt(1);
                        }
                    }
                }
            }

            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }

            cstmt = connection.prepareCall(Querys.PR_GENERA_CLAVE_CONSECUTIVO);

            cstmt.setString(1, "506");
            cstmt.setString(2, mensajeInsert.getId());
            cstmt.setString(3, pDocumento.getpIdSucursalEmp());
            cstmt.setString(4, pDocumento.getpIdPdvEmp());
            cstmt.setString(5, pDocumento.getpTipoDocumentoEmp());
            cstmt.setString(6, pDocumento.getpSituacionDoc());
            cstmt.setString(7, pDocumento.getpFechaEmision());
            cstmt.setString(8, pDocumento.getpConsecutivo());
            cstmt.setString(9, pToken.getIdEmpresaFe());

            cstmt.registerOutParameter(10, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(11, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(10));
            mensajeInsert.setMensaje(cstmt.getString(11));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                mensajeInsert.setId(null);

                return mensajeInsert;
            }

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeInsert updateDataDocumentoPlantilla(String pIdEmpresaFe, UpdateDataDocumentoPlantilla pDocumento) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();
        String pIdDocumento = null;

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_X_CLAVE);
            pstmt.setString(1, pDocumento.getpClaveDocumento());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                pIdDocumento = resultSet.getString(1);
            }

            if (pIdDocumento == null) {
                mensajeInsert.setStatus(Constantes.statusSuccess);
                mensajeInsert.setMensaje("La clave del documento enviado");
                return mensajeInsert;
            }

            /* INICIO inserts Plantilla terceros */

            /*
             * Insert Otros Texto Plantilla
             */
            if (pDocumento.getpOtrosTextoPlantilla() != null && !pDocumento.getpOtrosTextoPlantilla().equals("")) {
                pstmt = connection.prepareStatement(Querys.ADD_XML_VALORES_PLANTILLA, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, pIdDocumento);
                pstmt.setString(2, "OTROS");
                pstmt.setString(3, pDocumento.getpOtrosTextoPlantilla());
                pstmt.setString(4, "XML");
                pstmt.setString(5, "0");
                pstmt.setString(6, "1");

                pstmt.executeUpdate();
            }

            /*
             * Insert Plantilla tercero
             */
            if (pDocumento.getpDatosTextoPlantilla() != null) {
                for (DatosTextoPlantilla datosPlan : pDocumento.getpDatosTextoPlantilla()) {
                    pstmt = connection.prepareStatement(Querys.ADD_XML_VALORES_PLANTILLA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, pIdDocumento);
                    pstmt.setString(2, datosPlan.getpNombre());
                    pstmt.setString(3, datosPlan.getpValor());
                    pstmt.setString(4, datosPlan.getpTipo());
                    pstmt.setString(5, "0");
                    pstmt.setString(6, "1");

                    pstmt.executeUpdate();
                }
            }

            pstmt = connection.prepareStatement(Querys.ACTUALIZA_ESTADO_DOCUMENTO_PLANTILLA);
            pstmt.setString(1, pDocumento.getpEstado());
            pstmt.setString(2, pIdDocumento);

            int result = pstmt.executeUpdate();

            if (result == 1) {
                mensajeInsert.setStatus(Constantes.statusSuccess);
                mensajeInsert.setMensaje("Operacion completada");
                return mensajeInsert;
            } else {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Ocurrio un error completando la operacion");
                return mensajeInsert;
            }

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
    }

    @Override
    public MensajeInsert registraDocumentoTerceros(EmisorToken pToken, DocumentoAdd pDocumento) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();
        Double montoDescuento;
        Double totalServGrav;
        Double totalServExc;
        Double totalMercGrav;
        Double totalMercExc;
        Double totalGravado;
        Double totalExento;
        Double totalVentaResumen;
        Double totalDescuentoResumen;
        Double totalVentaNetaResumen;
        Double totalImpuestoResumen;
        Double totalComprobanteResumen;
        Double totalMediosPago;
        List<ImpuestoAdd> impuestos;
        List<InformacionReferenciaAdd> referencias;
        List<LineaDetalleAdd> lineas;
        List<LineaDetalleAdd> lineasFinales = new ArrayList<>();
        List<MedioPagoAdd> mediosPago;
        List<String> interesadosCorreo;
        Exoneracion exoneracion;
        boolean tieneImpuestos;
        int idLineaDetalle = 0;
        int idImpuesto;
        String tipoDocumento = null;
        boolean revisarMontos = false;
        String claveDocumentoRef = "";
        String correoReceptor = null;

        try {
            connection = dataSource.getConnection();

            connection.setAutoCommit(false);

            totalServGrav = Double.valueOf(0);
            totalServExc = Double.valueOf(0);
            totalMercGrav = Double.valueOf(0);
            totalMercExc = Double.valueOf(0);
            totalGravado = Double.valueOf(0);
            totalExento = Double.valueOf(0);
            totalVentaResumen = Double.valueOf(0);
            totalDescuentoResumen = Double.valueOf(0);
            totalVentaNetaResumen = Double.valueOf(0);
            totalImpuestoResumen = Double.valueOf(0);
            totalComprobanteResumen = Double.valueOf(0);
            totalMediosPago = Double.valueOf(0);

            if (pDocumento.getpTipoDocumentoEmp() == null || pDocumento.getpTipoDocumentoEmp().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El tipo de documento es requerido");
                return mensajeInsert;
            }

            if (pDocumento.getpClave() == null || pDocumento.getpClave().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("La clave del documento es requerida");
                return mensajeInsert;
            }

            if (pDocumento.getpConsecutivo() == null || pDocumento.getpConsecutivo().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El consecutivo es requerido");
                return mensajeInsert;
            }

            tipoDocumento = getTipoDocumento(pToken.getIdEmpresaFe(), pDocumento.getpTipoDocumentoEmp());
            referencias = pDocumento.getpInformacionReferencia();
            mediosPago = pDocumento.getpMediosPagoEmp();

            if (null == tipoDocumento) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("No se ha encontrado el tipo de documento mediante el id indicado " + pDocumento.getpTipoDocumentoEmp());
                return mensajeInsert;
            } else {
                switch (tipoDocumento) {
                    case "02":
                    case "03":
                        if (referencias == null || referencias.isEmpty()) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Debe indicar al menos un documento de referencia");
                            return mensajeInsert;
                        }
                        break;
                    case "01":
                    case "04":
                        if (mediosPago == null || mediosPago.isEmpty()) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Debe indicar al menos un medio de pago");
                            return mensajeInsert;
                        }
                        break;
                    default:
                        break;
                }
            }

            interesadosCorreo = pDocumento.getpCorreoEnvioFac();

            if (pDocumento.getpEnviaFactura() == null || !pDocumento.getpEnviaFactura().equals("1")) {
                if (interesadosCorreo != null && !interesadosCorreo.isEmpty()) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar el parametro de enviar documento en 1 si desea notificar al cliente");
                    return mensajeInsert;
                }
            } else if (pDocumento.getpEnviaFactura().equals("1")) {
                if (interesadosCorreo == null || interesadosCorreo.isEmpty()) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Debe indicar a cuales correos desea notificar al cliente");
                    return mensajeInsert;
                }
            }

            if (referencias != null) {
                for (InformacionReferenciaAdd infoRef : referencias) {
                    mensajeInsert.setStatus(Constantes.statusError);

                    if (infoRef.getpNumero() == null || infoRef.getpNumero().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El numero de documento de referencia es requerido");
                        return mensajeInsert;
                    }

                    if (infoRef.getpCodigo() == null || infoRef.getpCodigo().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El codigo en la informacion de referencia es requerido");
                        return mensajeInsert;
                    }

                    if (infoRef.getpExisteDb() == null || infoRef.getpExisteDb().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar si el documento de referencia existe en el sistema");
                        return mensajeInsert;
                    } else if (!infoRef.getpExisteDb().equals("1") && !infoRef.getpExisteDb().equals("0")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Solo se permiten los valores 1 y 0 para indicar si el documento referencia existe en la base de datos");
                        return mensajeInsert;
                    }

                    if (infoRef.getpExisteDb().equals("1")) {
                        if ((tipoDocumento.equals("03")) && (infoRef.getpCodigo().equals("01") || infoRef.getpCodigo().equals("03"))) { //tipoDocumento.equals("02") ||
                            revisarMontos = true;
                            claveDocumentoRef = infoRef.getpNumero();
                        }
                    }

                    if (infoRef.getpRazon() == null || infoRef.getpRazon().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("La razon en la informacion de referencia es requerido");
                        return mensajeInsert;
                    }

                    pstmt = connection.prepareStatement(Querys.GET_TIPO_DOCUMENTO_REFERENCIA);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, infoRef.getpTipoDocRefEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        infoRef.setpTipoDocRef(resultSet.getString(1));
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("No se ha encontrado el tipo de documento de referencia");
                        return mensajeInsert;
                    }
                }
            }

            if (mediosPago != null) {
                for (MedioPagoAdd medio : mediosPago) {
                    mensajeInsert.setStatus(Constantes.statusError);

                    if (medio.getpMedioPagoEmp() == null || medio.getpMedioPagoEmp().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El id del medio de pago de la empresa es requerido");
                        return mensajeInsert;
                    }

                    if (medio.getpMonto() == null || medio.getpMonto().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("El monto que cubre el medio de pago es requerido");
                        return mensajeInsert;
                    }

                    String comprobanteRequerido = "";

                    pstmt = connection.prepareStatement(Querys.GET_MEDIO_PAGO);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, medio.getpMedioPagoEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        medio.setpMedioPago(resultSet.getString(1));

                        comprobanteRequerido = resultSet.getString(2);
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("No se ha encontrado el medio de pago mediante el id " + medio.getpMedioPagoEmp());
                        return mensajeInsert;
                    }

//                    Fix para no solicitar comprobante medio de pago
//                    if (comprobanteRequerido.equals("1")  && (medio.getpComprobante() == null || medio.getpComprobante().equals(""))) {
//                        mensajeInsert.setStatus(Constantes.statusError);
//                        mensajeInsert.setMensaje("El comprobante es requerido para el medio de pago id " + medio.getpMedioPagoEmp());
//                        return mensajeInsert;
//                    }
                    totalMediosPago = totalMediosPago + Double.valueOf(medio.getpMonto());
                }
            }

            lineas = pDocumento.getpLineasDetalle();

            if (lineas == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos una linea de detalle");
                return mensajeInsert;
            }

            for (LineaDetalleAdd lineaDet : lineas) {
                mensajeInsert.setStatus(Constantes.statusError);

                tieneImpuestos = false;

                if (lineaDet.getpIdArticuloTercero() == null || lineaDet.getpIdArticuloTercero().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - Debe indicar el id articulo tercero");
                    return mensajeInsert;
                }

                //*** Inicio ***
                //Reviso si idArticulo Tercero esta registrado
                pstmt = connection.prepareStatement(Querys.GET_ARTICULO_TERCERO);

                pstmt.setString(1, lineaDet.getpIdArticuloTercero());
                pstmt.setString(2, pToken.getIdEmpresaFe());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    lineaDet.setpCodigo(resultSet.getString(1));
                }
                //*** Fin ***

                if (lineaDet.getpMercServ() == null) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - Debe indicar si el detalle es un producto o servicio (M/S)");
                    return mensajeInsert;
                }

                if (!lineaDet.getpMercServ().equals("M") && !lineaDet.getpMercServ().equals("S")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - Debe indicar si el detalle es un producto o servicio (M/S)");
                    return mensajeInsert;
                }

                if (lineaDet.getpNumeroLinea() == null || lineaDet.getpNumeroLinea().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El numero de linea en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpCantidad() == null || lineaDet.getpCantidad().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - La cantidad en la linea de detalle es requerida");
                    return mensajeInsert;
                }

                if (lineaDet.getpUnidadMedidaEmp() == null || lineaDet.getpUnidadMedidaEmp().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - La unidad de medida en la linea de detalle es requerida");
                    return mensajeInsert;
                }

                mensajeInsert.setStatus(Constantes.statusError);

                pstmt = connection.prepareStatement(Querys.GET_UNIDAD_MEDIDA_DOC);

                pstmt.setString(1, pToken.getIdEmpresaFe());
                pstmt.setString(2, lineaDet.getpUnidadMedidaEmp());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    mensajeInsert.setStatus(Constantes.statusSuccess);

                    lineaDet.setpUnidadMedida(resultSet.getString(1));
                }

                if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha encontrado la unidad de medida indicada para la linea de detalle");
                    return mensajeInsert;
                }

                if (lineaDet.getpDetalle() == null || lineaDet.getpDetalle().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El detalle en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpPrecioUnitario() == null || lineaDet.getpPrecioUnitario().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El precio unitario en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoDescuento() != null || lineaDet.getpNaturalezaDescuento() != null) {
                    if (!lineaDet.getpMontoDescuento().equals("") || !lineaDet.getpNaturalezaDescuento().equals("")) {
                        if (lineaDet.getpMontoDescuento() == null || lineaDet.getpMontoDescuento().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El monto de descuento es requerido si indica naturaleza descuento en la linea de detalle");
                            return mensajeInsert;
                        }

                        if (lineaDet.getpNaturalezaDescuento() == null || lineaDet.getpNaturalezaDescuento().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El naturaleza de descuento es requerido si indica monto descuento en la linea de detalle");
                            return mensajeInsert;
                        }
                    }
                }

                if (lineaDet.getpMontoTotal() == null || lineaDet.getpMontoTotal().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El monto total en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpSubTotal() == null || lineaDet.getpSubTotal().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El subtotal en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoTotalLinea() == null || lineaDet.getpMontoTotalLinea().equals("")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El monto total de la linea en la linea de detalle es requerido");
                    return mensajeInsert;
                }

                if (lineaDet.getpMontoDescuento() == null || lineaDet.getpMontoDescuento().equals("")) {
                    montoDescuento = Double.valueOf(0);
                    lineaDet.setpMontoDescuento(String.valueOf(montoDescuento));
                } else {
                    montoDescuento = Double.valueOf(lineaDet.getpMontoDescuento());
                }

                impuestos = lineaDet.getpImpuesto();

                if (impuestos != null) {
                    for (ImpuestoAdd imp : impuestos) {
                        mensajeInsert.setStatus(Constantes.statusError);

                        tieneImpuestos = true;

                        if (imp.getpIdImpuestoEmp() == null || imp.getpIdImpuestoEmp().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El id del impuesto es requerido");
                            return mensajeInsert;
                        }

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_FACTURA);

                        pstmt.setString(1, pToken.getIdEmpresaFe());
                        pstmt.setString(2, imp.getpIdImpuestoEmp());

                        resultSet = pstmt.executeQuery();

                        while (resultSet.next()) {
                            mensajeInsert.setStatus(Constantes.statusSuccess);

                            imp.setpCodigo(resultSet.getString(1));
                        }

                        if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha encontrado el impuesto indicado");
                            return mensajeInsert;
                        }

                        if (imp.getpTarifa() == null || imp.getpTarifa().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El codigo del impuesto es requerido");
                            return mensajeInsert;
                        }

                        if (imp.getpMonto() == null || imp.getpMonto().equals("")) {
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El monto del impuesto es requerido");
                            return mensajeInsert;
                        }

                        totalImpuestoResumen = totalImpuestoResumen + Double.valueOf(imp.getpMonto());

                        exoneracion = imp.getpExoneracion();

                        if (exoneracion != null) {
                            if (exoneracion.getpTipoDocumentoEmp() == null || exoneracion.getpTipoDocumentoEmp().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El tipo de documento de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            mensajeInsert.setStatus(Constantes.statusError);

                            pstmt = connection.prepareStatement(Querys.GET_EXONERACION_DOCUMENTO);

                            pstmt.setString(1, pToken.getIdEmpresaFe());
                            pstmt.setString(2, exoneracion.getpTipoDocumentoEmp());

                            resultSet = pstmt.executeQuery();

                            while (resultSet.next()) {
                                mensajeInsert.setStatus(Constantes.statusSuccess);

                                exoneracion.setpTipoDocumento(resultSet.getString(1));
                            }

                            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha encontrado la exoneracion indicada");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpNumeroDocumento() == null || exoneracion.getpNumeroDocumento().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El numero de documento de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpNombreInstitucion() == null || exoneracion.getpNombreInstitucion().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El nombre de la institucion que emitio la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpFechaEmision() == null || exoneracion.getpFechaEmision().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - La fecha de emision de la exoneracion es requerida");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpMontoImpuesto() == null || exoneracion.getpMontoImpuesto().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El monto del impuesto exonerado de la exoneracion es requerido");
                                return mensajeInsert;
                            }

                            if (exoneracion.getpPorcentajeCompra() == null || exoneracion.getpPorcentajeCompra().equals("")) {
                                mensajeInsert.setStatus(Constantes.statusError);
                                mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El porcentaje de compra exonerado de la exoneracion es requerido");
                                return mensajeInsert;
                            }
                        }
                    }
                }

                if (tieneImpuestos) {
                    if (lineaDet.getpMercServ().equals("M")) {
                        totalMercGrav = totalMercGrav + Double.valueOf(lineaDet.getpMontoTotal());
                    } else if (lineaDet.getpMercServ().equals("S")) {
                        totalServGrav = totalServGrav + Double.valueOf(lineaDet.getpMontoTotal());
                    }
                } else {
                    if (lineaDet.getpMercServ().equals("M")) {
                        totalMercExc = totalMercExc + Double.valueOf(lineaDet.getpMontoTotal());
                    } else if (lineaDet.getpMercServ().equals("S")) {
                        totalServExc = totalServExc + Double.valueOf(lineaDet.getpMontoTotal());
                    }
                }

                totalDescuentoResumen = totalDescuentoResumen + montoDescuento;

                if (revisarMontos) {
                    if (lineaDet.getpIdDocumentoDet() == null || lineaDet.getpIdDocumentoDet().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar a cual linea hace referencia para verificar montos");
                        return mensajeInsert;
                    }

                    cstmt = connection.prepareCall(Querys.PR_VALIDA_DOC_ANTERIORES);

                    cstmt.setString(1, lineaDet.getpIdDocumentoDet());
                    cstmt.setString(2, lineaDet.getpCantidad());
                    cstmt.setString(3, lineaDet.getpPrecioUnitario());

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeInsert.setStatus(cstmt.getString(4));
                    mensajeInsert.setMensaje(cstmt.getString(5));
                    String cantidadRestante = cstmt.getString(6);
                    String montoRestante = cstmt.getString(7);

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        mensajeInsert.setMensaje("No se puede agregar el producto/servicio " + lineaDet.getpDetalle() + ", ya que la cantidad o monto indicados superan al monto y cantidad restantes para aplicar Notas de Crédito, cantidad restante = " + cantidadRestante + ", monto restante = " + montoRestante);

                        return mensajeInsert;
                    }
                }

                if (lineaDet.getpCodigoArtEmp() == null || lineaDet.getpCodigoArtEmp().equals("")) {
                    if (lineaDet.getpCodigo() != null && !lineaDet.getpCodigo().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - El tipo de articulo es requerido si indica código en la linea de detalle");
                        return mensajeInsert;
                    }
                } else {

                    mensajeInsert.setStatus(Constantes.statusError);

                    pstmt = connection.prepareStatement(Querys.GET_CODIGO_ART_DOC);

                    pstmt.setString(1, pToken.getIdEmpresaFe());
                    pstmt.setString(2, lineaDet.getpCodigoArtEmp());

                    resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        mensajeInsert.setStatus(Constantes.statusSuccess);

                        lineaDet.setpCodigoArt(resultSet.getString(1));
                    }

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha encontrado el codigo de articulo indicado");
                        return mensajeInsert;
                    }

                    if (lineaDet.getpCodigo() == null || lineaDet.getpCodigo().equals("")) {

                        MensajeInsert mensajeArticulo = new MensajeInsert();

                        cstmt = connection.prepareCall(Querys.PR_ADD_ARTICULO_EMP_TERCERO);

                        cstmt.setString(1, pToken.getIdEmpresaFe());
                        cstmt.setString(2, null);
                        cstmt.setString(3, null);
                        cstmt.setString(4, lineaDet.getpDetalle());
                        cstmt.setString(5, lineaDet.getpUnidadMedidaEmp());
                        cstmt.setString(6, lineaDet.getpUnidadMedidaComercial());
                        cstmt.setString(7, lineaDet.getpMercServ());
                        cstmt.setString(8, lineaDet.getpPrecioUnitario());
                        cstmt.setString(9, null);
                        cstmt.setString(10, lineaDet.getpMontoDescuento());
                        cstmt.setString(11, "0");
                        cstmt.setString(12, null);
                        cstmt.setString(13, null);

                        cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);
                        cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
                        cstmt.registerOutParameter(16, java.sql.Types.VARCHAR);

                        cstmt.setString(17, lineaDet.getpIdArticuloTercero());

                        cstmt.execute();

                        mensajeArticulo.setStatus(cstmt.getString(14));
                        mensajeArticulo.setMensaje(cstmt.getString(15));
                        mensajeArticulo.setId(cstmt.getString(16));

                        if (!mensajeArticulo.getStatus().equals(Constantes.statusSuccess)) {
                            connection.rollback();
                            mensajeInsert.setStatus(Constantes.statusError);
                            mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha logrado agregar de forma automática el artículo");
                            return mensajeInsert;
                        } else {
                            lineaDet.setpCodigo(mensajeArticulo.getId());
                        }

                        if (impuestos != null) {
                            for (ImpuestoAdd imp : impuestos) {
                                cstmt = connection.prepareCall(Querys.PR_ADD_IMPUESTO_ARTICULO_EMP);
                                cstmt.setString(1, lineaDet.getpCodigo());
                                cstmt.setString(2, pToken.getIdEmpresaFe());
                                cstmt.setString(3, imp.getpIdImpuestoEmp());

                                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                                cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                                cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                                cstmt.execute();

                                mensajeArticulo.setStatus(cstmt.getString(4));
                                mensajeArticulo.setMensaje(cstmt.getString(5));
                                String idImpuestoArt = cstmt.getString(6);

                                if (!mensajeArticulo.getStatus().equals(Constantes.statusSuccess)) {
                                    connection.rollback();
                                    mensajeInsert.setStatus(Constantes.statusError);
                                    mensajeInsert.setMensaje("Linea N " + lineaDet.getpNumeroLinea() + " - No se ha logrado agregar el impuesto de forma automática el artículo");

                                    return mensajeInsert;
                                }
                            }
                        }
                    }
                }
                lineasFinales.add(lineaDet);
            }

            totalGravado = totalMercGrav + totalServGrav;
            totalExento = totalMercExc + totalServExc;
            totalVentaResumen = totalGravado + totalExento;
            totalVentaNetaResumen = totalVentaResumen - totalDescuentoResumen;
            totalComprobanteResumen = totalVentaNetaResumen + totalImpuestoResumen;

            if (tipoDocumento.equals("01") || tipoDocumento.equals("04")) {
                if (Math.abs(totalMediosPago - totalComprobanteResumen) > Double.valueOf("1")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El monto de los medios de pago deben cubrir la totalidad del documento");
                    return mensajeInsert;
                }
            }

            if (pDocumento.getpIdMonedaEmp() != null || pDocumento.getpTipoCambio() != null) {
                if (!pDocumento.getpIdMonedaEmp().equals("") || !pDocumento.getpTipoCambio().equals("")) {
                    if (pDocumento.getpIdMonedaEmp() == null || pDocumento.getpIdMonedaEmp().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar el id de la moneda");
                        return mensajeInsert;
                    }

                    if (pDocumento.getpTipoCambio() == null || pDocumento.getpTipoCambio().equals("")) {
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("Debe indicar el tipo de cambio de la moneda");
                        return mensajeInsert;
                    }
                }

            }

            if (pDocumento.getpNombreReceptor() == null && pDocumento.getpNombreReceptor().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El nombre del receptor es obligatorio");
                return mensajeInsert;
            }

            if (pDocumento.getpNumeroIdenReceptor() == null && pDocumento.getpNumeroIdenReceptor().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El numero del receptor es obligarotiro");
                return mensajeInsert;
            }
            
            if (pDocumento.getpCorreoEnvioFac() != null && !pDocumento.getpCorreoEnvioFac().isEmpty()) {
                    correoReceptor = pDocumento.getpCorreoEnvioFac().get(0);
            }
            
            MensajeInsert mensajeReceptor = new MensajeInsert();
            cstmt = connection.prepareCall(Querys.PR_RECEPTOR_TERCERO);
            
            cstmt.setString(1, (pDocumento.getpIdReceptorEmp().equals("")) ? null :  pDocumento.getpIdReceptorEmp());
            cstmt.setString(2, pToken.getIdEmpresaFe());
            cstmt.setString(3, null);
            cstmt.setString(4, pDocumento.getpNombreReceptor());
            cstmt.setString(5, null);

            if (pDocumento.getpTipoIdenReceptorEmp().equals(Constantes.ID_TIPO_IDENTIFICACION_EXT)) {
                cstmt.setString(6, Constantes.ID_TIPO_IDENTIFICACION_EXT);
                cstmt.setString(7, pDocumento.getpNumeroIdenReceptor());
                cstmt.setString(8, pDocumento.getpNumeroIdenReceptor());
                pDocumento.setpTipoIdenReceptorEmp(null);
                pDocumento.setpNumeroIdenReceptor(null);
            } else {

                cstmt.setString(6, pDocumento.getpTipoIdenReceptorEmp());
                cstmt.setString(7, pDocumento.getpNumeroIdenReceptor());
                cstmt.setString(8, null);
            }

            cstmt.setString(9, null);
            cstmt.setString(10, null);
            cstmt.setString(11, null);
            cstmt.setString(12, null);
            cstmt.setString(13, null);
            cstmt.setString(14, null);
            cstmt.setString(15, null);
            cstmt.setString(16, null);
            cstmt.setString(17, correoReceptor);
            cstmt.setString(18, "E");
            cstmt.setString(19, null);
            cstmt.setString(20, null);

            cstmt.registerOutParameter(21, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(23, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(24, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeReceptor.setId(cstmt.getString(21));
            mensajeReceptor.setStatus(cstmt.getString(22));
            mensajeReceptor.setMensaje(cstmt.getString(23));
            pDocumento.setpIdPlantillaXml(cstmt.getString(24));


            if (!mensajeReceptor.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje(mensajeReceptor.getMensaje());

                return mensajeInsert;
            } else {
                pDocumento.setpIdReceptorEmp(mensajeReceptor.getId());
            }



       /* if (pDocumento.getpIdReceptorEmp() == null || pDocumento.getpIdReceptorEmp().equals("")) {

               //INICIO CREACION DE RECEPTOR

                //  Inicio ** Reviso si cliente Existe
                //
             pstmt = connection.prepareStatement(Querys.GET_CLIENTE_TERCERO);

                pstmt.setString(1, pDocumento.getpNumeroIdenReceptor());
                pstmt.setString(2, pToken.getIdEmpresaFe());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    pDocumento.setpIdReceptorEmp(resultSet.getString(1));
                }
                //** Fin ** Reviso si cliente Existe

                MensajeInsert mensajeReceptor = new MensajeInsert();

                if (pDocumento.getpCorreoEnvioFac() != null && !pDocumento.getpCorreoEnvioFac().isEmpty()) {
                    correoReceptor = pDocumento.getpCorreoEnvioFac().get(0);
                }

                if (pDocumento.getpIdReceptorEmp() == null || pDocumento.getpIdReceptorEmp().equals("")) {
                    // Se crea cliente si no fue encontrado con su cedula en la empresa

                    cstmt = connection.prepareCall(Querys.PR_ADD_RECEPTOR);
                    cstmt.setString(1, pDocumento.getpNombreReceptor());

                    if (pDocumento.getpTipoIdenReceptorEmp().equals(Constantes.ID_TIPO_IDENTIFICACION_EXT)) {
                        cstmt.setString(2, Constantes.ID_TIPO_IDENTIFICACION_EXT);
                        cstmt.setString(3, pDocumento.getpNumeroIdenReceptor());
                        cstmt.setString(4, pDocumento.getpNumeroIdenReceptor());
                        pDocumento.setpTipoIdenReceptorEmp(null);
                        pDocumento.setpNumeroIdenReceptor(null);
                    } else {
                        cstmt.setString(2, pDocumento.getpTipoIdenReceptorEmp());
                        cstmt.setString(3, pDocumento.getpNumeroIdenReceptor());
                        cstmt.setString(4, null);
                    }

                    cstmt.setString(5, null);
                    cstmt.setString(6, null);
                    cstmt.setString(7, null);
                    cstmt.setString(8, null);
                    cstmt.setString(9, null);
                    cstmt.setString(10, null);
                    cstmt.setString(11, null);
                    cstmt.setString(12, null);
                    cstmt.setString(13, null);
                    cstmt.setString(14, null);
                    cstmt.setString(15, correoReceptor);
                    cstmt.setString(16, null);
                    cstmt.setString(17, "E");
                    cstmt.setString(18, pToken.getIdEmpresaFe());
                    cstmt.setString(19, null);

                    cstmt.registerOutParameter(20, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(21, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(22, java.sql.Types.VARCHAR);

                    cstmt.execute();
 
                    
                    mensajeReceptor.setStatus(cstmt.getString(20));
                    mensajeReceptor.setMensaje(cstmt.getString(21));
                    mensajeReceptor.setId(cstmt.getString(22));

                    if (!mensajeReceptor.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();
                        mensajeInsert.setStatus(Constantes.statusError);
                        mensajeInsert.setMensaje("No se ha logrado agregar el cliente de forma automática");

                        return mensajeInsert;
                    } else {
                        pDocumento.setpIdReceptorEmp(mensajeReceptor.getId());
                    }
                }
            }*/


            // LLAMO AL PROCEDIMIENTO QUE SE ENCARGA DE CREAR O ACTUALIZAR EL CLIENTE






            /* FIN CREACION DE RECEPTOR */
            cstmt = connection.prepareCall(Querys.PR_REGISTRA_DOCUMENTO);

            cstmt.setString(1, pDocumento.getpConsecutivo());
            cstmt.setString(2, pDocumento.getpFechaEmision());
            cstmt.setString(3, pDocumento.getpIdReceptorEmp());
            cstmt.setString(4, pDocumento.getpNombreReceptor());
            cstmt.setString(5, pDocumento.getpTipoIdenReceptorEmp());
            cstmt.setString(6, pDocumento.getpNumeroIdenReceptor());
            cstmt.setString(7, correoReceptor);
            cstmt.setString(8, pDocumento.getpEsReceptor());
            cstmt.setString(9, pDocumento.getpCondicionVentaEmp());
            cstmt.setString(10, pDocumento.getpPlazoCredito());
            cstmt.setString(11, pDocumento.getpOtroTexto());
            cstmt.setString(12, pDocumento.getpOtroContenido());
            cstmt.setString(13, pDocumento.getpTipoDocumentoEmp());
            /* If estado documento segun plantilla */

            if(pDocumento.getpIdReceptorEmp() != null 
                    && !pDocumento.getpIdReceptorEmp().equals("")){
                
                List<Plantilla> listaPlantillas = getPlantillasCliente("0", pDocumento.getpIdReceptorEmp(), "0").getPlantilas();
                if(listaPlantillas != null && !listaPlantillas.isEmpty()){
                    cstmt.setString(14, "PR");
                }else{
                    cstmt.setString(14, pDocumento.getpEstadoDocumento());
                } 
            }else{
                 cstmt.setString(14, pDocumento.getpEstadoDocumento());
            }
            
            /*Busco las plantillas del cliente .*/
       
            
         
          /*  if(pDocumento.getpIdPlantillaXml() != null
                    && !pDocumento.getpIdPlantillaXml().equals("") && !pDocumento.getpIdPlantillaXml().equals("-1")){

                cstmt.setString(14, "PR");

            }else{
                cstmt.setString(14, pDocumento.getpEstadoDocumento());
            }*/


            
            cstmt.setString(15, pToken.getIdEmpresaFe());
            cstmt.setString(16, pToken.getIdEmpresa());
            cstmt.setString(17, pDocumento.getpIdSucursalEmp());
            cstmt.setString(18, pDocumento.getpIdPdvEmp());
            cstmt.setString(19, pDocumento.getpSituacionDoc());
            cstmt.setString(20, pDocumento.getpIdMonedaEmp());
            cstmt.setString(21, pDocumento.getpTipoCambio());
            cstmt.setString(22, String.valueOf(totalServGrav));
            cstmt.setString(23, String.valueOf(totalMercGrav));
            cstmt.setString(24, String.valueOf(totalServExc));
            cstmt.setString(25, String.valueOf(totalMercExc));
            cstmt.setString(26, String.valueOf(totalGravado));
            cstmt.setString(27, String.valueOf(totalExento));
            cstmt.setString(28, String.valueOf(totalVentaResumen));
            cstmt.setString(29, String.valueOf(totalDescuentoResumen));
            cstmt.setString(30, String.valueOf(totalVentaNetaResumen));
            cstmt.setString(31, String.valueOf(totalImpuestoResumen));
            cstmt.setString(32, String.valueOf(totalComprobanteResumen));
            cstmt.setString(33, pDocumento.getpIdDocumentoEmp());
            cstmt.setString(34, pDocumento.getpEnviaFactura());
            cstmt.setString(35, pToken.getIdUsuario());

            cstmt.registerOutParameter(36, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(37, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(38, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(36));
            mensajeInsert.setMensaje(cstmt.getString(37));
            mensajeInsert.setId(cstmt.getString(38));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeInsert;
            }

            if (interesadosCorreo != null) {
                for (String correo : interesadosCorreo) {
                    if (correo != null && !correo.equals("")) {
                        pstmt = connection.prepareStatement(Querys.ADD_INTERESADO_CORREO, Statement.RETURN_GENERATED_KEYS);
                        pstmt.setString(1, correo);
                        pstmt.setString(2, mensajeInsert.getId());

                        pstmt.executeUpdate();
                    }
                }
            }

            if (referencias != null) {
                for (InformacionReferenciaAdd infoRef : referencias) {
                    pstmt = connection.prepareStatement(Querys.ADD_INFORMACION_REFERENCIA, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, infoRef.getpTipoDocRef());
                    pstmt.setString(2, infoRef.getpNumero());
                    pstmt.setString(3, infoRef.getpFechaEmision());
                    pstmt.setString(4, infoRef.getpCodigo());
                    pstmt.setString(5, infoRef.getpRazon());
                    pstmt.setString(6, mensajeInsert.getId());

                    pstmt.executeUpdate();
                }
            }

            if (mediosPago != null) {
                for (MedioPagoAdd medio : mediosPago) {
                    pstmt = connection.prepareStatement(Querys.ADD_MEDIO_PAGO_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, mensajeInsert.getId());
                    pstmt.setString(2, medio.getpMedioPago());
                    pstmt.setString(3, medio.getpMedioPagoEmp());
                    pstmt.setString(4, medio.getpMonto());

                    pstmt.executeUpdate();
                }
            }

            //Cambio a lineas finales por si hay creacion de producto desde documento
            for (LineaDetalleAdd linea : lineasFinales) {
                if (linea.getpIdDocumentoDet() == null || linea.getpIdDocumentoDet().equals("")) {
                    linea.setpIdDocumentoDet(null);
                }

                pstmt = connection.prepareStatement(Querys.ACTUALIZA_PRECIO_ARTICULO);
                pstmt.setString(1, linea.getpPrecioUnitario());
                pstmt.setString(2, linea.getpCodigo());
                pstmt.setString(3, pToken.getIdEmpresaFe());
                pstmt.setString(4, pToken.getIdEmpresaFe());

                pstmt.executeUpdate();

                pstmt = connection.prepareStatement(Querys.ADD_DETALLE_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, mensajeInsert.getId());
                pstmt.setString(2, linea.getpNumeroLinea());
                pstmt.setString(3, linea.getpCodigoArt());
                pstmt.setString(4, linea.getpCodigo());
                pstmt.setString(5, linea.getpCantidad());
                pstmt.setString(6, linea.getpUnidadMedida());
                pstmt.setString(7, linea.getpUnidadMedidaComercial());
                pstmt.setString(8, linea.getpDetalle());
                pstmt.setString(9, linea.getpPrecioUnitario());
                pstmt.setString(10, linea.getpMontoTotal());
                pstmt.setString(11, linea.getpMontoDescuento());
                pstmt.setString(12, linea.getpNaturalezaDescuento());
                pstmt.setString(13, linea.getpSubTotal());
                pstmt.setString(14, linea.getpMontoTotalLinea());
                pstmt.setString(15, linea.getpUnidadMedidaEmp());
                pstmt.setString(16, linea.getpCodigoArtEmp());
                pstmt.setString(17, linea.getpMercServ());
                pstmt.setString(18, linea.getpCantidad());
                pstmt.setString(19, linea.getpMontoTotal());
                pstmt.setString(20, linea.getpIdDocumentoDet());

                pstmt.executeUpdate();

                resultSet = pstmt.getGeneratedKeys();

                if (resultSet.next()) {
                    idLineaDetalle = resultSet.getInt(1);
                }

                impuestos = linea.getpImpuesto();

                if (impuestos != null) {
                    for (ImpuestoAdd imp : impuestos) {
                        exoneracion = imp.getpExoneracion();

                        if (exoneracion == null) {
                            exoneracion = new Exoneracion();
                        }

                        pstmt = connection.prepareStatement(Querys.ADD_IMPUESTO_DOCUMENTO, Statement.RETURN_GENERATED_KEYS);
                        pstmt.setString(1, String.valueOf(idLineaDetalle));
                        pstmt.setString(2, imp.getpCodigo());
                        pstmt.setString(3, imp.getpTarifa());
                        pstmt.setString(4, imp.getpMonto());
                        pstmt.setString(5, imp.getpIdImpuestoEmp());
                        pstmt.setString(6, exoneracion.getpTipoDocumento());
                        pstmt.setString(7, exoneracion.getpNumeroDocumento());
                        pstmt.setString(8, exoneracion.getpNombreInstitucion());
                        pstmt.setString(9, exoneracion.getpFechaEmision());
                        pstmt.setString(10, exoneracion.getpMontoImpuesto());
                        pstmt.setString(11, exoneracion.getpPorcentajeCompra());
                        pstmt.setString(12, exoneracion.getpTipoDocumentoEmp());

                        pstmt.executeUpdate();

                        resultSet = pstmt.getGeneratedKeys();

                        if (resultSet.next()) {
                            idImpuesto = resultSet.getInt(1);
                        }
                    }
                }
            }

            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }

            cstmt = connection.prepareCall(Querys.PR_GENERA_CLAVE_CONSECUTIVO_TERCERO);

            cstmt.setString(1, "506");
            cstmt.setString(2, mensajeInsert.getId());
            cstmt.setString(3, pDocumento.getpIdSucursalEmp());
            cstmt.setString(4, pDocumento.getpIdPdvEmp());
            cstmt.setString(5, pDocumento.getpTipoDocumentoEmp());
            cstmt.setString(6, pDocumento.getpSituacionDoc());
            cstmt.setString(7, pDocumento.getpFechaEmision());
            cstmt.setString(8, pDocumento.getpConsecutivo());
            cstmt.setString(9, pToken.getIdEmpresaFe());
            cstmt.setString(10, pDocumento.getpClave());

            cstmt.registerOutParameter(11, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(12, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(11));
            mensajeInsert.setMensaje(cstmt.getString(12));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                mensajeInsert.setId(null);

                return mensajeInsert;
            }

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeInsert registraDocumentoRecepcion(EmisorToken pToken, DocumentoRecepcionAdd pDocumento) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();
        List<ImpuestoRecepcionAdd> impuestos;
        List<InfReferenciaRecepcionAdd> referencias;
        List<LineaDetalleRecepcionAdd> lineas;
        List<String> mediosPago;
        Integer idLineaDetalle = 0;

        try {
            connection = dataSource.getConnection();

            connection.setAutoCommit(false);

            if (pDocumento.getpTotalComprobante() == null || pDocumento.getpTotalComprobante().equals("")) {
                pDocumento.setpTotalComprobante("0");
            }

            if (pDocumento.getpTotalDescuentos() == null || pDocumento.getpTotalDescuentos().equals("")) {
                pDocumento.setpTotalDescuentos("0");
            }

            if (pDocumento.getpTotalExento() == null || pDocumento.getpTotalExento().equals("")) {
                pDocumento.setpTotalExento("0");
            }

            if (pDocumento.getpTotalGravado() == null || pDocumento.getpTotalGravado().equals("")) {
                pDocumento.setpTotalGravado("0");
            }

            if (pDocumento.getpTotalImpuestos() == null || pDocumento.getpTotalImpuestos().equals("")) {
                pDocumento.setpTotalImpuestos("0");
            }

            if (pDocumento.getpTotalMercanciasExentas() == null || pDocumento.getpTotalMercanciasExentas().equals("")) {
                pDocumento.setpTotalMercanciasExentas("0");
            }

            if (pDocumento.getpTotalMercanciasGravadas() == null || pDocumento.getpTotalMercanciasGravadas().equals("")) {
                pDocumento.setpTotalMercanciasGravadas("0");
            }

            if (pDocumento.getpTotalServiciosExentos() == null || pDocumento.getpTotalServiciosExentos().equals("")) {
                pDocumento.setpTotalServiciosExentos("0");
            }

            if (pDocumento.getpTotalServiciosGravados() == null || pDocumento.getpTotalServiciosGravados().equals("")) {
                pDocumento.setpTotalServiciosGravados("0");
            }

            if (pDocumento.getpTotalVenta() == null || pDocumento.getpTotalVenta().equals("")) {
                pDocumento.setpTotalVenta("0");
            }

            if (pDocumento.getpTotalVentaNeta() == null || pDocumento.getpTotalVentaNeta().equals("")) {
                pDocumento.setpTotalVentaNeta("0");
            }

            pstmt = connection.prepareStatement(Querys.ADD_DOCUMENTO_RECEPCION, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, pDocumento.getpTipoComprobante());
            pstmt.setString(2, pDocumento.getpClave());
            pstmt.setString(3, pDocumento.getpConsecutivo());
            pstmt.setString(4, pDocumento.getpCondicionVenta());
            pstmt.setString(5, pDocumento.getpPlazoCredito());
            pstmt.setString(6, pDocumento.getpCodigoMoneda());
            pstmt.setString(7, pDocumento.getpTipoCambio());
            pstmt.setString(8, pDocumento.getpTotalServiciosGravados());
            pstmt.setString(9, pDocumento.getpTotalServiciosExentos());
            pstmt.setString(10, pDocumento.getpTotalMercanciasGravadas());
            pstmt.setString(11, pDocumento.getpTotalMercanciasExentas());
            pstmt.setString(12, pDocumento.getpTotalGravado());
            pstmt.setString(13, pDocumento.getpTotalExento());
            pstmt.setString(14, pDocumento.getpTotalVenta());
            pstmt.setString(15, pDocumento.getpTotalDescuentos());
            pstmt.setString(16, pDocumento.getpTotalVentaNeta());
            pstmt.setString(17, pDocumento.getpTotalImpuestos());
            pstmt.setString(18, pDocumento.getpTotalComprobante());
            pstmt.setString(19, Constantes.ESTADO_DOC_EN_COLA);
            pstmt.setString(20, pDocumento.getpNumeroResolucion());
            pstmt.setString(21, pDocumento.getpFechaResolucion());
            pstmt.setString(22, "Se registra el documento");
            pstmt.setString(23, pDocumento.getpTipoIdenCliente());
            pstmt.setString(24, pDocumento.getpNumeroIdenCliente());
            pstmt.setString(25, pDocumento.getpRazonSocialCliente());
            pstmt.setString(26, pDocumento.getpEmailCliente());
            pstmt.setString(27, "1");
            pstmt.setString(28, pDocumento.getpFechaEmision());
            pstmt.setString(29, pDocumento.getpTipoIdentificacionEmpresa());
            pstmt.setString(30, pDocumento.getpIdentificacionEmpresa());
            pstmt.setString(31, pDocumento.getpRazonSocialEmpresa());
            pstmt.setString(32, pDocumento.getpNombreComercialEmpresa());
            pstmt.setString(33, pDocumento.getpEmailEmpresa());
            pstmt.setString(34, "0");
            pstmt.setString(35, pDocumento.getpAccion());
            pstmt.setString(36, pToken.getIdEmpresaFe());
            pstmt.setString(37, pToken.getIdEmpresa());
            pstmt.setString(38, pDocumento.getpTagImpuesto());

            pstmt.execute();

            resultSet = pstmt.getGeneratedKeys();

            if (resultSet.next()) {
                mensajeInsert.setId(String.valueOf(resultSet.getInt(1)));
                mensajeInsert.setStatus(Constantes.statusSuccess);
                mensajeInsert.setMensaje("Su documento fue ingresado correctamente");
            }

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Error al ingresar el registro");

                connection.rollback();

                return mensajeInsert;
            }

            mediosPago = pDocumento.getpMediosPago();

            if (mediosPago != null) {
                for (String medio : mediosPago) {
                    pstmt = connection.prepareStatement(Querys.ADD_MEDIO_PAGO_RECEPCION, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, medio);
                    pstmt.setString(2, mensajeInsert.getId());

                    pstmt.executeUpdate();
                }
            }

            referencias = pDocumento.getpInformacionReferencia();

            if (referencias != null) {
                for (InfReferenciaRecepcionAdd referencia : referencias) {
                    pstmt = connection.prepareStatement(Querys.ADD_REFERENCIA_RECEPCION, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setString(1, mensajeInsert.getId());
                    pstmt.setString(2, referencia.getpTipoDocumento());
                    pstmt.setString(3, referencia.getpNumero());
                    pstmt.setString(4, referencia.getpFechaEmision());
                    pstmt.setString(5, referencia.getpCodigo());
                    pstmt.setString(6, referencia.getpRazon());

                    pstmt.executeUpdate();
                }
            }

            lineas = pDocumento.getpLineasDetalle();

            for (LineaDetalleRecepcionAdd linea : lineas) {
                if (linea.getpMontoDescuento() == null || linea.getpMontoDescuento().equals("")) {
                    linea.setpMontoDescuento("0");
                    linea.setpNaturalezaDescuento("0%");
                }

                if (linea.getpNaturalezaDescuento() == null || linea.getpNaturalezaDescuento().equals("")) {
                    linea.setpMontoDescuento("0");
                    linea.setpNaturalezaDescuento("0%");
                }

                pstmt = connection.prepareStatement(Querys.ADD_DETALLE_RECEPCION, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, mensajeInsert.getId());
                pstmt.setString(2, linea.getpNumeroLinea());
                pstmt.setString(3, linea.getpTipoCodigoArticulo());
                pstmt.setString(4, linea.getpCodigo());
                pstmt.setString(5, linea.getpCantidad());
                pstmt.setString(6, linea.getpUnidadMedida());
                pstmt.setString(7, linea.getpUnidadMedidaCom());
                pstmt.setString(8, linea.getpDetalle());
                pstmt.setString(9, linea.getpPrecioUnitario());
                pstmt.setString(10, linea.getpMontoTotal());
                pstmt.setString(11, linea.getpMontoDescuento());
                pstmt.setString(12, linea.getpNaturalezaDescuento());
                pstmt.setString(13, linea.getpSubTotal());
                pstmt.setString(14, linea.getpMontoTotalLinea());

                pstmt.executeUpdate();

                resultSet = pstmt.getGeneratedKeys();

                if (resultSet.next()) {
                    idLineaDetalle = resultSet.getInt(1);
                }

                impuestos = linea.getpImpuestos();

                if (impuestos != null) {
                    for (ImpuestoRecepcionAdd imp : impuestos) {
                        pstmt = connection.prepareStatement(Querys.ADD_IMPUESTO_RECEPCION, Statement.RETURN_GENERATED_KEYS);

                        pstmt.setString(1, String.valueOf(idLineaDetalle));
                        pstmt.setString(2, imp.getpCodigo());
                        pstmt.setString(3, imp.getpTarifa());
                        pstmt.setString(4, imp.getpMonto());
                        pstmt.setString(5, imp.getpTipoDocumentoExo());
                        pstmt.setString(6, imp.getpNumeroDocumentoExo());
                        pstmt.setString(7, imp.getpNombreInstitucionExo());
                        pstmt.setString(8, imp.getpFechaEmisionExo());
                        pstmt.setString(9, imp.getpMontoImpuestoExo());
                        pstmt.setString(10, imp.getpPorcentajeCompraExo());

                        pstmt.executeUpdate();
                    }
                }
            }

            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }

            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();

            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeArticulos getArticulos(String pidArticulo, String pidEmpresa, String pEstado, String pIdCliente) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Articulo> articulos = new ArrayList<>();
        List<ImpuestoArt> impuestos;
        List<ExoneracionArt> exoneraciones;
        ExoneracionArt exoneracion;
        Articulo art;
        String exoneracionesComa;
        String idsImpuestoComa;
        ImpuestoArt impuesto;
        MensajeArticulos mensajeArticulo = new MensajeArticulos();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_ARTICULOS);

            pstmt.setString(1, pidEmpresa);
            pstmt.setString(2, pidEmpresa);
            pstmt.setString(3, pidArticulo);
            pstmt.setString(4, pidArticulo);
            pstmt.setString(5, pEstado);
            pstmt.setString(6, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                art = new Articulo();
                art.setIdArticuloEmp(resultSet.getString(1));
                art.setIdCategoria(resultSet.getString(2));
                art.setDescripcion(resultSet.getString(3));
                art.setIdUnidadMedidaHacienda(resultSet.getString(4));
                art.setUnidadMedidaCom(resultSet.getString(5));
                art.setTipoArticulo(resultSet.getString(6));
                art.setPrecio(resultSet.getString(7));
                art.setPorcentaje(resultSet.getString(8));
                art.setDescuento(resultSet.getString(9));
                art.setEsRecargo(resultSet.getString(10));
                art.setEstado(resultSet.getString(11));
                String recargosComa = resultSet.getString(12);
                String impuestosComa = resultSet.getString(13);
                String unidadMedidaComa = resultSet.getString(14);
                String articulosComa = resultSet.getString(15);
                art.setCodigo(resultSet.getString(16));
                art.setCodigoDesc(resultSet.getString(17));
                art.setCosto(resultSet.getString(18));

                List<String> recargos = new ArrayList<>();

                if (recargosComa != null) {
                    String[] parts3 = recargosComa.split(",");

                    recargos = Arrays.asList(parts3);
                }
                art.setRecargos(recargos);

                List<String> articulosRec = new ArrayList<>();

                if (articulosComa != null) {
                    String[] parts3 = articulosComa.split(",");

                    articulosRec = Arrays.asList(parts3);
                }
                art.setArticulos(articulosRec);

                List<String> unidadesMedidaEmp = new ArrayList<>();

                if (unidadMedidaComa != null) {
                    String[] parts3 = unidadMedidaComa.split(",");

                    unidadesMedidaEmp = Arrays.asList(parts3);
                }
                art.setIdsUnidadMedidaEmp(unidadesMedidaEmp);

                impuestos = new ArrayList<>();

                if (impuestosComa != null) {
                    String[] parts = impuestosComa.split(",");

                    for (String idImpuesto : parts) {
                        exoneracionesComa = null;
                        idsImpuestoComa = null;
                        exoneraciones = new ArrayList<>();
                        impuesto = new ImpuestoArt();

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTOS_ART);

                        pstmt.setString(1, pIdCliente);
                        pstmt.setString(2, pidEmpresa);
                        pstmt.setString(3, idImpuesto);

                        resultSet2 = pstmt.executeQuery();

                        while (resultSet2.next()) {
                            impuesto.setIdImpuestoHacienda(resultSet2.getString(1));
                            impuesto.setDescripcion(resultSet2.getString(2));
                            impuesto.setPorcentaje(resultSet2.getString(3));
                            impuesto.setExcepcion(resultSet2.getString(4));

                            exoneracionesComa = resultSet2.getString(5);
                            idsImpuestoComa = resultSet2.getString(6);

                        }

                        List<String> idsImpuesto = new ArrayList<>();

                        if (idsImpuestoComa != null) {
                            String[] parts3 = idsImpuestoComa.split(",");

                            idsImpuesto = Arrays.asList(parts3);
                        }
                        impuesto.setIdsImpuestoEmp(idsImpuesto);

                        if (exoneracionesComa != null) {

                            String[] parts4 = exoneracionesComa.split(",");

                            for (String idExoneracion : parts4) {
                                pstmt = connection.prepareStatement(Querys.GET_EXONERACIONES_ARTICULO);

                                pstmt.setString(1, pidEmpresa);
                                pstmt.setString(2, idExoneracion);
                                pstmt.setString(3, idExoneracion);
                                pstmt.setString(4, pIdCliente);
                                pstmt.setString(5, art.getIdArticuloEmp());

                                resultSet3 = pstmt.executeQuery();

                                while (resultSet3.next()) {
                                    exoneracion = new ExoneracionArt();

                                    exoneracion.setCodigoTipoExo(resultSet3.getString(1));
                                    exoneracion.setNumeroDocumento(resultSet3.getString(2));
                                    exoneracion.setNombreInstitucion(resultSet3.getString(3));
                                    exoneracion.setFechaEmision(resultSet3.getString(4));
                                    exoneracion.setMontoImpuesto(resultSet3.getString(5));
                                    exoneracion.setPorcentajeCompra(resultSet3.getString(6));
                                    exoneracion.setIdCliente(resultSet3.getString(7));
                                    exoneracion.setFechaHasta(resultSet3.getString(8));
                                    exoneracion.setIdExoneracion(resultSet3.getString(9));
                                    exoneracion.setDescTipoExo(resultSet3.getString(10));
                                    exoneracion.setEstado(resultSet3.getString(11));
                                    exoneracion.setExoGlobal(resultSet3.getString(12));

                                    exoneraciones.add(exoneracion);
                                }
                            }
                        }
                        impuesto.setExoneraciones(exoneraciones);
                        impuestos.add(impuesto);
                    }
                }

                art.setImpuestos(impuestos);

                articulos.add(art);
            }

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

            mensajeArticulo.setStatus(Constantes.statusSuccess);
            mensajeArticulo.setMensaje("Consulta realizada con exito");
            mensajeArticulo.setArticulos(articulos);

        } catch (Exception e) {
            mensajeArticulo.setStatus(Constantes.statusError);
            mensajeArticulo.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeArticulo;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeArticulo;
    }

    @Override
    public MensajeExoneraciones getExoneraciones(String pIdExoneracion, String pIdEmpresa, String pIdCliente) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<ExoneracionArt> exoneraciones = new ArrayList<>();
        ExoneracionArt exoneracion = null;
        MensajeExoneraciones mensajeConsulta = new MensajeExoneraciones();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_EXONERACIONES);

            pstmt.setString(1, pIdEmpresa);
            pstmt.setString(2, pIdExoneracion);
            pstmt.setString(3, pIdExoneracion);
            pstmt.setString(4, pIdCliente);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                exoneracion = new ExoneracionArt();

                exoneracion.setCodigoTipoExo(resultSet.getString(1));
                exoneracion.setNumeroDocumento(resultSet.getString(2));
                exoneracion.setNombreInstitucion(resultSet.getString(3));
                exoneracion.setFechaEmision(resultSet.getString(4));
                exoneracion.setMontoImpuesto(resultSet.getString(5));
                exoneracion.setPorcentajeCompra(resultSet.getString(6));
                exoneracion.setIdCliente(resultSet.getString(7));
                exoneracion.setFechaHasta(resultSet.getString(8));
                exoneracion.setIdExoneracion(resultSet.getString(9));
                exoneracion.setDescTipoExo(resultSet.getString(10));
                exoneracion.setEstado(resultSet.getString(11));
                exoneracion.setExoGlobal(resultSet.getString(12));

                exoneraciones.add(exoneracion);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setExoneraciones(exoneraciones);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeArticulos addArticuloEmp(String pIdEmpresaFe, ArticuloAdd pArticulo) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        List<String> articulos;
        List<String> exoneraciones;
        List<ImpuestoExoClienteAdd> impuestos;
        MensajeArticulos mensajeInsert = new MensajeArticulos();

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_ADD_ARTICULO_EMP);

            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pArticulo.getIdArticuloEmp());
            cstmt.setString(3, pArticulo.getIdCategoria());
            cstmt.setString(4, pArticulo.getDescripcion());
            cstmt.setString(5, pArticulo.getIdUnidadMedidaEmp());
            cstmt.setString(6, pArticulo.getUnidadMedidaCom());
            cstmt.setString(7, pArticulo.getTipoArticulo());
            cstmt.setString(8, pArticulo.getPrecio());
            cstmt.setString(9, pArticulo.getPorcentaje());
            cstmt.setString(10, pArticulo.getDescuento());
            cstmt.setString(11, pArticulo.getEsRecargo());
            cstmt.setString(12, pArticulo.getCodigo());
            cstmt.setString(13, pArticulo.getCosto());

            cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(16, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(14));
            mensajeInsert.setMensaje(cstmt.getString(15));
            mensajeInsert.setId(cstmt.getString(16));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeInsert;
            }

            articulos = pArticulo.getIdsArticuloEmp();

            if (articulos != null) {
                for (String idArticuloEmp : articulos) {
                    cstmt = connection.prepareCall(Querys.PR_ADD_RECARGO_ARTICULO_EMP);
                    cstmt.setString(1, mensajeInsert.getId());
                    cstmt.setString(2, pIdEmpresaFe);
                    cstmt.setString(3, idArticuloEmp);

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeInsert.setStatus(cstmt.getString(4));
                    mensajeInsert.setMensaje(cstmt.getString(5));

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        return mensajeInsert;
                    }
                }
            }

            impuestos = pArticulo.getImpuestos();

            if (impuestos != null) {
                for (ImpuestoExoClienteAdd impuesto : impuestos) {
                    cstmt = connection.prepareCall(Querys.PR_ADD_IMPUESTO_ARTICULO_EMP);
                    cstmt.setString(1, mensajeInsert.getId());
                    cstmt.setString(2, pIdEmpresaFe);
                    cstmt.setString(3, impuesto.getIdImpuestoEmp());

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeInsert.setStatus(cstmt.getString(4));
                    mensajeInsert.setMensaje(cstmt.getString(5));
                    String idImpuesto = cstmt.getString(6);

                    if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        return mensajeInsert;
                    }

                    exoneraciones = impuesto.getIdsExoneracion();

                    if (exoneraciones != null) {
                        for (String idExoneracion : exoneraciones) {
                            cstmt = connection.prepareCall(Querys.PR_ADD_EXO_IMP_ARTICULO_EMP);
                            cstmt.setString(1, idImpuesto);
                            cstmt.setString(2, mensajeInsert.getId());
                            cstmt.setString(3, idExoneracion);
                            cstmt.setString(4, pIdEmpresaFe);

                            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                            cstmt.execute();

                            mensajeInsert.setStatus(cstmt.getString(5));
                            mensajeInsert.setMensaje(cstmt.getString(6));

                            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                                connection.rollback();

                                return mensajeInsert;
                            }
                        }
                    }
                }
            }

            connection.commit();

        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, cstmt, connection);

            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateArticuloEmp(String pIdEmpresaFe, ArticuloAdd pArticulo) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<String> articulos;
        List<String> exoneraciones;
        List<ImpuestoExoClienteAdd> impuestos;
        MensajeBase mensajeUpdate = new MensajeBase();
        String idArticulo;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            if (pArticulo.getDescuento() == null || pArticulo.getDescuento().equals("")) {
                pArticulo.setDescuento("0");
            }

            cstmt = connection.prepareCall(Querys.PR_UPDATE_ARTICULO_EMP);

            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pArticulo.getIdArticuloEmp());
            cstmt.setString(3, pArticulo.getIdCategoria());
            cstmt.setString(4, pArticulo.getDescripcion());
            cstmt.setString(5, pArticulo.getIdUnidadMedidaEmp());
            cstmt.setString(6, pArticulo.getUnidadMedidaCom());
            cstmt.setString(7, pArticulo.getTipoArticulo());
            cstmt.setString(8, pArticulo.getPrecio());
            cstmt.setString(9, pArticulo.getPorcentaje());
            cstmt.setString(10, pArticulo.getDescuento());
            cstmt.setString(11, pArticulo.getEsRecargo());
            cstmt.setString(12, pArticulo.getEstado());
            cstmt.setString(13, pArticulo.getCodigo());
            cstmt.setString(14, pArticulo.getCosto());

            cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(16, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(17, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeUpdate.setStatus(cstmt.getString(15));
            mensajeUpdate.setMensaje(cstmt.getString(16));
            idArticulo = cstmt.getString(17);

            if (!mensajeUpdate.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeUpdate;
            }

            pstmt = connection.prepareStatement(Querys.DELETE_RECARGOS_ARTICULOS);

            pstmt.setString(1, idArticulo);

            pstmt.executeUpdate();

            articulos = pArticulo.getIdsArticuloEmp();

            if (articulos != null) {
                for (String idArticuloEmp : articulos) {
                    cstmt = connection.prepareCall(Querys.PR_ADD_RECARGO_ARTICULO_EMP);
                    cstmt.setString(1, idArticulo);
                    cstmt.setString(2, pIdEmpresaFe);
                    cstmt.setString(3, idArticuloEmp);

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeUpdate.setStatus(cstmt.getString(4));
                    mensajeUpdate.setMensaje(cstmt.getString(5));

                    if (!mensajeUpdate.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        return mensajeUpdate;
                    }
                }
            }

            pstmt = connection.prepareStatement(Querys.DELETE_IMPUESTOS_ARTICULO);

            pstmt.setString(1, idArticulo);

            pstmt.executeUpdate();

            impuestos = pArticulo.getImpuestos();

            if (impuestos != null) {
                for (ImpuestoExoClienteAdd impuesto : impuestos) {
                    cstmt = connection.prepareCall(Querys.PR_ADD_IMPUESTO_ARTICULO_EMP);
                    cstmt.setString(1, idArticulo);
                    cstmt.setString(2, pIdEmpresaFe);
                    cstmt.setString(3, impuesto.getIdImpuestoEmp());

                    cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                    cstmt.execute();

                    mensajeUpdate.setStatus(cstmt.getString(4));
                    mensajeUpdate.setMensaje(cstmt.getString(5));
                    String idImpuesto = cstmt.getString(6);

                    if (!mensajeUpdate.getStatus().equals(Constantes.statusSuccess)) {
                        connection.rollback();

                        return mensajeUpdate;
                    }

                    exoneraciones = impuesto.getIdsExoneracion();

                    if (exoneraciones != null) {
                        for (String idExoneracion : exoneraciones) {
                            cstmt = connection.prepareCall(Querys.PR_ADD_EXO_IMP_ARTICULO_EMP);
                            cstmt.setString(1, idImpuesto);
                            cstmt.setString(2, idArticulo);
                            cstmt.setString(3, idExoneracion);
                            cstmt.setString(4, pIdEmpresaFe);

                            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
                            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

                            cstmt.execute();

                            mensajeUpdate.setStatus(cstmt.getString(4));
                            mensajeUpdate.setMensaje(cstmt.getString(6));

                            if (!mensajeUpdate.getStatus().equals(Constantes.statusSuccess)) {
                                connection.rollback();

                                return mensajeUpdate;
                            }
                        }
                    }
                }
            }

            pstmt.close();
            pstmt = null;

            connection.commit();

        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, pstmt, connection);

            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeUpdate;
    }

    @Override
    public MensajeBase deleteArticuloEmp(String pIdEmpresaFe, String pIdArticuloEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeBase = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_DELETE_ARTICULO_EMP);

            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pIdArticuloEmp);

            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeBase.setStatus(cstmt.getString(3));
            mensajeBase.setMensaje(cstmt.getString(4));

            if (!mensajeBase.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeBase;
            }

            connection.commit();

        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());

            return mensajeBase;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeBase;
    }

    @Override
    public MensajeBase agregarExoArticuloEmp(String pIdEmpresa, AgregarExoArticulo data) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeBase = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_ADD_EXO_IMP_ARTICULO_EMP);

            cstmt.setString(1, data.getIdImpuesto());
            cstmt.setString(2, data.getIdArticulo());
            cstmt.setString(3, data.getIdExoneracion());
            cstmt.setString(4, pIdEmpresa);
            cstmt.setString(5, data.getIdCliente());

            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeBase.setStatus(cstmt.getString(6));
            mensajeBase.setMensaje(cstmt.getString(7));

            if (!mensajeBase.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeBase;
            } else {
                mensajeBase.setStatus(Constantes.statusSuccess);
                mensajeBase.setMensaje("Exoneracion agregada correctamente!");
            }

            connection.commit();

        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());

            return mensajeBase;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeBase;
    }

    @Override
    public MensajeExoneracionesArticulos getListadoExoArticulo(String pIdEmpresa) throws Exception {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<ExoneracionesProducto> listExoneraciones = new ArrayList<>();
        ExoneracionesProducto exoneracion;
        MensajeExoneracionesArticulos mensajeExoneracionesArticulos = new MensajeExoneracionesArticulos();

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_LISTADO_EXO_PRODUCTO);

            pstmt.setString(1, pIdEmpresa);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                exoneracion = new ExoneracionesProducto();

                exoneracion.setId(resultSet.getString(1));
                exoneracion.setIdImpuesto(resultSet.getString(2));
                exoneracion.setIdExoneracion(resultSet.getString(3));
                exoneracion.setIdArticulo(resultSet.getString(4));
                exoneracion.setIdCliente(resultSet.getString(5));
                exoneracion.setCodigoArticulo(resultSet.getString(6));
                exoneracion.setDescripcionArticulo(resultSet.getString(7));
                exoneracion.setTipoArticulo(resultSet.getString(8));
                exoneracion.setCosto(resultSet.getString(9));
                exoneracion.setPrecio(resultSet.getString(10));
                exoneracion.setPorcentaje(resultSet.getString(11));
                exoneracion.setDescuento(resultSet.getString(12));
                exoneracion.setDescripcionDescuento(resultSet.getString(13));
                exoneracion.setPorcImpuesto(resultSet.getString(14));
                exoneracion.setCodigoTipoExo(resultSet.getString(15));
                exoneracion.setNumeroDocumento(resultSet.getString(16));
                exoneracion.setNombreInstitucion(resultSet.getString(17));
                exoneracion.setFechaEmision(resultSet.getString(18));
                exoneracion.setMontoImpuesto(resultSet.getString(19));
                exoneracion.setPorcCompra(resultSet.getString(20));
                exoneracion.setFechaHasta(resultSet.getString(21));
                exoneracion.setEstadoExoneracion(resultSet.getString(22));
                exoneracion.setNombreComercialReceptor(resultSet.getString(23));
                exoneracion.setTipoIdentificacionReceptor(resultSet.getString(24));
                exoneracion.setNumeroIdentificacionReceptor(resultSet.getString(25));
                exoneracion.setIdentificacionExtranjeroReceptor(resultSet.getString(26));
                exoneracion.setRazonSocialReceptor(resultSet.getString(27));
                exoneracion.setTipoIdentDesc(resultSet.getString(28));

                listExoneraciones.add(exoneracion);
            }

            mensajeExoneracionesArticulos.setStatus(Constantes.statusSuccess);
            mensajeExoneracionesArticulos.setMensaje("Consulta realizada con exito");
            mensajeExoneracionesArticulos.setArticulos(listExoneraciones);

            return mensajeExoneracionesArticulos;
        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, pstmt, connection);

            mensajeExoneracionesArticulos.setStatus(Constantes.statusError);
            mensajeExoneracionesArticulos.setMensaje(e.getMessage());

            return mensajeExoneracionesArticulos;
        } finally {
            closeDaoResources(null, pstmt, connection);
        }
    }

    @Override
    public MensajeSucursal getSucursales(String pIdEmpresaFe, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Sucursal> sucursales = new ArrayList<>();
        Sucursal sucursal;
        MensajeSucursal mensajeSucursal = new MensajeSucursal();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_SUCURSALES);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pEstado);
            pstmt.setString(3, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                sucursal = new Sucursal();

                sucursal.setIdSucursalEmp(resultSet.getString(1));
                sucursal.setNumSucursal(resultSet.getString(2));
                sucursal.setDescripcion(resultSet.getString(3));
                sucursal.setEstado(resultSet.getString(4));
                sucursal.setEstadoDesc(resultSet.getString(5));

                sucursales.add(sucursal);
            }

            mensajeSucursal.setStatus(Constantes.statusSuccess);
            mensajeSucursal.setMensaje("Consulta realizada con exito");
            mensajeSucursal.setSucursales(sucursales);

        } catch (Exception e) {
            mensajeSucursal.setStatus(Constantes.statusError);
            mensajeSucursal.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeSucursal;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeSucursal;
    }

    @Override
    public MensajePuntoVenta getPuntosVenta(String pIdEmpresaFe, String pIdSucursalEmp, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<PuntoVenta> puntosVenta = new ArrayList<>();
        PuntoVenta puntoVenta;
        MensajePuntoVenta mensajePuntoVenta = new MensajePuntoVenta();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PUNTOS_VENTA);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdEmpresaFe);
            pstmt.setString(3, pIdSucursalEmp);
            pstmt.setString(4, pEstado);
            pstmt.setString(5, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                puntoVenta = new PuntoVenta();

                puntoVenta.setIdSucursalEmp(pIdSucursalEmp);
                puntoVenta.setIdPuntoVentaEmp(resultSet.getString(1));
                puntoVenta.setNumPuntoVenta(resultSet.getString(2));
                puntoVenta.setDescripcion(resultSet.getString(3));
                puntoVenta.setEstado(resultSet.getString(4));
                puntoVenta.setEstadoDesc(resultSet.getString(5));
                puntoVenta.setConFactura(resultSet.getString(6));
                puntoVenta.setConTiquete(resultSet.getString(7));
                puntoVenta.setConNotaCredito(resultSet.getString(8));
                puntoVenta.setConNotaDebito(resultSet.getString(9));
                puntoVenta.setConAcepta(resultSet.getString(10));
                puntoVenta.setConAceptaParcial(resultSet.getString(11));
                puntoVenta.setConRechaza(resultSet.getString(12));

                puntosVenta.add(puntoVenta);
            }

            mensajePuntoVenta.setStatus(Constantes.statusSuccess);
            mensajePuntoVenta.setMensaje("Consulta realizada con exito");
            mensajePuntoVenta.setPuntosVenta(puntosVenta);

        } catch (Exception e) {
            mensajePuntoVenta.setStatus(Constantes.statusError);
            mensajePuntoVenta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajePuntoVenta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajePuntoVenta;
    }

    @Override
    public MensajeBase addSucursalEmp(String pIdEmpresaFe, SucursalAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_SUCURSAL_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pDatos.getIdSucursalEmp());
            cstmt.setString(3, pDatos.getDescripcion());

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));
            String id = cstmt.getString(6);

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateSucursalEmp(String pIdEmpresaFe, SucursalAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_UPDATE_SUCURSAL_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pDatos.getIdSucursalEmp());
            cstmt.setString(3, pDatos.getDescripcion());
            cstmt.setString(4, pDatos.getEstado());

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addPuntoVentaEmp(String pIdEmpresaFe, PuntoVentaAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_PUNTO_VENTA_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pDatos.getIdSucursalEmp());
            cstmt.setString(3, pDatos.getIdPuntoVentaEmp());
            cstmt.setString(4, pDatos.getDescripcion());

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));
            String id = cstmt.getString(7);

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updatePuntoVentaEmp(String pIdEmpresaFe, PuntoVentaEdit pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_UPDATE_PUNTO_VENTA_EMP);
            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pDatos.getIdSucursalEmp());
            cstmt.setString(3, pDatos.getIdPuntoVentaEmp());
            cstmt.setString(4, pDatos.getDescripcion());
            cstmt.setString(5, pDatos.getEstado());
            cstmt.setString(6, pDatos.getConFactura());
            cstmt.setString(7, pDatos.getConTiquete());
            cstmt.setString(8, pDatos.getConNotaCredito());
            cstmt.setString(9, pDatos.getConNotaDebito());
            cstmt.setString(10, pDatos.getConAcepta());
            cstmt.setString(11, pDatos.getConAceptaParcial());
            cstmt.setString(12, pDatos.getConRechaza());

            cstmt.registerOutParameter(13, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(13));
            mensajeInsert.setMensaje(cstmt.getString(14));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeDocumento getDocumento(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        String tipoDocumento = null;

        try {
            connection = dataSource.getConnection();

            if (pDatos.getpIdDocumentoEmp() == null || pDatos.getpIdDocumentoEmp().equals("")) {
                pDatos.setpIdDocumentoEmp("NA");
            }

            if (pDatos.getpVerDetalle() == null || pDatos.getpVerDetalle().equals("")) {
                pDatos.setpVerDetalle("0");
            }

            if (pDatos.getpEstado() == null || pDatos.getpEstado().isEmpty()) {
                estados.add("NA");
                pDatos.setpEstado(estados);
            }

            String estadosComa = String.join(",", pDatos.getpEstado());

            if (pDatos.getpEntidadEmite() == null || pDatos.getpEntidadEmite().equals("")) {
                pDatos.setpEntidadEmite("NA");
            }

            if (pDatos.getpFechaDesde() == null || pDatos.getpFechaDesde().equals("")) {
                pDatos.setpFechaDesde("NA");
            }

            if (pDatos.getpFechaHasta() == null || pDatos.getpFechaHasta().equals("")) {
                pDatos.setpFechaHasta("NA");
            }

            if (pDatos.getpIdenCliente() == null || pDatos.getpIdenCliente().equals("")) {
                pDatos.setpIdenCliente("NA");
            }

            if (pDatos.getpNombreCliente() == null || pDatos.getpNombreCliente().equals("")) {
                pDatos.setpNombreCliente("NA");
            }

            if (pDatos.getpClave() == null || pDatos.getpClave().equals("")) {
                pDatos.setpClave("NA");
            }

            if (pDatos.getpConsecutivo() == null || pDatos.getpConsecutivo().equals("")) {
                pDatos.setpConsecutivo("NA");
            }

            if (pDatos.getpMonto() == null || pDatos.getpMonto().equals("")) {
                pDatos.setpMonto("NA");
            }

            if (pDatos.getpTipoDocumentoEmp() == null || pDatos.getpTipoDocumentoEmp().equals("")) {
                tipoDocumento = "NA";
            } else {
                pstmt = connection.prepareStatement(Querys.GET_TIPO_DOC);

                pstmt.setString(1, pIdEmpresaFe);
                pstmt.setString(2, pDatos.getpTipoDocumentoEmp());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    tipoDocumento = resultSet.getString(1);
                }

                if (tipoDocumento == null) {
                    tipoDocumento = "NA";
                }
            }

            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDatos.getpIdDocumentoEmp());
            pstmt.setString(3, pDatos.getpIdDocumentoEmp());
            pstmt.setString(4, estadosComa);
            pstmt.setString(5, estadosComa);
            pstmt.setString(6, pDatos.getpEntidadEmite());
            pstmt.setString(7, pDatos.getpEntidadEmite());
            pstmt.setString(8, pDatos.getpFechaDesde());
            pstmt.setString(9, pDatos.getpFechaDesde());
            pstmt.setString(10, pDatos.getpFechaHasta());
            pstmt.setString(11, pDatos.getpFechaHasta());
            pstmt.setString(12, tipoDocumento);
            pstmt.setString(13, tipoDocumento);
            pstmt.setString(14, pDatos.getpIdenCliente());
            pstmt.setString(15, pDatos.getpIdenCliente());
            pstmt.setString(16, pDatos.getpNombreCliente());
            pstmt.setString(17, pDatos.getpNombreCliente());
            pstmt.setString(18, pDatos.getpClave());
            pstmt.setString(19, pDatos.getpClave());
            pstmt.setString(20, pDatos.getpConsecutivo());
            pstmt.setString(21, pDatos.getpConsecutivo());
            pstmt.setString(22, pDatos.getpMonto());
            pstmt.setString(23, pDatos.getpMonto());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                documento.setpIdEmpresa(pIdEmpresaFe);
                
            
                medioPagoComa = null;

                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpFechaRegistro(resultSet.getString(40));
                documento.setpEstadoAceptacion(resultSet.getString(41));
                documento.setpCondicionVentaDesc(resultSet.getString(42));
                documento.setpNombreEmpresaCom(resultSet.getString(43));
                documento.setpFaxEmpresa(resultSet.getString(44));
                documento.setpRazonSocialEmpEmitido(resultSet.getString(45));
                documento.setpNumIdentificacionEmitido(resultSet.getString(46));
                documento.setpCorreoElectronicoEmitido(resultSet.getString(47));

                
                documento.setpTotalSerGravados(resultSet.getString(48));
                documento.setpTotalSerExentos(resultSet.getString(49));
                documento.setpTotalMercanciasGravados(resultSet.getString(50));
                documento.setpTotalMercanciasExentos(resultSet.getString(51));
                documento.setpTotalGravado(resultSet.getString(52));
                documento.setpTotalExento(resultSet.getString(53));
                documento.setpTotalVenta(resultSet.getString(54));
                documento.setpTotalDescuentos(resultSet.getString(55));
                documento.setpReceptorNombreComercial(resultSet.getString(56));
                documento.setpTipoIdenReceptorDesc(resultSet.getString(57));
                documento.setEmitida(resultSet.getString(58));
                documento.setIdFacRef(resultSet.getString(59));
                documento.setOrdenCompra(resultSet.getString(60));
                
                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");
                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                if (pDatos.getpVerDetalle().equals("1")) {
                    pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO);

                    pstmt.setString(1, documento.getpIdDocumento());

                    resultSet2 = pstmt.executeQuery();

                    lineasDetalle = new ArrayList<>();

                    while (resultSet2.next()) {
                        lineaDetalle = new LineaDetalle();

                        lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                        lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                        lineaDetalle.setpLinea(resultSet2.getString(3));
                        lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                        lineaDetalle.setpCodigo(resultSet2.getString(5));
                        lineaDetalle.setpCantidad(resultSet2.getString(6));
                        lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                        lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                        lineaDetalle.setpDetalle(resultSet2.getString(9));
                        lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                        lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                        lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                        lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                        lineaDetalle.setpSubTotal(resultSet2.getString(14));
                        lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                        lineaDetalle.setpMercServ(resultSet2.getString(16));
                        lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                        lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                        pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                        resultSet3 = pstmt.executeQuery();

                        impuestos = new ArrayList<>();

                        while (resultSet3.next()) {
                            impuesto = new Impuesto();

                            impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                            impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                            impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                            impuesto.setpTarifa(resultSet3.getString(4));
                            impuesto.setpMonto(resultSet3.getString(5));
                            impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                            impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                            impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                            impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                            impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                            impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                            impuestos.add(impuesto);
                        }

                        lineaDetalle.setpImpuestos(impuestos);

                        lineasDetalle.add(lineaDetalle);

                    }

                    documento.setpLineasDetalle(lineasDetalle);
                }

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }

    @Override
    public MensajeDocumento getDocumentoDetEmitido(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        String tipoDocumento = null;

        try {
            connection = dataSource.getConnection();

            if (pDatos.getpIdDocumentoEmp() == null || pDatos.getpIdDocumentoEmp().equals("")) {
                pDatos.setpIdDocumentoEmp("NA");
            }

            if (pDatos.getpVerDetalle() == null || pDatos.getpVerDetalle().equals("")) {
                pDatos.setpVerDetalle("0");
            }

            if (pDatos.getpEstado() == null || pDatos.getpEstado().isEmpty()) {
                estados.add("NA");
                pDatos.setpEstado(estados);
            }

            String estadosComa = String.join(",", pDatos.getpEstado());

            if (pDatos.getpEntidadEmite() == null || pDatos.getpEntidadEmite().equals("")) {
                pDatos.setpEntidadEmite("NA");
            }

            if (pDatos.getpFechaDesde() == null || pDatos.getpFechaDesde().equals("")) {
                pDatos.setpFechaDesde("NA");
            }

            if (pDatos.getpFechaHasta() == null || pDatos.getpFechaHasta().equals("")) {
                pDatos.setpFechaHasta("NA");
            }

            if (pDatos.getpIdenCliente() == null || pDatos.getpIdenCliente().equals("")) {
                pDatos.setpIdenCliente("NA");
            }

            if (pDatos.getpNombreCliente() == null || pDatos.getpNombreCliente().equals("")) {
                pDatos.setpNombreCliente("NA");
            }

            if (pDatos.getpClave() == null || pDatos.getpClave().equals("")) {
                pDatos.setpClave("NA");
            }

            if (pDatos.getpConsecutivo() == null || pDatos.getpConsecutivo().equals("")) {
                pDatos.setpConsecutivo("NA");
            }

            if (pDatos.getpMonto() == null || pDatos.getpMonto().equals("")) {
                pDatos.setpMonto("NA");
            }

            if (pDatos.getpTipoDocumentoEmp() == null || pDatos.getpTipoDocumentoEmp().equals("")) {
                tipoDocumento = "NA";
            } else {
                pstmt = connection.prepareStatement(Querys.GET_TIPO_DOC);

                pstmt.setString(1, pIdEmpresaFe);
                pstmt.setString(2, pDatos.getpTipoDocumentoEmp());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    tipoDocumento = resultSet.getString(1);
                }

                if (tipoDocumento == null) {
                    tipoDocumento = "NA";
                }
            }

            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_DET_EMIT);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDatos.getpIdDocumentoEmp());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                documento.setpIdEmpresa(pIdEmpresaFe);
                
            
                medioPagoComa = null;

                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpFechaRegistro(resultSet.getString(40));
                documento.setpEstadoAceptacion(resultSet.getString(41));
                documento.setpCondicionVentaDesc(resultSet.getString(42));
                documento.setpNombreEmpresaCom(resultSet.getString(43));
                documento.setpFaxEmpresa(resultSet.getString(44));
                documento.setpRazonSocialEmpEmitido(resultSet.getString(45));
                documento.setpNumIdentificacionEmitido(resultSet.getString(46));
                documento.setpCorreoElectronicoEmitido(resultSet.getString(47));

                
                documento.setpTotalSerGravados(resultSet.getString(48));
                documento.setpTotalSerExentos(resultSet.getString(49));
                documento.setpTotalMercanciasGravados(resultSet.getString(50));
                documento.setpTotalMercanciasExentos(resultSet.getString(51));
                documento.setpTotalGravado(resultSet.getString(52));
                documento.setpTotalExento(resultSet.getString(53));
                documento.setpTotalVenta(resultSet.getString(54));
                documento.setpTotalDescuentos(resultSet.getString(55));
                documento.setpReceptorNombreComercial(resultSet.getString(56));
                documento.setpTipoIdenReceptorDesc(resultSet.getString(57));
                documento.setEmitida(resultSet.getString(58));
                documento.setIdFacRef(resultSet.getString(59));
                documento.setOrdenCompra(resultSet.getString(60));
                
                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");
                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                if (pDatos.getpVerDetalle().equals("1")) {

                    pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO);

                    pstmt.setString(1, documento.getpIdDocumento());

                    resultSet2 = pstmt.executeQuery();

                    lineasDetalle = new ArrayList<>();

                    while (resultSet2.next()) {
                        lineaDetalle = new LineaDetalle();

                        lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                        lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                        lineaDetalle.setpLinea(resultSet2.getString(3));
                        lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                        lineaDetalle.setpCodigo(resultSet2.getString(5));
                        lineaDetalle.setpCantidad(resultSet2.getString(6));
                        lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                        lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                        lineaDetalle.setpDetalle(resultSet2.getString(9));
                        lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                        lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                        lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                        lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                        lineaDetalle.setpSubTotal(resultSet2.getString(14));
                        lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                        lineaDetalle.setpMercServ(resultSet2.getString(16));
                        lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                        lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                        pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                        resultSet3 = pstmt.executeQuery();

                        impuestos = new ArrayList<>();

                        while (resultSet3.next()) {
                            impuesto = new Impuesto();

                            impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                            impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                            impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                            impuesto.setpTarifa(resultSet3.getString(4));
                            impuesto.setpMonto(resultSet3.getString(5));
                            impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                            impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                            impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                            impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                            impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                            impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                            impuestos.add(impuesto);
                        }

                        lineaDetalle.setpImpuestos(impuestos);

                        lineasDetalle.add(lineaDetalle);

                    }

                    documento.setpLineasDetalle(lineasDetalle);
                }

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }
    
    @Override
    public MensajeDocumento getDocumentosNC(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        String tipoDocumento = null;

        try {
            connection = dataSource.getConnection();

            if (pDatos.getpIdDocumentoEmp() == null || pDatos.getpIdDocumentoEmp().equals("")) {
                pDatos.setpIdDocumentoEmp("NA");
            }

            if (pDatos.getpFechaDesde() == null || pDatos.getpFechaDesde().equals("")) {
                pDatos.setpFechaDesde("NA");
            }

            if (pDatos.getpFechaHasta() == null || pDatos.getpFechaHasta().equals("")) {
                pDatos.setpFechaHasta("NA");
            }

            if (pDatos.getpIdenCliente() == null || pDatos.getpIdenCliente().equals("")) {
                pDatos.setpIdenCliente("NA");
            }

            if (pDatos.getpNombreCliente() == null || pDatos.getpNombreCliente().equals("")) {
                pDatos.setpNombreCliente("NA");
            }

            if (pDatos.getpClave() == null || pDatos.getpClave().equals("")) {
                pDatos.setpClave("NA");
            }

            if (pDatos.getpConsecutivo() == null || pDatos.getpConsecutivo().equals("")) {
                pDatos.setpConsecutivo("NA");
            }

            if (pDatos.getpMonto() == null || pDatos.getpMonto().equals("")) {
                pDatos.setpMonto("NA");
            }

            pDatos.setpVerDetalle("1");

            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_NC);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDatos.getpIdDocumentoEmp());
            pstmt.setString(3, pDatos.getpIdDocumentoEmp());
            pstmt.setString(4, pDatos.getpFechaDesde());
            pstmt.setString(5, pDatos.getpFechaDesde());
            pstmt.setString(6, pDatos.getpFechaHasta());
            pstmt.setString(7, pDatos.getpFechaHasta());
            pstmt.setString(8, pDatos.getpIdenCliente());
            pstmt.setString(9, pDatos.getpIdenCliente());
            pstmt.setString(10, pDatos.getpNombreCliente());
            pstmt.setString(11, pDatos.getpNombreCliente());
            pstmt.setString(12, pDatos.getpClave());
            pstmt.setString(13, pDatos.getpClave());
            pstmt.setString(14, pDatos.getpConsecutivo());
            pstmt.setString(15, pDatos.getpConsecutivo());
            pstmt.setString(16, pDatos.getpMonto());
            pstmt.setString(17, pDatos.getpMonto());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                medioPagoComa = null;

                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpCondicionVentaDesc(resultSet.getString(40));
                documento.setpNombreEmpresaCom(resultSet.getString(41));
                documento.setpFaxEmpresa(resultSet.getString(42));

                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");

                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                if (pDatos.getpVerDetalle().equals("1")) {
                    pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO_NC);

                    pstmt.setString(1, documento.getpIdDocumento());

                    resultSet2 = pstmt.executeQuery();

                    lineasDetalle = new ArrayList<>();

                    while (resultSet2.next()) {
                        lineaDetalle = new LineaDetalle();

                        lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                        lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                        lineaDetalle.setpLinea(resultSet2.getString(3));
                        lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                        lineaDetalle.setpCodigo(resultSet2.getString(5));
                        lineaDetalle.setpCantidad(resultSet2.getString(6));
                        lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                        lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                        lineaDetalle.setpDetalle(resultSet2.getString(9));
                        lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                        lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                        lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                        lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                        lineaDetalle.setpSubTotal(resultSet2.getString(14));
                        lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                        lineaDetalle.setpMercServ(resultSet2.getString(16));
                        lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                        lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                        pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                        resultSet3 = pstmt.executeQuery();

                        impuestos = new ArrayList<>();

                        while (resultSet3.next()) {
                            impuesto = new Impuesto();

                            impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                            impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                            impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                            impuesto.setpTarifa(resultSet3.getString(4));
                            impuesto.setpMonto(resultSet3.getString(5));
                            impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                            impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                            impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                            impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                            impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                            impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                            impuestos.add(impuesto);
                        }

                        lineaDetalle.setpImpuestos(impuestos);

                        lineasDetalle.add(lineaDetalle);

                    }

                    documento.setpLineasDetalle(lineasDetalle);
                }

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }

    @Override
    public MensajeDocumento getDocumentosLikeNC(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        String tipoDocumento = null;

        try {
            connection = dataSource.getConnection();

           
            if (pDatos.getpConsecutivo() == null || pDatos.getpConsecutivo().equals("")) {
                mensajeDocumento.setStatus(Constantes.statusError);
                mensajeDocumento.setMensaje("El consecutivo del documento es requerido para la busqueda");
                closeDaoResources(resultSet, pstmt, connection);
            }

           

            pDatos.setpVerDetalle("1");

            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_NC_LIKE);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDatos.getpConsecutivo());


            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                medioPagoComa = null;

                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpCondicionVentaDesc(resultSet.getString(40));
                documento.setpNombreEmpresaCom(resultSet.getString(41));
                documento.setpFaxEmpresa(resultSet.getString(42));

                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");

                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                if (pDatos.getpVerDetalle().equals("1")) {
                    pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO_NC);

                    pstmt.setString(1, documento.getpIdDocumento());

                    resultSet2 = pstmt.executeQuery();

                    lineasDetalle = new ArrayList<>();

                    while (resultSet2.next()) {
                        lineaDetalle = new LineaDetalle();

                        lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                        lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                        lineaDetalle.setpLinea(resultSet2.getString(3));
                        lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                        lineaDetalle.setpCodigo(resultSet2.getString(5));
                        lineaDetalle.setpCantidad(resultSet2.getString(6));
                        lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                        lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                        lineaDetalle.setpDetalle(resultSet2.getString(9));
                        lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                        lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                        lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                        lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                        lineaDetalle.setpSubTotal(resultSet2.getString(14));
                        lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                        lineaDetalle.setpMercServ(resultSet2.getString(16));
                        lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                        lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                        pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                        resultSet3 = pstmt.executeQuery();

                        impuestos = new ArrayList<>();

                        while (resultSet3.next()) {
                            impuesto = new Impuesto();

                            impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                            impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                            impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                            impuesto.setpTarifa(resultSet3.getString(4));
                            impuesto.setpMonto(resultSet3.getString(5));
                            impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                            impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                            impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                            impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                            impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                            impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                            impuestos.add(impuesto);
                        }

                        lineaDetalle.setpImpuestos(impuestos);

                        lineasDetalle.add(lineaDetalle);

                    }

                    documento.setpLineasDetalle(lineasDetalle);
                }

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }

    @Override
    public MensajeBase getDocumentoHistorico(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeUpdate = new MensajeBase();
        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_DEVUELVE_HISTORICO);

            cstmt.setString(1, pDatos.getpConsecutivo());
            cstmt.registerOutParameter(2, Types.VARCHAR);
            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeUpdate.setStatus(cstmt.getString(2));
            mensajeUpdate.setMensaje(cstmt.getString(3));

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeUpdate;
    }

    @Override
    public MensajeDocumento getDocumentoCorreo() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        File fXmlFile;
        DocumentBuilderFactory dbFactory;
        DocumentBuilder dBuilder;
        Document doc;
        StringWriter sw;
        TransformerFactory tf;
        Transformer transformer;

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_CORREO);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                medioPagoComa = null;
               
                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpCondicionVentaDesc(resultSet.getString(40));
                documento.setpNombreEmpresaCom(resultSet.getString(41));
                documento.setpFaxEmpresa(resultSet.getString(42));
                documento.setpTotalSerGravados(resultSet.getString(43));
                documento.setpTotalSerExentos(resultSet.getString(44));
                documento.setpTotalMercanciasGravados(resultSet.getString(45));
                documento.setpTotalMercanciasExentos(resultSet.getString(46));
                documento.setpTotalGravado(resultSet.getString(47));
                documento.setpTotalExento(resultSet.getString(48));
                documento.setpTotalVenta(resultSet.getString(49));
                documento.setpTotalDescuentos(resultSet.getString(50));
                documento.setpReceptorNombreComercial(resultSet.getString(51));
                documento.setpTipoIdenReceptorDesc(resultSet.getString(52));
                documento.setEmitida(resultSet.getString(53));
                documento.setIdFacRef(resultSet.getString(54));
                documento.setOrdenCompra(resultSet.getString(55));
                documento.setpIdEmpresa(resultSet.getString(56));

                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");

                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                lineasDetalle = new ArrayList<>();

                while (resultSet2.next()) {
                    lineaDetalle = new LineaDetalle();

                    lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                    lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                    lineaDetalle.setpLinea(resultSet2.getString(3));
                    lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                    lineaDetalle.setpCodigo(resultSet2.getString(5));
                    lineaDetalle.setpCantidad(resultSet2.getString(6));
                    lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                    lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                    lineaDetalle.setpDetalle(resultSet2.getString(9));
                    lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                    lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                    lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                    lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                    lineaDetalle.setpSubTotal(resultSet2.getString(14));
                    lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                    lineaDetalle.setpMercServ(resultSet2.getString(16));
                    lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                    lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                    pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                    pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                    resultSet3 = pstmt.executeQuery();

                    impuestos = new ArrayList<>();

                    while (resultSet3.next()) {
                        impuesto = new Impuesto();

                        impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                        impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                        impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                        impuesto.setpTarifa(resultSet3.getString(4));
                        impuesto.setpMonto(resultSet3.getString(5));
                        impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                        impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                        impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                        impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                        impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                        impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                        impuestos.add(impuesto);
                    }

                    lineaDetalle.setpImpuestos(impuestos);

                    lineasDetalle.add(lineaDetalle);

                }

                documento.setpLineasDetalle(lineasDetalle);

                if (documento.getpTipoDocumentoEmp().equals(Constantes.tipoDocOtros)) {
                    documento.setpXmlRespuesta(null);
                    documento.setpXmlFirmado(null);
                    
                    documentos.add(documento);
                } else {
                    //REVISA SI XML SE ENCUENTRAN EN FILE SYSTEM
                    try{
                        
                        fXmlFile = new File(documento.getpRutaXml() + ".xml");
                        dbFactory = DocumentBuilderFactory.newInstance();
                        dBuilder = dbFactory.newDocumentBuilder();
                        doc = dBuilder.parse(fXmlFile);

                        doc.getDocumentElement().normalize();

                        sw = new StringWriter();
                        tf = TransformerFactory.newInstance();
                        transformer = tf.newTransformer();
                        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                        transformer.setOutputProperty(OutputKeys.INDENT, "no");
                        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                        transformer.transform(new DOMSource(doc), new StreamResult(sw));

                        documento.setpXmlFirmado(sw.toString());

                        fXmlFile = new File(documento.getpRutaXml() + "R.xml");
                        dbFactory = DocumentBuilderFactory.newInstance();
                        dBuilder = dbFactory.newDocumentBuilder();
                        doc = dBuilder.parse(fXmlFile);

                        doc.getDocumentElement().normalize();

                        sw = new StringWriter();
                        tf = TransformerFactory.newInstance();
                        transformer = tf.newTransformer();
                        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                        transformer.setOutputProperty(OutputKeys.INDENT, "no");
                        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                        transformer.transform(new DOMSource(doc), new StreamResult(sw));

                        documento.setpXmlRespuesta(sw.toString());
                        
                        documentos.add(documento);

                    }catch (Exception e){
                        setDocumentoError(documento.getpIdDocumento() , "1" , e.getMessage());
                    }
                }
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }

    
    
 @Override
    public MensajeDocumento syncTerceroEnvio() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        File fXmlFile;
        DocumentBuilderFactory dbFactory;
        DocumentBuilder dBuilder;
        Document doc;
        StringWriter sw;
        TransformerFactory tf;
        Transformer transformer;

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO_SYNC);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                medioPagoComa = null;
               
                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpCondicionVentaDesc(resultSet.getString(40));
                documento.setpNombreEmpresaCom(resultSet.getString(41));
                documento.setpFaxEmpresa(resultSet.getString(42));
                documento.setpTotalSerGravados(resultSet.getString(43));
                documento.setpTotalSerExentos(resultSet.getString(44));
                documento.setpTotalMercanciasGravados(resultSet.getString(45));
                documento.setpTotalMercanciasExentos(resultSet.getString(46));
                documento.setpTotalGravado(resultSet.getString(47));
                documento.setpTotalExento(resultSet.getString(48));
                documento.setpTotalVenta(resultSet.getString(49));
                documento.setpTotalDescuentos(resultSet.getString(50));
                documento.setpReceptorNombreComercial(resultSet.getString(51));
                documento.setpTipoIdenReceptorDesc(resultSet.getString(52));
                documento.setEmitida(resultSet.getString(53));
                documento.setIdFacRef(resultSet.getString(54));
                documento.setOrdenCompra(resultSet.getString(55));
                documento.setpIdEmpresa(resultSet.getString(56));

                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");

                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                lineasDetalle = new ArrayList<>();

                while (resultSet2.next()) {
                    lineaDetalle = new LineaDetalle();

                    lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                    lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                    lineaDetalle.setpLinea(resultSet2.getString(3));
                    lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                    lineaDetalle.setpCodigo(resultSet2.getString(5));
                    lineaDetalle.setpCantidad(resultSet2.getString(6));
                    lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                    lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                    lineaDetalle.setpDetalle(resultSet2.getString(9));
                    lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                    lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                    lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                    lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                    lineaDetalle.setpSubTotal(resultSet2.getString(14));
                    lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                    lineaDetalle.setpMercServ(resultSet2.getString(16));
                    lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                    lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                    pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                    pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                    resultSet3 = pstmt.executeQuery();

                    impuestos = new ArrayList<>();

                    while (resultSet3.next()) {
                        impuesto = new Impuesto();

                        impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                        impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                        impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                        impuesto.setpTarifa(resultSet3.getString(4));
                        impuesto.setpMonto(resultSet3.getString(5));
                        impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                        impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                        impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                        impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                        impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                        impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                        impuestos.add(impuesto);
                    }

                    lineaDetalle.setpImpuestos(impuestos);

                    lineasDetalle.add(lineaDetalle);

                }

                documento.setpLineasDetalle(lineasDetalle);

                
                if (documento.getpTipoDocumentoEmp().equals(Constantes.tipoDocOtros)) {
                    documento.setpXmlRespuesta(null);
                    documento.setpXmlFirmado(null);
                    
                    documentos.add(documento);

                } else {
                     //REVISA SI XML SE ENCUENTRAN EN FILE SYSTEM
                    try{
                    
                    fXmlFile = new File(documento.getpRutaXml() + ".xml");
                    dbFactory = DocumentBuilderFactory.newInstance();
                    dBuilder = dbFactory.newDocumentBuilder();
                    doc = dBuilder.parse(fXmlFile);

                    doc.getDocumentElement().normalize();

                    sw = new StringWriter();
                    tf = TransformerFactory.newInstance();
                    transformer = tf.newTransformer();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                    transformer.setOutputProperty(OutputKeys.INDENT, "no");
                    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                    transformer.transform(new DOMSource(doc), new StreamResult(sw));

                    documento.setpXmlFirmado(sw.toString());

                    fXmlFile = new File(documento.getpRutaXml() + "R.xml");
                    dbFactory = DocumentBuilderFactory.newInstance();
                    dBuilder = dbFactory.newDocumentBuilder();
                    doc = dBuilder.parse(fXmlFile);

                    doc.getDocumentElement().normalize();

                    sw = new StringWriter();
                    tf = TransformerFactory.newInstance();
                    transformer = tf.newTransformer();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                    transformer.setOutputProperty(OutputKeys.INDENT, "no");
                    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                    transformer.transform(new DOMSource(doc), new StreamResult(sw));

                    documento.setpXmlRespuesta(sw.toString());
                    
                    documentos.add(documento);

                    }catch (Exception e){
                        setDocumentoError(documento.getpIdDocumento() , "1" , e.getMessage());
                    }
                }

            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }

    private boolean setDocumentoError(String idDocumento , String error , String message)throws Exception{
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.UPDATE_DOCUMENTO_ENC_ERROR);
            pstmt.setString(1, error);
            pstmt.setString(2, message);
            pstmt.setString(3, idDocumento);

            result = pstmt.executeUpdate();

            return result == 1;

        } catch (Exception e) {
            return false;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
    }
    
    @Override
    public MensajeValidaRecepcion validaRecepcion(String pIdEmpresaFe, String pClave) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeValidaRecepcion mensajeConsulta = new MensajeValidaRecepcion();
        Boolean existe = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.VALIDA_DOCUMENTO_RECEPCION);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pClave);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                mensajeConsulta.setpAccion(resultSet.getString(1));
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El comprobante ya ha sido procesado ante DGTD");
                existe = true;
            }

            if (!existe) {
                mensajeConsulta.setStatus(Constantes.statusSuccess);
                mensajeConsulta.setMensaje("Puede continuar con el proceso");
            }

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getEstadosDocumento() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_ESTADOS_DOCUMENTO);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getCodigoMensajeHacienda() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CODIGO_MENSAJE_HACIENDA);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTipos getTipoDocRef(EmisorToken mClaims) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeTipos mensajeConsulta = new MensajeTipos();
        List<CodDescripcion> lista = new ArrayList<>();
        CodDescripcion catalogo;
        String codigoEmp;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CODIGO_DOC_REF);

            pstmt.setString(1, mClaims.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new CodDescripcion();

                catalogo.setCodigo(resultSet.getString(1));
                catalogo.setDescripcion(resultSet.getString(2));

                codigoEmp = resultSet.getString(3);

                List<String> codigosEmp = new ArrayList<>();

                if (codigoEmp != null) {
                    String[] parts = codigoEmp.split(",");

                    codigosEmp = Arrays.asList(parts);

                }

                catalogo.setCodigosEmp(codigosEmp);

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTipos(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getSituacionDocumento() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_SITUACION_DOCUMENTO);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getTipoDocumentosDet() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPO_DOCUMENTOS_DET);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addTipoDocRefEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_DOC_REF_EMP);

            cstmt.setString(1, pIdEmpresaFe);
            cstmt.setString(2, pTipoDocumento);
            cstmt.setString(3, pTipoDocumentoEmp);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public boolean deleteTipoDocRefEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_TIPO_DOC_REF_EMP);
            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pTipoDocumento);
            pstmt.setString(3, pTipoDocumentoEmp);

            result = pstmt.executeUpdate();
            //resultSet = pstmt.getGeneratedKeys();

//            if (resultSet.next()) {
//                result = resultSet.getInt(1);
//            }
            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public MensajeCatalogoFactura getCodigoDocumentoRef() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CODIGO_DOCUMENTO_REF);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addXmlRecepcion(EmisorToken mClaims, String pClave, InputStream pXml, FormDataContentDisposition pXmlDetail) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeInsert = new MensajeBase();
        String pIdDocumento = null;
        String pFechaEmision = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.VERIFICA_DOC_RECEPCION);
            pstmt.setString(1, mClaims.getIdEmpresaFe());
            pstmt.setString(2, pClave);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                pIdDocumento = resultSet.getString(1);
                pFechaEmision = resultSet.getString(2);
            }

            if (pIdDocumento == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("No se ha encontrado el documento registrado en el sistema");

                return mensajeInsert;
            }

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

            String rutaBase = getRutaXmlBase();

            String caracterSeparador = getCaracterSeparadorCarpeta();

            String fileLocation = rutaBase + mClaims.getIdEmpresaFe() + caracterSeparador + "Recibidos" + caracterSeparador + pFechaEmision + caracterSeparador;

            actualizaRutaDocumento(pIdDocumento, fileLocation);

            new File(fileLocation).mkdirs();

            fileLocation = fileLocation + pClave + ".xml";

            OutputStream out = new FileOutputStream(new File(fileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = pXml.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();

            mensajeInsert.setStatus(Constantes.statusSuccess);
            mensajeInsert.setMensaje("Xml registrado con exito");

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajePreferencias getPreferencias(EmisorToken mClaims) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajePreferencias mensajeConsulta = new MensajePreferencias();
        Preferencias preferencias = new Preferencias();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PREFERENCIAS);

            pstmt.setString(1, mClaims.getIdEmpresaFe());
            pstmt.setString(2, mClaims.getIdUsuario());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                preferencias.setIdSucursalEmp(resultSet.getString(1));
                preferencias.setIdPdvEmp(resultSet.getString(2));
                preferencias.setIdPagina(resultSet.getString(3));
                preferencias.setIdClienteEmp(resultSet.getString(4));
                preferencias.setIdMedioPagoEmp(resultSet.getString(5));
                preferencias.setIdCondicionVentaEmp(resultSet.getString(6));
                preferencias.setIdMonedaEmp(resultSet.getString(7));
                preferencias.setIdCodigoMedidaEmp(resultSet.getString(8));
                preferencias.setIdPreferencia(resultSet.getString(9));
                preferencias.setMostrarFactura(resultSet.getString(10));
                preferencias.setActualizarPrecios(resultSet.getString(11));
                preferencias.setNotificarEmisor(resultSet.getString(12));
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setPreferencias(preferencias);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase procesaDocumento(String pClave, String pStatus) throws SQLException {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;
        MensajeBase mensajeProcesa = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ACTUALIZA_STATUS_DOC);
            pstmt.setString(1, pStatus);
            pstmt.setString(2, pClave);

            result = pstmt.executeUpdate();

            if (result == 1) {
                mensajeProcesa.setStatus(Constantes.statusSuccess);
                mensajeProcesa.setMensaje("Gracias por preferirnos");
            } else {
                mensajeProcesa.setStatus(Constantes.statusError);
                mensajeProcesa.setMensaje("Su respuesta ya fue emitida anteriormente");
            }

        } catch (Exception e) {
            mensajeProcesa.setStatus(Constantes.statusError);
            mensajeProcesa.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeProcesa;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return mensajeProcesa;
    }

    @Override
    public MensajeBase addPreferencias(EmisorToken pToken, Preferencias pPreferencias) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_PREFERENCIAS);

            cstmt.setString(1, pToken.getIdEmpresaFe());
            cstmt.setString(2, pToken.getIdUsuario());
            cstmt.setString(3, pPreferencias.getIdSucursalEmp());
            cstmt.setString(4, pPreferencias.getIdPdvEmp());
            cstmt.setString(5, pPreferencias.getIdMedioPagoEmp());
            cstmt.setString(6, pPreferencias.getIdCondicionVentaEmp());
            cstmt.setString(7, pPreferencias.getIdMonedaEmp());
            cstmt.setString(8, pPreferencias.getIdPagina());
            cstmt.setString(9, pPreferencias.getIdClienteEmp());
            cstmt.setString(10, pPreferencias.getIdCodigoMedidaEmp());
            cstmt.setString(11, pPreferencias.getMostrarFactura());
            cstmt.setString(12, pPreferencias.getActualizarPrecios());
            cstmt.setString(13, pPreferencias.getNotificarEmisor());

            cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(14));
            mensajeInsert.setMensaje(cstmt.getString(15));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updatePreferencias(EmisorToken pToken, Preferencias pPreferencias) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeInsert mensajeInsert = new MensajeInsert();

        try {
            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_UPDATE_PREFERENCIAS);

            cstmt.setString(1, pToken.getIdEmpresaFe());
            cstmt.setString(2, pToken.getIdUsuario());
            cstmt.setString(3, pPreferencias.getIdSucursalEmp());
            cstmt.setString(4, pPreferencias.getIdPdvEmp());
            cstmt.setString(5, pPreferencias.getIdMedioPagoEmp());
            cstmt.setString(6, pPreferencias.getIdCondicionVentaEmp());
            cstmt.setString(7, pPreferencias.getIdMonedaEmp());
            cstmt.setString(8, pPreferencias.getIdPagina());
            cstmt.setString(9, pPreferencias.getIdClienteEmp());
            cstmt.setString(10, pPreferencias.getIdCodigoMedidaEmp());
            cstmt.setString(11, pPreferencias.getMostrarFactura());
            cstmt.setString(12, pPreferencias.getActualizarPrecios());
            cstmt.setString(13, pPreferencias.getNotificarEmisor());

            cstmt.registerOutParameter(14, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(15, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(14));
            mensajeInsert.setMensaje(cstmt.getString(15));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase procesaSyncEnvio(List<ProcesaSyncEnvio> pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            if (pDatos != null) {
                connection = dataSource.getConnection();

                for (ProcesaSyncEnvio i : pDatos) {
                    pstmt = connection.prepareStatement(Querys.ACTUALIZA_STATUS_SYNC_TERCEROS);
                    pstmt.setString(1, i.getpEstado());
                    pstmt.setString(2, i.getpResponse());
                    pstmt.setString(3, i.getpIdDocumento());

                    pstmt.executeUpdate();
                }
                mensajeInsert.setStatus(Constantes.statusSuccess);
                mensajeInsert.setMensaje("Registros procesados con exito");
            } else {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos un registro");
            }

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeInsert;
    }
    
     @Override
    public MensajeBase procesaInteresados(List<ProcesaCorreo> pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            if (pDatos != null) {
                connection = dataSource.getConnection();

                for (ProcesaCorreo i : pDatos) {
                    pstmt = connection.prepareStatement(Querys.ACTUALIZA_STATUS_INTERESADO_CORREO);
                    pstmt.setString(1, i.getpEstado());
                    pstmt.setString(2, i.getpIdInteresado());

                    pstmt.executeUpdate();
                }
                mensajeInsert.setStatus(Constantes.statusSuccess);
                mensajeInsert.setMensaje("Registros procesados con exito");
            } else {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos un registro");
            }

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase actualizaClave(EmisorToken pToken, ActualizaClave pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeBase = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ACTUALIZA_CLAVE_USUARIO);
            pstmt.setString(1, pDatos.getpClaveNueva());
            pstmt.setString(2, pToken.getIdUsuario());
            pstmt.setString(3, pDatos.getpClaveAnterior());

            int result = pstmt.executeUpdate();

            if (result != 1) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("No se pudo actualizar la clave de usuario");
            } else {
                mensajeBase.setStatus(Constantes.statusSuccess);
                mensajeBase.setMensaje("Clave cambiada con exito");
            }

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeBase;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeBase;
    }

    @Override
    public MensajeBase olvideClave(DatosOlvideClave pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeBase = new MensajeBase();
        String correo = null;
        String clave = null;
        String idUsuario;
        String hashCorreo;
        String urlClave;
        int tokenTime;
        String logoEmpresa;

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_VALIDA_OLVIDE_CLAVE);

            cstmt.setString(1, pDatos.getpUsuario());

            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(9, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeBase.setStatus(cstmt.getString(2));
            mensajeBase.setMensaje(cstmt.getString(3));
            idUsuario = cstmt.getString(4);
            correo = cstmt.getString(5);
            clave = cstmt.getString(6);
            urlClave = cstmt.getString(7);
            tokenTime = cstmt.getInt(8);
            logoEmpresa = cstmt.getString(9);

            if (mensajeBase.getStatus().equals(Constantes.statusError)) {
                return mensajeBase;
            }

            hashCorreo = JWT.generateHashCorreo(idUsuario, 3);

            String correoSoporte = "soporte@fullfactura.com";

            urlClave = urlClave + "username=" + pDatos.getpUsuario() + "&hash=" + hashCorreo;

            String mensajeCorreo = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                    + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                    + "  <head>\n"
                    + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                    + "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                    + "    <title>Set up a new password for [Product Name]</title>\n"
                    + "    <!-- \n"
                    + "    The style block is collapsed on page load to save you some scrolling.\n"
                    + "    Postmark automatically inlines all CSS properties for maximum email client \n"
                    + "    compatibility. You can just update styles here, and Postmark does the rest.\n"
                    + "    -->\n"
                    + "    <style type=\"text/css\" rel=\"stylesheet\" media=\"all\">\n"
                    + "    /* Base ------------------------------ */\n"
                    + "    \n"
                    + "    *:not(br):not(tr):not(html) {\n"
                    + "      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n"
                    + "      box-sizing: border-box;\n"
                    + "    }\n"
                    + "    \n"
                    + "    body {\n"
                    + "      width: 100% !important;\n"
                    + "      height: 100%;\n"
                    + "      margin: 0;\n"
                    + "      line-height: 1.4;\n"
                    + "      background-color: #F2F4F6;\n"
                    + "      color: #74787E;\n"
                    + "      -webkit-text-size-adjust: none;\n"
                    + "    }\n"
                    + "    \n"
                    + "    p,\n"
                    + "    ul,\n"
                    + "    ol,\n"
                    + "    blockquote {\n"
                    + "      line-height: 1.4;\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    a {\n"
                    + "      color: #3869D4;\n"
                    + "    }\n"
                    + "    \n"
                    + "    a img {\n"
                    + "      border: none;\n"
                    + "    }\n"
                    + "    \n"
                    + "    td {\n"
                    + "      word-break: break-word;\n"
                    + "    }\n"
                    + "    /* Layout ------------------------------ */\n"
                    + "    \n"
                    + "    .email-wrapper {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      background-color: #F2F4F6;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-content {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "    }\n"
                    + "    /* Masthead ----------------------- */\n"
                    + "    \n"
                    + "    .email-masthead {\n"
                    + "      padding: 25px 0;\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-masthead_logo {\n"
                    + "      width: 94px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-masthead_name {\n"
                    + "      font-size: 16px;\n"
                    + "      font-weight: bold;\n"
                    + "      color: #bbbfc3;\n"
                    + "      text-decoration: none;\n"
                    + "      text-shadow: 0 1px 0 white;\n"
                    + "    }\n"
                    + "    /* Body ------------------------------ */\n"
                    + "    \n"
                    + "    .email-body {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      border-top: 1px solid #EDEFF2;\n"
                    + "      border-bottom: 1px solid #EDEFF2;\n"
                    + "      background-color: #FFFFFF;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-body_inner {\n"
                    + "      width: 570px;\n"
                    + "      margin: 0 auto;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 570px;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      background-color: #FFFFFF;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-footer {\n"
                    + "      width: 570px;\n"
                    + "      margin: 0 auto;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 570px;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .email-footer p {\n"
                    + "      color: #AEAEAE;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .body-action {\n"
                    + "      width: 100%;\n"
                    + "      margin: 30px auto;\n"
                    + "      padding: 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .body-sub {\n"
                    + "      margin-top: 25px;\n"
                    + "      padding-top: 25px;\n"
                    + "      border-top: 1px solid #EDEFF2;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .content-cell {\n"
                    + "      padding: 35px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .preheader {\n"
                    + "      display: none !important;\n"
                    + "      visibility: hidden;\n"
                    + "      mso-hide: all;\n"
                    + "      font-size: 1px;\n"
                    + "      line-height: 1px;\n"
                    + "      max-height: 0;\n"
                    + "      max-width: 0;\n"
                    + "      opacity: 0;\n"
                    + "      overflow: hidden;\n"
                    + "    }\n"
                    + "    /* Attribute list ------------------------------ */\n"
                    + "    \n"
                    + "    .attributes {\n"
                    + "      margin: 0 0 21px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .attributes_content {\n"
                    + "      background-color: #EDEFF2;\n"
                    + "      padding: 16px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .attributes_item {\n"
                    + "      padding: 0;\n"
                    + "    }\n"
                    + "    /* Related Items ------------------------------ */\n"
                    + "    \n"
                    + "    .related {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 25px 0 0 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .related_item {\n"
                    + "      padding: 10px 0;\n"
                    + "      color: #74787E;\n"
                    + "      font-size: 15px;\n"
                    + "      line-height: 18px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .related_item-title {\n"
                    + "      display: block;\n"
                    + "      margin: .5em 0 0;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .related_item-thumb {\n"
                    + "      display: block;\n"
                    + "      padding-bottom: 10px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .related_heading {\n"
                    + "      border-top: 1px solid #EDEFF2;\n"
                    + "      text-align: center;\n"
                    + "      padding: 25px 0 10px;\n"
                    + "    }\n"
                    + "    /* Discount Code ------------------------------ */\n"
                    + "    \n"
                    + "    .discount {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 24px;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "      background-color: #EDEFF2;\n"
                    + "      border: 2px dashed #9BA2AB;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .discount_heading {\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .discount_body {\n"
                    + "      text-align: center;\n"
                    + "      font-size: 15px;\n"
                    + "    }\n"
                    + "    /* Social Icons ------------------------------ */\n"
                    + "    \n"
                    + "    .social {\n"
                    + "      width: auto;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .social td {\n"
                    + "      padding: 0;\n"
                    + "      width: auto;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .social_icon {\n"
                    + "      height: 20px;\n"
                    + "      margin: 0 8px 10px 8px;\n"
                    + "      padding: 0;\n"
                    + "    }\n"
                    + "    /* Data table ------------------------------ */\n"
                    + "    \n"
                    + "    .purchase {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 35px 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_content {\n"
                    + "      width: 100%;\n"
                    + "      margin: 0;\n"
                    + "      padding: 25px 0 0 0;\n"
                    + "      -premailer-width: 100%;\n"
                    + "      -premailer-cellpadding: 0;\n"
                    + "      -premailer-cellspacing: 0;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_item {\n"
                    + "      padding: 10px 0;\n"
                    + "      color: #74787E;\n"
                    + "      font-size: 15px;\n"
                    + "      line-height: 18px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_heading {\n"
                    + "      padding-bottom: 8px;\n"
                    + "      border-bottom: 1px solid #EDEFF2;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_heading p {\n"
                    + "      margin: 0;\n"
                    + "      color: #9BA2AB;\n"
                    + "      font-size: 12px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_footer {\n"
                    + "      padding-top: 15px;\n"
                    + "      border-top: 1px solid #EDEFF2;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_total {\n"
                    + "      margin: 0;\n"
                    + "      text-align: right;\n"
                    + "      font-weight: bold;\n"
                    + "      color: #2F3133;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .purchase_total--label {\n"
                    + "      padding: 0 15px 0 0;\n"
                    + "    }\n"
                    + "    /* Utilities ------------------------------ */\n"
                    + "    \n"
                    + "    .align-right {\n"
                    + "      text-align: right;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .align-left {\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .align-center {\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    /*Media Queries ------------------------------ */\n"
                    + "    \n"
                    + "    @media only screen and (max-width: 600px) {\n"
                    + "      .email-body_inner,\n"
                    + "      .email-footer {\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    \n"
                    + "    @media only screen and (max-width: 500px) {\n"
                    + "      .button {\n"
                    + "        width: 100% !important;\n"
                    + "      }\n"
                    + "    }\n"
                    + "    /* Buttons ------------------------------ */\n"
                    + "    \n"
                    + "    .button {\n"
                    + "      background-color: #3869D4;\n"
                    + "      border-top: 10px solid #3869D4;\n"
                    + "      border-right: 18px solid #3869D4;\n"
                    + "      border-bottom: 10px solid #3869D4;\n"
                    + "      border-left: 18px solid #3869D4;\n"
                    + "      display: inline-block;\n"
                    + "      color: #FFF;\n"
                    + "      text-decoration: none;\n"
                    + "      border-radius: 3px;\n"
                    + "      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n"
                    + "      -webkit-text-size-adjust: none;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .button--green {\n"
                    + "      background-color: #22BC66;\n"
                    + "      border-top: 10px solid #22BC66;\n"
                    + "      border-right: 18px solid #22BC66;\n"
                    + "      border-bottom: 10px solid #22BC66;\n"
                    + "      border-left: 18px solid #22BC66;\n"
                    + "    }\n"
                    + "    \n"
                    + "    .button--red {\n"
                    + "      background-color: #FF6136;\n"
                    + "      border-top: 10px solid #FF6136;\n"
                    + "      border-right: 18px solid #FF6136;\n"
                    + "      border-bottom: 10px solid #FF6136;\n"
                    + "      border-left: 18px solid #FF6136;\n"
                    + "    }\n"
                    + "    /* Type ------------------------------ */\n"
                    + "    \n"
                    + "    h1 {\n"
                    + "      margin-top: 0;\n"
                    + "      color: #2F3133;\n"
                    + "      font-size: 19px;\n"
                    + "      font-weight: bold;\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    h2 {\n"
                    + "      margin-top: 0;\n"
                    + "      color: #2F3133;\n"
                    + "      font-size: 16px;\n"
                    + "      font-weight: bold;\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    h3 {\n"
                    + "      margin-top: 0;\n"
                    + "      color: #2F3133;\n"
                    + "      font-size: 14px;\n"
                    + "      font-weight: bold;\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    p {\n"
                    + "      margin-top: 0;\n"
                    + "      color: #74787E;\n"
                    + "      font-size: 16px;\n"
                    + "      line-height: 1.5em;\n"
                    + "      text-align: left;\n"
                    + "    }\n"
                    + "    \n"
                    + "    p.sub {\n"
                    + "      font-size: 12px;\n"
                    + "    }\n"
                    + "    \n"
                    + "    p.center {\n"
                    + "      text-align: center;\n"
                    + "    }\n"
                    + "    </style>\n"
                    + "  </head>\n"
                    + "  <body>\n"
                    + "    <table class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "      <tr>\n"
                    + "        <td align=\"center\">\n"
                    + "          <table class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "            <tr>\n"
                    + "              <td class=\"email-masthead\">\n"
                    + "                <img src=\"" + logoEmpresa + "\" alt=\"Full Factura\" width=\"200\">\n"
                    + "              </td>\n"
                    + "            </tr>\n"
                    + "            <!-- Email Body -->\n"
                    + "            <tr>\n"
                    + "              <td class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "                <table class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "                  <!-- Body content -->\n"
                    + "                  <tr>\n"
                    + "                    <td class=\"content-cell\">\n"
                    + "                      <h1>Hola,</h1>\n"
                    + "                      <p>Se ha solicitado el cambio de clave para el usuario: " + pDatos.getpUsuario() + ". Presiona el siguiente botón para proceder con el cambio. <strong>Esta solicitud es válida solo durante las siguientes " + tokenTime + " horas.</strong></p>\n"
                    + "                      <!-- Action -->\n"
                    + "                      <table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "                        <tr>\n"
                    + "                          <td align=\"center\">\n"
                    + "                            <!-- Border based button\n"
                    + "                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design -->\n"
                    + "                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                    + "                              <tr>\n"
                    + "                                <td align=\"center\">\n"
                    + "                                  <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                    + "                                    <tr>\n"
                    + "                                      <td>\n"
                    + "                                        <a href=\"" + urlClave + "\" class=\"button button--green\" target=\"_blank\">Cambiar clave</a>\n"
                    + "                                      </td>\n"
                    + "                                    </tr>\n"
                    + "                                  </table>\n"
                    + "                                </td>\n"
                    + "                              </tr>\n"
                    + "                            </table>\n"
                    + "                          </td>\n"
                    + "                        </tr>\n"
                    + "                      </table>\n"
                    + "                      <p>Si usted no ha solicitado dicho cambio, haga caso omiso a este correo o contáctenos al siguiente correo electrónico " + correoSoporte + " si tiene alguna consulta.</p>\n"
                    + "                      <p>Gracias,\n"
                    + "                        <br>Equipo de FullFactura</p>\n"
                    + "                      <!-- Sub copy -->\n"
                    + "                      <table class=\"body-sub\">\n"
                    + "                        <tr>\n"
                    + "                          <td>\n"
                    + "                            <p class=\"sub\">Si tiene problemas con el botón de cambio de clave, copie y pegue el siguiente enlace en su navegador.</p>\n"
                    + "                            <p class=\"sub\">" + urlClave + "</p>\n"
                    + "                          </td>\n"
                    + "                        </tr>\n"
                    + "                      </table>\n"
                    + "                    </td>\n"
                    + "                  </tr>\n"
                    + "                </table>\n"
                    + "              </td>\n"
                    + "            </tr>\n"
                    + "            <tr>\n"
                    + "              <td>\n"
                    + "                <table class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n"
                    + "                  <tr>\n"
                    + "                    <td class=\"content-cell\" align=\"center\">\n"
                    + "                      <p class=\"sub align-center\">Copyright &copy; 2018 <a href=\"http://fullfactura.com/\" >FullFactura</a> by GECAM Tecnología</p>\n"
                    + "                      <p class=\"sub align-center\">\n"
                    + "                        Todos los derechos reservados\n"
                    + "                      </p>\n"
                    + "                    </td>\n"
                    + "                  </tr>\n"
                    + "                </table>\n"
                    + "              </td>\n"
                    + "            </tr>\n"
                    + "          </table>\n"
                    + "        </td>\n"
                    + "      </tr>\n"
                    + "    </table>\n"
                    + "  </body>\n"
                    + "</html>";

            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

            // Get a Properties object
            Properties props = System.getProperties();
            props.setProperty("mail.smtps.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.port", "465");
            props.setProperty("mail.smtp.socketFactory.port", "465");
            props.setProperty("mail.smtps.auth", "true");

            props.put("mail.smtps.quitwait", "false");

            Session session = Session.getInstance(props, null);

            // -- Create a new message --
            final MimeMessage msg = new MimeMessage(session);
            final Multipart multipart = new MimeMultipart("alternative");
            final MimeBodyPart htmlPart = new MimeBodyPart();

            htmlPart.setContent(mensajeCorreo, "text/html; charset=utf-8");
            multipart.addBodyPart(htmlPart);

            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress(correo, "no-reply"));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(pDatos.getpUsuario(), false));

            msg.setSubject("Restaurar clave de usuario");
            msg.setContent(multipart);
            msg.setSentDate(new Date());

            SMTPTransport t = (SMTPTransport) session.getTransport("smtps");

            t.connect("smtp.gmail.com", correo, clave);
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeBase;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeBase;
    }

    @Override
    public MensajeBase reseteaClave(TokenCorreo pToken, String pClave) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeBase = new MensajeBase();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.RESETEA_CLAVE_USUARIO);
            pstmt.setString(1, pClave);
            pstmt.setString(2, pToken.getpIdUsuario());

            int result = pstmt.executeUpdate();

            if (result != 1) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("No se pudo actualizar la clave de usuario");
            } else {
                mensajeBase.setStatus(Constantes.statusSuccess);
                mensajeBase.setMensaje("Clave cambiada con exito");
            }

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeBase;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeBase;
    }

    @Override
    public MensajeAddLogo addLogoEmpresa(EmisorToken mClaims, InputStream pLogo, FormDataContentDisposition pLogoDetail, String pRutaGlassfish) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeAddLogo mensajeInsert = new MensajeAddLogo();
        String rutalogo;
        OutputStream out;

        try {

            String caracterSeparador = getCaracterSeparadorCarpeta();

            String rutaTabla = "resources" + caracterSeparador + mClaims.getIdEmpresaFe() + caracterSeparador;
            String pRutaGfTemp = pRutaGlassfish;
            pRutaGfTemp = pRutaGfTemp.replace("api-factura", "api-recursos");

            rutalogo = pRutaGfTemp + rutaTabla;

            int length = pLogo.available();

            new File(rutalogo).mkdirs();

            String mimeType = pLogoDetail.getFileName().substring(pLogoDetail.getFileName().indexOf(".") + 1);

            rutalogo = rutalogo + pLogoDetail.getFileName();

            String ipServidor = getIpServidor();

            rutaTabla = "http://" + ipServidor + ":8080/api-recursos/resources/" + mClaims.getIdEmpresaFe() + "/" + pLogoDetail.getFileName();

            out = new FileOutputStream(new File(rutalogo));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = pLogo.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();

            connection = dataSource.getConnection();
            cstmt = connection.prepareCall(Querys.PR_ADD_LOGO_EMPRESA);

            cstmt.setString(1, mClaims.getIdEmpresaFe());
            cstmt.setString(2, rutaTabla);
            cstmt.setString(3, mimeType);
            cstmt.setString(4, String.valueOf(length));
            cstmt.setString(5, Constantes.TIPO_ARCHIVO_LOGO);

            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(6));
            mensajeInsert.setMensaje(cstmt.getString(7));
            mensajeInsert.setRutaLogo(rutaTabla);

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    public String getIpServidor() throws Exception {
        String ipServidor = null;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_VALOR1_PARAMETROS);
            pstmt.setString(1, Constantes.ID_PARAMETRO_IP_SERVIDOR);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                ipServidor = resultSet.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return ipServidor;
    }

    @Override
    public MensajeDatosEmpresa getDatosEmpresa(String pIdEmpresaFe) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        DatosEmpresa empresa = null;
        MensajeDatosEmpresa mensajeConsulta = new MensajeDatosEmpresa();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_EMPRESA);

            pstmt.setString(1, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                empresa = new DatosEmpresa();

                empresa.setNombre(resultSet.getString(1));
                String tiposIdEmpComa = resultSet.getString(2);
                empresa.setNumIdentificacion(resultSet.getString(3));
                empresa.setNombreComercial(resultSet.getString(4));
                String provinciasEmpComa = resultSet.getString(5);
                String cantonEmpComa = resultSet.getString(6);
                String distritoEmpComa = resultSet.getString(7);
                String barrioEmpComa = resultSet.getString(8);
                empresa.setOtrasSenas(resultSet.getString(9));
                empresa.setCodPaisTel(resultSet.getString(10));
                empresa.setNumTel(resultSet.getString(11));
                empresa.setCodPaisFax(resultSet.getString(12));
                empresa.setFax(resultSet.getString(13));
                empresa.setCorreoElectronico(resultSet.getString(14));
                empresa.setIdEmpresaEmp(resultSet.getString(15));
                empresa.setHaciendaPinCertificado(resultSet.getString(16));
                empresa.setHaciendaUsuario(resultSet.getString(17));
                empresa.setHaciendaClave(resultSet.getString(18));
                empresa.setHaciendaCertificado(resultSet.getString(19));
                empresa.setRutaLogoEmpresa(resultSet.getString(20));
                empresa.setAmbiente(resultSet.getString(21));
                empresa.setHaciendaCorreoFe(resultSet.getString(22));

                List<String> tiposIdEmp = new ArrayList<>();

                if (tiposIdEmpComa != null) {
                    String[] parts = tiposIdEmpComa.split(",");

                    tiposIdEmp = Arrays.asList(parts);
                }

                empresa.setTiposIdentificacionEmp(tiposIdEmp);

                List<String> provinciasEmp = new ArrayList<>();

                if (provinciasEmpComa != null) {
                    String[] parts = provinciasEmpComa.split(",");

                    provinciasEmp = Arrays.asList(parts);
                }

                empresa.setProvinciaEmp(provinciasEmp);

                List<String> cantonEmp = new ArrayList<>();

                if (cantonEmpComa != null) {
                    String[] parts = cantonEmpComa.split(",");

                    cantonEmp = Arrays.asList(parts);
                }

                empresa.setCantonEmp(cantonEmp);

                List<String> distritoEmp = new ArrayList<>();

                if (distritoEmpComa != null) {
                    String[] parts = distritoEmpComa.split(",");

                    distritoEmp = Arrays.asList(parts);
                }

                empresa.setDistritoEmp(distritoEmp);

                List<String> barrioEmp = new ArrayList<>();

                if (barrioEmpComa != null) {
                    String[] parts = barrioEmpComa.split(",");

                    barrioEmp = Arrays.asList(parts);
                }

                empresa.setBarrioEmp(barrioEmp);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpDatosEmpresa(empresa);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getProvincias(String pIdEmpresaFe) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PROVINCIAS);

            pstmt.setString(1, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getCantones(String pIdEmpresaFe, String pIdProvinciaEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CANTONES);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdProvinciaEmp);
            pstmt.setString(3, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getDistritos(String pIdEmpresaFe, String pIdProvinciaEmp, String pIdCantonEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DISTRITOS);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdCantonEmp);
            pstmt.setString(3, pIdEmpresaFe);
            pstmt.setString(4, pIdProvinciaEmp);
            pstmt.setString(5, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getBarrios(String pIdEmpresaFe, String pIdProvinciaEmp, String pIdCantonEmp, String pIdDistritoEmp) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_BARRIOS);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdDistritoEmp);
            pstmt.setString(3, pIdEmpresaFe);
            pstmt.setString(4, pIdCantonEmp);
            pstmt.setString(5, pIdEmpresaFe);
            pstmt.setString(6, pIdProvinciaEmp);
            pstmt.setString(7, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getProvinciasReg() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PROVINCIAS_REG);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getCantonesReg(String pIdProvincia) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CANTONES_REG);

            pstmt.setString(1, pIdProvincia);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getDistritosReg(String pIdCanton) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DISTRITOS_REG);

            pstmt.setString(1, pIdCanton);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getBarriosReg(String pIdDistrito) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_BARRIOS_REG);

            pstmt.setString(1, pIdDistrito);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCatalogoFactura getTiposIdenReg() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        List<IdDescripcion> lista = new ArrayList<>();
        IdDescripcion catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_IDEN_REG);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new IdDescripcion();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajePags getPags() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajePags mensajeConsulta = new MensajePags();
        List<Pags> lista = new ArrayList<>();
        Pags catalogo;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PAGS);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new Pags();

                catalogo.setIdPagina(resultSet.getString(1));
                catalogo.setUrl(resultSet.getString(2));
                catalogo.setNombrePagina(resultSet.getString(3));
                catalogo.setIdPaginaPadre(resultSet.getString(4));
                catalogo.setNivel(resultSet.getString(5));
                catalogo.setMostrarMenu(resultSet.getString(6));
                catalogo.setSecuencia(resultSet.getString(7));
                catalogo.setIcon(resultSet.getString(8));
                catalogo.setColor(resultSet.getString(9));
                catalogo.setMostrarPreferencias(resultSet.getString(10));
                catalogo.setEstado(resultSet.getString(11));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setPaginas(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeConsultaGrupos getGrupos(String pIdGrupoSesion, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeConsultaGrupos mensajeConsulta = new MensajeConsultaGrupos();
        List<GruposUsuarioConsulta> lista = new ArrayList<>();
        GruposUsuarioConsulta catalogo;
        String gruposMostrar;

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            switch (pIdGrupoSesion) {
                case "3":
                    gruposMostrar = "3,4";
                    break;
                case "5":
                    gruposMostrar = "5";
                    break;
                case "6":
                    gruposMostrar = "6,3,4";
                    break;
                default:
                    gruposMostrar = "NA";
                    break;
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_GRUPOS);

            pstmt.setString(1, pEstado);
            pstmt.setString(2, pEstado);
            pstmt.setString(3, gruposMostrar);
            pstmt.setString(4, gruposMostrar);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new GruposUsuarioConsulta();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));
                catalogo.setpEstado(resultSet.getString(3));
                catalogo.setpEstadoDesc(resultSet.getString(4));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeUsr getUsers(EmisorToken mClaims) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        MensajeUsr mensajeConsulta = new MensajeUsr();
        List<Usr> lista = new ArrayList<>();
        List<EmpresaUsuario> empresas;
        List<GruposUsuario> grupos;
        Usr catalogo;
        boolean superUsuario = false;

        try {
            connection = dataSource.getConnection();

//            pstmt = connection.prepareStatement(Querys.VERIFICA_SUPER_USUARIO);
//
//            pstmt.setString(1, mClaims.getIdUsuario());
//
//            resultSet = pstmt.executeQuery();
//
//            while (resultSet.next()) {
//                superUsuario = resultSet.getBoolean(1);
//            }
//
//            if (superUsuario) {
//                pstmt = connection.prepareStatement(Querys.GET_USERS);
//            } else {
//                pstmt = connection.prepareStatement(Querys.GET_USERS_FILTRADOS);
//            }
            pstmt = connection.prepareStatement(Querys.GET_USERS_FILTRADOS);

            pstmt.setString(1, mClaims.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new Usr();

                catalogo.setIdUsuario(resultSet.getString(1));
                catalogo.setDsUsuario(resultSet.getString(2));
                catalogo.setDsClave(resultSet.getString(3));
                catalogo.setDsDescripcion(resultSet.getString(4));
                catalogo.setIdEmpresaDefault(resultSet.getString(5));
                catalogo.setStUsuario(resultSet.getString(6));
                catalogo.setNombreEmpresaDefault(resultSet.getString(7));
                catalogo.setStUsuarioDescripcion(resultSet.getString(8));

                pstmt = connection.prepareStatement(Querys.GET_EMPRESA_USUARIO);

                pstmt.setString(1, catalogo.getIdUsuario());

                resultSet2 = pstmt.executeQuery();

                empresas = new ArrayList<>();

                while (resultSet2.next()) {
                    EmpresaUsuario empresa = new EmpresaUsuario();

                    empresa.setIdEmpresa(resultSet2.getString(1));
                    empresa.setNombre(resultSet2.getString(2));

                    pstmt = connection.prepareStatement(Querys.GET_GRUPOS_EMPRESA);

                    pstmt.setString(1, catalogo.getIdUsuario());
                    pstmt.setString(2, empresa.getIdEmpresa());

                    resultSet3 = pstmt.executeQuery();

                    grupos = new ArrayList<>();

                    while (resultSet3.next()) {
                        GruposUsuario grupo = new GruposUsuario();

                        grupo.setIdGrupo(resultSet3.getString(1));
                        grupo.setNombre(resultSet3.getString(2));

                        grupos.add(grupo);
                    }

                    empresa.setGrupos(grupos);

                    empresas.add(empresa);
                }

                catalogo.setEmpresas(empresas);

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setUsuarios(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeKpi getKpis(EmisorToken mClaims, FiltroKpi pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeKpi mensajeConsulta = new MensajeKpi();
        List<KpiArtFacturado> listaArticulos = new ArrayList<>();
        KpiArtFacturado articulo;
        List<KpiClienteFacturado> listaClientes = new ArrayList<>();
        KpiClienteFacturado cliente;
        KpiResumen resumen = new KpiResumen();

        try {
            if (pDatos.getCantRegistros() == null || pDatos.getCantRegistros().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Debe indicar la cantidad de registros");

                return mensajeConsulta;
            }

            if (pDatos.getFechaInicio() == null || pDatos.getFechaInicio().equals("")) {
                pDatos.setFechaInicio("NA");
            }

            if (pDatos.getFechaFin() == null || pDatos.getFechaFin().equals("")) {
                pDatos.setFechaFin("NA");
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_KPI_TOTAL_VENTAS);

            pstmt.setString(1, pDatos.getFechaInicio());
            pstmt.setString(2, pDatos.getFechaInicio());
            pstmt.setString(3, pDatos.getFechaFin());
            pstmt.setString(4, pDatos.getFechaFin());
            pstmt.setString(5, pDatos.getFechaInicio());
            pstmt.setString(6, pDatos.getFechaInicio());
            pstmt.setString(7, pDatos.getFechaFin());
            pstmt.setString(8, pDatos.getFechaFin());
            pstmt.setString(9, pDatos.getFechaInicio());
            pstmt.setString(10, pDatos.getFechaInicio());
            pstmt.setString(11, pDatos.getFechaFin());
            pstmt.setString(12, pDatos.getFechaFin());
            pstmt.setString(13, pDatos.getFechaInicio());
            pstmt.setString(14, pDatos.getFechaInicio());
            pstmt.setString(15, pDatos.getFechaFin());
            pstmt.setString(16, pDatos.getFechaFin());
            pstmt.setString(17, mClaims.getIdEmpresaFe());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                resumen.setFacturasEmitidas(resultSet.getString(1));
                resumen.setFacturasEmitidasPorcentaje(resultSet.getString(2));
                resumen.setFacturasRecibidas(resultSet.getString(3));
                resumen.setFacturasRecibidasPorcentaje(resultSet.getString(4));
                resumen.setTotalFacturas(resultSet.getString(5));
                resumen.setTotalVentas(resultSet.getString(6));
            }

            pstmt = connection.prepareStatement(Querys.GET_KPI_ARTICULOS_FAC);

            pstmt.setString(1, mClaims.getIdEmpresaFe());
            pstmt.setString(2, pDatos.getFechaInicio());
            pstmt.setString(3, pDatos.getFechaInicio());
            pstmt.setString(4, pDatos.getFechaFin());
            pstmt.setString(5, pDatos.getFechaFin());
            pstmt.setInt(6, Integer.valueOf(pDatos.getCantRegistros()));

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                articulo = new KpiArtFacturado();

                articulo.setCodigo(resultSet.getString(1));
                articulo.setCantidad(resultSet.getString(2));
                articulo.setDetalle(resultSet.getString(3));

                listaArticulos.add(articulo);
            }

            pstmt = connection.prepareStatement(Querys.GET_KPI_CLIENTES_FAC);

            pstmt.setString(1, mClaims.getIdEmpresaFe());
            pstmt.setString(2, pDatos.getFechaInicio());
            pstmt.setString(3, pDatos.getFechaInicio());
            pstmt.setString(4, pDatos.getFechaFin());
            pstmt.setString(5, pDatos.getFechaFin());
            pstmt.setInt(6, Integer.valueOf(pDatos.getCantRegistros()));

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                cliente = new KpiClienteFacturado();

                cliente.setNombreCliente(resultSet.getString(1));
                cliente.setTotalComprobante(resultSet.getString(2));

                listaClientes.add(cliente);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setArticulosMasFac(listaArticulos);
            mensajeConsulta.setClientesMasFac(listaClientes);
            mensajeConsulta.setResumen(resumen);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeUsrAdd addUser(EmisorToken mClaims, UsrAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeUsrAdd mensajeInsert = new MensajeUsrAdd();
        String idUsuarioReg;
        Boolean validaEmpresaDef = false;

        try {
            if (pDatos.getpGruposEmpresas() == null || pDatos.getpGruposEmpresas().isEmpty()) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos 1 relación con una empresa y grupo de usuario");

                return mensajeInsert;
            }

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_ADD_USUARIO);

            cstmt.setString(1, mClaims.getIdUsuario());
            cstmt.setString(2, pDatos.getpIdEmpresaDefault());
            cstmt.setString(3, pDatos.getpNombreUsuario());
            cstmt.setString(4, pDatos.getpUsuario());

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(8, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));
            idUsuarioReg = cstmt.getString(7);
            mensajeInsert.setClave(cstmt.getString(8));
            mensajeInsert.setUsuario(pDatos.getpUsuario());

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeInsert;
            }

            for (EmpresaGrupoAdd i : pDatos.getpGruposEmpresas()) {
                if (pDatos.getpIdEmpresaDefault().equals(i.getpIdEmpresa())) {
                    validaEmpresaDef = true;
                }

                cstmt = connection.prepareCall(Querys.PR_ADD_USUARIO_RELACION);

                cstmt.setString(1, idUsuarioReg);
                cstmt.setString(2, i.getpIdEmpresa());
                cstmt.setString(3, i.getpIdGrupo());

                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

                cstmt.execute();

                mensajeInsert.setStatus(cstmt.getString(4));
                mensajeInsert.setMensaje(cstmt.getString(5));

                if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                    connection.rollback();

                    mensajeInsert.setUsuario(null);
                    mensajeInsert.setClave(null);

                    return mensajeInsert;
                }
            }

            if (!validaEmpresaDef) {
                connection.rollback();

                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El id empresa por defecto no esta referenciado en las relaciones del usuario");
                mensajeInsert.setUsuario(null);
                mensajeInsert.setClave(null);

                return mensajeInsert;
            }

            mensajeInsert.setMensaje("Usuario ingresado con éxito");

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateUser(EmisorToken mClaims, UsrAdd pDatos, String pIdUsuario) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt;
        MensajeBase mensajeInsert = new MensajeBase();
        Boolean validaEmpresaDef = false;

        try {
            if (pDatos.getpGruposEmpresas() == null || pDatos.getpGruposEmpresas().isEmpty()) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar al menos 1 relación con una empresa y grupo de usuario");

                return mensajeInsert;
            }

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_UPDATE_USUARIO);

            cstmt.setString(1, mClaims.getIdUsuario());
            cstmt.setString(2, pDatos.getpIdEmpresaDefault());
            cstmt.setString(3, pDatos.getpNombreUsuario());
            cstmt.setString(4, pDatos.getpUsuario());
            cstmt.setString(5, pDatos.getpClave());
            cstmt.setString(6, pDatos.getpEstado());
            cstmt.setString(7, pIdUsuario);

            cstmt.registerOutParameter(8, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(9, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(8));
            mensajeInsert.setMensaje(cstmt.getString(9));

            if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeInsert;
            }

            pstmt = connection.prepareStatement(Querys.DELETE_USUARIO_RELACIONES);
            pstmt.setString(1, pIdUsuario);

            pstmt.executeUpdate();

            pstmt.close();

            pstmt = null;

            for (EmpresaGrupoAdd i : pDatos.getpGruposEmpresas()) {
                if (pDatos.getpIdEmpresaDefault().equals(i.getpIdEmpresa())) {
                    validaEmpresaDef = true;
                }

                cstmt = connection.prepareCall(Querys.PR_ADD_USUARIO_RELACION);

                cstmt.setString(1, pIdUsuario);
                cstmt.setString(2, i.getpIdEmpresa());
                cstmt.setString(3, i.getpIdGrupo());

                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
                cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

                cstmt.execute();

                mensajeInsert.setStatus(cstmt.getString(4));
                mensajeInsert.setMensaje(cstmt.getString(5));

                if (!mensajeInsert.getStatus().equals(Constantes.statusSuccess)) {
                    connection.rollback();

                    return mensajeInsert;
                }
            }

            if (!validaEmpresaDef) {
                connection.rollback();

                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El id empresa por defecto no esta referenciado en las relaciones del usuario");

                return mensajeInsert;
            }

            mensajeInsert.setMensaje("Usuario modificado con éxito");

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajePlanes getPlanes(String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajePlanes mensajeConsulta = new MensajePlanes();
        List<Plan> lista = new ArrayList<>();
        Plan catalogo;

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PLANES);

            pstmt.setString(1, pEstado);
            pstmt.setString(2, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new Plan();

                catalogo.setIdPlan(resultSet.getString(1));
                catalogo.setDescripcion(resultSet.getString(2));
                catalogo.setPrecioMensual(resultSet.getString(3));
                catalogo.setCantidadUsuarios(resultSet.getString(4));
                catalogo.setCodigoMoneda(resultSet.getString(5));
                catalogo.setDescripcionMoneda(resultSet.getString(6));
                catalogo.setSimboloMoneda(resultSet.getString(7));
                catalogo.setEstado(resultSet.getString(8));
                catalogo.setEstadoDesc(resultSet.getString(9));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setPlanes(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeTiposPlan getTiposPlan(String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeTiposPlan mensajeConsulta = new MensajeTiposPlan();
        List<TipoPlan> lista = new ArrayList<>();
        TipoPlan catalogo;

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_TIPOS_PLAN);

            pstmt.setString(1, pEstado);
            pstmt.setString(2, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new TipoPlan();

                catalogo.setIdTipoPlan(resultSet.getString(1));
                catalogo.setDescripcion(resultSet.getString(2));
                catalogo.setCantidadMeses(resultSet.getString(3));
                catalogo.setEstado(resultSet.getString(4));
                catalogo.setEstadoDesc(resultSet.getString(5));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTiposPlan(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addPlan(EmisorToken mClaims, PlanAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_ADD_PLAN);

            cstmt.setString(1, pDatos.getDescripcion());
            cstmt.setString(2, pDatos.getPrecioMensual());
            cstmt.setString(3, pDatos.getCantidadUsuarios());
            cstmt.setString(4, pDatos.getCodigoMoneda());

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updatePlan(EmisorToken mClaims, PlanAdd pDatos, String pIdPlan) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_UPDATE_PLAN);

            cstmt.setString(1, pDatos.getDescripcion());
            cstmt.setString(2, pDatos.getPrecioMensual());
            cstmt.setString(3, pDatos.getCantidadUsuarios());
            cstmt.setString(4, pDatos.getCodigoMoneda());
            cstmt.setString(5, pDatos.getEstado());
            cstmt.setString(6, pIdPlan);

            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(8, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(7));
            mensajeInsert.setMensaje(cstmt.getString(8));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase addTipoPlan(EmisorToken mClaims, TipoPlanAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_ADD_TIPO_PLAN);

            cstmt.setString(1, pDatos.getDescripcion());
            cstmt.setString(2, pDatos.getCantidadMeses());

            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(3));
            mensajeInsert.setMensaje(cstmt.getString(4));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateTipoPlan(EmisorToken mClaims, TipoPlanAdd pDatos, String pIdTipoPlan) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_UPDATE_TIPO_PLAN);

            cstmt.setString(1, pDatos.getDescripcion());
            cstmt.setString(2, pDatos.getCantidadMeses());
            cstmt.setString(3, pDatos.getEstado());
            cstmt.setString(4, pIdTipoPlan);

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(5));
            mensajeInsert.setMensaje(cstmt.getString(6));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeDistribuidores getDistribuidores(String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        DatosDistribuidor empresa;
        List<DatosDistribuidor> empresas = new ArrayList<>();
        MensajeDistribuidores mensajeConsulta = new MensajeDistribuidores();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DISTRIBUIDORES);

            pstmt.setString(1, pEstado);
            pstmt.setString(2, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                empresa = new DatosDistribuidor();

                empresa.setNombre(resultSet.getString(1));
                String tiposIdEmpComa = resultSet.getString(2);
                empresa.setNumIdentificacion(resultSet.getString(3));
                empresa.setNombreComercial(resultSet.getString(4));
                String provinciasEmpComa = resultSet.getString(5);
                String cantonEmpComa = resultSet.getString(6);
                String distritoEmpComa = resultSet.getString(7);
                String barrioEmpComa = resultSet.getString(8);
                empresa.setOtrasSenas(resultSet.getString(9));
                empresa.setCodPaisTel(resultSet.getString(10));
                empresa.setNumTel(resultSet.getString(11));
                empresa.setCodPaisFax(resultSet.getString(12));
                empresa.setFax(resultSet.getString(13));
                empresa.setCorreoElectronico(resultSet.getString(14));
                empresa.setIdEmpresaEmp(resultSet.getString(15));
                empresa.setHaciendaPinCertificado(resultSet.getString(16));
                empresa.setHaciendaUsuario(resultSet.getString(17));
                empresa.setHaciendaClave(resultSet.getString(18));
                empresa.setHaciendaCertificado(resultSet.getString(19));
                empresa.setRutaLogoEmpresa(resultSet.getString(20));
                empresa.setEstado(resultSet.getString(21));
                empresa.setEstadoDesc(resultSet.getString(22));
                empresa.setAdministraCobro(resultSet.getString(23));
                empresa.setAdministraCobroDesc(resultSet.getString(24));

                List<String> tiposIdEmp = new ArrayList<>();

                if (tiposIdEmpComa != null) {
                    String[] parts = tiposIdEmpComa.split(",");

                    tiposIdEmp = Arrays.asList(parts);
                }

                empresa.setTiposIdentificacionEmp(tiposIdEmp);

                List<String> provinciasEmp = new ArrayList<>();

                if (provinciasEmpComa != null) {
                    String[] parts = provinciasEmpComa.split(",");

                    provinciasEmp = Arrays.asList(parts);
                }

                empresa.setProvinciaEmp(provinciasEmp);

                List<String> cantonEmp = new ArrayList<>();

                if (cantonEmpComa != null) {
                    String[] parts = cantonEmpComa.split(",");

                    cantonEmp = Arrays.asList(parts);
                }

                empresa.setCantonEmp(cantonEmp);

                List<String> distritoEmp = new ArrayList<>();

                if (distritoEmpComa != null) {
                    String[] parts = distritoEmpComa.split(",");

                    distritoEmp = Arrays.asList(parts);
                }

                empresa.setDistritoEmp(distritoEmp);

                List<String> barrioEmp = new ArrayList<>();

                if (barrioEmpComa != null) {
                    String[] parts = barrioEmpComa.split(",");

                    barrioEmp = Arrays.asList(parts);
                }

                empresa.setBarrioEmp(barrioEmp);

                empresas.add(empresa);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setDistribuidores(empresas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addGrupoUsuario(EmisorToken mClaims, GrupoUsuarioAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();
        List<String> paginas;
        String idGrupo;

        try {
            paginas = pDatos.getPaginas();

            if (paginas == null || paginas.isEmpty()) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar a cuales páginas tiene acceso el grupo");

                return mensajeInsert;
            }

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_ADD_GRUPO_USUARIO);

            cstmt.setString(1, pDatos.getDescripcion());

            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(2));
            mensajeInsert.setMensaje(cstmt.getString(3));
            idGrupo = cstmt.getString(4);

            if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                connection.rollback();

                return mensajeInsert;
            }

            for (String idPagina : paginas) {
                cstmt = connection.prepareCall(Querys.PR_ADD_GRUPO_PAGINA);

                cstmt.setString(1, idGrupo);
                cstmt.setString(2, idPagina);

                cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

                cstmt.execute();

                mensajeInsert.setStatus(cstmt.getString(3));
                mensajeInsert.setMensaje(cstmt.getString(4));

                if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                    connection.rollback();

                    return mensajeInsert;
                }
            }

            mensajeInsert.setMensaje("Registro ingresado con exito");

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeBase updateGrupoUsuario(EmisorToken mClaims, GrupoUsuarioAdd pDatos, String pIdGrupo) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();
        List<String> paginas;
        String idGrupo;
        PreparedStatement pstmt;

        try {
            paginas = pDatos.getPaginas();

            if (paginas == null || paginas.isEmpty()) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar a cuales páginas tiene acceso el grupo");

                return mensajeInsert;
            }

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_UPDATE_GRUPO_USUARIO);

            cstmt.setString(1, pDatos.getDescripcion());
            cstmt.setString(2, pDatos.getEstado());
            cstmt.setString(3, pIdGrupo);

            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(4));
            mensajeInsert.setMensaje(cstmt.getString(5));

            if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                connection.rollback();

                return mensajeInsert;
            }

            pstmt = connection.prepareStatement(Querys.DELETE_GRUPO_PAGINAS);
            pstmt.setString(1, pIdGrupo);

            pstmt.executeUpdate();

            pstmt.close();

            pstmt = null;

            for (String idPagina : paginas) {
                cstmt = connection.prepareCall(Querys.PR_ADD_GRUPO_PAGINA);

                cstmt.setString(1, pIdGrupo);
                cstmt.setString(2, idPagina);

                cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
                cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

                cstmt.execute();

                mensajeInsert.setStatus(cstmt.getString(3));
                mensajeInsert.setMensaje(cstmt.getString(4));

                if (mensajeInsert.getStatus().equals(Constantes.statusError)) {
                    connection.rollback();

                    return mensajeInsert;
                }
            }

            mensajeInsert.setMensaje("Registro modificado con exito");

            connection.commit();

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public StreamingOutput backUpXml(String pIdEmpresaFe) throws Exception {
        String rutaBase;
        String rutaXml;
        StreamingOutput streamFile = null;

        try {
            rutaBase = getRutaXmlBase();

            rutaXml = rutaBase + pIdEmpresaFe;

            ZipUtil.pack(new File(rutaXml), new File(rutaXml + ".zip"));

            final Path rutaZip = Paths.get(rutaXml + ".zip");

            streamFile = new StreamingOutput() {
                @Override
                public void write(final OutputStream output) throws IOException, WebApplicationException {
                    try {
                        Files.copy(rutaZip, output);
                    } finally {
                        Files.delete(rutaZip);
                    }
                }
            };

        } catch (Exception e) {
            e.printStackTrace();
        }
        return streamFile;
    }

    @Override
    public StreamingOutput backUpXmlEspecifico(String pIdEmpresaFe, List<IdDocumento> pDocumentos, String zipName) throws Exception {
        String idsDocumento = "";
        List<String> rutas;
        File archivo;
        FileOutputStream fos;
        ZipOutputStream zos;
        byte[] buffer = new byte[1024];
        StreamingOutput streamFile = null;

        try {
            for (IdDocumento documento : pDocumentos) {
                idsDocumento += documento.getIdDocumento() + ",";
            }
            idsDocumento = idsDocumento.substring(0, idsDocumento.length() - 1);

            String rutaBase = getRutaXmlBase();

            String rutaZip = rutaBase + pIdEmpresaFe + File.separatorChar + zipName;

            rutas = getRutasXml(pIdEmpresaFe, idsDocumento);

            if (rutas != null || !rutas.isEmpty()) {
                fos = new FileOutputStream(rutaZip);
                zos = new ZipOutputStream(fos);

                for (String ruta : rutas) {
                    archivo = new File(ruta);

                    if (archivo.isFile()) {
                        FileInputStream fis = new FileInputStream(archivo);

                        zos.putNextEntry(new ZipEntry(archivo.getName()));

                        int length;

                        while ((length = fis.read(buffer)) > 0) {
                            zos.write(buffer, 0, length);
                        }

                        zos.closeEntry();
                        fis.close();
                    }
                }

                zos.close();

                final Path pathZip = Paths.get(rutaZip);

                streamFile = new StreamingOutput() {
                    @Override
                    public void write(final OutputStream output) throws IOException, WebApplicationException {
                        try {
                            Files.copy(pathZip, output);
                        } finally {
                            Files.delete(pathZip);
                        }
                    }
                };
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return streamFile;
    }

    public List<String> getRutasXml(String idEmpresa, String idsDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<String> rutas = new ArrayList<>();

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_RUTAS_XML);

            pstmt.setString(1, idEmpresa);
            pstmt.setString(2, idsDocumento);
            pstmt.setString(3, idEmpresa);
            pstmt.setString(4, idsDocumento);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                rutas.add(resultSet.getString(1));
            }

        } catch (Exception e) {
            rutas = new ArrayList<>();

            closeDaoResources(resultSet, pstmt, connection);

            return rutas;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return rutas;
    }

    @Override
    public MensajeEmpresas getEmpresas(String pEsDistribuidor, String pAdministraCobros) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        Empresa empresa = null;
        List<Empresa> empresas = new ArrayList<>();
        MensajeEmpresas mensajeConsulta = new MensajeEmpresas();

        try {
            if (pEsDistribuidor == null || pEsDistribuidor.equals("")) {
                pEsDistribuidor = "NA";
            }

            if (pAdministraCobros == null || pAdministraCobros.equals("")) {
                pAdministraCobros = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_EMPRESAS_LISTADO);

            pstmt.setString(1, pEsDistribuidor);
            pstmt.setString(2, pEsDistribuidor);
            pstmt.setString(3, pAdministraCobros);
            pstmt.setString(4, pAdministraCobros);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                empresa = new Empresa();

                empresa.setNombre(resultSet.getString(1));
                empresa.setTipoIdentificacion(resultSet.getString(2));
                empresa.setNumIdentificacion(resultSet.getString(3));
                empresa.setNombreComercial(resultSet.getString(4));
                empresa.setProvincia(resultSet.getString(5));
                empresa.setCanton(resultSet.getString(6));
                empresa.setDistrito(resultSet.getString(7));
                empresa.setBarrio(resultSet.getString(8));
                empresa.setOtrasSenas(resultSet.getString(9));
                empresa.setCodPaisTel(resultSet.getString(10));
                empresa.setNumTel(resultSet.getString(11));
                empresa.setCodPaisFax(resultSet.getString(12));
                empresa.setFax(resultSet.getString(13));
                empresa.setCorreoElectronico(resultSet.getString(14));
                empresa.setIdEmpresa(resultSet.getString(15));
                empresa.setHaciendaPinCertificado(resultSet.getString(16));
                empresa.setHaciendaUsuario(resultSet.getString(17));
                empresa.setHaciendaClave(resultSet.getString(18));
                empresa.setIdPlan(resultSet.getString(19));
                empresa.setPlanDesc(resultSet.getString(20));
                empresa.setIdTipoPlan(resultSet.getString(21));
                empresa.setTipoPlanDesc(resultSet.getString(22));
                empresa.setIdEmpresaPadre(resultSet.getString(23));
                empresa.setNombreEmpresaPadre(resultSet.getString(24));
                empresa.setFechaRegistro(resultSet.getString(25));
                empresa.setEsDistribuidor(resultSet.getString(26));
                empresa.setAdministraCobros(resultSet.getString(27));
                empresa.setManejaHacienda(resultSet.getString(28));
                empresa.setFechaProximoPago(resultSet.getString(29));
                empresa.setCorreoFe(resultSet.getString(30));

                empresas.add(empresa);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setEmpresas(empresas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeEmpresas getReferidos(String pIdEmpresaFe) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        Empresa empresa = null;
        List<Empresa> empresas = new ArrayList<>();
        MensajeEmpresas mensajeConsulta = new MensajeEmpresas();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_REFERIDOS);

            pstmt.setString(1, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                empresa = new Empresa();

                empresa.setNombre(resultSet.getString(1));
                empresa.setTipoIdentificacion(resultSet.getString(2));
                empresa.setNumIdentificacion(resultSet.getString(3));
                empresa.setNombreComercial(resultSet.getString(4));
                empresa.setProvincia(resultSet.getString(5));
                empresa.setCanton(resultSet.getString(6));
                empresa.setDistrito(resultSet.getString(7));
                empresa.setBarrio(resultSet.getString(8));
                empresa.setOtrasSenas(resultSet.getString(9));
                empresa.setCodPaisTel(resultSet.getString(10));
                empresa.setNumTel(resultSet.getString(11));
                empresa.setCodPaisFax(resultSet.getString(12));
                empresa.setFax(resultSet.getString(13));
                empresa.setCorreoElectronico(resultSet.getString(14));
                empresa.setIdEmpresa(resultSet.getString(15));
                empresa.setHaciendaPinCertificado(resultSet.getString(16));
                empresa.setHaciendaUsuario(resultSet.getString(17));
                empresa.setHaciendaClave(resultSet.getString(18));
                empresa.setIdPlan(resultSet.getString(19));
                empresa.setPlanDesc(resultSet.getString(20));
                empresa.setIdTipoPlan(resultSet.getString(21));
                empresa.setTipoPlanDesc(resultSet.getString(22));
                empresa.setIdEmpresaPadre(resultSet.getString(23));
                empresa.setNombreEmpresaPadre(resultSet.getString(24));
                empresa.setFechaRegistro(resultSet.getString(25));
                empresa.setEsDistribuidor(resultSet.getString(26));
                empresa.setAdministraCobros(resultSet.getString(27));
                empresa.setManejaHacienda(resultSet.getString(28));
                empresa.setFechaProximoPago(resultSet.getString(29));
                empresa.setCorreoFe(resultSet.getString(30));

                empresas.add(empresa);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setEmpresas(empresas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBancos getBancos(String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        Banco banco;
        List<Banco> bancos = new ArrayList<>();
        MensajeBancos mensajeConsulta = new MensajeBancos();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_BANCOS);

            pstmt.setString(1, pEstado);
            pstmt.setString(2, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                banco = new Banco();

                banco.setIdBanco(resultSet.getString(1));
                banco.setDescripcion(resultSet.getString(2));
                banco.setEstado(resultSet.getString(3));
                banco.setEstadoDesc(resultSet.getString(4));

                bancos.add(banco);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setBancos(bancos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeCuentasBanco getCuentasBanco(String pIdBanco, String pEstado) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        CuentaBanco cuenta;
        List<CuentaBanco> cuentas = new ArrayList<>();
        MensajeCuentasBanco mensajeConsulta = new MensajeCuentasBanco();

        try {
            if (pEstado == null || pEstado.equals("")) {
                pEstado = "NA";
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_CUENTAS_BANCO);

            pstmt.setString(1, pIdBanco);
            pstmt.setString(2, pEstado);
            pstmt.setString(3, pEstado);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                cuenta = new CuentaBanco();

                cuenta.setIdCuenta(resultSet.getString(1));
                cuenta.setIdBanco(resultSet.getString(2));
                cuenta.setCuentaCorriente(resultSet.getString(3));
                cuenta.setCuentaCliente(resultSet.getString(4));
                cuenta.setCuentaIban(resultSet.getString(5));
                cuenta.setCodMoneda(resultSet.getString(6));
                cuenta.setMonedaDesc(resultSet.getString(7));
                cuenta.setEstado(resultSet.getString(8));
                cuenta.setEstadoDesc(resultSet.getString(9));
                cuenta.setSimboloMoneda(resultSet.getString(10));

                cuentas.add(cuenta);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setCuentas(cuentas);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase addDepositoTransf(EmisorToken mClaims, DepositoTransfAdd pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        MensajeBase mensajeInsert = new MensajeBase();

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_ADD_DEPOSITO_TRANSF);

            cstmt.setString(1, mClaims.getIdEmpresaFe());
            cstmt.setString(2, mClaims.getIdUsuario());
            cstmt.setString(3, pDatos.getIdCuenta());
            cstmt.setString(4, pDatos.getNumeroComprobante());
            cstmt.setString(5, pDatos.getMonto());
            cstmt.setString(6, pDatos.getFechaDeposito());

            cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(8, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeInsert.setStatus(cstmt.getString(7));
            mensajeInsert.setMensaje(cstmt.getString(8));

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());

            closeDaoResources(null, cstmt, connection);

            return mensajeInsert;
        } finally {
            closeDaoResources(null, cstmt, connection);
        }
        return mensajeInsert;
    }

    @Override
    public MensajeDepositoTransf getDepositosTransf(String pIdEmpresaFe) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        DepositoTransf movimiento;
        List<DepositoTransf> movimientos = new ArrayList<>();
        MensajeDepositoTransf mensajeConsulta = new MensajeDepositoTransf();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DEPOSITOS_TRANSF);

            pstmt.setString(1, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                movimiento = new DepositoTransf();

                movimiento.setIdCuenta(resultSet.getString(1));
                movimiento.setNumeroComprobante(resultSet.getString(2));
                movimiento.setMonto(resultSet.getString(3));
                movimiento.setFechaDeposito(resultSet.getString(4));
                movimiento.setFechaRegistro(resultSet.getString(5));
                movimiento.setFechaValida(resultSet.getString(6));
                movimiento.setEstado(resultSet.getString(7));
                movimiento.setEstadoDesc(resultSet.getString(8));
                movimiento.setNumeroCuentaCorriente(resultSet.getString(9));
                movimiento.setNumeroCuentaCliente(resultSet.getString(10));
                movimiento.setNumeroCuentaIban(resultSet.getString(11));
                movimiento.setIdBanco(resultSet.getString(12));
                movimiento.setNombreBanco(resultSet.getString(13));
                movimiento.setObservaciones(resultSet.getString(14));
                movimiento.setIdMovimiento(resultSet.getString(15));
                movimiento.setMonedaDesc(resultSet.getString(16));
                movimiento.setEmpresa(resultSet.getString(17));

                movimientos.add(movimiento);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTransacciones(movimientos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeDepositoTransf getDepositosCheck() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        DepositoTransf movimiento;
        List<DepositoTransf> movimientos = new ArrayList<>();
        MensajeDepositoTransf mensajeConsulta = new MensajeDepositoTransf();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_DEPOSITOS_CHECK);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                movimiento = new DepositoTransf();

                movimiento.setIdCuenta(resultSet.getString(1));
                movimiento.setNumeroComprobante(resultSet.getString(2));
                movimiento.setMonto(resultSet.getString(3));
                movimiento.setFechaDeposito(resultSet.getString(4));
                movimiento.setFechaRegistro(resultSet.getString(5));
                movimiento.setFechaValida(resultSet.getString(6));
                movimiento.setEstado(resultSet.getString(7));
                movimiento.setEstadoDesc(resultSet.getString(8));
                movimiento.setNumeroCuentaCorriente(resultSet.getString(9));
                movimiento.setNumeroCuentaCliente(resultSet.getString(10));
                movimiento.setNumeroCuentaIban(resultSet.getString(11));
                movimiento.setIdBanco(resultSet.getString(12));
                movimiento.setNombreBanco(resultSet.getString(13));
                movimiento.setObservaciones(resultSet.getString(14));
                movimiento.setIdMovimiento(resultSet.getString(15));
                movimiento.setMonedaDesc(resultSet.getString(16));
                movimiento.setEmpresa(resultSet.getString(17));

                movimientos.add(movimiento);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setTransacciones(movimientos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeSaldosEmp getSaldosEmpresa(String pIdEmpresaFe) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        SaldosEmpresa datos = new SaldosEmpresa();
        MensajeSaldosEmp mensajeConsulta = new MensajeSaldosEmp();
        Double saldoFavor;
        Double saldoContra;
        String totalCobros = "0";
        String pagos = "0";

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_SALDOS_EMPRESA);

            pstmt.setString(1, pIdEmpresaFe);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                datos.setIdEmpresa(resultSet.getString(1));
                pagos = resultSet.getString(2);
                totalCobros = resultSet.getString(3);
            }

            saldoFavor = Double.valueOf(pagos) - Double.valueOf(totalCobros);

            if (saldoFavor < 0) {
                saldoContra = saldoFavor * -1;

                saldoFavor = 0.00;
            } else {
                saldoContra = 0.00;
            }

            datos.setSaldoFavor(saldoFavor.toString());
            datos.setSaldoContra(saldoContra.toString());

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setResumenSaldo(datos);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase validaTransaccion(EmisorToken mClaims, ValidaDeposito pDatos) throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeConsulta = new MensajeBase();
        int result = 0;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            cstmt = connection.prepareCall(Querys.PR_VALIDA_DEPOSITO);

            cstmt.setString(1, pDatos.getEstado());
            cstmt.setString(2, mClaims.getIdUsuario());
            cstmt.setString(3, pDatos.getObservaciones());
            cstmt.setString(4, pDatos.getIdMovimiento());

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeConsulta.setStatus(cstmt.getString(5));
            mensajeConsulta.setMensaje(cstmt.getString(6));

            if (!mensajeConsulta.getStatus().equals(Constantes.statusSuccess)) {
                connection.rollback();

                return mensajeConsulta;
            }

            connection.commit();

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            if (connection != null) {
                connection.rollback();
            }

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase generaCobros() throws Exception {
        Connection connection = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        MensajeBase mensajeConsulta = new MensajeBase();
        int result = 0;

        try {
            connection = dataSource.getConnection();

            cstmt = connection.prepareCall(Querys.PR_GENERA_COBROS);

            cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);

            cstmt.execute();

            mensajeConsulta.setStatus(cstmt.getString(1));
            mensajeConsulta.setMensaje(cstmt.getString(2));

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, cstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, cstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeInteresados getInteresadosDocumento(String pIdEmpresa, String pClaveDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeInteresados mensajeInteresados = new MensajeInteresados();
        List<InteresadoDocumento> lista = new ArrayList<>();
        InteresadoDocumento interesado;

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_ENVIOS_DOCUMENTO);
            pstmt.setString(1, pClaveDocumento);
            pstmt.setString(2, pIdEmpresa);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                interesado = new InteresadoDocumento();

                interesado.setpCorreo(resultSet.getString(1));
                interesado.setpEnviado(resultSet.getString(2));
                interesado.setpFechaEnvio(resultSet.getString(3));

                lista.add(interesado);
            }

            mensajeInteresados.setStatus(Constantes.statusSuccess);
            mensajeInteresados.setMensaje("Consulta realizada con exito");
            mensajeInteresados.setInteresadosDocumento(lista);

        } catch (Exception e) {
            mensajeInteresados.setStatus(Constantes.statusError);
            mensajeInteresados.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeInteresados;

        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return mensajeInteresados;
    }

    @Override
    public MensajeConsultaGrupos getGruposLista() throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeConsultaGrupos mensajeConsulta = new MensajeConsultaGrupos();
        List<GruposUsuarioConsulta> lista = new ArrayList<>();
        GruposUsuarioConsulta catalogo;
        String gruposMostrar;

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_GRUPOS_LISTA);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                catalogo = new GruposUsuarioConsulta();

                catalogo.setpId(resultSet.getString(1));
                catalogo.setpDescripcion(resultSet.getString(2));
                catalogo.setpEstado(resultSet.getString(3));
                catalogo.setpEstadoDesc(resultSet.getString(4));

                lista.add(catalogo);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setpLista(lista);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeBase reenviarCorreo(String pIdDocumento, List<Correo> correos) throws SQLException {
        MensajeBase mensajeUpdate = new MensajeBase();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();

            for (Correo correo : correos) {
                pstmt = connection.prepareStatement(Querys.ADD_INTERESADO_CORREO, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, correo.getpCorreo());
                pstmt.setString(2, pIdDocumento);

                pstmt.executeUpdate();
            }

            mensajeUpdate.setStatus(Constantes.statusSuccess);
            mensajeUpdate.setMensaje("Se han notificado los correos indicados");

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeUpdate;
    }



    @Override
    public MensajeBase reenviarCorreoMasivo(String pIdEmpresa, CorreoMasivo correos) throws SQLException {
        MensajeBase mensajeUpdate = new MensajeBase();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        CallableStatement cstmt = null;


        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();

            if(Integer.parseInt( correos.getTipoBusqueda()) == 4){
                //consecudtipos son los ID_DOCUEMNTO de la tabla principal

                for (String email : correos.getCorreos()) {
                    for (String idDoc : correos.getConsecutivos()) {

                        pstmt = connection.prepareStatement(Querys.ADD_INTERESADO_CORREO, Statement.RETURN_GENERATED_KEYS);
                        pstmt.setString(1, email);
                        pstmt.setString(2, idDoc);
                        pstmt.executeUpdate();
                    }
                }
            }
            else{

                for (String email : correos.getCorreos()) {

                    cstmt = connection.prepareCall(Querys.ADD_INTERESADO_CORREO_PROCEDURE);

                    cstmt.setString(1, email);
                    cstmt.setString(2, correos.getClave());
                    cstmt.setString(3, correos.getConsecutivo());
                    cstmt.setString(4, correos.getFechaInicio());
                    cstmt.setString(5, correos.getFechaFin());
                    cstmt.setString(6, correos.getTipoComprobante());
                    cstmt.setString(7, correos.getEstado());
                    cstmt.setString(8, correos.getIdentificacion());
                    cstmt.setString(9, correos.getRazonSocial());
                    cstmt.setInt(10, Integer.parseInt(correos.getTipoBusqueda()));
                    cstmt.registerOutParameter(11, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(12, java.sql.Types.VARCHAR);

                    cstmt.execute();
                }

            }
            mensajeUpdate.setStatus(Constantes.statusSuccess);
            mensajeUpdate.setMensaje("Se han notificado los correos indicados");

        } catch (Exception e) {

            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            if(cstmt != null){
                closeDaoResources(resultSet, cstmt, connection);

            }else{
                closeDaoResources(resultSet, pstmt, connection);

            }

            return mensajeUpdate;
        } finally {
            if(cstmt != null){
                closeDaoResources(resultSet, cstmt, connection);

            }else{
                closeDaoResources(resultSet, pstmt, connection);

            }
        }
        return mensajeUpdate;
    }



    @Override
    public MensajeBase reenviarCorreoMasivoTerceros(String pIdEmpresa, CorreoMasivo correos) throws SQLException {
        MensajeBase mensajeUpdate = new MensajeBase();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        CallableStatement cstmt = null;


        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();

            if(Integer.parseInt( correos.getTipoBusqueda()) == 4){
                //consecudtipos son los ID_DOCUEMNTO de la tabla principal

                for (String email : correos.getCorreos()) {
                    for (String idDoc : correos.getConsecutivos()) {

                        cstmt = connection.prepareCall(Querys.ADD_INTERESADO_CORREO_TERCEROS);

                        cstmt.setString(1, email);
                        cstmt.setString(2, idDoc);
                        cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
                        cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

                        cstmt.execute();
                    }
                }
            }
            else{

                for (String email : correos.getCorreos()) {

                    cstmt = connection.prepareCall(Querys.ADD_INTERESADO_CORREO_PROCEDURE);

                    cstmt.setString(1, email);
                    cstmt.setString(2, correos.getClave());
                    cstmt.setString(3, correos.getConsecutivo());
                    cstmt.setString(4, correos.getFechaInicio());
                    cstmt.setString(5, correos.getFechaFin());
                    cstmt.setString(6, correos.getTipoComprobante());
                    cstmt.setString(7, correos.getEstado());
                    cstmt.setString(8, correos.getIdentificacion());
                    cstmt.setString(9, correos.getRazonSocial());
                    cstmt.setInt(10, Integer.parseInt(correos.getTipoBusqueda()));
                    cstmt.registerOutParameter(11, java.sql.Types.VARCHAR);
                    cstmt.registerOutParameter(12, java.sql.Types.VARCHAR);

                    cstmt.execute();
                }

            }
            mensajeUpdate.setStatus(Constantes.statusSuccess);
            mensajeUpdate.setMensaje("Se han notificado los correos indicados");

        } catch (Exception e) {

            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            if(cstmt != null){
                closeDaoResources(resultSet, cstmt, connection);

            }else{
                closeDaoResources(resultSet, pstmt, connection);

            }

            return mensajeUpdate;
        } finally {
            if(cstmt != null){
                closeDaoResources(resultSet, cstmt, connection);

            }else{
                closeDaoResources(resultSet, pstmt, connection);

            }
        }
        return mensajeUpdate;
    }


    @Override
    public boolean deleteExoneracionCliente(String pIdExoneracion, String pIdEmpresa) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_EXONERACION_CLIENTE_EMPRESA);
            pstmt.setString(1, pIdExoneracion);
            pstmt.setString(2, pIdEmpresa);

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean deleteExoArticulo(String pId, String pIdArticulo, String pImpuesto) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.DELETE_EXO_CLIENTE_ARTICULO);
            pstmt.setString(1, pId);
            pstmt.setString(2, pIdArticulo);
            pstmt.setString(3, pImpuesto);

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public boolean updateExoneracionCliente(String pIdEmpresa, ExoneracionArt exoneracion) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.ACTUALIZA_EXONERACION_CLIENTE_EMP);
            pstmt.setString(1, exoneracion.getCodigoTipoExo());
            pstmt.setString(2, exoneracion.getNumeroDocumento());
            pstmt.setString(3, exoneracion.getNombreInstitucion());
            pstmt.setString(4, exoneracion.getFechaEmision());
            pstmt.setString(5, exoneracion.getMontoImpuesto());
            pstmt.setString(6, exoneracion.getPorcentajeCompra());
            pstmt.setString(7, exoneracion.getFechaHasta());
            pstmt.setString(8, exoneracion.getEstado());
            pstmt.setString(9, exoneracion.getIdExoneracion());
            pstmt.setString(10, pIdEmpresa);
            pstmt.setString(11, exoneracion.getIdCliente());

            result = pstmt.executeUpdate();

            if (result == 1) {
                resultado = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return resultado;
    }

    @Override
    public MensajeBase addExoneracionClienteEmpresa(String pIdEmpresa, ExoneracionArt exoneracion) throws SQLException {
        MensajeBase mensajeUpdate = new MensajeBase();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;
        boolean resultado = false;

        try {
            connection = dataSource.getConnection();

            pstmt = connection.prepareStatement(Querys.ADD_EXONERACION_CLIENTE_EMPRESA, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, pIdEmpresa);
            pstmt.setString(2, exoneracion.getIdCliente());
            pstmt.setString(3, exoneracion.getCodigoTipoExo());
            pstmt.setString(4, exoneracion.getNumeroDocumento());
            pstmt.setString(5, exoneracion.getNombreInstitucion());
            pstmt.setString(6, exoneracion.getFechaEmision());
            pstmt.setString(7, exoneracion.getMontoImpuesto());
            pstmt.setString(8, exoneracion.getPorcentajeCompra());
            pstmt.setString(9, exoneracion.getFechaHasta());

            pstmt.executeUpdate();

            mensajeUpdate.setStatus(Constantes.statusSuccess);
            mensajeUpdate.setMensaje("Se agrego la exoneracion al cliente");

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeUpdate;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeUpdate;
    }

    @Override
    public MensajeArticulosAplicaExo getArticulosAplicaExo(String pidEmpresa, String idReceptor) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<ArticuloAplicaExo> articulos = new ArrayList<>();
        ArticuloAplicaExo articuloAplicaExo;

        MensajeArticulosAplicaExo mensajeArticulosAplicaExo = new MensajeArticulosAplicaExo();

        try {

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_ARTICULOS_APLICA_EXO);

            pstmt.setString(1, idReceptor);
            pstmt.setString(2, pidEmpresa);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                articuloAplicaExo = new ArticuloAplicaExo();
                articuloAplicaExo.setIdImpuesto(resultSet.getString(1));
                articuloAplicaExo.setCodImpuestoHacienda(resultSet.getString(2));
                articuloAplicaExo.setDescImpuesto(resultSet.getString(3));
                articuloAplicaExo.setPorcentajeImpuesto(resultSet.getString(4));
                articuloAplicaExo.setExcepcionImp(resultSet.getString(5));
                articuloAplicaExo.setIdArticuloEmp(resultSet.getString(6));
                articuloAplicaExo.setIdCategorias(resultSet.getString(7));
                articuloAplicaExo.setDescProducto(resultSet.getString(8));
                articuloAplicaExo.setIdUnidadMedidaHacienda(resultSet.getString(9));
                articuloAplicaExo.setUnidadMedidaComercial(resultSet.getString(10));
                articuloAplicaExo.setTipoArticulo(resultSet.getString(11));
                articuloAplicaExo.setPrecio(resultSet.getString(12));
                articuloAplicaExo.setPorcentaje(resultSet.getString(13));
                articuloAplicaExo.setDescuento(resultSet.getString(14));
                articuloAplicaExo.setRecargo(resultSet.getString(15));
                articuloAplicaExo.setEstado(resultSet.getString(16));
                articuloAplicaExo.setCodigo(resultSet.getString(17));
                articuloAplicaExo.setDescCompleta(resultSet.getString(18));
                articuloAplicaExo.setCosto(resultSet.getString(19));

                articulos.add(articuloAplicaExo);
            }

            if (resultSet != null) {
                resultSet.close();
                resultSet = null;
            }

            mensajeArticulosAplicaExo.setStatus(Constantes.statusSuccess);
            mensajeArticulosAplicaExo.setMensaje("Consulta realizada con exito");
            mensajeArticulosAplicaExo.setArticulos(articulos);

        } catch (Exception e) {
            mensajeArticulosAplicaExo.setStatus(Constantes.statusError);
            mensajeArticulosAplicaExo.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeArticulosAplicaExo;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeArticulosAplicaExo;
    }

    @Override
    public MensajePlantillas getPlantillas(String pidPlantilla) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Plantilla> plantillas = new ArrayList<>();
        Plantilla plantilla;

        MensajePlantillas mensajePlantillas = new MensajePlantillas();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PLANTILLAS);

            pstmt.setString(1, pidPlantilla);
            pstmt.setString(2, pidPlantilla);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                plantilla = new Plantilla();

                plantilla.setIdPlantilla(resultSet.getString(1));
                plantilla.setCodTipoDocEmp(resultSet.getString(2));
                plantilla.setNombrePlantilla(resultSet.getString(3));
                plantilla.setXML(resultSet.getString(4));
                plantilla.setEstado(resultSet.getString(5));
                plantilla.setArchivos(resultSet.getInt(6));
                plantilla.setOtroTexto(resultSet.getInt(7));
                plantilla.setOtroContenido(resultSet.getInt(8));
                plantilla.setTipoCodiogArticulo(resultSet.getString(9));

                plantillas.add(plantilla);
            }

            mensajePlantillas.setStatus(Constantes.statusSuccess);
            mensajePlantillas.setMensaje("Consulta realizada con exito");
            mensajePlantillas.setPlantilas(plantillas);

        } catch (Exception e) {
            mensajePlantillas.setStatus(Constantes.statusError);
            mensajePlantillas.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajePlantillas;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajePlantillas;
    }
    
    @Override
    public MensajePlantillas getPlantillasCliente(String pidPlantilla, String pIdCliente, String pIdDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Plantilla> plantillas = new ArrayList<>();
        Plantilla plantilla;
        
        MensajePlantillas mensajePlantillas = new MensajePlantillas();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PLANTILLAS_CLIENTE);

            pstmt.setString(1, pIdCliente);
            pstmt.setString(2, pidPlantilla);
            pstmt.setString(3, pidPlantilla);
            pstmt.setString(4, pIdDocumento);
            pstmt.setString(5, pIdDocumento);
            
            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                plantilla = new Plantilla();

                plantilla.setIdPlantilla(resultSet.getString(1));
                plantilla.setCodTipoDocEmp(resultSet.getString(2));
                plantilla.setNombrePlantilla(resultSet.getString(3));
                plantilla.setXML(resultSet.getString(4));
                plantilla.setEstado(resultSet.getString(5));
                plantilla.setArchivos(resultSet.getInt(6));
                plantilla.setOtroTexto(resultSet.getInt(7));
                plantilla.setOtroContenido(resultSet.getInt(8));
                plantilla.setTipoCodiogArticulo(resultSet.getString(9));
                plantilla.setTags(getPlantillasTags("0", plantilla.getIdPlantilla()).getPlantilas());
                plantillas.add(plantilla);
            }

            mensajePlantillas.setStatus(Constantes.statusSuccess);
            mensajePlantillas.setMensaje("Consulta realizada con exito");
            mensajePlantillas.setPlantilas(plantillas);

        } catch (Exception e) {
            mensajePlantillas.setStatus(Constantes.statusError);
            mensajePlantillas.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajePlantillas;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajePlantillas;
    }

    @Override
    public MensajeTags getPlantillasTags(String pIdTag, String pidPlantilla) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<PlantillaTags> tags = new ArrayList<>();
        PlantillaTags tag;

        MensajeTags mensajeTags = new MensajeTags();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_PLANTILLA_TAGS);

            pstmt.setString(1, pIdTag);
            pstmt.setString(2, pIdTag);
            pstmt.setString(3, pidPlantilla);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                tag = new PlantillaTags();
                tag.setIdTag(resultSet.getString(1));
                tag.setIdPlantilla(resultSet.getString(2));
                tag.setDescripcion(resultSet.getString(3));
                tag.setCodTagReplace(resultSet.getString(4));
                tag.setKeyValueTag(resultSet.getString(5));
                tag.setTypeTag(resultSet.getString(6));
                tag.setOpcionalTag(resultSet.getString(7));
                tag.setLongitudTag(resultSet.getString(8));
                tag.setEsAdjunto(resultSet.getString(9));
                tag.setEstado(resultSet.getString(10));
                tag.setIdTagPadre(resultSet.getString(11));
                tag.setSecuencia(resultSet.getString(12));
                tag.setIdTipoTag(resultSet.getInt(13));
                tag.setHtml(resultSet.getInt(14));
                tag.setCdHtml(resultSet.getString(15));
                tag.setVisible(resultSet.getInt(16));
                tag.setAutoIncrement(resultSet.getInt(17));
                tag.setEndTagXml(resultSet.getString(18));
                tag.setEsExtension(resultSet.getInt(19));
                tags.add(tag);
            }

            mensajeTags.setStatus(Constantes.statusSuccess);
            mensajeTags.setMensaje("Consulta realizada con exito");
            mensajeTags.setPlantilas(tags);

        } catch (Exception e) {
            mensajeTags.setStatus(Constantes.statusError);
            mensajeTags.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeTags;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeTags;
    }

    
    @Override
    public MensajeDocumento getDocumentoEspeciales(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        ResultSet resultSet4 = null;
        ResultSet resultSet5 = null;
        List<Documento> documentos = new ArrayList<>();
        List<LineaDetalle> lineasDetalle;
        LineaDetalle lineaDetalle;
        List<Impuesto> impuestos;
        Impuesto impuesto;
        List<String> estados = new ArrayList<>();
        List<InteresadoDocumento> interesadosDoc;
        InteresadoDocumento interesadoDoc;
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        String medioPagoComa;
        String tipoDocumento = null;

        try {
            connection = dataSource.getConnection();

            if (pDatos.getpIdDocumentoEmp() == null || pDatos.getpIdDocumentoEmp().equals("")) {
                pDatos.setpIdDocumentoEmp("NA");
            }

            if (pDatos.getpVerDetalle() == null || pDatos.getpVerDetalle().equals("")) {
                pDatos.setpVerDetalle("0");
            }

            if (pDatos.getpEstado() == null || pDatos.getpEstado().isEmpty()) {
                estados.add("NA");
                pDatos.setpEstado(estados);
            }

            String estadosComa = String.join(",", pDatos.getpEstado());

            if (pDatos.getpEntidadEmite() == null || pDatos.getpEntidadEmite().equals("")) {
                pDatos.setpEntidadEmite("NA");
            }

            if (pDatos.getpFechaDesde() == null || pDatos.getpFechaDesde().equals("")) {
                pDatos.setpFechaDesde("NA");
            }

            if (pDatos.getpFechaHasta() == null || pDatos.getpFechaHasta().equals("")) {
                pDatos.setpFechaHasta("NA");
            }

            if (pDatos.getpIdenCliente() == null || pDatos.getpIdenCliente().equals("")) {
                pDatos.setpIdenCliente("NA");
            }

            if (pDatos.getpNombreCliente() == null || pDatos.getpNombreCliente().equals("")) {
                pDatos.setpNombreCliente("NA");
            }

            if (pDatos.getpClave() == null || pDatos.getpClave().equals("")) {
                pDatos.setpClave("NA");
            }

            if (pDatos.getpConsecutivo() == null || pDatos.getpConsecutivo().equals("")) {
                pDatos.setpConsecutivo("NA");
            }

            if (pDatos.getpMonto() == null || pDatos.getpMonto().equals("")) {
                pDatos.setpMonto("NA");
            }

            if (pDatos.getpTipoDocumentoEmp() == null || pDatos.getpTipoDocumentoEmp().equals("")) {
                tipoDocumento = "NA";
            } else {
                pstmt = connection.prepareStatement(Querys.GET_TIPO_DOC);

                pstmt.setString(1, pIdEmpresaFe);
                pstmt.setString(2, pDatos.getpTipoDocumentoEmp());

                resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    tipoDocumento = resultSet.getString(1);
                }

                if (tipoDocumento == null) {
                    tipoDocumento = "NA";
                }
            }

            pstmt = connection.prepareStatement(Querys.GET_DOCUMENTO);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pDatos.getpIdDocumentoEmp());
            pstmt.setString(3, pDatos.getpIdDocumentoEmp());
            pstmt.setString(4, estadosComa);
            pstmt.setString(5, estadosComa);
            pstmt.setString(6, pDatos.getpEntidadEmite());
            pstmt.setString(7, pDatos.getpEntidadEmite());
            pstmt.setString(8, pDatos.getpFechaDesde());
            pstmt.setString(9, pDatos.getpFechaDesde());
            pstmt.setString(10, pDatos.getpFechaHasta());
            pstmt.setString(11, pDatos.getpFechaHasta());
            pstmt.setString(12, tipoDocumento);
            pstmt.setString(13, tipoDocumento);
            pstmt.setString(14, pDatos.getpIdenCliente());
            pstmt.setString(15, pDatos.getpIdenCliente());
            pstmt.setString(16, pDatos.getpNombreCliente());
            pstmt.setString(17, pDatos.getpNombreCliente());
            pstmt.setString(18, pDatos.getpClave());
            pstmt.setString(19, pDatos.getpClave());
            pstmt.setString(20, pDatos.getpConsecutivo());
            pstmt.setString(21, pDatos.getpConsecutivo());
            pstmt.setString(22, pDatos.getpMonto());
            pstmt.setString(23, pDatos.getpMonto());

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                medioPagoComa = null;

                documento.setpIdDocumento(resultSet.getString(1));
                documento.setpIdDocumentoEmp(resultSet.getString(2));
                documento.setpTipoDocumentoEmp(resultSet.getString(3));
                documento.setpClave(resultSet.getString(4));
                documento.setpConsecutivo(resultSet.getString(5));
                documento.setpIdSucursalEmp(resultSet.getString(6));
                documento.setpIdPuntoVentaEmp(resultSet.getString(7));
                documento.setpCondicionVentaEmp(resultSet.getString(8));
                documento.setpPlazoCredito(resultSet.getString(9));
                medioPagoComa = resultSet.getString(10);
                documento.setpIdMonedaEmp(resultSet.getString(11));
                documento.setpTipoCambio(resultSet.getString(12));
                documento.setpEstado(resultSet.getString(13));
                documento.setpSituacion(resultSet.getString(14));
                documento.setpUltNovedad(resultSet.getString(15));
                documento.setpIdClienteEmp(resultSet.getString(16));
                documento.setpTipoIdClienteEmp(resultSet.getString(17));
                documento.setpNumeroIdCliente(resultSet.getString(18));
                documento.setpNombreCliente(resultSet.getString(19));
                documento.setpCorreoCliente(resultSet.getString(20));
                documento.setpEsReceptor(resultSet.getString(21));
                documento.setpEmitida(resultSet.getString(22));
                documento.setpCodMensajeHacienda(resultSet.getString(23));
                documento.setpTotalVentaNeta(resultSet.getString(24));
                documento.setpTotalImpuestos(resultSet.getString(25));
                documento.setpTotalComprobante(resultSet.getString(26));
                documento.setpEstadoDescripcion(resultSet.getString(27));
                documento.setpTipoDocumentoDesc(resultSet.getString(28));
                documento.setpFechaEmision(resultSet.getString(29));
                documento.setpCorreoEmpresa(resultSet.getString(30));
                documento.setpNombreEmpresa(resultSet.getString(31));
                documento.setpIdentificacionEmpresa(resultSet.getString(32));
                documento.setpTelefonoEmpresa(resultSet.getString(33));
                documento.setpRutaXml(resultSet.getString(34));
                documento.setpSimboloMoneda(resultSet.getString(35));
                documento.setpRutaLogo(resultSet.getString(36));
                documento.setpOtrasSenasEmpresa(resultSet.getString(37));
                documento.setpOtrasSenasCliente(resultSet.getString(38));
                documento.setpObservaciones(resultSet.getString(39));
                documento.setpFechaRegistro(resultSet.getString(40));
                documento.setpEstadoAceptacion(resultSet.getString(41));
                documento.setpCondicionVentaDesc(resultSet.getString(42));
                documento.setpNombreEmpresaCom(resultSet.getString(43));
                documento.setpFaxEmpresa(resultSet.getString(44));
                documento.setpRazonSocialEmpEmitido(resultSet.getString(45));
                documento.setpNumIdentificacionEmitido(resultSet.getString(46));
                documento.setpCorreoElectronicoEmitido(resultSet.getString(47));

                /*BUSCO EL ID DE LA PLANTILLA SI EL CLIENTE TIENE*/
                
                     /*Busco las plantillas del cliente .*/
                List<Plantilla> listaPlantillas = getPlantillasCliente("0", documento.getpIdClienteEmp(), "0").getPlantilas();
                if (listaPlantillas != null && !listaPlantillas.isEmpty()) { 
                      documento.setListaPlantillas(listaPlantillas);
                }
                
           
              /*  if (documento.getpIdClienteEmp() != null && documento.getpIdClienteEmp() != "") {
                    pstmt = connection.prepareStatement(Querys.GET_ID_PLANTILLA);
                    pstmt.setString(1, pIdEmpresaFe);
                    pstmt.setString(2, documento.getpIdClienteEmp());
                    resultSet4 = pstmt.executeQuery();
                    String idPlantilla = "";
                    while (resultSet4.next()) {
                        idPlantilla = resultSet4.getString(1);
                    }

                    Plantilla myPlantilla = getPlantillas(idPlantilla, null).getPlantilas().get(0);
                    myPlantilla.setTags(getPlantillasTags(pIdEmpresaFe, idPlantilla).getPlantilas());
                    documento.setPlantilla(myPlantilla);
                }*/

                List<String> mediosPagoEmp = new ArrayList<>();

                if (medioPagoComa != null) {
                    String[] parts3 = medioPagoComa.split(",");

                    mediosPagoEmp = Arrays.asList(parts3);
                }

                documento.setpMedioPagoEmp(mediosPagoEmp);

                pstmt = connection.prepareStatement(Querys.GET_INTERESADO_DOCUMENTO);

                pstmt.setString(1, documento.getpIdDocumento());

                resultSet2 = pstmt.executeQuery();

                interesadosDoc = new ArrayList<>();

                while (resultSet2.next()) {
                    interesadoDoc = new InteresadoDocumento();

                    interesadoDoc.setpIdDocumento(resultSet2.getString(1));
                    interesadoDoc.setpCorreo(resultSet2.getString(2));
                    interesadoDoc.setpEnviado(resultSet2.getString(3));
                    interesadoDoc.setpIdInteresado(resultSet2.getString(4));

                    interesadosDoc.add(interesadoDoc);

                }

                documento.setpInteresadosDoc(interesadosDoc);

                if (pDatos.getpVerDetalle().equals("1")) {
                    pstmt = connection.prepareStatement(Querys.GET_DETALLE_DOCUMENTO);

                    pstmt.setString(1, documento.getpIdDocumento());

                    resultSet2 = pstmt.executeQuery();

                    lineasDetalle = new ArrayList<>();

                    while (resultSet2.next()) {
                        lineaDetalle = new LineaDetalle();

                        lineaDetalle.setpIdDocumento(resultSet2.getString(1));
                        lineaDetalle.setpIdDocumentoDet(resultSet2.getString(2));
                        lineaDetalle.setpLinea(resultSet2.getString(3));
                        lineaDetalle.setpCodigoArtEmp(resultSet2.getString(4));
                        lineaDetalle.setpCodigo(resultSet2.getString(5));
                        lineaDetalle.setpCantidad(resultSet2.getString(6));
                        lineaDetalle.setpUnidadMedidaEmp(resultSet2.getString(7));
                        lineaDetalle.setpUnidadMedidaComercial(resultSet2.getString(8));
                        lineaDetalle.setpDetalle(resultSet2.getString(9));
                        lineaDetalle.setpPrecioUnitario(resultSet2.getString(10));
                        lineaDetalle.setpMontoTotal(resultSet2.getString(11));
                        lineaDetalle.setpMontoDescuento(resultSet2.getString(12));
                        lineaDetalle.setpNaturalezaDescuento(resultSet2.getString(13));
                        lineaDetalle.setpSubTotal(resultSet2.getString(14));
                        lineaDetalle.setpMontoTotalLinea(resultSet2.getString(15));
                        lineaDetalle.setpMercServ(resultSet2.getString(16));
                        lineaDetalle.setpCantidadRestante(resultSet2.getString(17));
                        lineaDetalle.setpMontoRestante(resultSet2.getString(18));

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTO_DETALLE);

                        pstmt.setString(1, lineaDetalle.getpIdDocumentoDet());

                        resultSet3 = pstmt.executeQuery();

                        impuestos = new ArrayList<>();

                        while (resultSet3.next()) {
                            impuesto = new Impuesto();

                            impuesto.setpIdDocumentoDet(resultSet3.getString(1));
                            impuesto.setpIdDocumentoImp(resultSet3.getString(2));
                            impuesto.setpCodigoImpuestoEmp(resultSet3.getString(3));
                            impuesto.setpTarifa(resultSet3.getString(4));
                            impuesto.setpMonto(resultSet3.getString(5));
                            impuesto.setpTipoExoneracionEmp(resultSet3.getString(6));
                            impuesto.setpNumeroDocumentoExo(resultSet3.getString(7));
                            impuesto.setpNombreInstitucionExo(resultSet3.getString(8));
                            impuesto.setpFechaEmisionExo(resultSet3.getString(9));
                            impuesto.setpMontoImpuestoExo(resultSet3.getString(10));
                            impuesto.setpPorcentajeCompraExo(resultSet3.getString(11));

                            impuestos.add(impuesto);
                        }

                        lineaDetalle.setpImpuestos(impuestos);

                        lineasDetalle.add(lineaDetalle);

                    }

                    documento.setpLineasDetalle(lineasDetalle);
                }

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

            if (resultSet4 != null) {
                resultSet4.close();
                resultSet4 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
    }
    
    
    @Override
    public MensajeResumenVenta getResumenVenta(String pIdEmpresaFe, String dInicio , String dFinal) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        MensajeResumenVenta mensajeConsulta = new MensajeResumenVenta();
        List<ResumenVenta> lVenta = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_RESUMEN_VENTA);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, dInicio);
            pstmt.setString(3, dFinal);

            
            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                
             ResumenVenta  venta = new ResumenVenta();

                venta.setIdTipoDocumento(resultSet.getString(1));
                venta.setTotal(resultSet.getString(2));
                venta.setCantidad(resultSet.getString(3));

                lVenta.add(venta);
            }

            mensajeConsulta.setStatus(Constantes.statusSuccess);
            mensajeConsulta.setMensaje("Consulta realizada con exito");
            mensajeConsulta.setmVenta(lVenta);

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeConsulta;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeConsulta;
    }

    @Override
    public MensajeDocumento getURLDoc(String pIdEmpresaFe, DocUrl pDatos) throws Exception {
       
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        ResultSet resultSet4 = null;
        List<Documento> documentos = new ArrayList<>();
     
        Documento documento;
        MensajeDocumento mensajeDocumento = new MensajeDocumento();

        try {
            connection = dataSource.getConnection();

            if (pDatos.getpIdDocumentoEmp() == null || pDatos.getpIdDocumentoEmp().equals("")) {
                pDatos.setpIdDocumentoEmp("NA");
            }

            

            if (pDatos.getpClave() == null || pDatos.getpClave().equals("")) {
                pDatos.setpClave("NA");
            }

            pstmt = connection.prepareStatement(Querys.GET_URL_DOCUMENTO);

            pstmt.setString(1, pDatos.getpIdDocumentoEmp());
            pstmt.setString(2, pDatos.getpClave());
            

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                documento = new Documento();
                documento.setpRutaXml(resultSet.getString(1));
                documento.setpFechaEmision(resultSet.getString(2));
                documento.setpFechaRegistro(resultSet.getString(3));

                documentos.add(documento);
            }

            mensajeDocumento.setStatus(Constantes.statusSuccess);
            mensajeDocumento.setMensaje("Consulta realizada con exito");
            mensajeDocumento.setDocumentos(documentos);

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

            if (resultSet4 != null) {
                resultSet4.close();
                resultSet4 = null;
            }

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeDocumento;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeDocumento;
         
    }

    @Override
    public MensajeReceptor getReceptorInfo(String pIdEmpresaFe, BuscarReceptor pDatos) throws Exception {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        List<Receptor> receptores = new ArrayList<>();
        List<Plantilla> listaPlantillas;
        Plantilla plantilla;
        Receptor receptor;
        MensajeReceptor mensajeReceptor = new MensajeReceptor();
        
        String nombre ;
        String ced;
        
        try {
            
            if(pDatos.getpCedula().trim().equals("")){
                ced = "NA";
            }else{
                ced = pDatos.getpCedula();
            }
            
            if(pDatos.getpNombre().trim().equals("")){
                nombre = "NA";
            }else{
                nombre = pDatos.getpCedula();
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_RECEPTOR_INFO);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, nombre);
            pstmt.setString(3, '%'+nombre+'%');
            pstmt.setString(4, ced);
            pstmt.setString(5, '%'+ced+'%');

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                receptor = new Receptor();
                receptor.setNombre(resultSet.getString(1));
                String tiposIdEmpComa = resultSet.getString(2);
                receptor.setNumIdentificacion(resultSet.getString(3));
                receptor.setIdentificacionExtranjero(resultSet.getString(4));
                receptor.setNombreComercial(resultSet.getString(5));
                String provinciasEmpComa = resultSet.getString(6);
                String cantonEmpComa = resultSet.getString(7);
                String distritoEmpComa = resultSet.getString(8);
                String barrioEmpComa = resultSet.getString(9);
                receptor.setOtrasSenas(resultSet.getString(10));
                receptor.setCodPaisTel(resultSet.getString(11));
                receptor.setNumTel(resultSet.getString(12));
                receptor.setCodPaisFax(resultSet.getString(13));
                receptor.setFax(resultSet.getString(14));
                receptor.setCorreoElectronico(resultSet.getString(15));
                receptor.setIdReceptorEmp(resultSet.getString(16));
                receptor.setTipoTributario(resultSet.getString(17));
                receptor.setEstado(resultSet.getString(18));
                receptor.setIdPlantilla(resultSet.getString(19));
                receptor.setEstadoDesc(resultSet.getString(20));
                receptor.setTipoIdentificacionDesc(resultSet.getString(21));
                receptor.setTipoTributarioDesc(resultSet.getString(22));

                List<String> tiposIdEmp = new ArrayList<>();

                if (tiposIdEmpComa != null) {
                    String[] parts = tiposIdEmpComa.split(",");

                    tiposIdEmp = Arrays.asList(parts);
                }

                receptor.setTiposIdentificacionEmp(tiposIdEmp);

                List<String> provinciasEmp = new ArrayList<>();

                if (provinciasEmpComa != null) {
                    String[] parts = provinciasEmpComa.split(",");

                    provinciasEmp = Arrays.asList(parts);
                }

                receptor.setProvinciaEmp(provinciasEmp);

                List<String> cantonEmp = new ArrayList<>();

                if (cantonEmpComa != null) {
                    String[] parts = cantonEmpComa.split(",");

                    cantonEmp = Arrays.asList(parts);
                }

                receptor.setCantonEmp(cantonEmp);

                List<String> distritoEmp = new ArrayList<>();

                if (distritoEmpComa != null) {
                    String[] parts = distritoEmpComa.split(",");

                    distritoEmp = Arrays.asList(parts);
                }

                receptor.setDistritoEmp(distritoEmp);

                List<String> barrioEmp = new ArrayList<>();

                if (barrioEmpComa != null) {
                    String[] parts = barrioEmpComa.split(",");

                    barrioEmp = Arrays.asList(parts);
                }

                receptor.setBarrioEmp(barrioEmp);

                List<ExoneracionArt> ExoList = getExoneraciones("0", pIdEmpresaFe, receptor.getIdReceptorEmp()).getExoneraciones();
                if (ExoList != null && ExoList.size() > 0) {
                    receptor.setExoneraciones(ExoList);
                    receptor.setExoGLobal(ExoList.get(0).getExoGlobal());
                } else {
                    receptor.setExoGLobal("0");
                }

                
                  /*Busco las plantillas del cliente .*/
                listaPlantillas = getPlantillasCliente("0", receptor.getIdReceptorEmp(), "0").getPlantilas();
                if (listaPlantillas != null && !listaPlantillas.isEmpty()) { 
                    receptor.setListaPlantillas(listaPlantillas); 
                }
                
                receptores.add(receptor);
            }

            mensajeReceptor.setStatus(Constantes.statusSuccess);
            mensajeReceptor.setMensaje("Consulta realizada con exito");
            mensajeReceptor.setReceptores(receptores);

        } catch (Exception e) {
            mensajeReceptor.setStatus(Constantes.statusError);
            mensajeReceptor.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeReceptor;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeReceptor;

    }

    @Override
    public MensajeArticulos getArticuloInfo(String pIdEmpresaFe, BuscarArticulo pDatos) throws Exception {

        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        ResultSet resultSet3 = null;
        List<Articulo> articulos = new ArrayList<>();
        List<ImpuestoArt> impuestos;
        List<ExoneracionArt> exoneraciones;
        ExoneracionArt exoneracion;
        Articulo art;
        String exoneracionesComa;
        String idsImpuestoComa;
        ImpuestoArt impuesto;
        MensajeArticulos mensajeArticulo = new MensajeArticulos();
        String nombre ;
        String codigo;
        String query;
        try {
             if(pDatos.getpCodigoArticulo().trim().equals("")){
                codigo = "NA";
            }else{
                codigo = pDatos.getpCodigoArticulo();
            }
            
            if(pDatos.getpNombreArticulo().trim().equals("")){
                nombre = "NA";
            }else{
                nombre = pDatos.getpNombreArticulo();
            }

            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.GET_ARTICULOS_INFO);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, pIdEmpresaFe);
            pstmt.setString(3, nombre);
            pstmt.setString(4, nombre);
            pstmt.setString(5, codigo);
            pstmt.setString(6, codigo);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                art = new Articulo();
                art.setIdArticuloEmp(resultSet.getString(1));
                art.setIdCategoria(resultSet.getString(2));
                art.setDescripcion(resultSet.getString(3));
                art.setIdUnidadMedidaHacienda(resultSet.getString(4));
                art.setUnidadMedidaCom(resultSet.getString(5));
                art.setTipoArticulo(resultSet.getString(6));
                art.setPrecio(resultSet.getString(7));
                art.setPorcentaje(resultSet.getString(8));
                art.setDescuento(resultSet.getString(9));
                art.setEsRecargo(resultSet.getString(10));
                art.setEstado(resultSet.getString(11));
                String recargosComa = resultSet.getString(12);
                String impuestosComa = resultSet.getString(13);
                String unidadMedidaComa = resultSet.getString(14);
                String articulosComa = resultSet.getString(15);
                art.setCodigo(resultSet.getString(16));
                art.setCodigoDesc(resultSet.getString(17));
                art.setCosto(resultSet.getString(18));

                List<String> recargos = new ArrayList<>();

                if (recargosComa != null) {
                    String[] parts3 = recargosComa.split(",");

                    recargos = Arrays.asList(parts3);
                }
                art.setRecargos(recargos);

                List<String> articulosRec = new ArrayList<>();

                if (articulosComa != null) {
                    String[] parts3 = articulosComa.split(",");

                    articulosRec = Arrays.asList(parts3);
                }
                art.setArticulos(articulosRec);

                List<String> unidadesMedidaEmp = new ArrayList<>();

                if (unidadMedidaComa != null) {
                    String[] parts3 = unidadMedidaComa.split(",");

                    unidadesMedidaEmp = Arrays.asList(parts3);
                }
                art.setIdsUnidadMedidaEmp(unidadesMedidaEmp);

                impuestos = new ArrayList<>();

                if (impuestosComa != null) {
                    String[] parts = impuestosComa.split(",");

                    for (String idImpuesto : parts) {
                        exoneracionesComa = null;
                        idsImpuestoComa = null;
                        exoneraciones = new ArrayList<>();
                        impuesto = new ImpuestoArt();

                        pstmt = connection.prepareStatement(Querys.GET_IMPUESTOS_ART);

                        pstmt.setString(1, pDatos.getpIdCliente());
                        pstmt.setString(2, pIdEmpresaFe);
                        pstmt.setString(3, idImpuesto);

                        resultSet2 = pstmt.executeQuery();

                        while (resultSet2.next()) {
                            impuesto.setIdImpuestoHacienda(resultSet2.getString(1));
                            impuesto.setDescripcion(resultSet2.getString(2));
                            impuesto.setPorcentaje(resultSet2.getString(3));
                            impuesto.setExcepcion(resultSet2.getString(4));

                            exoneracionesComa = resultSet2.getString(5);
                            idsImpuestoComa = resultSet2.getString(6);

                        }

                        List<String> idsImpuesto = new ArrayList<>();

                        if (idsImpuestoComa != null) {
                            String[] parts3 = idsImpuestoComa.split(",");

                            idsImpuesto = Arrays.asList(parts3);
                        }
                        impuesto.setIdsImpuestoEmp(idsImpuesto);

                        if (exoneracionesComa != null) {

                            String[] parts4 = exoneracionesComa.split(",");

                            for (String idExoneracion : parts4) {
                                pstmt = connection.prepareStatement(Querys.GET_EXONERACIONES_ARTICULO);

                                pstmt.setString(1, pIdEmpresaFe);
                                pstmt.setString(2, idExoneracion);
                                pstmt.setString(3, idExoneracion);
                                pstmt.setString(4, pDatos.getpIdCliente());
                                pstmt.setString(5, art.getIdArticuloEmp());

                                resultSet3 = pstmt.executeQuery();

                                while (resultSet3.next()) {
                                    exoneracion = new ExoneracionArt();

                                    exoneracion.setCodigoTipoExo(resultSet3.getString(1));
                                    exoneracion.setNumeroDocumento(resultSet3.getString(2));
                                    exoneracion.setNombreInstitucion(resultSet3.getString(3));
                                    exoneracion.setFechaEmision(resultSet3.getString(4));
                                    exoneracion.setMontoImpuesto(resultSet3.getString(5));
                                    exoneracion.setPorcentajeCompra(resultSet3.getString(6));
                                    exoneracion.setIdCliente(resultSet3.getString(7));
                                    exoneracion.setFechaHasta(resultSet3.getString(8));
                                    exoneracion.setIdExoneracion(resultSet3.getString(9));
                                    exoneracion.setDescTipoExo(resultSet3.getString(10));
                                    exoneracion.setEstado(resultSet3.getString(11));
                                    exoneracion.setExoGlobal(resultSet3.getString(12));

                                    exoneraciones.add(exoneracion);
                                }
                            }
                        }
                        impuesto.setExoneraciones(exoneraciones);
                        impuestos.add(impuesto);
                    }
                }

                art.setImpuestos(impuestos);

                articulos.add(art);
            }

            if (resultSet2 != null) {
                resultSet2.close();
                resultSet2 = null;
            }

            if (resultSet3 != null) {
                resultSet3.close();
                resultSet3 = null;
            }

            mensajeArticulo.setStatus(Constantes.statusSuccess);
            mensajeArticulo.setMensaje("Consulta realizada con exito");
            mensajeArticulo.setArticulos(articulos);

        } catch (Exception e) {
            mensajeArticulo.setStatus(Constantes.statusError);
            mensajeArticulo.setMensaje(e.getMessage());

            closeDaoResources(resultSet, pstmt, connection);

            return mensajeArticulo;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }
        return mensajeArticulo;
    }


    @Override
    public List<ReporteEmitidos> getReportEmitidos(String pIdEmpresaFe, String fechaInicio, String fechaFin, String estado, String tipoDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ReporteEmitidos row = null;
        List<ReporteEmitidos> reporte  = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.REPORT_EMITIDOS);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, fechaInicio);
            pstmt.setString(3, fechaFin);
            pstmt.setString(4, estado);
            pstmt.setString(5, tipoDocumento);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                row = new ReporteEmitidos();
                row.setTipoDescDocumento(resultSet.getString(1));
                row.setClave(resultSet.getString(2));
                row.setConsecutivo(resultSet.getString(3));
                row.setCliente(resultSet.getString(4));
                row.setIdentificacion(resultSet.getString(5));
                row.setCorreo(resultSet.getString(6));
                row.setCondicionVenta(resultSet.getString(7));
                row.setPlazoCredito(resultSet.getString(8));
                row.setTipoCambio(resultSet.getBigDecimal(9));
                row.setTotalServiciosGrav(resultSet.getBigDecimal(10));
                row.setTotalServiciosExen(resultSet.getBigDecimal(11));
                row.setTotalMercanciasExen(resultSet.getBigDecimal(12));
                row.setTotalMercanciasGrav(resultSet.getBigDecimal(13));
                row.setTotalGravado(resultSet.getBigDecimal(14));
                row.setTotalExento(resultSet.getBigDecimal(15));
                row.setTotalDescuento(resultSet.getBigDecimal(16));
                row.setTotalImpuestos(resultSet.getBigDecimal(17));
                row.setTotalComprobante(resultSet.getBigDecimal(18));
                row.setMoneda(resultSet.getString(19));
                row.setDescEstado(resultSet.getString(20));
                row.setFechaEmision(resultSet.getString(21));
                row.setMotivoRechazo(resultSet.getString(22));
                reporte.add(row);

            }



        } catch (Exception e) {
            closeDaoResources(resultSet, pstmt, connection);
            return null;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return reporte;

    }


    @Override
    public List<ReporteRecibidos> getReportRecibidos(String pIdEmpresaFe, String fechaInicio, String fechaFin, String estado, String tipoDocumento) throws Exception {
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        ReporteRecibidos row = null;
        List<ReporteRecibidos> reporte  = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            pstmt = connection.prepareStatement(Querys.REPORT_RECIBIDOS);

            pstmt.setString(1, pIdEmpresaFe);
            pstmt.setString(2, fechaInicio);
            pstmt.setString(3, fechaFin);
            pstmt.setString(4, estado);
            pstmt.setString(5, tipoDocumento);

            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                row = new ReporteRecibidos();
                row.setIdDocumento(resultSet.getString(1));
                row.setDescTipoDocumento(resultSet.getString(2));
                row.setConsecutivo(resultSet.getString(3));
                row.setIdentificacionEmisor(resultSet.getString(4));
                row.setRazonSocial(resultSet.getString(5));
                row.setNombreComercial(resultSet.getString(6));
                row.setCorreoElectronico(resultSet.getString(7));
                row.setTotalComprobante(resultSet.getBigDecimal(8));
                row.setFechaEmision(resultSet.getString(9));
                row.setFechaAceptacion(resultSet.getString(10));
                row.setDescAceptacion(resultSet.getString(11));
                row.setDescEstado(resultSet.getString(12));
                row.setMoneda(resultSet.getString(13));
                row.setMotivoRechazo(resultSet.getString(14));
                reporte.add(row);

            }



        } catch (Exception e) {
            closeDaoResources(resultSet, pstmt, connection);
            return null;
        } finally {
            closeDaoResources(resultSet, pstmt, connection);
        }

        return reporte;

    }


}
