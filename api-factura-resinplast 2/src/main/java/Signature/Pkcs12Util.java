/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Signature;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Objects;
import javax.security.auth.x500.X500Principal;

/**
 *
 * @author msalasch
 */
public class Pkcs12Util {
   private File certificate;
   private String password;
   private KeyStore keyStore;

   public Pkcs12Util(String certPath, String password) {
       this(new File(certPath), password);
   }

   public Pkcs12Util(File certificate, String password) {
       Objects.nonNull(certificate);
       Objects.nonNull(password);

       this.certificate = certificate;
       this.password = password;

   }

   public boolean init() {
       try {
           keyStore = KeyStore.getInstance("pkcs12");
           keyStore.load(new FileInputStream(certificate),
                   password.toCharArray());

           return true;
       } catch (Exception e) {
           return false;
       }
   }

   /**
    * @return true if the certificate is valid, false if certificate expired.
    */
   public boolean isValidCert() {
       try {
           Enumeration<String> e = keyStore.aliases();
           while (e.hasMoreElements()) {
               String alias = e.nextElement();
               X509Certificate certificate = (X509Certificate) keyStore
                       .getCertificate(alias);
               try {
                   certificate.checkValidity();
                   return true;
               } catch (CertificateExpiredException ex) {
                   return false;
               }
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return true;
   }

   /**
    * @return Issuer name
    */
   public String getIssuerName() {
       try {
           Enumeration<String> e = keyStore.aliases();
           while (e.hasMoreElements()) {
               String alias = e.nextElement();
               X509Certificate certificate = (X509Certificate) keyStore
                       .getCertificate(alias);
               X500Principal issuer = certificate.getIssuerX500Principal();

               return issuer.getName();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return "";
   }

   public String getDetails() {
       try {
           Enumeration<String> e = keyStore.aliases();
           while (e.hasMoreElements()) {
               String alias = e.nextElement();
               X509Certificate certificate = (X509Certificate) keyStore
                       .getCertificate(alias);

               return certificate.toString();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return "";
   }
   
   public Date getExpire() {
       Date datefinal = null;
       Date dateinit = null;
        try {
            Enumeration<String> e = keyStore.aliases();
            while (e.hasMoreElements()) {
                String alias = e.nextElement();
                X509Certificate cert = (X509Certificate) keyStore
                        .getCertificate(alias);
                dateinit = cert.getNotBefore();
                datefinal = cert.getNotAfter();

                return datefinal;
            }
        } catch (Exception e) {
            return null;
        }
        return datefinal;
    }
}
