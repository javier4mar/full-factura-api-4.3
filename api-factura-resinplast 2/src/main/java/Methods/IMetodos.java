package Methods;

import Entities.Catalogos.RadiusExoneraciones;
import Entities.FE.ActualizaClave;
import Entities.FE.ArticuloAdd;
import Entities.FE.Correo;
import Entities.FE.IdDocumento;
import Entities.FE.DatosOlvideClave;
import Entities.FE.DepositoTransfAdd;
import Entities.FE.DocumentoAdd;
import Entities.FE.DocumentoRecepcionAdd;
import Entities.FE.EmisorToken;
import Entities.FE.FiltraDocumento;
import Entities.FE.FiltroKpi;
import Entities.FE.GrupoUsuarioAdd;
import Entities.FE.PlanAdd;
import Entities.FE.Preferencias;
import Entities.FE.ProcesaCorreo;
import Entities.FE.PuntoVentaAdd;
import Entities.FE.PuntoVentaEdit;
import Entities.FE.ReceptorAdd;
import Entities.FE.SucursalAdd;
import Entities.FE.TipoPlanAdd;
import Entities.FE.TokenCorreo;
import Entities.FE.UsrAdd;
import Entities.FE.ValidaDeposito;
import Entities.Hacienda.Callback;
import Entities.Hacienda.GeneraToken;
import Entities.MensajeWs.MensajeAddLogo;
import Entities.MensajeWs.MensajeArticulos;
import Entities.MensajeWs.MensajeBancos;
import Entities.MensajeWs.MensajeBase;
import Entities.MensajeWs.MensajeCatalogoFactura;
import Entities.MensajeWs.MensajeCondicionesVenta;
import Entities.MensajeWs.MensajeConsultaGrupos;
import Entities.MensajeWs.MensajeConsultaHacienda;
import Entities.MensajeWs.MensajeConsultaXml;
import Entities.MensajeWs.MensajeCuentasBanco;
import Entities.MensajeWs.MensajeDatosEmpresa;
import Entities.MensajeWs.MensajeDepositoTransf;
import Entities.MensajeWs.MensajeDistGeografica;
import Entities.MensajeWs.MensajeDistribuidores;
import Entities.MensajeWs.MensajeDocumento;
import Entities.MensajeWs.MensajeEmpresa;
import Entities.MensajeWs.MensajeEmpresas;
import Entities.MensajeWs.MensajeExoneraciones;
import Entities.MensajeWs.MensajeImpuestos;
import Entities.MensajeWs.MensajeInsert;
import Entities.MensajeWs.MensajeInteresados;
import Entities.MensajeWs.MensajeKpi;
import Entities.MensajeWs.MensajeLogin;
import Entities.MensajeWs.MensajeMediosPago;
import Entities.MensajeWs.MensajeMenuLateral;
import Entities.MensajeWs.MensajeMonedas;
import Entities.MensajeWs.MensajePaginas;
import Entities.MensajeWs.MensajePags;
import Entities.MensajeWs.MensajePlanes;
import Entities.MensajeWs.MensajePreferencias;
import Entities.MensajeWs.MensajePuntoVenta;
import Entities.MensajeWs.MensajeRadiusExoneraciones;
import Entities.MensajeWs.MensajeReceptor;
import Entities.MensajeWs.MensajeSaldosEmp;
import Entities.MensajeWs.MensajeSucursal;
import Entities.MensajeWs.MensajeTipoIdentificacion;
import Entities.MensajeWs.MensajeTipos;
import Entities.MensajeWs.MensajeTiposPlan;
import Entities.MensajeWs.MensajeUnidadesMedida;
import Entities.MensajeWs.MensajeUsr;
import Entities.MensajeWs.MensajeUsrAdd;
import Entities.MensajeWs.MensajeUsuarios;
import Entities.MensajeWs.MensajeValidaRecepcion;
import com.sun.jersey.core.header.FormDataContentDisposition;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipOutputStream;
import javax.ws.rs.core.StreamingOutput;


public interface IMetodos {
   
   String getToken(GeneraToken data) throws Exception;
   
   MensajeBase enviarDocumentoHacienda(EmisorToken pToken, String pIdDocumento) throws Exception;
   
   MensajeConsultaHacienda consultarDocumentoHacienda(EmisorToken pToken, String pIdDocumento) throws Exception;
   
   MensajeConsultaXml consultarXml(EmisorToken pToken, String pIdDocumento, String pTipo) throws Exception;
   
   void callBack(Callback pRespuesta) throws Exception;
   
   MensajeUnidadesMedida getUnidadesMedida(EmisorToken pToken) throws Exception;
   
   MensajeCondicionesVenta getCondicionesVenta(EmisorToken pToken) throws Exception;
   
   MensajeDistGeografica getDistGeografica(EmisorToken pToken) throws Exception;
   
   MensajeMediosPago getMediosPago(EmisorToken pToken) throws Exception;
   
   MensajeImpuestos getImpuestos(EmisorToken pToken) throws Exception;
   
   MensajeMonedas getMonedas(EmisorToken pToken) throws Exception;
   
   MensajeTipos getTiposDocumento(EmisorToken pToken) throws Exception;
   
   MensajeTipos getTiposExoneracion(EmisorToken pToken) throws Exception;
   
   MensajeTipoIdentificacion getTiposIdentificacion(EmisorToken pToken) throws Exception;
   
   MensajeTipos getTiposCodigoArt(EmisorToken pToken) throws Exception;
   
   MensajeBase addUnidadMedidaEmp(String pIdEmpresaFe, String pUnidadMedida, String pUnidadMedidaEmp) throws Exception;
   
   MensajeBase addCondicionVentaEmp(String pIdEmpresaFe, String pCondicionVenta, String pCondicionVentaEmp) throws Exception;
   
   MensajeBase addDistGeoEmp(String pIdEmpresaFe, String pDistGeo, String pDistGeoEmp) throws Exception;
   
   MensajeBase addMedioPagoEmp(String pIdEmpresaFe, String pMedioPago, String pMedioPagoEmp) throws Exception;
   
   MensajeBase addImpuestoEmp(String pIdEmpresaFe, String pImpuesto, String pImpuestoEmp) throws Exception;
   
   MensajeBase addMonedaEmp(String pIdEmpresaFe, String pMoneda, String pMonedaEmp) throws Exception;
   
   MensajeBase addTipoDocumentoEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp, String pDescripcion) throws Exception;
   
   MensajeBase addTipoExoneracionEmp(String pIdEmpresaFe, String pTipoExoneracion, String pTipoExoneracionEmp) throws Exception;
   
   MensajeBase addTipoIdentificacionEmp(String pIdEmpresaFe, String pTipoIdentificacion, String pTipoIdentificacionEmp) throws Exception;
   
   MensajeBase addTipoCodigoArtEmp(String pIdEmpresaFe, String pTipoCodigoArt, String pTipoCodigoArtEmp) throws Exception;
   
   boolean deleteUnidadMedidaEmp(String pIdEmpresaFe, String pUnidadMedida, String pUnidadMedidaEmp) throws Exception;
   
   boolean deleteCondicionVentaEmp(String pIdEmpresaFe, String pCondicionVenta, String pCondicionVentaEmp) throws Exception;
   
   boolean deleteDistGeoEmp(String pIdEmpresaFe, String pDistGeo, String pDistGeoEmp) throws Exception;
   
   boolean deleteMedioPagoEmp(String pIdEmpresaFe, String pMedioPago, String pMedioPagoEmp) throws Exception;
   
   boolean deleteImpuestoEmp(String pIdEmpresaFe, String pImpuesto, String pImpuestoEmp) throws Exception;
   
   boolean deleteMonedaEmp(String pIdEmpresaFe, String pMoneda, String pMonedaEmp) throws Exception;
   
   boolean deleteTipoDocumentoEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception;
   
   boolean deleteTipoExoneracionEmp(String pIdEmpresaFe, String pTipoExoneracion, String pTipoExoneracionEmp) throws Exception;
   
   boolean deleteTipoIdentificacionEmp(String pIdEmpresaFe, String pTipoIdentificacion, String pTipoIdentificacionEmp) throws Exception;
   
   boolean deleteTipoCodigoArtEmp(String pIdEmpresaFe, String pTipoCodigoArt, String pTipoCodigoArtEmp) throws Exception;
   
   MensajeInsert addArticuloEmp(String pIdEmpresaFe, ArticuloAdd pArticulo) throws Exception;
   
   MensajeBase updateArticuloEmp(String pIdEmpresaFe, ArticuloAdd pArticulo) throws Exception;
   
   MensajeBase deleteArticuloEmp(String pIdEmpresaFe, String pIdArticuloEmp) throws Exception;
   
   MensajeArticulos getArticulos(String pIdArticulo, String pIdEmpresa, String pEstado) throws Exception;
   
   MensajeExoneraciones getExoneraciones(String pIdExoneracion, String pIdEmpresa) throws Exception;
   
   MensajeLogin login(String pUsuario, String pClave, String pIdEmpresaFe, String pActualizaDef) throws Exception;
   
   MensajePaginas getPaginas(String pIdGrupo, String pPreferencias) throws Exception;
   
   MensajeUsuarios getUsuarios(String pIdUsuario) throws Exception;

   MensajeMenuLateral getMenuLateral(String pIdGrupo) throws Exception;
   
   MensajeEmpresa addEmpresa(String pIdHomEmpresa, String pRazonSocial, String pNombreComercial, String pTipoIdentificacion, 
                    String pNumIdentificacion, String pIdProvincia, String pIdCanton, String pIdDistrito, String pIdBarrio, String pOtrasSenas, String pCodPaisTel, 
                    String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail, 
                    String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario, InputStream pLogo, FormDataContentDisposition pLogoDetail, 
                    String pRutaGlassfish, String pIdGrupo, String pIdPlan, String pIdTipoPlan, String pEsDistribuidor, String pIdPadre, String pAdministraCobros, String pManejaHacienda) throws Exception;
   
   MensajeAddLogo updateEmpresaRef(String pIdHomEmpresa, String pRazonSocial, String pNombreComercial, String pTipoIdentificacion, 
                    String pNumIdentificacion, String pIdProvincia, String pIdCanton, String pIdDistrito, String pIdBarrio, String pOtrasSenas, String pCodPaisTel, 
                    String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail, 
                    String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario, InputStream pLogo, FormDataContentDisposition pLogoDetail, 
                    String pRutaGlassfish, String pIdGrupo, String pEsDistribuidor, String pAdministraCobros, String pManejaHacienda, String pIdEmpresa) throws Exception;
   
   MensajeInsert addReceptor(EmisorToken pToken, ReceptorAdd pReceptor) throws Exception;
   
   MensajeReceptor getReceptores(String pIdEmpresaFe, String pEstado) throws Exception;
   
   MensajeBase updateReceptor(EmisorToken pToken, ReceptorAdd pReceptor) throws Exception;
   
   boolean deleteReceptor(String pIdEmpresaFe, String pIdReceptor) throws Exception;
   
   MensajeBase addSucursalEmp(String pIdEmpresaFe, SucursalAdd pReceptor) throws Exception;
   
   MensajeBase updateSucursalEmp(String pIdEmpresaFe, SucursalAdd pReceptor) throws Exception;
   
   MensajeSucursal getSucursales(String pIdEmpresaFe, String pEstado) throws Exception;
   
   MensajeBase addPuntoVentaEmp(String pIdEmpresaFe, PuntoVentaAdd pReceptor) throws Exception;
   
   MensajeBase updatePuntoVentaEmp(String pIdEmpresaFe, PuntoVentaEdit pReceptor) throws Exception;
   
   MensajePuntoVenta getPuntosVenta(String pIdEmpresaFe, String pIdSucursalEmp, String pEstado) throws Exception;
   
   MensajeInsert registraDocumento(EmisorToken pToken, DocumentoAdd pDocumento) throws Exception;
   
   MensajeInsert registraDocumentoRecepcion(EmisorToken pToken, DocumentoRecepcionAdd pDocumento) throws Exception;
   
   MensajeDocumento getDocumento(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception;
   
   MensajeDocumento getDocumentosNC(String pIdEmpresaFe, FiltraDocumento pDatos) throws Exception;
   
   MensajeValidaRecepcion validaRecepcion(String pIdEmpresaFe, String pClave) throws Exception;
   
   MensajeCatalogoFactura getEstadosDocumento() throws Exception;
   
   MensajeCatalogoFactura getCodigoMensajeHacienda() throws Exception;
   
   MensajeTipos getTipoDocRef(EmisorToken mClaims) throws Exception;
   
   MensajeBase addTipoDocRefEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception;
   
   boolean deleteTipoDocRefEmp(String pIdEmpresaFe, String pTipoDocumento, String pTipoDocumentoEmp) throws Exception;
   
   MensajeCatalogoFactura getSituacionDocumento() throws Exception;
   
   MensajeCatalogoFactura getTipoDocumentosDet() throws Exception;
   
   MensajeCatalogoFactura getCodigoDocumentoRef() throws Exception;
   
   MensajeBase addXmlRecepcion(EmisorToken mClaims, String pClave, InputStream pXml, FormDataContentDisposition pXmlDetail) throws Exception;
   
   MensajePreferencias getPreferencias(EmisorToken mClaims) throws Exception;
   
   MensajeBase addPreferencias(EmisorToken pToken, Preferencias pPreferencias) throws Exception;
   
   MensajeBase updatePreferencias(EmisorToken pToken, Preferencias pPreferencias) throws Exception;
   
   MensajeBase procesaDocumento(String pClave, String pStatus) throws Exception;
   
   MensajeDocumento getDocumentoCorreo() throws Exception;
   
   MensajeBase procesaInteresados(List<ProcesaCorreo> pDatos) throws Exception;
   
   MensajeBase actualizaClave(EmisorToken pToken, ActualizaClave pDatos) throws Exception;
   
   MensajeBase olvideClave(DatosOlvideClave pDatos) throws Exception;
   
   MensajeBase reseteaClave(TokenCorreo pToken, String pClave) throws Exception;
   
   MensajeAddLogo addLogoEmpresa(EmisorToken mClaims, InputStream pLogo, FormDataContentDisposition pLogoDetail, String pRutaGlassfish) throws Exception;
   
   MensajeDatosEmpresa getDatosEmpresa(String pIdEmpresaFe) throws Exception;
   
   MensajeAddLogo updateEmpresa(String pRazonSocial, String pNombreComercial, String pTipoIdentificacionEmp, 
                    String pNumIdentificacion, String pIdProvincia, String pIdCanton, String pIdDistrito, String pIdBarrio, String pOtrasSenas, String pCodPaisTel, 
                    String pTelefono, String pCodPaisFax, String pFax, String pCorreoElectronico, InputStream pCertificado, FormDataContentDisposition pCertificadoDetail, 
                    String pPinCertificado, String pUsuarioHacienda, String pClaveHacienda, String pCorreoFe, String pNombreUsuario, InputStream pLogo, 
                    FormDataContentDisposition pLogoDetail, String pRutaGlassfish, EmisorToken pToken) throws Exception;
   
   MensajeCatalogoFactura getProvincias(String pIdEmpresaFe) throws Exception;
   
   MensajeCatalogoFactura getCantones(String pIdEmpresaFe, String pIdProvinciaEmp) throws Exception;
   
   MensajeCatalogoFactura getDistritos(String pIdEmpresaFe, String pIdProvinciaEmp, String pIdCantonEmp) throws Exception;
   
   MensajeCatalogoFactura getBarrios(String pIdEmpresaFe, String pIdProvinciaEmp, String pIdCantonEmp, String pIdDistritoEmp) throws Exception;
   
   MensajeCatalogoFactura getProvinciasReg() throws Exception;
   
   MensajeCatalogoFactura getCantonesReg(String pIdProvincia) throws Exception;
   
   MensajeCatalogoFactura getDistritosReg(String pIdCanton) throws Exception;
   
   MensajeCatalogoFactura getBarriosReg(String pIdDistrito) throws Exception;
   
   MensajeCatalogoFactura getTiposIdenReg() throws Exception;
   
   MensajePags getPags() throws Exception;
   
   MensajeConsultaGrupos getGrupos(String pIdGrupoSesion, String pEstado) throws Exception;
   
   MensajeUsr getUsers(EmisorToken mClaims) throws Exception;
   
   MensajeKpi getKpis(EmisorToken mClaims, FiltroKpi pDatos) throws Exception;
   
   MensajeUsrAdd addUser(EmisorToken mClaims, UsrAdd pDatos) throws Exception;
   
   MensajeBase updateUser(EmisorToken mClaims, UsrAdd pDatos, String pIdUsuario) throws Exception;
   
   MensajePlanes getPlanes(String pEstado) throws Exception;
   
   MensajeTiposPlan getTiposPlan(String pEstado) throws Exception;
   
   MensajeBase addPlan(EmisorToken mClaims, PlanAdd pDatos) throws Exception;
   
   MensajeBase updatePlan(EmisorToken mClaims, PlanAdd pDatos, String pIdPlan) throws Exception;
   
   MensajeBase addTipoPlan(EmisorToken mClaims, TipoPlanAdd pDatos) throws Exception;
   
   MensajeBase updateTipoPlan(EmisorToken mClaims, TipoPlanAdd pDatos, String pIdTipoPlan) throws Exception;
   
   MensajeDistribuidores getDistribuidores(String pEstado) throws Exception;
   
   MensajeBase addGrupoUsuario(EmisorToken mClaims, GrupoUsuarioAdd pDatos) throws Exception;
   
   MensajeBase updateGrupoUsuario(EmisorToken mClaims, GrupoUsuarioAdd pDatos, String pIdGrupo) throws Exception;
   
   StreamingOutput backUpXml(String pIdEmpresaFe) throws Exception;
   
   StreamingOutput backUpXmlEspecifico(String pIdEmpresaFe, List<IdDocumento> pDocumentos, String zipName) throws Exception;
   
   MensajeEmpresas getEmpresas(String pEsDistribuidor, String pAdministraCobros) throws Exception;
   
   MensajeEmpresas getReferidos(String pIdEmpresaFe) throws Exception;
   
   MensajeBancos getBancos(String pEstado) throws Exception;
   
   MensajeCuentasBanco getCuentasBanco(String pIdBanco, String pEstado) throws Exception;
   
   MensajeBase addDepositoTransf(EmisorToken mClaims, DepositoTransfAdd pDatos) throws Exception;
   
   MensajeDepositoTransf getDepositosTransf(String pIdEmpresaFe) throws Exception;
   
   MensajeDepositoTransf getDepositosCheck() throws Exception;
   
   MensajeSaldosEmp getSaldosEmpresa(String pIdEmpresaFe) throws Exception;
   
   MensajeBase validaTransaccion(EmisorToken mClaims, ValidaDeposito pDatos) throws Exception;
   
   MensajeBase generaCobros() throws Exception;
   
   MensajeConsultaGrupos getGruposLista() throws Exception;
   
   MensajeBase reenviarCorreo(String pIdDocumento, List<Correo> correos) throws Exception;
   
   MensajeRadiusExoneraciones getRadiusExoneraciones() throws Exception;

   MensajeInsert addExoneracionRadius(RadiusExoneraciones mRadiusExoneraciones) throws Exception;
   
   MensajeBase updateExoneracionRadius(RadiusExoneraciones mRadiusExoneraciones) throws Exception;

   MensajeInteresados getInteresadosDocumento(String pIdEmpresa,String pClaveDocumento) throws Exception;

}
