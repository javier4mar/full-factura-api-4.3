/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Catalogos;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class ImpuestoCat {
    private String codigo;
    private String descripcion;
    private String porcentajeImp;
    private String excepcion;
    private List<String> codigosEmp;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPorcentajeImp() {
        return porcentajeImp;
    }

    public void setPorcentajeImp(String porcentajeImp) {
        this.porcentajeImp = porcentajeImp;
    }

    public String getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(String excepcion) {
        this.excepcion = excepcion;
    }

    public List<String> getCodigosEmp() {
        return codigosEmp;
    }

    public void setCodigosEmp(List<String> codigosEmp) {
        this.codigosEmp = codigosEmp;
    }
    
}
