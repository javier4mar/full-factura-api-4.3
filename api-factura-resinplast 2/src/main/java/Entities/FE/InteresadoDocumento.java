/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class InteresadoDocumento {
    private String pIdDocumento;
    private String pCorreo;
    private String pEnviado;
    private String pIdInteresado;
    private String pFechaEnvio;

    public String getpIdDocumento() {
        return pIdDocumento;
    }

    public void setpIdDocumento(String pIdDocumento) {
        this.pIdDocumento = pIdDocumento;
    }

    public String getpCorreo() {
        return pCorreo;
    }

    public void setpCorreo(String pCorreo) {
        this.pCorreo = pCorreo;
    }

    public String getpEnviado() {
        return pEnviado;
    }

    public void setpEnviado(String pEnviado) {
        this.pEnviado = pEnviado;
    }

    public String getpIdInteresado() {
        return pIdInteresado;
    }

    public void setpIdInteresado(String pIdInteresado) {
        this.pIdInteresado = pIdInteresado;
    }

    public String getpFechaEnvio() {
        return pFechaEnvio;
    }

    public void setpFechaEnvio(String pFechaEnvio) {
        this.pFechaEnvio = pFechaEnvio;
    }
    
    
}
