/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class InfReferenciaRecepcionAdd {
    private String pTipoDocumento;
    private String pNumero;
    private String pFechaEmision;
    private String pCodigo;
    private String pRazon;

    public String getpTipoDocumento() {
        return pTipoDocumento;
    }

    public void setpTipoDocumento(String pTipoDocumento) {
        this.pTipoDocumento = pTipoDocumento;
    }

    public String getpNumero() {
        return pNumero;
    }

    public void setpNumero(String pNumero) {
        this.pNumero = pNumero;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpRazon() {
        return pRazon;
    }

    public void setpRazon(String pRazon) {
        this.pRazon = pRazon;
    }
    
}
