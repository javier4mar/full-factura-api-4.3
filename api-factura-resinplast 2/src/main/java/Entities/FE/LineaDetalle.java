/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class LineaDetalle {
    private String pIdDocumento;
    private String pIdDocumentoDet;
    private String pLinea;
    private String pCodigoArtEmp;
    private String pCodigo;
    private String pCantidad;
    private String pUnidadMedidaEmp;
    private String pUnidadMedidaComercial;
    private String pDetalle;
    private String pPrecioUnitario;
    private String pMontoTotal;
    private String pMontoDescuento;
    private String pNaturalezaDescuento;
    private String pSubTotal;
    private String pMontoTotalLinea;
    private String pMercServ;
    private String pCantidadRestante;
    private String pMontoRestante;
    private List<Impuesto> pImpuestos;

    public String getpIdDocumento() {
        return pIdDocumento;
    }

    public void setpIdDocumento(String pIdDocumento) {
        this.pIdDocumento = pIdDocumento;
    }

    public String getpIdDocumentoDet() {
        return pIdDocumentoDet;
    }

    public void setpIdDocumentoDet(String pIdDocumentoDet) {
        this.pIdDocumentoDet = pIdDocumentoDet;
    }

    public String getpLinea() {
        return pLinea;
    }

    public void setpLinea(String pLinea) {
        this.pLinea = pLinea;
    }

    public String getpCodigoArtEmp() {
        return pCodigoArtEmp;
    }

    public void setpCodigoArtEmp(String pCodigoArtEmp) {
        this.pCodigoArtEmp = pCodigoArtEmp;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpCantidad() {
        return pCantidad;
    }

    public void setpCantidad(String pCantidad) {
        this.pCantidad = pCantidad;
    }

    public String getpUnidadMedidaEmp() {
        return pUnidadMedidaEmp;
    }

    public void setpUnidadMedidaEmp(String pUnidadMedidaEmp) {
        this.pUnidadMedidaEmp = pUnidadMedidaEmp;
    }

    public String getpUnidadMedidaComercial() {
        return pUnidadMedidaComercial;
    }

    public void setpUnidadMedidaComercial(String pUnidadMedidaComercial) {
        this.pUnidadMedidaComercial = pUnidadMedidaComercial;
    }

    public String getpDetalle() {
        return pDetalle;
    }

    public void setpDetalle(String pDetalle) {
        this.pDetalle = pDetalle;
    }

    public String getpPrecioUnitario() {
        return pPrecioUnitario;
    }

    public void setpPrecioUnitario(String pPrecioUnitario) {
        this.pPrecioUnitario = pPrecioUnitario;
    }

    public String getpMontoTotal() {
        return pMontoTotal;
    }

    public void setpMontoTotal(String pMontoTotal) {
        this.pMontoTotal = pMontoTotal;
    }

    public String getpMontoDescuento() {
        return pMontoDescuento;
    }

    public void setpMontoDescuento(String pMontoDescuento) {
        this.pMontoDescuento = pMontoDescuento;
    }

    public String getpNaturalezaDescuento() {
        return pNaturalezaDescuento;
    }

    public void setpNaturalezaDescuento(String pNaturalezaDescuento) {
        this.pNaturalezaDescuento = pNaturalezaDescuento;
    }

    public String getpSubTotal() {
        return pSubTotal;
    }

    public void setpSubTotal(String pSubTotal) {
        this.pSubTotal = pSubTotal;
    }

    public String getpMontoTotalLinea() {
        return pMontoTotalLinea;
    }

    public void setpMontoTotalLinea(String pMontoTotalLinea) {
        this.pMontoTotalLinea = pMontoTotalLinea;
    }

    public String getpMercServ() {
        return pMercServ;
    }

    public void setpMercServ(String pMercServ) {
        this.pMercServ = pMercServ;
    }

    public List<Impuesto> getpImpuestos() {
        return pImpuestos;
    }

    public void setpImpuestos(List<Impuesto> pImpuestos) {
        this.pImpuestos = pImpuestos;
    }

    public String getpCantidadRestante() {
        return pCantidadRestante;
    }

    public void setpCantidadRestante(String pCantidadRestante) {
        this.pCantidadRestante = pCantidadRestante;
    }

    public String getpMontoRestante() {
        return pMontoRestante;
    }

    public void setpMontoRestante(String pMontoRestante) {
        this.pMontoRestante = pMontoRestante;
    }
    
}
