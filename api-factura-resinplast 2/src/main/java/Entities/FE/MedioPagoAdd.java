/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class MedioPagoAdd {
    private String pMedioPagoEmp;
    private String pMedioPago;
    private String pMonto;
    private String pComprobante;

    public String getpMedioPagoEmp() {
        return pMedioPagoEmp;
    }

    public void setpMedioPagoEmp(String pMedioPagoEmp) {
        this.pMedioPagoEmp = pMedioPagoEmp;
    }

    public String getpMonto() {
        return pMonto;
    }

    public void setpMonto(String pMonto) {
        this.pMonto = pMonto;
    }

    public String getpMedioPago() {
        return pMedioPago;
    }

    public void setpMedioPago(String pMedioPago) {
        this.pMedioPago = pMedioPago;
    }

    public String getpComprobante() {
        return pComprobante;
    }

    public void setpComprobante(String pComprobante) {
        this.pComprobante = pComprobante;
    }
    
}
