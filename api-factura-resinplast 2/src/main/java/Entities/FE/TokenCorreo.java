/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class TokenCorreo {
    private String pIdUsuario;
    private int pTokenExp;

    public String getpIdUsuario() {
        return pIdUsuario;
    }

    public void setpIdUsuario(String pIdUsuario) {
        this.pIdUsuario = pIdUsuario;
    }

    public int getpTokenExp() {
        return pTokenExp;
    }

    public void setpTokenExp(int pTokenExp) {
        this.pTokenExp = pTokenExp;
    }
    
}
