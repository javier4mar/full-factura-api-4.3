/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class DocumentoRecepcionAdd {
    private String pTipoComprobante;
    private String pClave;
    private String pConsecutivo;
    private String pCondicionVenta;
    private String pPlazoCredito;
    private String pCodigoMoneda;
    private String pTipoCambio;
    private String pTotalServiciosGravados;
    private String pTotalServiciosExentos;
    private String pTotalMercanciasGravadas;
    private String pTotalMercanciasExentas;
    private String pTotalGravado;
    private String pTotalExento;
    private String pTotalVenta;
    private String pTotalDescuentos;
    private String pTotalVentaNeta;
    private String pTotalImpuestos;
    private String pTotalComprobante;
    private String pNumeroResolucion;
    private String pFechaResolucion;
    private String pTipoIdenCliente;
    private String pNumeroIdenCliente;
    private String pRazonSocialCliente;
    private String pEmailCliente;
    private String pFechaEmision;
    private String pTipoIdentificacionEmpresa;
    private String pIdentificacionEmpresa;
    private String pRazonSocialEmpresa;
    private String pNombreComercialEmpresa;
    private String pEmailEmpresa;
    private String pAccion;
    private String pTagImpuesto;
    private List<String> pMediosPago;
    private List<LineaDetalleRecepcionAdd> pLineasDetalle;
    private List<InfReferenciaRecepcionAdd> pInformacionReferencia;

    public String getpTipoComprobante() {
        return pTipoComprobante;
    }

    public void setpTipoComprobante(String pTipoComprobante) {
        this.pTipoComprobante = pTipoComprobante;
    }

    public String getpClave() {
        return pClave;
    }

    public void setpClave(String pClave) {
        this.pClave = pClave;
    }

    public String getpConsecutivo() {
        return pConsecutivo;
    }

    public void setpConsecutivo(String pConsecutivo) {
        this.pConsecutivo = pConsecutivo;
    }

    public String getpCondicionVenta() {
        return pCondicionVenta;
    }

    public void setpCondicionVenta(String pCondicionVenta) {
        this.pCondicionVenta = pCondicionVenta;
    }

    public String getpPlazoCredito() {
        return pPlazoCredito;
    }

    public void setpPlazoCredito(String pPlazoCredito) {
        this.pPlazoCredito = pPlazoCredito;
    }

    public String getpCodigoMoneda() {
        return pCodigoMoneda;
    }

    public void setpCodigoMoneda(String pCodigoMoneda) {
        this.pCodigoMoneda = pCodigoMoneda;
    }

    public String getpTipoCambio() {
        return pTipoCambio;
    }

    public void setpTipoCambio(String pTipoCambio) {
        this.pTipoCambio = pTipoCambio;
    }

    public String getpTotalServiciosGravados() {
        return pTotalServiciosGravados;
    }

    public void setpTotalServiciosGravados(String pTotalServiciosGravados) {
        this.pTotalServiciosGravados = pTotalServiciosGravados;
    }

    public String getpTotalServiciosExentos() {
        return pTotalServiciosExentos;
    }

    public void setpTotalServiciosExentos(String pTotalServiciosExentos) {
        this.pTotalServiciosExentos = pTotalServiciosExentos;
    }

    public String getpTotalMercanciasGravadas() {
        return pTotalMercanciasGravadas;
    }

    public void setpTotalMercanciasGravadas(String pTotalMercanciasGravadas) {
        this.pTotalMercanciasGravadas = pTotalMercanciasGravadas;
    }

    public String getpTotalMercanciasExentas() {
        return pTotalMercanciasExentas;
    }

    public void setpTotalMercanciasExentas(String pTotalMercanciasExentas) {
        this.pTotalMercanciasExentas = pTotalMercanciasExentas;
    }

    public String getpTotalGravado() {
        return pTotalGravado;
    }

    public void setpTotalGravado(String pTotalGravado) {
        this.pTotalGravado = pTotalGravado;
    }

    public String getpTotalExento() {
        return pTotalExento;
    }

    public void setpTotalExento(String pTotalExento) {
        this.pTotalExento = pTotalExento;
    }

    public String getpTotalVenta() {
        return pTotalVenta;
    }

    public void setpTotalVenta(String pTotalVenta) {
        this.pTotalVenta = pTotalVenta;
    }

    public String getpTotalDescuentos() {
        return pTotalDescuentos;
    }

    public void setpTotalDescuentos(String pTotalDescuentos) {
        this.pTotalDescuentos = pTotalDescuentos;
    }

    public String getpTotalVentaNeta() {
        return pTotalVentaNeta;
    }

    public void setpTotalVentaNeta(String pTotalVentaNeta) {
        this.pTotalVentaNeta = pTotalVentaNeta;
    }

    public String getpTotalImpuestos() {
        return pTotalImpuestos;
    }

    public void setpTotalImpuestos(String pTotalImpuestos) {
        this.pTotalImpuestos = pTotalImpuestos;
    }

    public String getpTotalComprobante() {
        return pTotalComprobante;
    }

    public void setpTotalComprobante(String pTotalComprobante) {
        this.pTotalComprobante = pTotalComprobante;
    }

    public String getpNumeroResolucion() {
        return pNumeroResolucion;
    }

    public void setpNumeroResolucion(String pNumeroResolucion) {
        this.pNumeroResolucion = pNumeroResolucion;
    }

    public String getpFechaResolucion() {
        return pFechaResolucion;
    }

    public void setpFechaResolucion(String pFechaResolucion) {
        this.pFechaResolucion = pFechaResolucion;
    }

    public String getpTipoIdenCliente() {
        return pTipoIdenCliente;
    }

    public void setpTipoIdenCliente(String pTipoIdenCliente) {
        this.pTipoIdenCliente = pTipoIdenCliente;
    }

    public String getpNumeroIdenCliente() {
        return pNumeroIdenCliente;
    }

    public void setpNumeroIdenCliente(String pNumeroIdenCliente) {
        this.pNumeroIdenCliente = pNumeroIdenCliente;
    }

    public String getpRazonSocialCliente() {
        return pRazonSocialCliente;
    }

    public void setpRazonSocialCliente(String pRazonSocialCliente) {
        this.pRazonSocialCliente = pRazonSocialCliente;
    }

    public String getpEmailCliente() {
        return pEmailCliente;
    }

    public void setpEmailCliente(String pEmailCliente) {
        this.pEmailCliente = pEmailCliente;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpTipoIdentificacionEmpresa() {
        return pTipoIdentificacionEmpresa;
    }

    public void setpTipoIdentificacionEmpresa(String pTipoIdentificacionEmpresa) {
        this.pTipoIdentificacionEmpresa = pTipoIdentificacionEmpresa;
    }

    public String getpIdentificacionEmpresa() {
        return pIdentificacionEmpresa;
    }

    public void setpIdentificacionEmpresa(String pIdentificacionEmpresa) {
        this.pIdentificacionEmpresa = pIdentificacionEmpresa;
    }

    public String getpRazonSocialEmpresa() {
        return pRazonSocialEmpresa;
    }

    public void setpRazonSocialEmpresa(String pRazonSocialEmpresa) {
        this.pRazonSocialEmpresa = pRazonSocialEmpresa;
    }

    public String getpNombreComercialEmpresa() {
        return pNombreComercialEmpresa;
    }

    public void setpNombreComercialEmpresa(String pNombreComercialEmpresa) {
        this.pNombreComercialEmpresa = pNombreComercialEmpresa;
    }

    public String getpEmailEmpresa() {
        return pEmailEmpresa;
    }

    public void setpEmailEmpresa(String pEmailEmpresa) {
        this.pEmailEmpresa = pEmailEmpresa;
    }

    public String getpAccion() {
        return pAccion;
    }

    public void setpAccion(String pAccion) {
        this.pAccion = pAccion;
    }

    public List<String> getpMediosPago() {
        return pMediosPago;
    }

    public void setpMediosPago(List<String> pMediosPago) {
        this.pMediosPago = pMediosPago;
    }

    public List<LineaDetalleRecepcionAdd> getpLineasDetalle() {
        return pLineasDetalle;
    }

    public void setpLineasDetalle(List<LineaDetalleRecepcionAdd> pLineasDetalle) {
        this.pLineasDetalle = pLineasDetalle;
    }

    public List<InfReferenciaRecepcionAdd> getpInformacionReferencia() {
        return pInformacionReferencia;
    }

    public void setpInformacionReferencia(List<InfReferenciaRecepcionAdd> pInformacionReferencia) {
        this.pInformacionReferencia = pInformacionReferencia;
    }

    public String getpTagImpuesto() {
        return pTagImpuesto;
    }

    public void setpTagImpuesto(String pTagImpuesto) {
        this.pTagImpuesto = pTagImpuesto;
    }
    
}
