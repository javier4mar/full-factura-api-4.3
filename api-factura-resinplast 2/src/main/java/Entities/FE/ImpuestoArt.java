/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class ImpuestoArt {
    private String idImpuestoHacienda;
    private String descripcion;
    private String porcentaje;
    private String excepcion;
    private List<ExoneracionArt> exoneraciones;
    private List<String> idsImpuestoEmp;

    public String getIdImpuestoHacienda() {
        return idImpuestoHacienda;
    }

    public void setIdImpuestoHacienda(String idImpuestoHacienda) {
        this.idImpuestoHacienda = idImpuestoHacienda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(String excepcion) {
        this.excepcion = excepcion;
    }

    public List<ExoneracionArt> getExoneraciones() {
        return exoneraciones;
    }

    public void setExoneraciones(List<ExoneracionArt> exoneraciones) {
        this.exoneraciones = exoneraciones;
    }

    public List<String> getIdsImpuestoEmp() {
        return idsImpuestoEmp;
    }

    public void setIdsImpuestoEmp(List<String> idsImpuestoEmp) {
        this.idsImpuestoEmp = idsImpuestoEmp;
    }
    
}
