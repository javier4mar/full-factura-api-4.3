/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class LineaDetalleRecepcionAdd {
    private String pNumeroLinea;
    private String pTipoCodigoArticulo;
    private String pCodigo;
    private String pCantidad;
    private String pUnidadMedida;
    private String pUnidadMedidaCom;
    private String pDetalle;
    private String pPrecioUnitario;
    private String pMontoTotal;
    private String pMontoDescuento;
    private String pNaturalezaDescuento;
    private String pSubTotal;
    private String pMontoTotalLinea;
    private List<ImpuestoRecepcionAdd> pImpuestos;

    public String getpNumeroLinea() {
        return pNumeroLinea;
    }

    public void setpNumeroLinea(String pNumeroLinea) {
        this.pNumeroLinea = pNumeroLinea;
    }

    public String getpTipoCodigoArticulo() {
        return pTipoCodigoArticulo;
    }

    public void setpTipoCodigoArticulo(String pTipoCodigoArticulo) {
        this.pTipoCodigoArticulo = pTipoCodigoArticulo;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpCantidad() {
        return pCantidad;
    }

    public void setpCantidad(String pCantidad) {
        this.pCantidad = pCantidad;
    }

    public String getpUnidadMedida() {
        return pUnidadMedida;
    }

    public void setpUnidadMedida(String pUnidadMedida) {
        this.pUnidadMedida = pUnidadMedida;
    }

    public String getpUnidadMedidaCom() {
        return pUnidadMedidaCom;
    }

    public void setpUnidadMedidaCom(String pUnidadMedidaCom) {
        this.pUnidadMedidaCom = pUnidadMedidaCom;
    }

    public String getpDetalle() {
        return pDetalle;
    }

    public void setpDetalle(String pDetalle) {
        this.pDetalle = pDetalle;
    }

    public String getpPrecioUnitario() {
        return pPrecioUnitario;
    }

    public void setpPrecioUnitario(String pPrecioUnitario) {
        this.pPrecioUnitario = pPrecioUnitario;
    }

    public String getpMontoTotal() {
        return pMontoTotal;
    }

    public void setpMontoTotal(String pMontoTotal) {
        this.pMontoTotal = pMontoTotal;
    }

    public String getpMontoDescuento() {
        return pMontoDescuento;
    }

    public void setpMontoDescuento(String pMontoDescuento) {
        this.pMontoDescuento = pMontoDescuento;
    }

    public String getpNaturalezaDescuento() {
        return pNaturalezaDescuento;
    }

    public void setpNaturalezaDescuento(String pNaturalezaDescuento) {
        this.pNaturalezaDescuento = pNaturalezaDescuento;
    }

    public String getpSubTotal() {
        return pSubTotal;
    }

    public void setpSubTotal(String pSubTotal) {
        this.pSubTotal = pSubTotal;
    }

    public String getpMontoTotalLinea() {
        return pMontoTotalLinea;
    }

    public void setpMontoTotalLinea(String pMontoTotalLinea) {
        this.pMontoTotalLinea = pMontoTotalLinea;
    }

    public List<ImpuestoRecepcionAdd> getpImpuestos() {
        return pImpuestos;
    }

    public void setpImpuestos(List<ImpuestoRecepcionAdd> pImpuestos) {
        this.pImpuestos = pImpuestos;
    }
    
}
