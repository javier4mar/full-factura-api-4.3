/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class EmisorToken {
    private String idEmpresaFe;
    private String idEmpresa;
    private String razonSocial;
    private String nombreComercial;
    private String correoAdmFe;
    private String usuarioHacienda;
    private String claveHacienda;
    private String tieneHomogen;
    private String rutaCertificado;
    private String passwordCertificado;
    private String callBack;
    private String tipoIdentificacion;
    private String identificacion;
    private String idUsuario;
    private int tokenExp;

    public String getIdEmpresaFe() {
        return idEmpresaFe;
    }

    public void setIdEmpresaFe(String idEmpresaFe) {
        this.idEmpresaFe = idEmpresaFe;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getCorreoAdmFe() {
        return correoAdmFe;
    }

    public void setCorreoAdmFe(String correoAdmFe) {
        this.correoAdmFe = correoAdmFe;
    }

    public String getUsuarioHacienda() {
        return usuarioHacienda;
    }

    public void setUsuarioHacienda(String usuarioHacienda) {
        this.usuarioHacienda = usuarioHacienda;
    }

    public String getClaveHacienda() {
        return claveHacienda;
    }

    public void setClaveHacienda(String claveHacienda) {
        this.claveHacienda = claveHacienda;
    }

    public String getTieneHomogen() {
        return tieneHomogen;
    }

    public void setTieneHomogen(String tieneHomogen) {
        this.tieneHomogen = tieneHomogen;
    }

    public int getTokenExp() {
        return tokenExp;
    }

    public void setTokenExp(int tokenExp) {
        this.tokenExp = tokenExp;
    }

    public String getRutaCertificado() {
        return rutaCertificado;
    }

    public void setRutaCertificado(String rutaCertificado) {
        this.rutaCertificado = rutaCertificado;
    }

    public String getPasswordCertificado() {
        return passwordCertificado;
    }

    public void setPasswordCertificado(String passwordCertificado) {
        this.passwordCertificado = passwordCertificado;
    }

    public String getCallBack() {
        return callBack;
    }

    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    
}
