/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class Empresa {
    private String nombre;
    private String tipoIdentificacion;
    private String numIdentificacion;
    private String nombreComercial;
    private String provincia;
    private String canton;
    private String distrito;
    private String barrio;
    private String otrasSenas;
    private String codPaisTel;
    private String numTel;
    private String codPaisFax;
    private String fax;
    private String correoElectronico;
    private String idEmpresa;
    private String haciendaPinCertificado;
    private String haciendaUsuario;
    private String haciendaClave;
    private String rutaLogoEmpresa;
    private String idPlan;
    private String planDesc;
    private String idTipoPlan;
    private String tipoPlanDesc;
    private String idEmpresaPadre;
    private String nombreEmpresaPadre;
    private String fechaRegistro;
    private String esDistribuidor;
    private String administraCobros;
    private String manejaHacienda;
    private String fechaProximoPago;
    private String correoFe;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumIdentificacion() {
        return numIdentificacion;
    }

    public void setNumIdentificacion(String numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getCodPaisTel() {
        return codPaisTel;
    }

    public void setCodPaisTel(String codPaisTel) {
        this.codPaisTel = codPaisTel;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getCodPaisFax() {
        return codPaisFax;
    }

    public void setCodPaisFax(String codPaisFax) {
        this.codPaisFax = codPaisFax;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getHaciendaPinCertificado() {
        return haciendaPinCertificado;
    }

    public void setHaciendaPinCertificado(String haciendaPinCertificado) {
        this.haciendaPinCertificado = haciendaPinCertificado;
    }

    public String getHaciendaUsuario() {
        return haciendaUsuario;
    }

    public void setHaciendaUsuario(String haciendaUsuario) {
        this.haciendaUsuario = haciendaUsuario;
    }

    public String getHaciendaClave() {
        return haciendaClave;
    }

    public void setHaciendaClave(String haciendaClave) {
        this.haciendaClave = haciendaClave;
    }

    public String getRutaLogoEmpresa() {
        return rutaLogoEmpresa;
    }

    public void setRutaLogoEmpresa(String rutaLogoEmpresa) {
        this.rutaLogoEmpresa = rutaLogoEmpresa;
    }

    public String getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    public String getPlanDesc() {
        return planDesc;
    }

    public void setPlanDesc(String planDesc) {
        this.planDesc = planDesc;
    }

    public String getIdTipoPlan() {
        return idTipoPlan;
    }

    public void setIdTipoPlan(String idTipoPlan) {
        this.idTipoPlan = idTipoPlan;
    }

    public String getTipoPlanDesc() {
        return tipoPlanDesc;
    }

    public void setTipoPlanDesc(String tipoPlanDesc) {
        this.tipoPlanDesc = tipoPlanDesc;
    }

    public String getIdEmpresaPadre() {
        return idEmpresaPadre;
    }

    public void setIdEmpresaPadre(String idEmpresaPadre) {
        this.idEmpresaPadre = idEmpresaPadre;
    }

    public String getNombreEmpresaPadre() {
        return nombreEmpresaPadre;
    }

    public void setNombreEmpresaPadre(String nombreEmpresaPadre) {
        this.nombreEmpresaPadre = nombreEmpresaPadre;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaProximoPago() {
        return fechaProximoPago;
    }

    public void setFechaProximoPago(String fechaProximoPago) {
        this.fechaProximoPago = fechaProximoPago;
    }

    public String getCorreoFe() {
        return correoFe;
    }

    public void setCorreoFe(String correoFe) {
        this.correoFe = correoFe;
    }

    public String getEsDistribuidor() {
        return esDistribuidor;
    }

    public void setEsDistribuidor(String esDistribuidor) {
        this.esDistribuidor = esDistribuidor;
    }

    public String getAdministraCobros() {
        return administraCobros;
    }

    public void setAdministraCobros(String administraCobros) {
        this.administraCobros = administraCobros;
    }

    public String getManejaHacienda() {
        return manejaHacienda;
    }

    public void setManejaHacienda(String manejaHacienda) {
        this.manejaHacienda = manejaHacienda;
    }
    
}
