/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class Impuesto {
    private String pIdDocumentoDet;
    private String pIdDocumentoImp;
    private String pCodigoImpuestoEmp;
    private String pTarifa;
    private String pMonto;
    private String pTipoExoneracionEmp;
    private String pNumeroDocumentoExo;
    private String pNombreInstitucionExo;
    private String pFechaEmisionExo;
    private String pMontoImpuestoExo;
    private String pPorcentajeCompraExo;

    public String getpIdDocumentoDet() {
        return pIdDocumentoDet;
    }

    public void setpIdDocumentoDet(String pIdDocumentoDet) {
        this.pIdDocumentoDet = pIdDocumentoDet;
    }

    public String getpIdDocumentoImp() {
        return pIdDocumentoImp;
    }

    public void setpIdDocumentoImp(String pIdDocumentoImp) {
        this.pIdDocumentoImp = pIdDocumentoImp;
    }

    public String getpCodigoImpuestoEmp() {
        return pCodigoImpuestoEmp;
    }

    public void setpCodigoImpuestoEmp(String pCodigoImpuestoEmp) {
        this.pCodigoImpuestoEmp = pCodigoImpuestoEmp;
    }

    public String getpTarifa() {
        return pTarifa;
    }

    public void setpTarifa(String pTarifa) {
        this.pTarifa = pTarifa;
    }

    public String getpMonto() {
        return pMonto;
    }

    public void setpMonto(String pMonto) {
        this.pMonto = pMonto;
    }

    public String getpTipoExoneracionEmp() {
        return pTipoExoneracionEmp;
    }

    public void setpTipoExoneracionEmp(String pTipoExoneracionEmp) {
        this.pTipoExoneracionEmp = pTipoExoneracionEmp;
    }

    public String getpNumeroDocumentoExo() {
        return pNumeroDocumentoExo;
    }

    public void setpNumeroDocumentoExo(String pNumeroDocumentoExo) {
        this.pNumeroDocumentoExo = pNumeroDocumentoExo;
    }

    public String getpNombreInstitucionExo() {
        return pNombreInstitucionExo;
    }

    public void setpNombreInstitucionExo(String pNombreInstitucionExo) {
        this.pNombreInstitucionExo = pNombreInstitucionExo;
    }

    public String getpFechaEmisionExo() {
        return pFechaEmisionExo;
    }

    public void setpFechaEmisionExo(String pFechaEmisionExo) {
        this.pFechaEmisionExo = pFechaEmisionExo;
    }

    public String getpMontoImpuestoExo() {
        return pMontoImpuestoExo;
    }

    public void setpMontoImpuestoExo(String pMontoImpuestoExo) {
        this.pMontoImpuestoExo = pMontoImpuestoExo;
    }

    public String getpPorcentajeCompraExo() {
        return pPorcentajeCompraExo;
    }

    public void setpPorcentajeCompraExo(String pPorcentajeCompraExo) {
        this.pPorcentajeCompraExo = pPorcentajeCompraExo;
    }
    
}
