/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class ImpuestoRecepcionAdd {
    private String pCodigo;
    private String pTarifa;
    private String pMonto;
    private String pTipoDocumentoExo;
    private String pNumeroDocumentoExo;
    private String pNombreInstitucionExo;
    private String pFechaEmisionExo;
    private String pMontoImpuestoExo;
    private String pPorcentajeCompraExo;

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpTarifa() {
        return pTarifa;
    }

    public void setpTarifa(String pTarifa) {
        this.pTarifa = pTarifa;
    }

    public String getpMonto() {
        return pMonto;
    }

    public void setpMonto(String pMonto) {
        this.pMonto = pMonto;
    }

    public String getpTipoDocumentoExo() {
        return pTipoDocumentoExo;
    }

    public void setpTipoDocumentoExo(String pTipoDocumentoExo) {
        this.pTipoDocumentoExo = pTipoDocumentoExo;
    }

    public String getpNumeroDocumentoExo() {
        return pNumeroDocumentoExo;
    }

    public void setpNumeroDocumentoExo(String pNumeroDocumentoExo) {
        this.pNumeroDocumentoExo = pNumeroDocumentoExo;
    }

    public String getpNombreInstitucionExo() {
        return pNombreInstitucionExo;
    }

    public void setpNombreInstitucionExo(String pNombreInstitucionExo) {
        this.pNombreInstitucionExo = pNombreInstitucionExo;
    }

    public String getpFechaEmisionExo() {
        return pFechaEmisionExo;
    }

    public void setpFechaEmisionExo(String pFechaEmisionExo) {
        this.pFechaEmisionExo = pFechaEmisionExo;
    }

    public String getpMontoImpuestoExo() {
        return pMontoImpuestoExo;
    }

    public void setpMontoImpuestoExo(String pMontoImpuestoExo) {
        this.pMontoImpuestoExo = pMontoImpuestoExo;
    }

    public String getpPorcentajeCompraExo() {
        return pPorcentajeCompraExo;
    }

    public void setpPorcentajeCompraExo(String pPorcentajeCompraExo) {
        this.pPorcentajeCompraExo = pPorcentajeCompraExo;
    }
    
}
