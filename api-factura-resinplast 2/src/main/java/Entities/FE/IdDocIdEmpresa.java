/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class IdDocIdEmpresa {
    private String idDocumento;
    private String idEmpresaFe;
    private String emitida;
    private String fechaEmision;

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getIdEmpresaFe() {
        return idEmpresaFe;
    }

    public void setIdEmpresaFe(String idEmpresaFe) {
        this.idEmpresaFe = idEmpresaFe;
    }

    public String getEmitida() {
        return emitida;
    }

    public void setEmitida(String emitida) {
        this.emitida = emitida;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    
}
