/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class ReceptorAdd {
    private String nombre;
    private String tipoIdentificacionEmp;
    private String numIdentificacion;
    private String identificacionExtranjero;
    private String nombreComercial;
    private String provinciaEmp;
    private String cantonEmp;
    private String distritoEmp;
    private String barrioEmp;
    private String otrasSenas;
    private String codPaisTel;
    private String numTel;
    private String codPaisFax;
    private String fax;
    private String correoElectronico;
    private String idReceptorEmp;
    private String tipoTributario;
    private String estado;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoIdentificacionEmp() {
        return tipoIdentificacionEmp;
    }

    public void setTipoIdentificacionEmp(String tipoIdentificacionEmp) {
        this.tipoIdentificacionEmp = tipoIdentificacionEmp;
    }

    public String getNumIdentificacion() {
        return numIdentificacion;
    }

    public void setNumIdentificacion(String numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

    public String getIdentificacionExtranjero() {
        return identificacionExtranjero;
    }

    public void setIdentificacionExtranjero(String identificacionExtranjero) {
        this.identificacionExtranjero = identificacionExtranjero;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getCodPaisTel() {
        return codPaisTel;
    }

    public void setCodPaisTel(String codPaisTel) {
        this.codPaisTel = codPaisTel;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getCodPaisFax() {
        return codPaisFax;
    }

    public void setCodPaisFax(String codPaisFax) {
        this.codPaisFax = codPaisFax;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getTipoTributario() {
        return tipoTributario;
    }

    public void setTipoTributario(String tipoTributario) {
        this.tipoTributario = tipoTributario;
    }

    public String getProvinciaEmp() {
        return provinciaEmp;
    }

    public void setProvinciaEmp(String provinciaEmp) {
        this.provinciaEmp = provinciaEmp;
    }

    public String getCantonEmp() {
        return cantonEmp;
    }

    public void setCantonEmp(String cantonEmp) {
        this.cantonEmp = cantonEmp;
    }

    public String getDistritoEmp() {
        return distritoEmp;
    }

    public void setDistritoEmp(String distritoEmp) {
        this.distritoEmp = distritoEmp;
    }

    public String getBarrioEmp() {
        return barrioEmp;
    }

    public void setBarrioEmp(String barrioEmp) {
        this.barrioEmp = barrioEmp;
    }

    public String getIdReceptorEmp() {
        return idReceptorEmp;
    }

    public void setIdReceptorEmp(String idReceptorEmp) {
        this.idReceptorEmp = idReceptorEmp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
