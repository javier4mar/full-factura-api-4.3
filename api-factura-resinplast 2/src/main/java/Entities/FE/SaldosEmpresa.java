/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class SaldosEmpresa {
    private String saldoFavor;
    private String saldoContra;
    private String idEmpresa;

    public String getSaldoFavor() {
        return saldoFavor;
    }

    public void setSaldoFavor(String saldoFavor) {
        this.saldoFavor = saldoFavor;
    }

    public String getSaldoContra() {
        return saldoContra;
    }

    public void setSaldoContra(String saldoContra) {
        this.saldoContra = saldoContra;
    }

    public String getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    
}
