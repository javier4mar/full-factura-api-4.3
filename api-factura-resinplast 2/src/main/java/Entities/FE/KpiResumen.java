/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class KpiResumen {
    private String facturasEmitidas;
    private String facturasEmitidasPorcentaje;
    private String facturasRecibidas;
    private String facturasRecibidasPorcentaje;
    private String totalVentas;
    private String totalFacturas;

    public String getFacturasEmitidas() {
        return facturasEmitidas;
    }

    public void setFacturasEmitidas(String facturasEmitidas) {
        this.facturasEmitidas = facturasEmitidas;
    }

    public String getFacturasEmitidasPorcentaje() {
        return facturasEmitidasPorcentaje;
    }

    public void setFacturasEmitidasPorcentaje(String facturasEmitidasPorcentaje) {
        this.facturasEmitidasPorcentaje = facturasEmitidasPorcentaje;
    }

    public String getFacturasRecibidas() {
        return facturasRecibidas;
    }

    public void setFacturasRecibidas(String facturasRecibidas) {
        this.facturasRecibidas = facturasRecibidas;
    }

    public String getFacturasRecibidasPorcentaje() {
        return facturasRecibidasPorcentaje;
    }

    public void setFacturasRecibidasPorcentaje(String facturasRecibidasPorcentaje) {
        this.facturasRecibidasPorcentaje = facturasRecibidasPorcentaje;
    }

    public String getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(String totalVentas) {
        this.totalVentas = totalVentas;
    }

    public String getTotalFacturas() {
        return totalFacturas;
    }

    public void setTotalFacturas(String totalFacturas) {
        this.totalFacturas = totalFacturas;
    }
    
}
