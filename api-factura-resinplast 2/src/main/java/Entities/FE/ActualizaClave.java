/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class ActualizaClave {
    private String pClaveNueva;
    private String pClaveAnterior;

    public String getpClaveNueva() {
        return pClaveNueva;
    }

    public void setpClaveNueva(String pClaveNueva) {
        this.pClaveNueva = pClaveNueva;
    }

    public String getpClaveAnterior() {
        return pClaveAnterior;
    }

    public void setpClaveAnterior(String pClaveAnterior) {
        this.pClaveAnterior = pClaveAnterior;
    }
    
}
