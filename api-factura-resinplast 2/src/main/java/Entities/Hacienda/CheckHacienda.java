/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Hacienda;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author msalasch
 */
public class CheckHacienda {
    private String clave;
    private String fecha;
    @SerializedName("ind-estado") 
    private String ind_estado;
    @SerializedName("respuesta-xml") 
    private String respuesta_xml;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getInd_estado() {
        return ind_estado;
    }

    public void setInd_estado(String ind_estado) {
        this.ind_estado = ind_estado;
    }

    public String getRespuesta_xml() {
        return respuesta_xml;
    }

    public void setRespuesta_xml(String respuesta_xml) {
        this.respuesta_xml = respuesta_xml;
    }
    
    
}
