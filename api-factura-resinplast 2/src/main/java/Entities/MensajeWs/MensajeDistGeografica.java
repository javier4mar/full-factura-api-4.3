/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.CodDescripcion;
import Entities.Catalogos.DistGeografica;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeDistGeografica  extends MensajeBase {
    private List<DistGeografica> distGeografica;
    private List<CodDescripcion> niveles;

    public List<DistGeografica> getDistGeografica() {
        return distGeografica;
    }

    public void setDistGeografica(List<DistGeografica> distGeografica) {
        this.distGeografica = distGeografica;
    }

    public List<CodDescripcion> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<CodDescripcion> niveles) {
        this.niveles = niveles;
    }
    
    
}
