/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Documento;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeDocumento extends MensajeBase {
    List<Documento> documentos;

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }
    
}
