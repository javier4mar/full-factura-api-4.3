/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.PuntoVenta;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajePuntoVenta extends MensajeBase {
    List<PuntoVenta> puntosVenta;

    public List<PuntoVenta> getPuntosVenta() {
        return puntosVenta;
    }

    public void setPuntosVenta(List<PuntoVenta> puntosVenta) {
        this.puntosVenta = puntosVenta;
    }
    
}
