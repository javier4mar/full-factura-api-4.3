/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Sucursal;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeSucursal extends MensajeBase {
    List<Sucursal> sucursales;

    public List<Sucursal> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    
}
