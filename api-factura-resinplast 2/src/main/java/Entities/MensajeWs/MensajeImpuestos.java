/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.ImpuestoCat;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeImpuestos extends MensajeBase {
    List<ImpuestoCat> impuestos;

    public List<ImpuestoCat> getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(List<ImpuestoCat> impuestos) {
        this.impuestos = impuestos;
    }
    
}
