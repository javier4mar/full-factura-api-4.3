/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.Pagina;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajePaginas extends MensajeBase {
    private List<Pagina> paginas;

    public List<Pagina> getPaginas() {
        return paginas;
    }

    public void setPaginas(List<Pagina> paginas) {
        this.paginas = paginas;
    }
    
}
