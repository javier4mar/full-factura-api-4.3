/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Banco;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeBancos extends MensajeBase {
    private List<Banco> bancos;

    public List<Banco> getBancos() {
        return bancos;
    }

    public void setBancos(List<Banco> bancos) {
        this.bancos = bancos;
    }
    
    
}
