/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.ExoneracionArt;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeExoneraciones extends MensajeBase  {
    private List<ExoneracionArt> exoneraciones;

    public List<ExoneracionArt> getExoneraciones() {
        return exoneraciones;
    }

    public void setExoneraciones(List<ExoneracionArt> exoneraciones) {
        this.exoneraciones = exoneraciones;
    }
    
    
}
