/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.SaldosEmpresa;

/**
 *
 * @author msalasch
 */
public class MensajeSaldosEmp extends MensajeBase {
    private SaldosEmpresa resumenSaldo;

    public SaldosEmpresa getResumenSaldo() {
        return resumenSaldo;
    }

    public void setResumenSaldo(SaldosEmpresa resumenSaldo) {
        this.resumenSaldo = resumenSaldo;
    }
    
    
}
