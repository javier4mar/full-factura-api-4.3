/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Receptor;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeReceptor extends MensajeBase {
    List<Receptor> receptores;

    public List<Receptor> getReceptores() {
        return receptores;
    }

    public void setReceptores(List<Receptor> receptores) {
        this.receptores = receptores;
    }

}
