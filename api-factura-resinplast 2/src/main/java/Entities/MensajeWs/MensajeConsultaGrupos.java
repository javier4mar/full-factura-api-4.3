/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.GruposUsuarioConsulta;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeConsultaGrupos extends MensajeBase {
    private List<GruposUsuarioConsulta> pLista;

    public List<GruposUsuarioConsulta> getpLista() {
        return pLista;
    }

    public void setpLista(List<GruposUsuarioConsulta> pLista) {
        this.pLista = pLista;
    }
    
    
}
