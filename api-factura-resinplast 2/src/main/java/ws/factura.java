/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import Auth.JWT;
import Constants.Constantes;
import Entities.FE.Correo;
import Entities.FE.IdDocumento;
import Entities.FE.DocumentoAdd;
import Entities.FE.DocumentoRecepcionAdd;
import Entities.FE.EmisorToken;
import Entities.FE.FiltraDocumento;
import Entities.FE.ProcesaCorreo;
import Entities.FE.PuntoVentaAdd;
import Entities.FE.PuntoVentaEdit;
import Entities.FE.ReceptorAdd;
import Entities.FE.ReceptorPk;
import Entities.FE.SucursalAdd;
import Entities.MensajeWs.MensajeBase;
import Entities.MensajeWs.MensajeCatalogoFactura;
import Entities.MensajeWs.MensajeDocumento;
import Entities.MensajeWs.MensajeInsert;
import Entities.MensajeWs.MensajeInteresados;
import Entities.MensajeWs.MensajePuntoVenta;
import Entities.MensajeWs.MensajeReceptor;
import Entities.MensajeWs.MensajeSucursal;
import Entities.MensajeWs.MensajeValidaRecepcion;
import Methods.IMetodos;
import Methods.Metodos;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.multipart.MultiPart;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;


/**
 *
 * @author Administrador
 */
@Path("/facturaws")
public class factura {
    @Context ServletContext servletContext;
    
    @POST
    @Path("/addReceptor")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInsert addReceptor(@HeaderParam("pToken") String pToken, ReceptorAdd pReceptor) {
        
        MensajeInsert mensajeInsert = new MensajeInsert();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addReceptor(mClaims, pReceptor);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateReceptor")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateReceptor(@HeaderParam("pToken") String pToken, ReceptorAdd pReceptor) {
        
        MensajeBase mensajeUpdate = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("El token es requerido");
                return mensajeUpdate;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("Token invalido o expirado");
                return mensajeUpdate;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeUpdate = metodos.updateReceptor(mClaims, pReceptor);

            return mensajeUpdate;

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());
            return mensajeUpdate;
        }
    }
    
    @GET
    @Path("/receptores")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeReceptor receptores(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado) {
        MensajeReceptor mensajeReceptor = new MensajeReceptor();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeReceptor.setStatus(Constantes.statusError);
                mensajeReceptor.setMensaje("El token es requerido");
                return mensajeReceptor;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeReceptor.setStatus(Constantes.statusError);
                mensajeReceptor.setMensaje("Token invalido o expirado");
                return mensajeReceptor;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeReceptor = metodos.getReceptores(mClaims.getIdEmpresaFe(), pEstado);
            
            return mensajeReceptor;

        } catch (Exception e) {
            mensajeReceptor.setStatus(Constantes.statusError);
            mensajeReceptor.setMensaje(e.getMessage());
            return mensajeReceptor;
        }
    }
    
    @POST
    @Path("/deleteReceptor")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteReceptor(@HeaderParam("pToken") String pToken,  ReceptorPk data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }
            
            if (data.getpIdReceptorEmp()== null || data.getpIdReceptorEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El id del receptor de la empresa es requerido");
                return mensajeDelete;
            }
            
            IMetodos metodos = new Metodos();
            
            if (metodos.deleteReceptor(mClaims.getIdEmpresaFe(), data.getpIdReceptorEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }
            
            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }
    
    @POST
    @Path("/registraDocumento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInsert registraDocumento(@HeaderParam("pToken") String pToken, DocumentoAdd pDocumento) {
        
        MensajeInsert mensajeInsert = new MensajeInsert();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.registraDocumento(mClaims, pDocumento);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/addSucursalEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addSucursalEmp(@HeaderParam("pToken") String pToken, SucursalAdd data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addSucursalEmp(mClaims.getIdEmpresaFe(), data);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateSucursalEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateSucursalEmp(@HeaderParam("pToken") String pToken, SucursalAdd data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updateSucursalEmp(mClaims.getIdEmpresaFe(), data);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/sucursales")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeSucursal sucursales(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado) {
        MensajeSucursal mensajeSucursal = new MensajeSucursal();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeSucursal.setStatus(Constantes.statusError);
                mensajeSucursal.setMensaje("El token es requerido");
                return mensajeSucursal;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeSucursal.setStatus(Constantes.statusError);
                mensajeSucursal.setMensaje("Token invalido o expirado");
                return mensajeSucursal;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeSucursal = metodos.getSucursales(mClaims.getIdEmpresaFe(), pEstado);
            
            return mensajeSucursal;

        } catch (Exception e) {
            mensajeSucursal.setStatus(Constantes.statusError);
            mensajeSucursal.setMensaje(e.getMessage());
            return mensajeSucursal;
        }
    }
    
    @POST
    @Path("/addPuntoVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addPuntoVentaEmp(@HeaderParam("pToken") String pToken, PuntoVentaAdd data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addPuntoVentaEmp(mClaims.getIdEmpresaFe(), data);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updatePuntoVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updatePuntoVentaEmp(@HeaderParam("pToken") String pToken, PuntoVentaEdit data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updatePuntoVentaEmp(mClaims.getIdEmpresaFe(), data);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/puntosventa/{pIdSucursalEmp}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePuntoVenta puntosventa(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado, @PathParam("pIdSucursalEmp") String pIdSucursalEmp) {
        MensajePuntoVenta mensajePuntoVenta = new MensajePuntoVenta();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajePuntoVenta.setStatus(Constantes.statusError);
                mensajePuntoVenta.setMensaje("El token es requerido");
                return mensajePuntoVenta;
            }
            
            if (pIdSucursalEmp == null || pIdSucursalEmp.equals("")) {
                mensajePuntoVenta.setStatus(Constantes.statusError);
                mensajePuntoVenta.setMensaje("El id de la sucursal es requerido");
                return mensajePuntoVenta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajePuntoVenta.setStatus(Constantes.statusError);
                mensajePuntoVenta.setMensaje("Token invalido o expirado");
                return mensajePuntoVenta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajePuntoVenta = metodos.getPuntosVenta(mClaims.getIdEmpresaFe(), pIdSucursalEmp, pEstado);
            
            return mensajePuntoVenta;

        } catch (Exception e) {
            mensajePuntoVenta.setStatus(Constantes.statusError);
            mensajePuntoVenta.setMensaje(e.getMessage());
            return mensajePuntoVenta;
        }
    }
    
    @POST
    @Path("/documento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDocumento documento(@HeaderParam("pToken") String pToken, FiltraDocumento data) {
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDocumento.setStatus(Constantes.statusError);
                mensajeDocumento.setMensaje("El token es requerido");
                return mensajeDocumento;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeDocumento.setStatus(Constantes.statusError);
                mensajeDocumento.setMensaje("Token invalido o expirado");
                return mensajeDocumento;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeDocumento = metodos.getDocumento(mClaims.getIdEmpresaFe(), data);
            
            return mensajeDocumento;

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());
            return mensajeDocumento;
        }
    }
    
    @POST
    @Path("/documentosNC")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDocumento documentosNC(@HeaderParam("pToken") String pToken, FiltraDocumento data) {
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDocumento.setStatus(Constantes.statusError);
                mensajeDocumento.setMensaje("El token es requerido");
                return mensajeDocumento;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeDocumento.setStatus(Constantes.statusError);
                mensajeDocumento.setMensaje("Token invalido o expirado");
                return mensajeDocumento;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeDocumento = metodos.getDocumentosNC(mClaims.getIdEmpresaFe(), data);
            
            return mensajeDocumento;

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());
            return mensajeDocumento;
        }
    }
    
    @GET
    @Path("/validaRecepcion/{pClave}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeValidaRecepcion validaRecepcion(@HeaderParam("pToken") String pToken, @PathParam("pClave") String pClave) {
        MensajeValidaRecepcion mensajeValida = new MensajeValidaRecepcion();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeValida.setStatus(Constantes.statusError);
                mensajeValida.setMensaje("El token es requerido");
                return mensajeValida;
            }
            
            if (pClave == null || pClave.equals("")) {
                mensajeValida.setStatus(Constantes.statusError);
                mensajeValida.setMensaje("La clave del documento es requerida");
                return mensajeValida;
            }
            
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeValida.setStatus(Constantes.statusError);
                mensajeValida.setMensaje("Token invalido o expirado");
                return mensajeValida;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeValida = metodos.validaRecepcion(mClaims.getIdEmpresaFe(), pClave);
            
            return mensajeValida;

        } catch (Exception e) {
            mensajeValida.setStatus(Constantes.statusError);
            mensajeValida.setMensaje(e.getMessage());
            return mensajeValida;
        }
    }
    
    @POST
    @Path("/registraDocumentoRecepcion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInsert registraDocumentoRecepcion(@HeaderParam("pToken") String pToken, DocumentoRecepcionAdd pDocumento) {
        
        MensajeInsert mensajeInsert = new MensajeInsert();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.registraDocumentoRecepcion(mClaims, pDocumento);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/estadosDocumento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura estadosDocumento(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getEstadosDocumento();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/codigoMensajeHacienda")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura codigoMensajeHacienda(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getCodigoMensajeHacienda();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/situacionDocumento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura situacionDocumento(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getSituacionDocumento();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/tipoDocumentosDet")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura tipoDocumentosDet(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getTipoDocumentosDet();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/codigoDocumentoRef")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura codigoDocumentoRef(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getCodigoDocumentoRef();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addXmlRecepcion")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addXmlRecepcion(@HeaderParam("pToken") String pToken, 
                                        @FormDataParam("pClave") String pClave,
                                        @FormDataParam("pXml") InputStream pXml,
                                        @FormDataParam("pXml") FormDataContentDisposition pXmlDetail) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            if (pClave == null || pClave.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El clave del documento es requerida");
                return mensajeInsert;
            }
            
            if (pXml == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El xml es requerido");
                return mensajeInsert;
            }
            
            if (!pXmlDetail.getFileName().toUpperCase().endsWith(".XML")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El archivo debe ser de tipo .xml");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addXmlRecepcion(mClaims, pClave, pXml, pXmlDetail);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/procesaDocumento/{pClave}/{pStatus}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase procesaDocumento(@PathParam("pClave") String pClave, @PathParam("pStatus") String pStatus) {
        MensajeBase mensajeProcesa = new MensajeBase();
        
        try {
            IMetodos metodos = new Metodos();
            
            mensajeProcesa = metodos.procesaDocumento(pClave, pStatus);

        } catch (Exception e) {
            mensajeProcesa.setStatus(Constantes.statusError);
            mensajeProcesa.setMensaje(e.getMessage());
            
            return mensajeProcesa;
        }
        
        return mensajeProcesa;
    }
    
    @GET
    @Path("/documentoCorreo")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDocumento documentoCorreo() {
        MensajeDocumento mensajeDocumento = new MensajeDocumento();
        
        try {
            
            IMetodos metodos = new Metodos();
            
            mensajeDocumento = metodos.getDocumentoCorreo();
            
            return mensajeDocumento;

        } catch (Exception e) {
            mensajeDocumento.setStatus(Constantes.statusError);
            mensajeDocumento.setMensaje(e.getMessage());
            return mensajeDocumento;
        }
    }
    
    @POST
    @Path("/procesaInteresados")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase procesaInteresados(List<ProcesaCorreo> pDatos) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.procesaInteresados(pDatos);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/backUpXml")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response backUpXml(@HeaderParam("pToken") String pToken) {
        StreamingOutput zipFile = null;
        EmisorToken mClaims;
        
        try {
            if (pToken == null || pToken.equals("")) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "El token es requerido").build();
            }
            
            mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "Token invalido o expirado").build();
            }
            
            IMetodos metodos = new Metodos();
            
            zipFile = metodos.backUpXml(mClaims.getIdEmpresaFe());
            

        } catch (Exception e) {
            return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", e.getMessage()).build();
        }
        return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "SUCCESS").header("mensaje", "Backup realizado con exito").header("Content-Disposition","attachment; filename="+ mClaims.getIdEmpresaFe() +".zip").build();
    }
    
    @POST
    @Path("/backUpXmlEspecifico")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response backUpXmlEspecifico(@HeaderParam("pToken") String pToken, List<IdDocumento> documentos) {
        StreamingOutput zipFile = null;
        EmisorToken mClaims;
        String fileName = "Documentos.zip";
        
        try {
            if (pToken == null || pToken.equals("")) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "El token es requerido").build();
            }
            
            mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "Token invalido o expirado").build();
            }
            
            if (documentos == null || documentos.isEmpty()) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "Debe indicar al menos un documento").build();
            }
            
            IMetodos metodos = new Metodos();
            
            zipFile = metodos.backUpXmlEspecifico(mClaims.getIdEmpresaFe(), documentos, fileName);
            
            if (zipFile == null) {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", "No se han encontrado documentos").build();
            } else {
                return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "SUCCESS").header("mensaje", "Backup realizado con exito").header("Content-Disposition","attachment; filename="+ fileName ).build();
            }
            

        } catch (Exception e) {
            return Response.ok(zipFile, MediaType.APPLICATION_OCTET_STREAM).header("status", "ERROR").header("mensaje", e.getMessage()).build();
        }
        
    }
    
    @POST
    @Path("/reenviarCorreo/{pIdDocumento}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase reenviarCorreo(@HeaderParam("pToken") String pToken, @PathParam("pIdDocumento") String pIdDocumento, List<Correo> correos) {
        MensajeBase mensajeConsulta = new MensajeBase();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            if (pIdDocumento == null || pIdDocumento.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El documento es requerido");
                return mensajeConsulta;
            }
            
            if (correos == null || correos.isEmpty()) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Debe indicar al menos un correo");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.reenviarCorreo(pIdDocumento, correos);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/envios/{pIdClave}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInteresados enviosDocumento(@HeaderParam("pToken") String pToken, @PathParam("pIdClave") String pIdClave) {
        MensajeInteresados mensajeInteresado = new MensajeInteresados();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInteresado.setStatus(Constantes.statusError);
                mensajeInteresado.setMensaje("El token es requerido");
                return mensajeInteresado;
            }
            
            if (pIdClave == null || pIdClave.equals("")) {
                mensajeInteresado.setStatus(Constantes.statusError);
                mensajeInteresado.setMensaje("La Clave del documento es requerida");
                return mensajeInteresado;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInteresado.setStatus(Constantes.statusError);
                mensajeInteresado.setMensaje("Token invalido o expirado");
                return mensajeInteresado;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInteresado = metodos.getInteresadosDocumento(mClaims.getIdEmpresa(),pIdClave);
            
            return mensajeInteresado;

        } catch (Exception e) {
            mensajeInteresado.setStatus(Constantes.statusError);
            mensajeInteresado.setMensaje(e.getMessage());
            return mensajeInteresado;
        }
    }
}
