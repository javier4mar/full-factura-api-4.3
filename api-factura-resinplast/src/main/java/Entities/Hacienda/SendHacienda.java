/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Hacienda;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author msalasch
 */
public class SendHacienda {
    private boolean DocumentoEnviado;
    @SerializedName("error") 
    private String Mensaje;

    public boolean isDocumentoEnviado() {
        return DocumentoEnviado;
    }

    public void setDocumentoEnviado(boolean DocumentoEnviado) {
        this.DocumentoEnviado = DocumentoEnviado;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String Mensaje) {
        this.Mensaje = Mensaje;
    }
    
    
}
