/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class ArticuloAdd {
    private String idArticuloEmp;
    private String idCategoria;
    private String codigo;
    private String descripcion;
    private String idUnidadMedidaEmp;
    private String unidadMedidaCom;
    private String tipoArticulo;
    private String precio;
    private String porcentaje;
    private String descuento;
    private String esRecargo;
    private String estado;
    private String costo;
    private List<String> idsArticuloEmp;
    private List<ImpuestoExoClienteAdd> impuestos;
    

    public String getIdArticuloEmp() {
        return idArticuloEmp;
    }

    public void setIdArticuloEmp(String idArticuloEmp) {
        this.idArticuloEmp = idArticuloEmp;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdUnidadMedidaEmp() {
        return idUnidadMedidaEmp;
    }

    public void setIdUnidadMedidaEmp(String idUnidadMedidaEmp) {
        this.idUnidadMedidaEmp = idUnidadMedidaEmp;
    }

    public String getUnidadMedidaCom() {
        return unidadMedidaCom;
    }

    public void setUnidadMedidaCom(String unidadMedidaCom) {
        this.unidadMedidaCom = unidadMedidaCom;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getEsRecargo() {
        return esRecargo;
    }

    public void setEsRecargo(String esRecargo) {
        this.esRecargo = esRecargo;
    }

    public List<ImpuestoExoClienteAdd> getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(List<ImpuestoExoClienteAdd> impuestos) {
        this.impuestos = impuestos;
    }

    public List<String> getIdsArticuloEmp() {
        return idsArticuloEmp;
    }

    public void setIdsArticuloEmp(List<String> idsArticuloEmp) {
        this.idsArticuloEmp = idsArticuloEmp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }
    
    
}
