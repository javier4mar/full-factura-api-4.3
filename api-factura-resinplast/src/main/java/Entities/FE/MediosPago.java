/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import Entities.Catalogos.CodDescripcion;

/**
 *
 * @author msalasch
 */
public class MediosPago extends CodDescripcion {
    private String solicitaComprobante;

    public String getSolicitaComprobante() {
        return solicitaComprobante;
    }

    public void setSolicitaComprobante(String solicitaComprobante) {
        this.solicitaComprobante = solicitaComprobante;
    }
    
    
}
