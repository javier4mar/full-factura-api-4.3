/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class Pags {
    private String idPagina;
    private String url;
    private String nombrePagina;
    private String idPaginaPadre;
    private String nivel;
    private String mostrarMenu;
    private String secuencia;
    private String icon;
    private String color;
    private String mostrarPreferencias;
    private String estado;

    public String getIdPagina() {
        return idPagina;
    }

    public void setIdPagina(String idPagina) {
        this.idPagina = idPagina;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombrePagina() {
        return nombrePagina;
    }

    public void setNombrePagina(String nombrePagina) {
        this.nombrePagina = nombrePagina;
    }

    public String getIdPaginaPadre() {
        return idPaginaPadre;
    }

    public void setIdPaginaPadre(String idPaginaPadre) {
        this.idPaginaPadre = idPaginaPadre;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getMostrarMenu() {
        return mostrarMenu;
    }

    public void setMostrarMenu(String mostrarMenu) {
        this.mostrarMenu = mostrarMenu;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMostrarPreferencias() {
        return mostrarPreferencias;
    }

    public void setMostrarPreferencias(String mostrarPreferencias) {
        this.mostrarPreferencias = mostrarPreferencias;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
