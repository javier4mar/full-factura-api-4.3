package Entities.FE;


import java.util.List;

/**
 * Clase que contiene la informacion necesaria para actualizar la NC de resinplast
 * @author Arturo Chanto
 */
public class ActualizaNC {
    private String pidDocumentoNC;
    private String estado;
    private String pTipoDocRefEmp;
    private String pTipoDocRef;
    private String pNumero;
    private String pFechaEmision;
    private String pCodigo;
    private String pRazon;
    private String pIdDocumentoEmp;
    private String pExisteDb;

    public String getPidDocumentoNC() {
        return pidDocumentoNC;
    }

    public void setPidDocumentoNC(String pidDocumentoNC) {
        this.pidDocumentoNC = pidDocumentoNC;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getpTipoDocRefEmp() {
        return pTipoDocRefEmp;
    }

    public void setpTipoDocRefEmp(String pTipoDocRefEmp) {
        this.pTipoDocRefEmp = pTipoDocRefEmp;
    }

    public String getpTipoDocRef() {
        return pTipoDocRef;
    }

    public void setpTipoDocRef(String pTipoDocRef) {
        this.pTipoDocRef = pTipoDocRef;
    }

    public String getpNumero() {
        return pNumero;
    }

    public void setpNumero(String pNumero) {
        this.pNumero = pNumero;
    }

    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpCodigo() {
        return pCodigo;
    }

    public void setpCodigo(String pCodigo) {
        this.pCodigo = pCodigo;
    }

    public String getpRazon() {
        return pRazon;
    }

    public void setpRazon(String pRazon) {
        this.pRazon = pRazon;
    }

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public String getpExisteDb() {
        return pExisteDb;
    }

    public void setpExisteDb(String pExisteDb) {
        this.pExisteDb = pExisteDb;
    }

}
