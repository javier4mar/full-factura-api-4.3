/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class EmpresaGrupoAdd {
    private String pIdEmpresa;
    private String pIdGrupo;

    public String getpIdEmpresa() {
        return pIdEmpresa;
    }

    public void setpIdEmpresa(String pIdEmpresa) {
        this.pIdEmpresa = pIdEmpresa;
    }

    public String getpIdGrupo() {
        return pIdGrupo;
    }

    public void setpIdGrupo(String pIdGrupo) {
        this.pIdGrupo = pIdGrupo;
    }
    
}
