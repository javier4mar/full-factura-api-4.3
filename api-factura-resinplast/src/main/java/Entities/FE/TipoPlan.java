/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class TipoPlan {
    private String idTipoPlan;
    private String descripcion;
    private String cantidadMeses;
    private String estado;
    private String estadoDesc;

    public String getIdTipoPlan() {
        return idTipoPlan;
    }

    public void setIdTipoPlan(String idTipoPlan) {
        this.idTipoPlan = idTipoPlan;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidadMeses() {
        return cantidadMeses;
    }

    public void setCantidadMeses(String cantidadMeses) {
        this.cantidadMeses = cantidadMeses;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }
    
}
