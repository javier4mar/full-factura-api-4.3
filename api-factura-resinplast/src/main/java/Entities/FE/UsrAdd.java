/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class UsrAdd {
    private String pUsuario;
    private String pNombreUsuario;
    private String pIdEmpresaDefault;
    private String pClave;
    private String pEstado;
    private List<EmpresaGrupoAdd> pGruposEmpresas;

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpNombreUsuario() {
        return pNombreUsuario;
    }

    public void setpNombreUsuario(String pNombreUsuario) {
        this.pNombreUsuario = pNombreUsuario;
    }

    public String getpIdEmpresaDefault() {
        return pIdEmpresaDefault;
    }

    public void setpIdEmpresaDefault(String pIdEmpresaDefault) {
        this.pIdEmpresaDefault = pIdEmpresaDefault;
    }

    public List<EmpresaGrupoAdd> getpGruposEmpresas() {
        return pGruposEmpresas;
    }

    public void setpGruposEmpresas(List<EmpresaGrupoAdd> pGruposEmpresas) {
        this.pGruposEmpresas = pGruposEmpresas;
    }

    public String getpClave() {
        return pClave;
    }

    public void setpClave(String pClave) {
        this.pClave = pClave;
    }

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }
    
}
