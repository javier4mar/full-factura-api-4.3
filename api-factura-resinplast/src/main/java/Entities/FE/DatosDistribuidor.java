/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class DatosDistribuidor {
    private String nombre;
    private List<String> tiposIdentificacionEmp;
    private String numIdentificacion;
    private String nombreComercial;
    private List<String> provinciaEmp;
    private List<String> cantonEmp;
    private List<String> distritoEmp;
    private List<String> barrioEmp;
    private String otrasSenas;
    private String codPaisTel;
    private String numTel;
    private String codPaisFax;
    private String fax;
    private String correoElectronico;
    private String idEmpresaEmp;
    private String haciendaPinCertificado;
    private String haciendaUsuario;
    private String haciendaClave;
    private String haciendaCertificado;
    private String rutaLogoEmpresa;
    private String estado;
    private String estadoDesc;
    private String administraCobro;
    private String administraCobroDesc;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getTiposIdentificacionEmp() {
        return tiposIdentificacionEmp;
    }

    public void setTiposIdentificacionEmp(List<String> tiposIdentificacionEmp) {
        this.tiposIdentificacionEmp = tiposIdentificacionEmp;
    }

    public String getNumIdentificacion() {
        return numIdentificacion;
    }

    public void setNumIdentificacion(String numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public List<String> getProvinciaEmp() {
        return provinciaEmp;
    }

    public void setProvinciaEmp(List<String> provinciaEmp) {
        this.provinciaEmp = provinciaEmp;
    }

    public List<String> getCantonEmp() {
        return cantonEmp;
    }

    public void setCantonEmp(List<String> cantonEmp) {
        this.cantonEmp = cantonEmp;
    }

    public List<String> getDistritoEmp() {
        return distritoEmp;
    }

    public void setDistritoEmp(List<String> distritoEmp) {
        this.distritoEmp = distritoEmp;
    }

    public List<String> getBarrioEmp() {
        return barrioEmp;
    }

    public void setBarrioEmp(List<String> barrioEmp) {
        this.barrioEmp = barrioEmp;
    }

    public String getOtrasSenas() {
        return otrasSenas;
    }

    public void setOtrasSenas(String otrasSenas) {
        this.otrasSenas = otrasSenas;
    }

    public String getCodPaisTel() {
        return codPaisTel;
    }

    public void setCodPaisTel(String codPaisTel) {
        this.codPaisTel = codPaisTel;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getCodPaisFax() {
        return codPaisFax;
    }

    public void setCodPaisFax(String codPaisFax) {
        this.codPaisFax = codPaisFax;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getIdEmpresaEmp() {
        return idEmpresaEmp;
    }

    public void setIdEmpresaEmp(String idEmpresaEmp) {
        this.idEmpresaEmp = idEmpresaEmp;
    }

    public String getHaciendaPinCertificado() {
        return haciendaPinCertificado;
    }

    public void setHaciendaPinCertificado(String haciendaPinCertificado) {
        this.haciendaPinCertificado = haciendaPinCertificado;
    }

    public String getHaciendaUsuario() {
        return haciendaUsuario;
    }

    public void setHaciendaUsuario(String haciendaUsuario) {
        this.haciendaUsuario = haciendaUsuario;
    }

    public String getHaciendaClave() {
        return haciendaClave;
    }

    public void setHaciendaClave(String haciendaClave) {
        this.haciendaClave = haciendaClave;
    }

    public String getHaciendaCertificado() {
        return haciendaCertificado;
    }

    public void setHaciendaCertificado(String haciendaCertificado) {
        this.haciendaCertificado = haciendaCertificado;
    }

    public String getRutaLogoEmpresa() {
        return rutaLogoEmpresa;
    }

    public void setRutaLogoEmpresa(String rutaLogoEmpresa) {
        this.rutaLogoEmpresa = rutaLogoEmpresa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public String getAdministraCobro() {
        return administraCobro;
    }

    public void setAdministraCobro(String administraCobro) {
        this.administraCobro = administraCobro;
    }

    public String getAdministraCobroDesc() {
        return administraCobroDesc;
    }

    public void setAdministraCobroDesc(String administraCobroDesc) {
        this.administraCobroDesc = administraCobroDesc;
    }
    
}
