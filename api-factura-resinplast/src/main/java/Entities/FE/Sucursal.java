/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class Sucursal {
    private String idSucursalEmp;
    private String numSucursal;
    private String descripcion;
    private String estado;
    private String estadoDesc;

    public String getIdSucursalEmp() {
        return idSucursalEmp;
    }

    public void setIdSucursalEmp(String idSucursalEmp) {
        this.idSucursalEmp = idSucursalEmp;
    }

    public String getNumSucursal() {
        return numSucursal;
    }

    public void setNumSucursal(String numSucursal) {
        this.numSucursal = numSucursal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }
    
}
