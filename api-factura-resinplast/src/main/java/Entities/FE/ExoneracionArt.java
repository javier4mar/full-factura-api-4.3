/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author Administrador
 */
public class ExoneracionArt {
    private String codigoTipoExo;
    private String numeroDocumento;
    private String nombreInstitucion;
    private String fechaEmision;
    private String montoImpuesto;
    private String porcentajeCompra;
    private String idExoneracion;

    public String getCodigoTipoExo() {
        return codigoTipoExo;
    }

    public void setCodigoTipoExo(String codigoTipoExo) {
        this.codigoTipoExo = codigoTipoExo;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getMontoImpuesto() {
        return montoImpuesto;
    }

    public void setMontoImpuesto(String montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public String getPorcentajeCompra() {
        return porcentajeCompra;
    }

    public void setPorcentajeCompra(String porcentajeCompra) {
        this.porcentajeCompra = porcentajeCompra;
    }

    public String getIdExoneracion() {
        return idExoneracion;
    }

    public void setIdExoneracion(String idExoneracion) {
        this.idExoneracion = idExoneracion;
    }
    
}
