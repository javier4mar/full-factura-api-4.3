/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author msalasch
 */
public class FiltraDocumento {
    private String pIdDocumentoEmp;
    private String pVerDetalle;
    private String pFechaDesde;
    private String pFechaHasta;
    private List<String> pEstado;
    private String pEntidadEmite;
    private String pTipoDocumentoEmp;
    private String pNombreCliente;
    private String pIdenCliente;
    private String pClave;
    private String pConsecutivo;
    private String pMonto;

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public String getpVerDetalle() {
        return pVerDetalle;
    }

    public void setpVerDetalle(String pVerDetalle) {
        this.pVerDetalle = pVerDetalle;
    }

    public String getpFechaDesde() {
        return pFechaDesde;
    }

    public void setpFechaDesde(String pFechaDesde) {
        this.pFechaDesde = pFechaDesde;
    }

    public String getpFechaHasta() {
        return pFechaHasta;
    }

    public void setpFechaHasta(String pFechaHasta) {
        this.pFechaHasta = pFechaHasta;
    }

    public String getpEntidadEmite() {
        return pEntidadEmite;
    }

    public void setpEntidadEmite(String pEntidadEmite) {
        this.pEntidadEmite = pEntidadEmite;
    }

    public List<String> getpEstado() {
        return pEstado;
    }

    public void setpEstado(List<String> pEstado) {
        this.pEstado = pEstado;
    }

    public String getpTipoDocumentoEmp() {
        return pTipoDocumentoEmp;
    }

    public void setpTipoDocumentoEmp(String pTipoDocumentoEmp) {
        this.pTipoDocumentoEmp = pTipoDocumentoEmp;
    }

    public String getpNombreCliente() {
        return pNombreCliente;
    }

    public void setpNombreCliente(String pNombreCliente) {
        this.pNombreCliente = pNombreCliente;
    }

    public String getpIdenCliente() {
        return pIdenCliente;
    }

    public void setpIdenCliente(String pIdenCliente) {
        this.pIdenCliente = pIdenCliente;
    }

    public String getpClave() {
        return pClave;
    }

    public void setpClave(String pClave) {
        this.pClave = pClave;
    }

    public String getpConsecutivo() {
        return pConsecutivo;
    }

    public void setpConsecutivo(String pConsecutivo) {
        this.pConsecutivo = pConsecutivo;
    }

    public String getpMonto() {
        return pMonto;
    }

    public void setpMonto(String pMonto) {
        this.pMonto = pMonto;
    }
    
}
