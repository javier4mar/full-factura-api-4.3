/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class DocumentoAdd {
    private String pConsecutivo;
    private String pFechaEmision;
    private String pIdReceptorEmp;
    private String pNombreReceptor;
    private String pTipoIdenReceptorEmp;
    private String pNumeroIdenReceptor;
    private String pEsReceptor;
    private String pCondicionVentaEmp;
    private String pPlazoCredito;
    private List<MedioPagoAdd> pMediosPagoEmp;
    private List<LineaDetalleAdd> pLineasDetalle;
    private List<InformacionReferenciaAdd> pInformacionReferencia;
    private String pOtroTexto;
    private String pOtroContenido;
    private String pTipoDocumentoEmp;
    private String pIdSucursalEmp;
    private String pIdPdvEmp;
    private String pSituacionDoc;
    private String pIdMonedaEmp;
    private String pTipoCambio;
    private String pIdDocumentoEmp;
    private List<String> pCorreoEnvioFac;
    private String pEnviaFactura;
    private String pEstadoDocumento;
    
    public String getpConsecutivo() {
        return pConsecutivo;
    }

    public void setpConsecutivo(String pConsecutivo) {
        this.pConsecutivo = pConsecutivo;
    }
    
    public String getpFechaEmision() {
        return pFechaEmision;
    }

    public void setpFechaEmision(String pFechaEmision) {
        this.pFechaEmision = pFechaEmision;
    }

    public String getpIdReceptorEmp() {
        return pIdReceptorEmp;
    }

    public void setpIdReceptorEmp(String pIdReceptorEmp) {
        this.pIdReceptorEmp = pIdReceptorEmp;
    }

    public String getpNombreReceptor() {
        return pNombreReceptor;
    }

    public void setpNombreReceptor(String pNombreReceptor) {
        this.pNombreReceptor = pNombreReceptor;
    }

    public String getpTipoIdenReceptorEmp() {
        return pTipoIdenReceptorEmp;
    }

    public void setpTipoIdenReceptorEmp(String pTipoIdenReceptorEmp) {
        this.pTipoIdenReceptorEmp = pTipoIdenReceptorEmp;
    }

    public String getpNumeroIdenReceptor() {
        return pNumeroIdenReceptor;
    }

    public void setpNumeroIdenReceptor(String pNumeroIdenReceptor) {
        this.pNumeroIdenReceptor = pNumeroIdenReceptor;
    }

    public String getpEsReceptor() {
        return pEsReceptor;
    }

    public void setpEsReceptor(String pEsReceptor) {
        this.pEsReceptor = pEsReceptor;
    }
    
    public String getpPlazoCredito() {
        return pPlazoCredito;
    }

    public void setpPlazoCredito(String pPlazoCredito) {
        this.pPlazoCredito = pPlazoCredito;
    }

    public List<LineaDetalleAdd> getpLineasDetalle() {
        return pLineasDetalle;
    }

    public void setpLineasDetalle(List<LineaDetalleAdd> pLineasDetalle) {
        this.pLineasDetalle = pLineasDetalle;
    }

    public List<InformacionReferenciaAdd> getpInformacionReferencia() {
        return pInformacionReferencia;
    }

    public void setpInformacionReferencia(List<InformacionReferenciaAdd> pInformacionReferencia) {
        this.pInformacionReferencia = pInformacionReferencia;
    }

    public String getpOtroTexto() {
        return pOtroTexto;
    }

    public void setpOtroTexto(String pOtroTexto) {
        this.pOtroTexto = pOtroTexto;
    }

    public String getpOtroContenido() {
        return pOtroContenido;
    }

    public void setpOtroContenido(String pOtroContenido) {
        this.pOtroContenido = pOtroContenido;
    }

    public String getpTipoCambio() {
        return pTipoCambio;
    }

    public void setpTipoCambio(String pTipoCambio) {
        this.pTipoCambio = pTipoCambio;
    }

    public String getpSituacionDoc() {
        return pSituacionDoc;
    }

    public void setpSituacionDoc(String pSituacionDoc) {
        this.pSituacionDoc = pSituacionDoc;
    }

    public String getpCondicionVentaEmp() {
        return pCondicionVentaEmp;
    }

    public void setpCondicionVentaEmp(String pCondicionVentaEmp) {
        this.pCondicionVentaEmp = pCondicionVentaEmp;
    }
    
    public String getpTipoDocumentoEmp() {
        return pTipoDocumentoEmp;
    }

    public void setpTipoDocumentoEmp(String pTipoDocumentoEmp) {
        this.pTipoDocumentoEmp = pTipoDocumentoEmp;
    }

    public String getpIdSucursalEmp() {
        return pIdSucursalEmp;
    }

    public void setpIdSucursalEmp(String pIdSucursalEmp) {
        this.pIdSucursalEmp = pIdSucursalEmp;
    }

    public String getpIdPdvEmp() {
        return pIdPdvEmp;
    }

    public void setpIdPdvEmp(String pIdPdvEmp) {
        this.pIdPdvEmp = pIdPdvEmp;
    }

    public String getpIdMonedaEmp() {
        return pIdMonedaEmp;
    }

    public void setpIdMonedaEmp(String pIdMonedaEmp) {
        this.pIdMonedaEmp = pIdMonedaEmp;
    }

    public String getpIdDocumentoEmp() {
        return pIdDocumentoEmp;
    }

    public void setpIdDocumentoEmp(String pIdDocumentoEmp) {
        this.pIdDocumentoEmp = pIdDocumentoEmp;
    }

    public List<String> getpCorreoEnvioFac() {
        return pCorreoEnvioFac;
    }

    public void setpCorreoEnvioFac(List<String> pCorreoEnvioFac) {
        this.pCorreoEnvioFac = pCorreoEnvioFac;
    }

    public List<MedioPagoAdd> getpMediosPagoEmp() {
        return pMediosPagoEmp;
    }

    public void setpMediosPagoEmp(List<MedioPagoAdd> pMediosPagoEmp) {
        this.pMediosPagoEmp = pMediosPagoEmp;
    }

    public String getpEnviaFactura() {
        return pEnviaFactura;
    }

    public void setpEnviaFactura(String pEnviaFactura) {
        this.pEnviaFactura = pEnviaFactura;
    }

    public String getpEstadoDocumento() {
        return pEstadoDocumento;
    }

    public void setpEstadoDocumento(String pEstadoDocumento) {
        this.pEstadoDocumento = pEstadoDocumento;
    }
    
}
