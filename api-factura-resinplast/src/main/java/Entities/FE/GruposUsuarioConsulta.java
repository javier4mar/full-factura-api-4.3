/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.FE;

/**
 *
 * @author msalasch
 */
public class GruposUsuarioConsulta extends IdDescripcion {
    private String pEstado;
    private String pEstadoDesc;

    public String getpEstado() {
        return pEstado;
    }

    public void setpEstado(String pEstado) {
        this.pEstado = pEstado;
    }

    public String getpEstadoDesc() {
        return pEstadoDesc;
    }

    public void setpEstadoDesc(String pEstadoDesc) {
        this.pEstadoDesc = pEstadoDesc;
    }
    
    
}
