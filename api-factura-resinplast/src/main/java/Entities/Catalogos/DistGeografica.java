/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Catalogos;

import java.util.List;

/**
 *
 * @author Administrador
 */
public class DistGeografica {
    private String idDistGeo;
    private String idDistGeoPertenece;
    private String nivel;
    private String nombre;
    private String codigo;
    private List<String> codigosPerteneceEmp;
    private List<String> codigosEmp;

    public String getIdDistGeo() {
        return idDistGeo;
    }

    public void setIdDistGeo(String idDistGeo) {
        this.idDistGeo = idDistGeo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<String> getCodigosEmp() {
        return codigosEmp;
    }

    public void setCodigosEmp(List<String> codigosEmp) {
        this.codigosEmp = codigosEmp;
    }

    public List<String> getCodigosPerteneceEmp() {
        return codigosPerteneceEmp;
    }

    public void setCodigosPerteneceEmp(List<String> codigosPerteneceEmp) {
        this.codigosPerteneceEmp = codigosPerteneceEmp;
    }

    public String getIdDistGeoPertenece() {
        return idDistGeoPertenece;
    }

    public void setIdDistGeoPertenece(String idDistGeoPertenece) {
        this.idDistGeoPertenece = idDistGeoPertenece;
    }

}
