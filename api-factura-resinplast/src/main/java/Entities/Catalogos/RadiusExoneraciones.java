/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.Catalogos;

/**
 *
 * @author javierhernandez
 */
public class RadiusExoneraciones {
    
    private String id;
    private String CodCliente;
    private String CodImpuesto;
    private String CedCliente;
    private String RazonSocial;
    private String NombreComercial;
    private String NumExo;
    private String PorcImpuesto;
    private String BaseImpuesto;
    private String PorcExo;
    private String FechaDesde;
    private String FechaHasta;
    private String EnteEmisor;
    private String UltimaModificacion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodCliente() {
        return CodCliente;
    }

    public void setCodCliente(String CodCliente) {
        this.CodCliente = CodCliente;
    }

    public String getCodImpuesto() {
        return CodImpuesto;
    }

    public void setCodImpuesto(String CodImpuesto) {
        this.CodImpuesto = CodImpuesto;
    }

    public String getNumExo() {
        return NumExo;
    }

    public void setNumExo(String NumExo) {
        this.NumExo = NumExo;
    }

    public String getPorcImpuesto() {
        return PorcImpuesto;
    }

    public void setPorcImpuesto(String PorcImpuesto) {
        this.PorcImpuesto = PorcImpuesto;
    }

    public String getBaseImpuesto() {
        return BaseImpuesto;
    }

    public void setBaseImpuesto(String BaseImpuesto) {
        this.BaseImpuesto = BaseImpuesto;
    }

    public String getPorcExo() {
        return PorcExo;
    }

    public void setPorcExo(String PorcExo) {
        this.PorcExo = PorcExo;
    }

    public String getFechaDesde() {
        return FechaDesde;
    }

    public void setFechaDesde(String FechaDesde) {
        this.FechaDesde = FechaDesde;
    }

    public String getFechaHasta() {
        return FechaHasta;
    }

    public void setFechaHasta(String FechaHasta) {
        this.FechaHasta = FechaHasta;
    }

    public String getEnteEmisor() {
        return EnteEmisor;
    }

    public void setEnteEmisor(String EnteEmisor) {
        this.EnteEmisor = EnteEmisor;
    }

    public String getUltimaModificacion() {
        return UltimaModificacion;
    }

    public void setUltimaModificacion(String UltimaModificacion) {
        this.UltimaModificacion = UltimaModificacion;
    }

    public String getCedCliente() {
        return CedCliente;
    }

    public void setCedCliente(String CedCliente) {
        this.CedCliente = CedCliente;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String RazonSocial) {
        this.RazonSocial = RazonSocial;
    }

    public String getNombreComercial() {
        return NombreComercial;
    }

    public void setNombreComercial(String NombreComercial) {
        this.NombreComercial = NombreComercial;
    }

           
}
