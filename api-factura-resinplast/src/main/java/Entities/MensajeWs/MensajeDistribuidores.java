/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.DatosDistribuidor;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeDistribuidores extends MensajeBase {
    private List<DatosDistribuidor> distribuidores;

    public List<DatosDistribuidor> getDistribuidores() {
        return distribuidores;
    }

    public void setDistribuidores(List<DatosDistribuidor> distribuidores) {
        this.distribuidores = distribuidores;
    }
    
}
