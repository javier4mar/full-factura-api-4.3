/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.InteresadoDocumento;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeInteresados extends MensajeBase {
    private List<InteresadoDocumento> interesadoDocumento;

    public List<InteresadoDocumento> getListInteresados() {
        return interesadoDocumento;
    }

    public void setInteresadosDocumento(List<InteresadoDocumento> interesados) {
        this.interesadoDocumento = interesados;
    }
    
}
