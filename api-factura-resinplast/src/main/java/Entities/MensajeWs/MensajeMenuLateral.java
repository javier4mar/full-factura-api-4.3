/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.OpcionMenu;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeMenuLateral extends MensajeBase {
    private List<OpcionMenu> opciones;

    public List<OpcionMenu> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionMenu> opciones) {
        this.opciones = opciones;
    }

}
