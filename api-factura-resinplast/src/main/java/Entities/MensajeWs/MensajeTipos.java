/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.CodDescripcion;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeTipos extends MensajeBase {
    List<CodDescripcion> tipos;

    public List<CodDescripcion> getTipos() {
        return tipos;
    }

    public void setTipos(List<CodDescripcion> tipos) {
        this.tipos = tipos;
    }

}
