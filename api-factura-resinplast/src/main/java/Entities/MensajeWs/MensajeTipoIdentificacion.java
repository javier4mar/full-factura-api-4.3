/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.Identificacion;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeTipoIdentificacion extends MensajeBase {
    List<Identificacion> tipos;

    public List<Identificacion> getTipos() {
        return tipos;
    }

    public void setTipos(List<Identificacion> tipos) {
        this.tipos = tipos;
    }

}
