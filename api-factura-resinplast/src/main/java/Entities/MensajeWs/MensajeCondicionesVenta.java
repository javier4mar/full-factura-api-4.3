/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.CodDescripcion;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeCondicionesVenta extends MensajeBase {
    List<CodDescripcion> condicionesVenta;

    public List<CodDescripcion> getCondicionesVenta() {
        return condicionesVenta;
    }

    public void setCondicionesVenta(List<CodDescripcion> condicionesVenta) {
        this.condicionesVenta = condicionesVenta;
    }

    
}
