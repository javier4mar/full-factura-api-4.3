/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.Catalogos.CodDescripcion;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeUnidadesMedida extends MensajeBase {
    List<CodDescripcion> unidadesMedida;

    public List<CodDescripcion> getUnidadesMedida() {
        return unidadesMedida;
    }

    public void setUnidadesMedida(List<CodDescripcion> unidadesMedida) {
        this.unidadesMedida = unidadesMedida;
    }

}
