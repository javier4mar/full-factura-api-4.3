/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.IdDescripcion;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeCatalogoFactura extends MensajeBase  {
    private List<IdDescripcion> pLista;

    public List<IdDescripcion> getpLista() {
        return pLista;
    }

    public void setpLista(List<IdDescripcion> pLista) {
        this.pLista = pLista;
    }
    
}
