/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.DepositoTransf;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajeDepositoTransf extends MensajeBase {
    private List<DepositoTransf> transacciones;

    public List<DepositoTransf> getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(List<DepositoTransf> transacciones) {
        this.transacciones = transacciones;
    }
    
}
