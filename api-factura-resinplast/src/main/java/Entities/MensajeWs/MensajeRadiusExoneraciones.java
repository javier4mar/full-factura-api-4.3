package Entities.MensajeWs;

import Entities.Catalogos.RadiusExoneraciones;
import java.util.List;


public class MensajeRadiusExoneraciones extends MensajeBase  {
    private List<RadiusExoneraciones> pLista;

    public List<RadiusExoneraciones> getpLista() {
        return pLista;
    }

    public void setpLista(List<RadiusExoneraciones> pLista) {
        this.pLista = pLista;
    }
}
