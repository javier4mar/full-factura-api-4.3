/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.Pags;
import java.util.List;

/**
 *
 * @author msalasch
 */
public class MensajePags extends MensajeBase {
    private List<Pags> paginas;

    public List<Pags> getPaginas() {
        return paginas;
    }

    public void setPaginas(List<Pags> paginas) {
        this.paginas = paginas;
    }
    
}
