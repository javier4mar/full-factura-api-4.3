/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities.MensajeWs;

import Entities.FE.MediosPago;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class MensajeMediosPago extends MensajeBase {
    List<MediosPago> mediosPago;

    public List<MediosPago> getMediosPago() {
        return mediosPago;
    }

    public void setMediosPago(List<MediosPago> mediosPago) {
        this.mediosPago = mediosPago;
    }

}
