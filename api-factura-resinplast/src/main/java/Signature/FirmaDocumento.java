/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Signature;

import Constants.Constantes;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import xades4j.UnsupportedAlgorithmException;
import xades4j.algorithms.Algorithm;
import xades4j.algorithms.EnvelopedSignatureTransform;
import xades4j.algorithms.GenericAlgorithm;
import xades4j.production.DataObjectReference;
import xades4j.production.SignedDataObjects;
import xades4j.production.XadesEpesSigningProfile;
import xades4j.production.XadesSigner;
import xades4j.production.XadesSigningProfile;
import xades4j.properties.DataObjectDesc;
import xades4j.properties.DataObjectFormatProperty;
import xades4j.properties.IdentifierType;
import xades4j.properties.ObjectIdentifier;
import xades4j.properties.SignaturePolicyBase;
import xades4j.properties.SignaturePolicyIdentifierProperty;
import xades4j.properties.SignerRoleProperty;
import xades4j.properties.SigningTimeProperty;
import xades4j.providers.AlgorithmsProviderEx;
import xades4j.providers.BasicSignatureOptionsProvider;
import xades4j.providers.KeyingDataProvider;
import xades4j.providers.SignaturePolicyInfoProvider;
import xades4j.providers.SignaturePropertiesCollector;
import xades4j.providers.SignaturePropertiesProvider;
import xades4j.providers.impl.FileSystemKeyStoreKeyingDataProvider;

/**
 *
 * @author msalasch
 */
public class FirmaDocumento {
    public String firmarXML(String pNombreCertificado, String pPasswordCertificado, String pXml) {
        String xmlFirmado = null;
        try {
            //Se convierte el String a Documento
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(pXml.getBytes(StandardCharsets.UTF_8))); 
            
            Element elemToSign = doc.getDocumentElement();
            
            SignaturePolicyInfoProvider policyInfoProvider = new SignaturePolicyInfoProvider() {
                @Override
                public SignaturePolicyBase getSignaturePolicy() {
                    return new SignaturePolicyIdentifierProperty(
                            new ObjectIdentifier("https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.2/ResolucionComprobantesElectronicosDGT-R-48-2016_4.2.pdf", IdentifierType.OIDAsURI),
                            new ByteArrayInputStream("Politica de Factura Digital".getBytes()));
                }
            };
            
            AlgorithmsProviderEx mAlgorithmsProviderEx = new AlgorithmsProviderEx(){
                @Override
                public Algorithm getSignatureAlgorithm(String string) throws UnsupportedAlgorithmException {
                       return new GenericAlgorithm(XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA256);
                }
                
                @Override
                public Algorithm getCanonicalizationAlgorithmForSignature() {
                        return new GenericAlgorithm(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS);
                }

                @Override
                public Algorithm getCanonicalizationAlgorithmForTimeStampProperties() {
                        return new GenericAlgorithm(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS);
                }

                @Override
                public String getDigestAlgorithmForDataObjsReferences() {
                        return "http://www.w3.org/2001/04/xmlenc#sha256";
                }

                @Override
                public String getDigestAlgorithmForReferenceProperties() {
                         return Constants.ALGO_ID_DIGEST_SHA1;
                }

                @Override
                public String getDigestAlgorithmForTimeStampProperties() {
                        return "http://www.w3.org/2001/04/xmlenc#sha256";
                }
            };

            BasicSignatureOptionsProvider mBasicSignatureOptionsProvider = new BasicSignatureOptionsProvider() {
                @Override
                public boolean includeSigningCertificate() {
                    return true; //To change body of generated methods, choose Tools | Templates.
                }
                @Override
                public boolean includePublicKey() {
                    return true;
                }
                @Override
                public boolean signSigningCertificate() {
                    return true;
                }
            };
            
//            SignaturePropertiesProvider mSignaturePropertiesProvider = new SignaturePropertiesProvider() {
//                @Override
//                public void provideProperties(SignaturePropertiesCollector signedPropsCol) {
//                    signedPropsCol.setSignerRole(new SignerRoleProperty("emisor"));
//                    signedPropsCol.setSigningTime(new SigningTimeProperty(cal));
//                }
//            };
            
            KeyingDataProvider kp = new FileSystemKeyStoreKeyingDataProvider(
                       Constantes.KEY_STORE, 
                       pNombreCertificado,
                       new FirstCertificateSelector(), 
                       new DirectPasswordProvider(pPasswordCertificado),
                       new DirectPasswordProvider(pPasswordCertificado), 
                       false);
            
            DataObjectDesc obj1 = new DataObjectReference("").withDataObjectFormat(new DataObjectFormatProperty("text/xml", "utf-8")).withTransform(new EnvelopedSignatureTransform());
            SignedDataObjects dataObjs = new SignedDataObjects(obj1);

            XadesSigner signer = new XadesEpesSigningProfile(kp, policyInfoProvider).withBasicSignatureOptionsProvider(mBasicSignatureOptionsProvider).withAlgorithmsProviderEx(mAlgorithmsProviderEx).newSigner(); //.withSignaturePropertiesProvider(mSignaturePropertiesProvider)
            signer.sign(dataObjs, elemToSign);
            
            StringWriter sw = new StringWriter();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(elemToSign), new StreamResult(sw));

            xmlFirmado = sw.toString();
            
        } catch (Exception e) {
            return null;
        }catch (Throwable t) {
             return null;
        }
        
        return xmlFirmado;
    }
}
