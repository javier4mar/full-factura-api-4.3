/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 *
 * @author Administrador
 */
public class Querys {
    
    public static final String DATOS_TOKEN = "SELECT \n" +
                "	ID_EMPRESA,\n" +
                "	EMPRESA,\n" +
                "	RAZON_SOCIAL,\n" +
                "	NOMBRE_COMERCIAL,\n" +
                "	CORREO_ADM_FE,\n" +
                "	IDEN_INGRESO,\n" +
                "	CLAVE_INGRESO,\n" +
                "	TIENE_HOMOGEN,\n" +
                "	TOKEN_WS_EXP,\n" +
                "	RUTA_CERTIFICADO,\n" +
                "	PIN_CERTIFICADO,\n" +
                "	URL_CALLBACK,\n" +
                "	TIPO_IDENTIFICACION,\n" +
                "	NUM_IDENTIFICACION  \n" +
                "FROM \n" +
                "	EFE_EMPRESAS \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? ";
    
    public static final String PR_ADD_EMPRESA = "{CALL PR_ADD_EMPRESA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_EMPRESA = "{CALL PR_UPDATE_EMPRESA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_EMPRESA_REF = "{CALL PR_UPDATE_EMPRESA_REF(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_RECEPTOR = "{CALL PR_ADD_RECEPTOR(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_RECEPTOR = "{CALL PR_UPDATE_RECEPTOR(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String ACTUALIZA_RUTA_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET RUTA_XML = ? WHERE ID_DOCUMENTO = ? ";
    
    public static final String CLAVE_DOCUMENTO = "SELECT \n" +
                "	CLAVENUMERICA \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND ID_DOCUMENTO = ? ";
    
    public static final String TIPO_DOCUMENTO = "SELECT \n" +
                "	CODIGOTIPODOC \n" +
                "FROM \n" +
                "	EFE_TIPOS_DOCUMENTOS_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOTIPODOC_EMPRESA = ? ";
    
    public static final String RUTA_XML = "SELECT \n" +
                "	RUTA_XML,\n" +
                "	CLAVENUMERICA \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND ID_DOCUMENTO = ? ";
    
    public static final String TOKEN_HACIENDA = "SELECT \n" +
                "	ULT_TOKEN, \n" +
                "	FECHA_HORA_TOKEN \n" +
                "FROM \n" +
                "	EFE_EMPRESAS \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? ";
    
    public static final String UPDATE_TOKEN = "UPDATE EFE_EMPRESAS SET ULT_TOKEN = ?, FECHA_HORA_TOKEN = ? WHERE ID_EMPRESA = ?";

    public static final String GET_XML = "{CALL PR_GENERA_XML_FACTURA(?, ?)}";
    
    public static final String GET_VALOR1_PARAMETROS = "SELECT \n" +
                "	VALOR_1 \n" +
                "FROM \n" +
                "	EFE_PARAMETROS \n" +
                "WHERE \n" +
                "	ID_PARAMETRO = ?";
    
    public static final String GET_UNIDADES_MEDIDA = "SELECT \n" +
                "	CODIGOMEDIDA, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOMEDIDA_EMPRESA) FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIDA = EFE_CODIGOSMEDIDA.CODIGOMEDIDA) \n" +
                "FROM \n" +
                "	EFE_CODIGOSMEDIDA ";
    
    public static final String GET_CONDICIONES_VENTA = "SELECT \n" +
                "	CODIGOCONDICION, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOCONDICION_EMPRESA) FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_EMPRESA = ? AND CODIGOCONDICION = EFE_CONDICIONESVENTAS.CODIGOCONDICION) \n" +
                "FROM \n" +
                "	EFE_CONDICIONESVENTAS ";
    
    public static final String GET_DIST_GEO = "SELECT \n" +
                "	ID_DIST_GEO, NIVEL, NOMBRE, CODIGO,\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = EFE_DIST_GEOGRAFICA.ID_DIST_GEO_PERTENECE), \n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = EFE_DIST_GEOGRAFICA.ID_DIST_GEO), \n" +
                "	ID_DIST_GEO_PERTENECE \n" +
                "FROM \n" +
                "	EFE_DIST_GEOGRAFICA ";

    public static final String GET_MEDIOS_PAGO = "SELECT \n" +
                "	CODIGOMEDIOPAGO, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOMEDIOPAGO_EMP) FROM EFE_MEDIOSPAGO_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIOPAGO = EFE_MEDIOSPAGO.CODIGOMEDIOPAGO)," +
                "       COMPROBANTE_REQUERIDO \n" +
                "FROM \n" +
                "	EFE_MEDIOSPAGO ";
    
    public static final String GET_IMPUESTOS = "SELECT \n" +
                "	CODIGOIMPUESTO, DESCRIPCION, PORCENTAJE_IMP, EXCEPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOIMPUESTO_EMPRESA) FROM EFE_IMPUESTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOIMPUESTO = EFE_IMPUESTOS.CODIGOIMPUESTO) \n" +
                "FROM \n" +
                "	EFE_IMPUESTOS ";
    
    public static final String GET_MONEDAS = "SELECT \n" +
                "	CODIGOMONEDA, DESCRIPCION, SIMBOLO,\n" +
                "	(SELECT GROUP_CONCAT(COD_MONEDA_EMPRESA) FROM EFE_MONEDAS_EMP WHERE ID_EMPRESA = ? AND CODIGOMONEDA = EFE_MONEDAS.CODIGOMONEDA) \n" +
                "FROM \n" +
                "	EFE_MONEDAS ";
    
    public static final String GET_TIPOS_DOCUMENTO = "SELECT \n" +
                "	CODIGOTIPODOC, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOTIPODOC_EMPRESA) FROM EFE_TIPOS_DOCUMENTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPODOC = EFE_TIPOS_DOCUMENTOS.CODIGOTIPODOC) \n" +
                "FROM \n" +
                "	EFE_TIPOS_DOCUMENTOS ";
    
    public static final String GET_TIPOS_EXONERACION = "SELECT \n" +
                "	CODIGOTIPOEXO, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(CODIGOTIPOEXO_EMPRESA) FROM EFE_TIPOS_EXON_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPOEXO = EFE_TIPOS_EXON.CODIGOTIPOEXO) \n" +
                "FROM \n" +
                "	EFE_TIPOS_EXON ";
    
    public static final String GET_TIPOS_IDENTIFICACION = "SELECT \n" +
                "	TIPO_IDENT, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE ID_EMPRESA = ? AND TIPO_IDEN = EFE_TIPOS_IDEN.TIPO_IDENT), \n" +
                "	LONGITUD \n" +
                "FROM \n" +
                "	EFE_TIPOS_IDEN ";
    
    public static final String GET_TIPOS_CODIGO_ART = "SELECT \n" +
                "	TIPOCODIGO, DESCRIPCION,\n" +
                "	(SELECT GROUP_CONCAT(TIPOCODIGO_EMPRESA) FROM EFE_TIPOSCODIGOART_EMP WHERE ID_EMPRESA = ? AND TIPOCODIGO = EFE_TIPOSCODIGOART.TIPOCODIGO) \n" +
                "FROM \n" +
                "	EFE_TIPOSCODIGOART ";
    
    public static final String GET_EMPRESAS = "SELECT \n" +
                "	ID_EMPRESA,\n" +
                "	RAZON_SOCIAL \n" +
                "FROM \n" +
                "	EFE_EMPRESAS ";
    
    public static final String PR_ADD_UNIDAD_MEDIDA_EMP = "{CALL PR_ADD_UNIDAD_MEDIDA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_CONDICION_VENTA_EMP = "{CALL PR_ADD_CONDICION_VENTA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_DIST_GEO_EMP = "{CALL PR_ADD_DIST_GEO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_IMPUESTO_EMP = "{CALL PR_ADD_IMPUESTO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_MEDIO_PAGO_EMP = "{CALL PR_ADD_MEDIO_PAGO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_MONEDA_EMP = "{CALL PR_ADD_MONEDA_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_DOCUMENTO_EMP = "{CALL PR_ADD_TIPO_DOCUMENTO_EMP(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_EXONERACION_EMP = "{CALL PR_ADD_TIPO_EXONERACION_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_IDENTIFICACION_EMP = "{CALL PR_ADD_TIPO_IDENTIFICACION_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_COD_ARTICULO_EMP = "{CALL PR_ADD_TIPO_COD_ARTICULO_EMP(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_DOC_REF_EMP = "{CALL PR_ADD_TIPO_DOC_REF_EMP(?, ?, ?, ?, ?)}";
    
    public static final String DELETE_TIPO_DOC_REF_EMP = "DELETE FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND CODIGO = ? AND "
                + " CODIGO_EMP = ?  ";
    
    public static final String DELETE_UNIDAD_MEDIDA_EMP = "DELETE FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIDA = ? AND "
                + " CODIGOMEDIDA_EMPRESA = ?  ";
    
    public static final String DELETE_CONDICION_VENTA_EMP = "DELETE FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_EMPRESA = ? AND CODIGOCONDICION = ? AND "
                + " CODIGOCONDICION_EMPRESA = ?  ";
    
    public static final String DELETE_DIST_GEO_EMP = "DELETE FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_EMPRESA = ? AND ID_DIST_GEO = ? AND "
                + " CODIGO_LUGAR_EMP = ? ";
    
    public static final String DELETE_IMPUESTO_EMP = "DELETE FROM EFE_IMPUESTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOIMPUESTO = ? AND "
                + " CODIGOIMPUESTO_EMPRESA = ? ";
    
    public static final String DELETE_MEDIO_PAGO_EMP = "DELETE FROM EFE_MEDIOSPAGO_EMP WHERE ID_EMPRESA = ? AND CODIGOMEDIOPAGO = ? AND "
                + " CODIGOMEDIOPAGO_EMP = ? ";
    
    public static final String DELETE_MONEDA_EMP = "DELETE FROM EFE_MONEDAS_EMP WHERE ID_EMPRESA = ? AND CODIGOMONEDA = ? AND "
                + " COD_MONEDA_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_DOCUMENTO_EMP = "DELETE FROM EFE_TIPOS_DOCUMENTOS_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPODOC = ? AND "
                + " CODIGOTIPODOC_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_EXONERACION_EMP = "DELETE FROM EFE_TIPOS_EXON_EMP WHERE ID_EMPRESA = ? AND CODIGOTIPOEXO = ? AND "
                + " CODIGOTIPOEXO_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_IDENTIFICACION_EMP = "DELETE FROM EFE_TIPOS_IDEN_EMP WHERE ID_EMPRESA = ? AND TIPO_IDEN = ? AND "
                + " TIPO_IDEN_EMPRESA = ? ";
    
    public static final String DELETE_TIPO_COD_ARTICULO_EMP = "DELETE FROM EFE_TIPOSCODIGOART_EMP WHERE ID_EMPRESA = ? AND TIPOCODIGO = ? AND "
                + " TIPOCODIGO_EMPRESA = ? ";
    
    public static final String LOGIN = "SELECT \n" +
                "	SEG_USUARIOS.ID_USUARIO,\n" +
                "	SEG_USUARIOS.DS_DESCRIPCION,\n" +
                "	SEG_USUARIOS.ID_EMPRESA_DEFAULT,\n" +
                "	IFNULL((SELECT 1 FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = SEG_USUARIOS.ID_USUARIO AND ID_GRUPO_SEGURIDAD = 1),'0'), \n" +   
                "	SEG_USUARIOS.ST_USUARIO \n" +
                "FROM \n" +
                "	SEG_USUARIOS WHERE SEG_USUARIOS.DS_USUARIO = ? AND SEG_USUARIOS.DS_USUARIO_PASSWORD = ? ";
    
    public static final String ACTUALIZA_EMPRESA_DEFAULT = "UPDATE SEG_USUARIOS SET ID_EMPRESA_DEFAULT = ? WHERE ID_USUARIO = ? AND ? IN (SELECT ID_EMPRESA FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ?)";
    
    public static final String ACTUALIZA_GRUPO_DEFAULT = "UPDATE SEG_USUARIOS SET ID_GRUPO_SEGURIDAD_DEFAULT = ? WHERE ID_USUARIO = ? AND ? IN (SELECT ID_GRUPO_SEGURIDAD FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_EMPRESA = ?)";
    
    public static final String GRUPOS_USUARIO = "SELECT \n" +
                "	SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD, \n" +
                "	(SELECT SEG_GRUPOS.DS_GRUPO_SEGURIDAD FROM SEG_GRUPOS WHERE SEG_GRUPOS.ID_GRUPO_SEGURIDAD = SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD) " +
                "FROM \n" +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE SEG_USUARIOS_EMP_GRUPOS.ID_USUARIO = ? AND SEG_USUARIOS_EMP_GRUPOS.ID_EMPRESA = ?";
    
    public static final String GET_PAGINAS_USUARIO = "SELECT \n" +
                "	SEG_GRUPOS_PAGINAS.ID_PAGINA, \n" +
                "	(SELECT SEG_PAGINAS.DS_PAGINA FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA)," +
                "	(SELECT SEG_PAGINAS.URL FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA) " +
                "FROM \n" +
                "	SEG_GRUPOS_PAGINAS WHERE SEG_GRUPOS_PAGINAS.ID_GRUPO_SEGURIDAD = ?";
    
    public static final String GET_PAGINAS_PREFERENCIAS = "SELECT \n" +
                "	SEG_GRUPOS_PAGINAS.ID_PAGINA, \n" +
                "	(SELECT CONCAT((SELECT DS_PAGINA FROM SEG_PAGINAS PAGS WHERE SEG_PAGINAS.ID_PADRE = PAGS.ID_PAGINA), ' - ', SEG_PAGINAS.DS_PAGINA) FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA),\n" +
                "	(SELECT SEG_PAGINAS.URL FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA)\n" +
                "FROM \n" +
                "	SEG_GRUPOS_PAGINAS WHERE SEG_GRUPOS_PAGINAS.ID_GRUPO_SEGURIDAD = ? AND 1 = (SELECT SEG_PAGINAS.PREFERENCIA FROM SEG_PAGINAS WHERE SEG_PAGINAS.ID_PAGINA = SEG_GRUPOS_PAGINAS.ID_PAGINA)\n" +
                "ORDER BY 2 ASC";
    
    public static final String GET_MENU_LATERAL = "SELECT  CONCAT(REPEAT('    ', level - 1), CAST(hi.id AS CHAR)) AS treeitem, \n" +
"hi.id as pagina, hi.DS_PAGINA, hi.URL, parent, hi.level, hi.ICON, hi.COLOR, hi.SECUENCIA\n" +
"FROM    (\n" +
"        SELECT  hierarchy_connect_by_parent_eq_prior_id(id) AS id, @level \n" +
"        FROM    (\n" +
"                SELECT  @start_with := 0,\n" +
"                        @id := @start_with,\n" +
"                        @level := 0\n" +
"                ) vars, t_hierarchy \n" +
"        WHERE   @id IS NOT NULL\n" +
"	\n" +
"        ) ho \n" +
"JOIN    t_hierarchy hi \n" +
"ON      hi.id = ho.id AND hi.id in (SELECT  sgp.ID_PAGINA FROM SEG_GRUPOS_PAGINAS AS sgp WHERE sgp.ID_GRUPO_SEGURIDAD = ?)";
    
    
    public static final String GET_USUARIOS = "select ID_USUARIO, DS_USUARIO, DS_DESCRIPCION,\n" +
"ID_EMPRESA_DEFAULT, ST_USUARIO FROM SEG_USUARIOS WHERE ID_USUARIO = IF(? = 0, ID_USUARIO, ?)";
    
    public static final String GET_RECEPTORES = "SELECT \n" +
                "	EFE_CLIENTES.RAZON_SOCIAL,\n" +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_CLIENTES.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA),\n" +
                "	EFE_CLIENTES.NUM_IDENTIFICACION,\n" +
                "	EFE_CLIENTES.IDENTIFICACION_EXTRANJERO,\n" +
                "	EFE_CLIENTES.NOMBRE_COMERCIAL,\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_PROVINCIA AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_CANTON AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_DISTRITO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_CLIENTES.ID_BARRIO AND ID_EMPRESA = EFE_CLIENTES.ID_EMPRESA),\n" +
                "	EFE_CLIENTES.OTRAS_SENAS,\n" +
                "	EFE_CLIENTES.COD_PAIS_TELEFONO,\n" +
                "	EFE_CLIENTES.NUM_TELEFONO,\n" +
                "	EFE_CLIENTES.COD_PAIS_FAX,\n" +
                "	EFE_CLIENTES.NUM_FAX,\n" +
                "	EFE_CLIENTES.CORREO_ELECTRONICO,\n" +
                "	EFE_CLIENTES.COD_CLIENTE_REF,\n" +
                "	EFE_CLIENTES.TIPO_TRIBUTARIO, \n" +
                "	EFE_CLIENTES.ESTADO, \n" +
                "	CASE  \n" +
                "                       WHEN EFE_CLIENTES.ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END, \n" +
                "	(SELECT DESCRIPCION FROM EFE_TIPOS_IDEN WHERE TIPO_IDENT = EFE_CLIENTES.TIPO_IDENTIFICACION), \n" +
                "	CASE  \n" +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'E' THEN \n" +
                "                               'Emisor / Receptor' \n" +
                "                       WHEN EFE_CLIENTES.TIPO_TRIBUTARIO = 'R' THEN \n" +
                "                               'Receptor no emisor' \n" +
                "                       ELSE\n" +
                "                               'No aplica' \n" +
                "                       END \n" +
                "FROM \n" +
                "	EFE_CLIENTES WHERE ID_EMPRESA = ? AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String DELETE_RECEPTOR = "DELETE FROM EFE_CLIENTES WHERE ID_EMPRESA = ? AND COD_CLIENTE_REF = ?  ";
    
    public static final String ID_DOC_EMPRESA_FE = "SELECT \n" +
                "	ID_EMPRESA, ID_DOCUMENTO, EMITIDA, DATE_FORMAT(FECHAEMISION , \"%Y-%m-%d\") as FECHAEMISION  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	CLAVENUMERICA = ? ";
    
    public static final String ACTUALIZA_ESTADO_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET ESTADO = ?, MOTIVO_RECHAZO = ?, FECHA_ULT_CONSULTA = NOW() WHERE ID_DOCUMENTO = ? ";
    
    public static final String PR_REGISTRA_DOCUMENTO = "{CALL PR_REGISTRA_DOCUMENTO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DOCUMENTO_REFERENCIA = "SELECT \n" +
                "	NUMEROCONSECUTIVO,\n" +
                "	FECHAEMISION \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE = ?";
    
    public static final String GET_IMPUESTO_FACTURA = "SELECT \n" +
                "	CODIGOIMPUESTO \n" +
                "FROM \n" +
                "	EFE_IMPUESTOS_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOIMPUESTO_EMPRESA = ?";
    
    public static final String GET_CODIGO_ART_DOC = "SELECT \n" +
                "	TIPOCODIGO \n" +
                "FROM \n" +
                "	EFE_TIPOSCODIGOART_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND TIPOCODIGO_EMPRESA = ?";
    
    public static final String GET_EXONERACION_DOCUMENTO = "SELECT \n" +
                "	CODIGOTIPOEXO \n" +
                "FROM \n" +
                "	EFE_TIPOS_EXON_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOTIPOEXO_EMPRESA = ?";
    
    public static final String GET_UNIDAD_MEDIDA_DOC = "SELECT \n" +
                "	CODIGOMEDIDA \n" +
                "FROM \n" +
                "	EFE_CODIGOSMEDIDA_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOMEDIDA_EMPRESA = ?";
    
    public static final String ADD_INFORMACION_REFERENCIA = "INSERT INTO EFE_DOCUMENTOS_REF (TIPO_COMPROBANTE_REF, DOCUMENTO_REF,"
                + " FECHAEMISION, CODIGO, RAZON, ID_DOCUMENTO) VALUES (?,?,?,?,?,?) ";
    
    public static final String ADD_INTERESADO_CORREO = "INSERT INTO EFE_INTERESADO_DOCUMENTO (CORREO, ID_DOCUMENTO) VALUES (?,?) ";
     
    public static final String ADD_DETALLE_DOCUMENTO = "insert into EFE_DOCUMENTOS_DET ( ID_DOCUMENTO, LINEA, TIPOCODIGO, CODIGO, "
                + "CANTIDAD, CODIGOMEDIDA, UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, "
                + "NATURALEZADESCUENTO, SUBTOTAL, MONTOTOTALLINEA, CODIGOMEDIDA_EMP, TIPOCODIGO_EMP, MERC_O_SERV, CANTIDAD_RESTANTE, MONTO_RESTANTE, ID_DOCUMENTO_DET_PADRE) "
                + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    
    public static final String ADD_IMPUESTO_DOCUMENTO = "insert into EFE_DOCUMENTOS_DET_IMP ( ID_DOCUMENTO_DET, CODIGOIMPUESTO, TARIFA, "
                + "MONTO, CODIGOIMPUESTO_EMPRESA, CODIGOTIPOEXO, NUMERODOCUMENTO, NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, "
                + "PORCENTAJECOMPRA, TIPO_EXON_EMPRESA) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
    
    public static final String ADD_MEDIO_PAGO_DOCUMENTO = "insert into EFE_DOCUMENTOS_MEDIO_PAGO ( ID_DOCUMENTO, CODIGOMEDIOPAGO, CODIGOMEDIOPAGO_EMP, "
                + "MONTO) values (?,?,?,?) ";
    
    public static final String GET_ARTICULOS = "SELECT ID_ARTICULO_EMP, ID_CATEGORIA, DESCRIPCION, ID_UNIDAD_MEDIDA, UNIDAD_MEDIDA_COMERCIAL,\n" +
            "TIPO_ARTICULO, PRECIO, PORCENTAJE, DESCUENTO, RECARGO, ESTADO, \n" +
            "(SELECT GROUP_CONCAT(ID_ARTICULO_RECARGO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO), \n" +
            "(SELECT GROUP_CONCAT(ID_IMPUESTO) FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_ARTICULO = EFE_ARTICULOS_EMP.ID_ARTICULO), \n" +
            "(SELECT GROUP_CONCAT(CODIGOMEDIDA_EMPRESA) FROM EFE_CODIGOSMEDIDA_EMP WHERE CODIGOMEDIDA = EFE_ARTICULOS_EMP.ID_UNIDAD_MEDIDA AND ID_EMPRESA = ?), \n" +
            "(SELECT GROUP_CONCAT(ID_ARTICULO) FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO_RECARGO = EFE_ARTICULOS_EMP.ID_ARTICULO), \n" +
            "CODIGO, \n" +
            "CASE WHEN CODIGO IS NULL THEN DESCRIPCION ELSE CONCAT(CODIGO, ' - ', DESCRIPCION) END, \n" +
            "COSTO \n" +
            "FROM EFE_ARTICULOS_EMP WHERE ID_EMPRESA = ? AND ID_ARTICULO = IF (? = 0, ID_ARTICULO, ?) AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String GET_IMPUESTOS_ART = "SELECT \n" +
            "COD_IMPUESTO_HACIENDA, \n" +
            "(SELECT DESCRIPCION FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA), \n" +
            "(SELECT PORCENTAJE_IMP FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA), \n" +
            "(SELECT EXCEPCION FROM EFE_IMPUESTOS WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA), \n" +
            "(SELECT GROUP_CONCAT(ID_EXONERACION) FROM EFE_ARTICULOS_EMP_IMP_EXO WHERE ID_IMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.ID_IMPUESTO), \n" +
            "(SELECT GROUP_CONCAT(CODIGOIMPUESTO_EMPRESA) FROM EFE_IMPUESTOS_EMP WHERE CODIGOIMPUESTO = EFE_ARTICULOS_EMP_IMPUESTOS.COD_IMPUESTO_HACIENDA AND ID_EMPRESA = ?) \n" +
            "FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_IMPUESTO = ?;";
    
    public static final String GET_EXONERACIONES = "SELECT \n" +
            "CODIGOTIPOEXO, NUMERODOCUMENTO, NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA \n" +
            "FROM EFE_EXONERACIONES_EMP WHERE ID_EMPRESA = ? AND ID_EXONERACION = IF (? = 0, ID_EXONERACION, ?);";
    
    public static final String PR_ADD_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_ARTICULO_EMP = "{CALL PR_UPDATE_ARTICULO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_RECARGO_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_RECARGO(?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_IMPUESTO_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_IMPUESTO(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_EXO_IMP_ARTICULO_EMP = "{CALL PR_ADD_ARTICULO_IMP_EXO(?, ?, ?, ?, ?)}";
    
    public static final String PR_DELETE_ARTICULO_EMP = "{CALL PR_DELETE_ARTICULO(?, ?, ?, ?)}";
    
    public static final String GET_SUCURSALES = "SELECT \n" +
                "	COD_SUCURSAL_EMP, NUM_SUCURSAL, DESC_SUCURSAL_EMP, \n" +
                "	ESTADO, \n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM \n" +
                "	EFE_SUCURSALES \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String GET_PUNTOS_VENTA = "SELECT \n" +
                "	COD_PUNTO_VENTA, NUM_PUNTO_VENTA, DESC_PUNTO_VENTA, \n" +
                "	ESTADO, \n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END, \n" +
                "	CONSECUTIVO_FACTURA, CONSECUTIVO_TIQUETE, CONSECUTIVO_NOTA_CREDITO, CONSECUTIVO_NOTA_DEBITO, CONSECUTIVO_ACEPTA, CONSECUTIVO_ACEPTA_PARCIAL, CONSECUTIVO_RECHAZA \n" +
                "FROM \n" +
                "	EFE_PUNTOS_DE_VENTA \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND ID_SUCURSALES = (SELECT ID_SUCURSALES FROM EFE_SUCURSALES WHERE ID_EMPRESA = ? AND COD_SUCURSAL_EMP = ?) AND ESTADO = IF(? = 'NA', ESTADO, ?)";
    
    public static final String PR_ADD_SUCURSAL_EMP = "{CALL PR_ADD_SUCURSAL(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_PUNTO_VENTA_EMP = "{CALL PR_ADD_PDV(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_SUCURSAL_EMP = "{CALL PR_UPDATE_SUCURSAL(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PUNTO_VENTA_EMP = "{CALL PR_UPDATE_PDV(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DOCUMENTO = "SELECT \n" +
                "	ID_DOCUMENTO ,\n" +
                "	NO_DOCUMENTO_REFE ,\n" +
                "	COD_TIPO_DOC_EMP ,\n" +
                "	CLAVENUMERICA ,\n" +
                "	NUMEROCONSECUTIVO ,\n" +
                "	CODSUCURSAL_EMP ,\n" +
                "	PUNTO_DE_VENTA ,\n" +
                "	CONDICIONVENTA_EMP ,\n" +
                "	PLAZOCREDITO ,\n" +
                "	MEDIOS_PAGO ,\n" +
                "	CODIGOMONEDA_EMP ,\n" +
                "	TIPOCAMBIO ,\n" +
                "	ESTADO ,\n" +
                "	SITUACION ,\n" +
                "	ULT_NOVEDAD ,\n" +
                "	COD_CLIENTE ,\n" +
                "	TIPO_IDEN_RECEPTOR_EMP ,\n" +
                "	IDEN_RECEPTOR ,\n" +
                "	NOMBRE_RECEPTOR ,\n" +
                "	CORREO_RECEPTOR,\n" +
                "	ES_RECEPTOR ,\n" +
                "	EMITIDA ,\n" +
                "	COD_MENSAJE_HACIENDA ,\n" +
                "	TOTALVENTANETA ,\n" +
                "	TOTALIMPUESTOS ,\n" +
                "	TOTALCOMPROBANTE ,\n" +
                "	DESC_ESTADO ,\n" +
                "	DESC_TIPO_DOCUMENTO ,\n" +
                "	FECHA_EMISION,\n" +
                "	CORREO_EMPRESA ,\n" +
                "	NOMBRE_EMPRESA ,\n" +
                "	IDEN_EMPRESA, \n" +
                "	TELEFONO_EMPRESA, \n" +
                "	RUTA_XML, \n" +	
                "	SIMBOLO_MONEDA, \n" +
                "	RUTA_LOGO ,\n" +
                "	SENAS_EMPRESA, \n" +
                "	SENAS_RECEPTOR, \n" +
                "	OBSERVACION_FACTURA, \n" +
                "	FECHA_PROCESO, \n" +
                "	DESC_MENSAJE_HAC, \n" +
                "	CONDICIONVENTA_DESC, \n" +
                "	NOMBRE_EMPRESA_COM, \n" +
                "	FAX_EMPRESA, \n" +
                "	RAZON_SOCIAL_EMP as RAZON_SOCIAL_EMP_RECEPCION, \n" +
                "	NUM_IDENTIFICACION_EMP as NUM_IDENTIFICACION_EMP_RECEPCION, \n" +
                "	CORREO_ELECTRONICO_EMP as CORREO_ELECTRONICO_EMP_RECEPCION \n" +
                "FROM\n" +
                "	VIEW_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE <=> IF(? = 'NA', NO_DOCUMENTO_REFE, ?) AND FIND_IN_SET (ESTADO,IF(? = 'NA', ESTADO, ?)) " +
                "       AND EMITIDA = IF(? = 'NA', EMITIDA, ?) AND FECHAEMISION >=  IF(? = 'NA', FECHAEMISION, ?) AND FECHAEMISION <= IF(? = 'NA', FECHAEMISION, ?) " +
                "       AND TIPO_COMPROBANTE = IF(? = 'NA', TIPO_COMPROBANTE, ?) AND IDEN_RECEPTOR = IF(? = 'NA', IDEN_RECEPTOR, ?) AND NOMBRE_RECEPTOR LIKE IF(? = 'NA', NOMBRE_RECEPTOR, ?) " +
                "       AND CLAVENUMERICA = IF(? = 'NA', CLAVENUMERICA, ?) AND NUMEROCONSECUTIVO = IF(? = 'NA', NUMEROCONSECUTIVO, ?) AND TOTALCOMPROBANTE = IF(? = 'NA', TOTALCOMPROBANTE, ?)";  
    
    public static final String GET_ENVIOS_DOCUMENTO = "SELECT \n" +
                "       EFE_INTERESADO_DOCUMENTO.CORREO, \n" +
                "	EFE_INTERESADO_DOCUMENTO.ENVIADO, \n" +
                "	EFE_INTERESADO_DOCUMENTO.FECHAENVIO \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC,\n" +
                "	EFE_INTERESADO_DOCUMENTO \n" +
                "WHERE \n" +
                "	EFE_DOCUMENTOS_ENC.CLAVENUMERICA = ? AND \n" +
                "	EFE_DOCUMENTOS_ENC.ID_EMPRESA = ? AND \n" +
                "	EFE_DOCUMENTOS_ENC.ID_DOCUMENTO = EFE_INTERESADO_DOCUMENTO.ID_DOCUMENTO"; 
        
    /* Fix Or en cantidad restante para notas de creditos parciales*/
    public static final String GET_DOCUMENTO_NC = "SELECT \n" +
                "	ID_DOCUMENTO ,\n" +
                "	NO_DOCUMENTO_REFE ,\n" +
                "	COD_TIPO_DOC_EMP ,\n" +
                "	CLAVENUMERICA ,\n" +
                "	NUMEROCONSECUTIVO ,\n" +
                "	CODSUCURSAL_EMP ,\n" +
                "	PUNTO_DE_VENTA ,\n" +
                "	CONDICIONVENTA_EMP ,\n" +
                "	PLAZOCREDITO ,\n" +
                "	MEDIOS_PAGO ,\n" +
                "	CODIGOMONEDA_EMP ,\n" +
                "	TIPOCAMBIO ,\n" +
                "	ESTADO ,\n" +
                "	SITUACION ,\n" +
                "	ULT_NOVEDAD ,\n" +
                "	COD_CLIENTE ,\n" +
                "	TIPO_IDEN_RECEPTOR_EMP ,\n" +
                "	IDEN_RECEPTOR ,\n" +
                "	NOMBRE_RECEPTOR ,\n" +
                "	CORREO_RECEPTOR,\n" +
                "	ES_RECEPTOR ,\n" +
                "	EMITIDA ,\n" +
                "	COD_MENSAJE_HACIENDA ,\n" +
                "	TOTALVENTANETA ,\n" +
                "	TOTALIMPUESTOS ,\n" +
                "	TOTALCOMPROBANTE ,\n" +
                "	DESC_ESTADO ,\n" +
                "	DESC_TIPO_DOCUMENTO ,\n" +
                "	FECHA_EMISION,\n" +
                "	CORREO_EMPRESA ,\n" +
                "	NOMBRE_EMPRESA ,\n" +
                "	IDEN_EMPRESA, \n" +
                "	TELEFONO_EMPRESA, \n" +
                "	RUTA_XML, \n" +	
                "	SIMBOLO_MONEDA, \n" +
                "	RUTA_LOGO ,\n" +
                "	SENAS_EMPRESA, \n" +
                "	SENAS_RECEPTOR, \n" +
                "	OBSERVACION_FACTURA, \n" +
                "	CONDICIONVENTA_DESC, \n" +
                "	NOMBRE_EMPRESA_COM, \n" +
                "	FAX_EMPRESA \n" +
                "FROM\n" +
                "	VIEW_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND NO_DOCUMENTO_REFE <=> IF(? = 'NA', NO_DOCUMENTO_REFE, ?) AND FIND_IN_SET (ESTADO,'AP,RE,EN') " +
                "       AND EMITIDA = 1 AND FECHAEMISION >=  IF(? = 'NA', FECHAEMISION, ?) AND FECHAEMISION <= IF(? = 'NA', FECHAEMISION, ?) " +
                "       AND IDEN_RECEPTOR = IF(? = 'NA', IDEN_RECEPTOR, ?) AND NOMBRE_RECEPTOR LIKE IF(? = 'NA', NOMBRE_RECEPTOR, ?) " +
                "       AND CLAVENUMERICA = IF(? = 'NA', CLAVENUMERICA, ?) AND NUMEROCONSECUTIVO = IF(? = 'NA', NUMEROCONSECUTIVO, ?) " +
                "       AND TOTALCOMPROBANTE = IF(? = 'NA', TOTALCOMPROBANTE, ?) AND 0 < (SELECT COUNT(ID_DOCUMENTO_DET) FROM EFE_DOCUMENTOS_DET WHERE ID_DOCUMENTO = VIEW_DOCUMENTOS_ENC.ID_DOCUMENTO AND (CANTIDAD_RESTANTE > 0 OR MONTO_RESTANTE > 0)) " +
                "       AND TIPO_COMPROBANTE <> '03'";  
    
    public static final String GET_DOCUMENTO_CORREO = "SELECT \n" +
                "	ID_DOCUMENTO ,\n" +
                "	NO_DOCUMENTO_REFE ,\n" +
                "	COD_TIPO_DOC_EMP ,\n" +
                "	CLAVENUMERICA ,\n" +
                "	NUMEROCONSECUTIVO ,\n" +
                "	CODSUCURSAL_EMP ,\n" +
                "	PUNTO_DE_VENTA ,\n" +
                "	CONDICIONVENTA_EMP ,\n" +
                "	PLAZOCREDITO ,\n" +
                "	MEDIOS_PAGO ,\n" +
                "	CODIGOMONEDA_EMP ,\n" +
                "	TIPOCAMBIO ,\n" +
                "	ESTADO ,\n" +
                "	SITUACION ,\n" +
                "	ULT_NOVEDAD ,\n" +
                "	COD_CLIENTE ,\n" +
                "	TIPO_IDEN_RECEPTOR_EMP ,\n" +
                "	IDEN_RECEPTOR ,\n" +
                "	NOMBRE_RECEPTOR ,\n" +
                "	CORREO_RECEPTOR,\n" +
                "	ES_RECEPTOR ,\n" +
                "	EMITIDA ,\n" +
                "	COD_MENSAJE_HACIENDA ,\n" +
                "	TOTALVENTANETA ,\n" +
                "	TOTALIMPUESTOS ,\n" +
                "	TOTALCOMPROBANTE ,\n" +
                "	DESC_ESTADO ,\n" +
                "	DESC_TIPO_DOCUMENTO ,\n" +
                "	FECHA_EMISION,\n" +
                "	CORREO_EMPRESA ,\n" +
                "	NOMBRE_EMPRESA ,\n" +
                "	IDEN_EMPRESA, \n" +
                "	TELEFONO_EMPRESA, \n" +
                "	RUTA_XML, \n" +	
                "	SIMBOLO_MONEDA, \n" +
                "	RUTA_LOGO ,\n" +
                "	SENAS_EMPRESA, \n" +
                "	SENAS_RECEPTOR, \n" +
                "	OBSERVACION_FACTURA, \n" +
                "	CONDICIONVENTA_DESC, \n" +
                "	NOMBRE_EMPRESA_COM, \n" +
                "	FAX_EMPRESA \n" +
                "FROM\n" +
                "	VIEW_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ESTADO IN ('AP','VA') AND  0 < (SELECT COUNT(ID_INTERESADO) FROM EFE_INTERESADO_DOCUMENTO WHERE ID_DOCUMENTO = VIEW_DOCUMENTOS_ENC.ID_DOCUMENTO AND ENVIADO = 0)";  
    
    public static final String GET_DETALLE_DOCUMENTO = "SELECT \n" +
                "	ID_DOCUMENTO, ID_DOCUMENTO_DET, LINEA, TIPOCODIGO_EMP, CODIGO, CANTIDAD, CODIGOMEDIDA_EMP,  \n" +
                "	UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, NATURALEZADESCUENTO, \n" +
                "	SUBTOTAL, MONTOTOTALLINEA, MERC_O_SERV, CANTIDAD_RESTANTE, MONTO_RESTANTE  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_DET \n" +
                "WHERE \n" +
                "	ID_DOCUMENTO = ? ";  
    
    public static final String GET_DETALLE_DOCUMENTO_NC = "SELECT \n" +
                "	ID_DOCUMENTO, ID_DOCUMENTO_DET, LINEA, TIPOCODIGO_EMP, CODIGO, CANTIDAD, CODIGOMEDIDA_EMP,  \n" +
                "	UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, NATURALEZADESCUENTO, \n" +
                "	SUBTOTAL, MONTOTOTALLINEA, MERC_O_SERV, CANTIDAD_RESTANTE, MONTO_RESTANTE  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_DET \n" +
                "WHERE \n" +
                "	ID_DOCUMENTO = ? AND CANTIDAD_RESTANTE > 0 AND MONTO_RESTANTE > 0 ";  
    
    public static final String GET_IMPUESTO_DETALLE = "SELECT \n" +
                "	ID_DOCUMENTO_DET, ID_DOC_DETIMPUESTOS, CODIGOIMPUESTO_EMPRESA, TARIFA, MONTO, TIPO_EXON_EMPRESA, NUMERODOCUMENTO,  \n" +
                "	NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_DET_IMP \n" +
                "WHERE \n" +
                "	ID_DOCUMENTO_DET = ? "; 
    
    public static final String GET_MEDIO_PAGO = "SELECT \n" +
                "	CODIGOMEDIOPAGO,  \n" +
                "	(SELECT COMPROBANTE_REQUERIDO FROM EFE_MEDIOSPAGO WHERE CODIGOMEDIOPAGO = EFE_MEDIOSPAGO_EMP.CODIGOMEDIOPAGO)   \n" +
                "FROM \n" +
                "	EFE_MEDIOSPAGO_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOMEDIOPAGO_EMP = ? "; 
    
    public static final String VALIDA_DOCUMENTO_RECEPCION = "SELECT \n" +
                "	COD_MENSAJE_HACIENDA  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ENC \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CLAVENUMERICA = ? AND EMITIDA = 0"; 
    
    public static final String ADD_DOCUMENTO_RECEPCION = "insert into EFE_DOCUMENTOS_ENC ( TIPO_COMPROBANTE, CLAVENUMERICA, NUMEROCONSECUTIVO, "
            + "CONDICIONVENTA, PLAZOCREDITO, CODIGOMONEDA, TIPOCAMBIO, TOTALSERVGRAVADOS, TOTALSERVEXENTOS, TOTALMERCANCIASGRAVADOS, "
            + "TOTALMERCANCIASEXENTOS, TOTALGRAVADO, TOTALEXENTO, TOTALVENTA, TOTALDESCUENTOS, TOTALVENTANETA, TOTALIMPUESTOS, "
            + "TOTALCOMPROBANTE, ESTADO, NUMERORESOLUCION, FECHARESOLUCION, ULT_NOVEDAD, TIPO_IDEN_RECEPTOR, NUMERO_IDEN_RECEPTOR, "
            + "RAZON_SOCIAL_RECEPTOR, EMAIL_RECEPTOR, ES_RECEPTOR, FECHAEMISION, TIPO_IDENTIFICACION_EMP, NUM_IDENTIFICACION_EMP, "
            + "RAZON_SOCIAL_EMP, NOMBRE_COMERCIAL_EMP, CORREO_ELECTRONICO_EMP, EMITIDA, COD_MENSAJE_HACIENDA, ID_EMPRESA, EMPRESA, TAG_IMPUESTO) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
        
    public static final String ADD_MEDIO_PAGO_RECEPCION = "insert into EFE_DOCUMENTOS_MEDIO_PAGO ( CODIGOMEDIOPAGO, ID_DOCUMENTO) "
            + "VALUES (?,?)"; 
    
    public static final String ADD_REFERENCIA_RECEPCION = "insert into EFE_DOCUMENTOS_REF ( ID_DOCUMENTO, TIPO_COMPROBANTE_REF, DOCUMENTO_REF,"
            + "FECHAEMISION, CODIGO, RAZON) "
            + "VALUES (?,?,?,?,?,?)"; 
    
    public static final String ADD_DETALLE_RECEPCION = "insert into EFE_DOCUMENTOS_DET ( ID_DOCUMENTO, LINEA, TIPOCODIGO, CODIGO, CANTIDAD,"
            + "CODIGOMEDIDA, UNIDADMEDIDACOMERCIAL, DETALLE, PRECIOUNITARIO, MONTOTOTAL, MONTODESCUENTO, NATURALEZADESCUENTO, SUBTOTAL, MONTOTOTALLINEA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 
    
    public static final String ADD_IMPUESTO_RECEPCION = "insert into EFE_DOCUMENTOS_DET_IMP ( ID_DOCUMENTO_DET, CODIGOIMPUESTO, TARIFA, MONTO, CODIGOTIPOEXO,"
            + "NUMERODOCUMENTO, NOMBREINSTITUCION, FECHAEMISION, MONTOIMPUESTO, PORCENTAJECOMPRA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?)"; 
    
    public static final String GET_ESTADOS_DOCUMENTO = "SELECT \n" +
                "	ESTADO, DESCRIPCION  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_ESTADO ORDER BY ORDEN ASC"; 
    
    public static final String GET_CODIGO_MENSAJE_HACIENDA = "SELECT \n" +
                "	COD_MENSAJE_HACIENDA, DESCRIPCION  \n" +
                "FROM \n" +
                "	EFE_MENSAJE_HACIENDA_REC "; 
    
    public static final String GET_CODIGO_DOC_REF = "SELECT \n" +
                "	CODIGO, DESCRIPCION, (SELECT GROUP_CONCAT(CODIGO_EMP) FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND CODIGO = EFE_TIPOS_DOCUMENTOS_REF.CODIGO)  \n" +
                "FROM \n" +
                "	EFE_TIPOS_DOCUMENTOS_REF "; 
    
    public static final String GET_SITUACION_DOCUMENTO = "SELECT \n" +
                "	SITUACION, DESCRIPCION  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_SITUACION "; 
    
    public static final String GET_TIPO_DOCUMENTOS_DET = "SELECT \n" +
                "	MERC_O_SERV, DESCRIPCION  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_DET_TIPO "; 
    
    public static final String GET_TIPO_DOCUMENTO_REFERENCIA = "SELECT CODIGO FROM EFE_TIPOS_DOCUMENTOS_REF_EMP WHERE ID_EMPRESA = ? AND "
                + " CODIGO_EMP = ?  ";
    
    public static final String GET_CODIGO_DOCUMENTO_REF = "SELECT \n" +
                "	CODIGO, DESCRIPCION  \n" +
                "FROM \n" +
                "	EFE_DOCUMENTOS_REF_COD "; 
    
    public static final String VERIFICA_DOC_RECEPCION = "SELECT \n" +
                "	ID_DOCUMENTO , DATE(FECHAEMISION) AS FECHAEMISION \n" +
                " FROM \n" +
                "	EFE_DOCUMENTOS_ENC WHERE ID_EMPRESA = ? AND CLAVENUMERICA = ? AND EMITIDA = 0"; 
    
    public static final String GET_INTERESADO_DOCUMENTO = "SELECT \n" +
                "	ID_DOCUMENTO, CORREO, ENVIADO, ID_INTERESADO  \n" +
                "FROM \n" +
                "	EFE_INTERESADO_DOCUMENTO WHERE ID_DOCUMENTO = ?"; 
    
    public static final String GET_PREFERENCIAS = "SELECT \n" +
                "	(SELECT COD_SUCURSAL_EMP FROM EFE_SUCURSALES WHERE ID_SUCURSALES = EFE_PREFERENCIAS_USUARIO.ID_SUCURSALES AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	(SELECT COD_PUNTO_VENTA FROM EFE_PUNTOS_DE_VENTA WHERE ID_PUNTOS_DE_VENTA = EFE_PREFERENCIAS_USUARIO.ID_PUNTOS_DE_VENTA AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	ID_PAGINA,  \n" +
                "	(SELECT COD_CLIENTE_REF FROM EFE_CLIENTES WHERE ID_CLIENTE = EFE_PREFERENCIAS_USUARIO.ID_CLIENTE AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	(SELECT CODIGOMEDIOPAGO_EMP FROM EFE_MEDIOSPAGO_EMP WHERE ID_MEDIOPAGO_EMP = EFE_PREFERENCIAS_USUARIO.ID_MEDIOPAGO_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	(SELECT CODIGOCONDICION_EMPRESA FROM EFE_CONDICIONESVENTAS_EMP WHERE ID_CONDICIONVENTA_EMP = EFE_PREFERENCIAS_USUARIO.ID_CONDICIONVENTA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA), \n" +
                "	(SELECT COD_MONEDA_EMPRESA FROM EFE_MONEDAS_EMP WHERE ID_MONEDA_EMP = EFE_PREFERENCIAS_USUARIO.ID_MONEDA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	(SELECT CODIGOMEDIDA_EMPRESA FROM EFE_CODIGOSMEDIDA_EMP WHERE ID_CODIGOMEDIDA_EMP = EFE_PREFERENCIAS_USUARIO.ID_CODIGOMEDIDA_EMP AND ID_EMPRESA = EFE_PREFERENCIAS_USUARIO.ID_EMPRESA),  \n" +
                "	ID_PREFERENCIA, MOSTRAR_FACTURA, ACTUALIZAR_PRECIOS, NOTIFICAR_EMISOR \n" +
                "FROM \n" +
                "	EFE_PREFERENCIAS_USUARIO WHERE ID_EMPRESA = ? AND ID_USUARIO = ? "; 
    
    public static final String ACTUALIZA_STATUS_DOC = "UPDATE EFE_DOCUMENTOS_ENC SET ACEPTADO_CLIENTE = ?, FECHA_ACEPTADO = NOW() WHERE CLAVENUMERICA = ? AND EMITIDA = '1' AND ESTADO IN ('AP', 'RE') AND ACEPTADO_CLIENTE IS NULL ";
    
    public static final String PR_ADD_PREFERENCIAS = "{CALL PR_ADD_PREFERENCIAS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PREFERENCIAS = "{CALL PR_UPDATE_PREFERENCIAS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String ADD_LOGO_EMPRESA = "insert into EFE_ARCHIVOS ( ID_EMPRESA, NOMBRE, RUTA_ARCHIVO,"
            + "MIME_TYPE, DOC_SIZE, ID_TIPO_ARCHIVO) "
            + "VALUES (?,'Logo empresa',?,?,?,?)"; 
    
    public static final String ACTUALIZA_STATUS_INTERESADO_CORREO = "UPDATE EFE_INTERESADO_DOCUMENTO SET ENVIADO = ? WHERE ID_INTERESADO = ? ";
    
    public static final String GET_DATOS_LOGIN_EMPRESA = "SELECT \n" +
                "	(SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EMP.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1),  \n" +
                "       EMP.FECHA_PROXIMO_PAGO, \n" +
                "       EMP.ID_PLAN, \n" +
                "       EMP.ID_TIPO_PLAN, \n" +
                "       (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_TIPO_ARCHIVO = 1 AND ESTADO = 1 AND ID_EMPRESA = (SELECT ID_EMPRESA FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE AND ES_DISTRIBUIDOR = 1)), \n" +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?),\n" +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?), \n" +
                "       (SELECT VALOR_1 FROM EFE_PARAMETROS WHERE ID_PARAMETRO = ?), \n" +
                "       EMP.FECHA_EXPIRA_CERTIFICADO \n" +
                " FROM EFE_EMPRESAS EMP WHERE EMP.ID_EMPRESA = ? " ;
    
    public static final String ACTUALIZA_CLAVE_USUARIO = "UPDATE SEG_USUARIOS SET DS_USUARIO_PASSWORD = ? WHERE ID_USUARIO = ? AND DS_USUARIO_PASSWORD = ?";
    
    public static final String PR_VALIDA_OLVIDE_CLAVE = "{CALL PR_VALIDA_OLVIDE_CLAVE(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String RESETEA_CLAVE_USUARIO = "UPDATE SEG_USUARIOS SET DS_USUARIO_PASSWORD = ? WHERE ID_USUARIO = ?";
    
    public static final String PR_ADD_LOGO_EMPRESA = "{CALL PR_ADD_LOGO_EMPRESA(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_TIPO_DOC = "SELECT \n" +
                "	CODIGOTIPODOC \n" +
                "FROM \n" +
                "	EFE_TIPOS_DOCUMENTOS_EMP \n" +
                "WHERE \n" +
                "	ID_EMPRESA = ? AND CODIGOTIPODOC_EMPRESA = ?";
    
    public static final String GET_EMPRESA = "SELECT \n" +
                "	EFE_EMPRESAS.RAZON_SOCIAL,\n" +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_EMPRESAS.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	EFE_EMPRESAS.NUM_IDENTIFICACION,\n" +
                "	EFE_EMPRESAS.NOMBRE_COMERCIAL,\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_PROVINCIA AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_CANTON AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_DISTRITO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_BARRIO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	EFE_EMPRESAS.OTRAS_SENAS,\n" +
                "	EFE_EMPRESAS.COD_PAIS_TELEFONO,\n" +
                "	EFE_EMPRESAS.NUM_TELEFONO,\n" +
                "	EFE_EMPRESAS.COD_PAIS_FAX,\n" +
                "	EFE_EMPRESAS.NUM_FAX,\n" +
                "	EFE_EMPRESAS.CORREO_ELECTRONICO,\n" +
                "	EFE_EMPRESAS.EMPRESA,\n" +
                "	EFE_EMPRESAS.PIN_CERTIFICADO,\n" +
                "	EFE_EMPRESAS.IDEN_INGRESO,\n" +
                "	EFE_EMPRESAS.CLAVE_INGRESO,\n" +
                "	CASE  \n" +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN \n" +
                "                               'pruebas.p12'\n" +
                "                       ELSE\n" +
                "                               'produccion.p12'\n" +
                "                       END, \n" +
                "	CASE  \n" +
                "                       WHEN (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) IS NOT NULL THEN \n" +
                "                               (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1)\n" +
                "                       ELSE\n" +
                "                               '' \n" +
                "                       END, \n" +
                "	CASE  \n" +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN \n" +
                "                               'Pruebas' \n" +
                "                       ELSE\n" +
                "                               'Producción' \n" +
                "                       END, \n" +
                "	EFE_EMPRESAS.CORREO_ADM_FE \n" +
                "FROM \n" +
                "	EFE_EMPRESAS WHERE ID_EMPRESA = ?";
    
    public static final String PR_GENERA_CLAVE_CONSECUTIVO = "{CALL PR_GENERA_CLAVE_CONSECUTIVO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_VALIDA_DOC_ANTERIORES = "{CALL PR_VALIDA_DOC_ANTERIORES_PR(?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_PROVINCIAS = "SELECT\n" +
                "	CODIGO_LUGAR_EMP ,\n" +
                "	( SELECT NOMBRE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO)\n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA_EMP\n" +
                "WHERE\n" +
                "	ID_EMPRESA = ?\n" +
                "AND 1 = (SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO)";
    
    public static final String GET_CANTONES = "SELECT\n" +
                "	CODIGO_LUGAR_EMP,\n" +
                "	NOMBRE\n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA_EMP GEO, EFE_DIST_GEOGRAFICA GEO_EMP\n" +
                "WHERE\n" +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO\n" +
                "	AND ID_EMPRESA = ?\n" +
                "	AND NIVEL = 2\n" +
                "	AND ID_DIST_GEO_PERTENECE = (SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 1 = (SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO))";
    
    public static final String GET_DISTRITOS = "SELECT\n" +
                "	CODIGO_LUGAR_EMP ,\n" +
                "	NOMBRE\n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA_EMP GEO ,\n" +
                "	EFE_DIST_GEOGRAFICA GEO_EMP\n" +
                "WHERE\n" +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO\n" +
                "AND ID_EMPRESA = ? \n" +
                "AND NIVEL = 3\n" +
                "AND ID_DIST_GEO_PERTENECE =(\n" +
                "	SELECT\n" +
                "		ID_DIST_GEO\n" +
                "	FROM\n" +
                "		EFE_DIST_GEOGRAFICA_EMP\n" +
                "	WHERE\n" +
                "		CODIGO_LUGAR_EMP = ?\n" +
                "	AND ID_EMPRESA = ?\n" +
                "	AND 2 =(\n" +
                "		SELECT\n" +
                "			NIVEL\n" +
                "		FROM\n" +
                "			EFE_DIST_GEOGRAFICA\n" +
                "		WHERE\n" +
                "			ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "	)\n" +
                "	AND(\n" +
                "		SELECT\n" +
                "			ID_DIST_GEO_PERTENECE\n" +
                "		FROM\n" +
                "			EFE_DIST_GEOGRAFICA\n" +
                "		WHERE\n" +
                "			ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "	) =(\n" +
                "		SELECT\n" +
                "			ID_DIST_GEO\n" +
                "		FROM\n" +
                "			EFE_DIST_GEOGRAFICA_EMP\n" +
                "		WHERE\n" +
                "			CODIGO_LUGAR_EMP = ?\n" +
                "		AND ID_EMPRESA = ?\n" +
                "		AND 1 =(\n" +
                "			SELECT\n" +
                "				NIVEL\n" +
                "			FROM\n" +
                "				EFE_DIST_GEOGRAFICA\n" +
                "			WHERE\n" +
                "				ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "		)\n" +
                "	)\n" +
                ")";
    
    public static final String GET_BARRIOS = "SELECT\n" +
                "	CODIGO_LUGAR_EMP ,\n" +
                "	NOMBRE\n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA_EMP GEO ,\n" +
                "	EFE_DIST_GEOGRAFICA GEO_EMP\n" +
                "WHERE\n" +
                "	GEO.ID_DIST_GEO = GEO_EMP.ID_DIST_GEO\n" +
                "AND ID_EMPRESA = ?\n" +
                "AND NIVEL = 4\n" +
                "AND ID_DIST_GEO_PERTENECE =(\n" +
                "	SELECT\n" +
                "		ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 3 =(\n" +
                "			SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "		) AND(\n" +
                "			SELECT ID_DIST_GEO_PERTENECE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "		) =(\n" +
                "			SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 2 =(\n" +
                "				SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "			) AND(\n" +
                "				SELECT ID_DIST_GEO_PERTENECE FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "			) =(\n" +
                "				SELECT ID_DIST_GEO FROM EFE_DIST_GEOGRAFICA_EMP WHERE CODIGO_LUGAR_EMP = ? AND ID_EMPRESA = ? AND 1 =(\n" +
                "					SELECT NIVEL FROM EFE_DIST_GEOGRAFICA WHERE ID_DIST_GEO = EFE_DIST_GEOGRAFICA_EMP.ID_DIST_GEO\n" +
                "				)\n" +
                "			)\n" +
                "		)\n" +
                ")";
    
    public static final String DELETE_RECARGOS_ARTICULOS = "DELETE FROM EFE_ARTICULOS_EMP_RECARGO WHERE ID_ARTICULO_RECARGO = ? ";
    
    public static final String DELETE_IMPUESTOS_ARTICULO = "DELETE FROM EFE_ARTICULOS_EMP_IMPUESTOS WHERE ID_ARTICULO = ? ";
    
    public static final String GET_PROVINCIAS_REG = "SELECT\n" +
                "	ID_DIST_GEO ,\n" +
                "	NOMBRE \n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA \n" +
                "WHERE\n" +
                "	NIVEL = 1";
    
    public static final String GET_CANTONES_REG = "SELECT\n" +
                "	ID_DIST_GEO ,\n" +
                "	NOMBRE \n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA \n" +
                "WHERE\n" +
                "	NIVEL = 2 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_DISTRITOS_REG = "SELECT\n" +
                "	ID_DIST_GEO ,\n" +
                "	NOMBRE \n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA \n" +
                "WHERE\n" +
                "	NIVEL = 3 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_BARRIOS_REG = "SELECT\n" +
                "	ID_DIST_GEO ,\n" +
                "	NOMBRE \n" +
                "FROM\n" +
                "	EFE_DIST_GEOGRAFICA \n" +
                "WHERE\n" +
                "	NIVEL = 4 AND ID_DIST_GEO_PERTENECE = ?";
    
    public static final String GET_TIPOS_IDEN_REG = "SELECT\n" +
                "	TIPO_IDENT ,\n" +
                "	DESCRIPCION \n" +
                "FROM\n" +
                "	EFE_TIPOS_IDEN";
    
    public static final String GET_GRUPOS = "SELECT\n" +
                "	ID_GRUPO_SEGURIDAD, \n" +
                "	DS_GRUPO_SEGURIDAD, \n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_GRUPOS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) AND FIND_IN_SET (ID_GRUPO_SEGURIDAD,IF(? = 'NA', ID_GRUPO_SEGURIDAD, ?))";
    
    public static final String GET_PAGS = "SELECT\n" +
                "	ID_PAGINA ,\n" +
                "	URL, \n" +
                "	DS_PAGINA, \n" +
                "	ID_PADRE, \n" +
                "	NIVEL, \n" +
                "	MENU, \n" +
                "	SECUENCIA, \n" +
                "	ICON, \n" +
                "	COLOR, \n" +
                "	PREFERENCIA, \n" +
                "	ESTADO \n" +
                "FROM\n" +
                "	SEG_PAGINAS";
    
    public static final String GET_USERS = "SELECT\n" +
                "	ID_USUARIO ,\n" +
                "	DS_USUARIO, \n" +
                "	DS_USUARIO_PASSWORD, \n" +
                "	DS_DESCRIPCION, \n" +
                "	ID_EMPRESA_DEFAULT, \n" +
                "	ST_USUARIO, \n" +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS.ID_EMPRESA_DEFAULT), \n" +
                "	CASE  \n" +
                "                       WHEN ST_USUARIO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_USUARIOS";
    
    public static final String GET_USERS_FILTRADOS = "SELECT\n" +
                "	ID_USUARIO ,\n" +
                "	DS_USUARIO, \n" +
                "	DS_USUARIO_PASSWORD, \n" +
                "	DS_DESCRIPCION, \n" +
                "	ID_EMPRESA_DEFAULT, \n" +
                "	ST_USUARIO, \n" +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS.ID_EMPRESA_DEFAULT), \n" +
                "	CASE  \n" +
                "                       WHEN ST_USUARIO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_USUARIOS WHERE ID_USUARIO IN (SELECT ID_USUARIO FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_GRUPO_SEGURIDAD <> 1 AND ID_GRUPO_SEGURIDAD <> 2 AND ID_EMPRESA = ?)";
    
    public static final String VERIFICA_SUPER_USUARIO = "SELECT IFNULL((SELECT 1 FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_GRUPO_SEGURIDAD = 1), 0)";
    
    public static final String GET_EMPRESA_USUARIO = "SELECT\n" +
                "	ID_EMPRESA ,\n" +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_USUARIOS_EMP_GRUPOS.ID_EMPRESA)  \n" +
                "FROM\n" +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? GROUP BY ID_EMPRESA";
    
    public static final String GET_GRUPOS_EMPRESA = "SELECT\n" +
                "	ID_GRUPO_SEGURIDAD ,\n" +
                "	(SELECT DS_GRUPO_SEGURIDAD FROM SEG_GRUPOS WHERE ID_GRUPO_SEGURIDAD = SEG_USUARIOS_EMP_GRUPOS.ID_GRUPO_SEGURIDAD)  \n" +
                "FROM\n" +
                "	SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? AND ID_EMPRESA = ? GROUP BY ID_GRUPO_SEGURIDAD";
    
    public static final String GET_KPI_TOTAL_VENTAS = "SELECT\n" +
                "	VISTA.FAC_EMITIDAS,\n" +
                "	CASE WHEN VISTA.FAC_EMITIDAS <> 0 THEN FORMAT(((VISTA.FAC_EMITIDAS * 100) / (VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) ),2) ELSE 0 END AS FAC_EMITIDAS_PORC,\n" +
                "	VISTA.FAC_RECIBIDAS,\n" +
                "	CASE WHEN FAC_RECIBIDAS <> 0 THEN FORMAT(((VISTA.FAC_RECIBIDAS * 100) / (VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) ),2) ELSE 0 END AS FAC_RECIBIDAS_PORC,\n" +
                "	(VISTA.FAC_EMITIDAS + VISTA.FAC_RECIBIDAS) AS TOTAL_FACTURAS,\n" +
                "	CASE WHEN VISTA.TOTAL_VENTAS <> 0 THEN FORMAT(VISTA.TOTAL_VENTAS,2)  ELSE 0 END \n" +
                "FROM\n" +
                "	( SELECT \n" +
                "		(\n" +
                "			SELECT\n" +
                "				COUNT(V1.ID_DOCUMENTO) AS FAC_EMITIDAS\n" +
                "			FROM\n" +
                "				VIEW_DOCUMENTOS_KPI V1\n" +
                "			WHERE\n" +
                "				V1.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA \n" +
                "			AND V1.EMITIDA = 1\n" +
                "			AND (\n" +
                "				(V1.ESTADO = 'AP')\n" +
                "				OR (V1.ESTADO = 'RE'))\n" +
                "			 AND DATE(V1.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V1.FECHA_ULT_CONSULTA), ?) AND DATE(V1.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V1.FECHA_ULT_CONSULTA), ?)\n" +
                "		) AS FAC_EMITIDAS,\n" +
                "		(\n" +
                "			SELECT\n" +
                "				COUNT(V2.ID_DOCUMENTO) AS FAC_RECIBIDAS\n" +
                "			FROM\n" +
                "				VIEW_DOCUMENTOS_KPI V2\n" +
                "			WHERE\n" +
                "				V2.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA \n" +
                "			AND V2.EMITIDA = 0\n" +
                "			AND (\n" +
                "				(V2.ESTADO = 'AP')\n" +
                "				OR (V2.ESTADO = 'RE'))\n" +
                "			AND DATE(V2.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V2.FECHA_ULT_CONSULTA), ?) AND DATE(V2.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V2.FECHA_ULT_CONSULTA), ?)\n" +
                "		) AS FAC_RECIBIDAS,\n" +
                "		(IFNULL((\n" +
                "			SELECT\n" +
                "				SUM(TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTAL_VENTAS\n" +
                "			FROM\n" +
                "				VIEW_DOCUMENTOS_KPI V3\n" +
                "			WHERE\n" +
                "				V3.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA \n" +
                "			AND V3.EMITIDA = 1\n" +
                "			AND (\n" +
                "				(V3.ESTADO = 'AP')\n" +
                "				OR (V3.ESTADO = 'RE'))\n" +
                "			AND DATE(V3.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) AND DATE(V3.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?)\n" +
                "			AND V3.TIPO_COMPROBANTE <> '03'), 0) - \n" +
                "		IFNULL((\n" +
                "			SELECT\n" +
                "				SUM(TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTAL_VENTAS\n" +
                "			FROM\n" +
                "				VIEW_DOCUMENTOS_KPI V3\n" +
                "			WHERE\n" +
                "				V3.ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA \n" +
                "			AND V3.EMITIDA = 1\n" +
                "			AND (\n" +
                "				(V3.ESTADO = 'AP')\n" +
                "				OR (V3.ESTADO = 'RE'))\n" +
                "			AND DATE(V3.FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?) AND DATE(V3.FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(V3.FECHA_ULT_CONSULTA), ?)\n" +
                "			AND V3.TIPO_COMPROBANTE = '03'), 0)) AS TOTAL_VENTAS \n" +
                "	FROM EFE_EMPRESAS WHERE ID_EMPRESA = ?) VISTA ";
    
    public static final String GET_KPI_ARTICULOS_FAC = "SELECT\n" +
                "	CODIGO,\n" +
                "	SUM(CANTIDAD) AS CANTIDAD,\n" +
                "	DETALLE\n" +
                "FROM\n" +
                "	VIEW_DOCUMENTO_ALL\n" +
                "WHERE ID_EMPRESA = ? AND EMITIDA = 1 AND (ESTADO = 'AP' OR ESTADO = 'RE') \n" +
                "	AND DATE(FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) AND DATE(FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?)\n" +
                "GROUP BY CODIGO ORDER BY CANTIDAD DESC LIMIT ? ";
    
    public static final String GET_KPI_CLIENTES_FAC = "SELECT\n" +
                "	 NOMBRE_RECEPTOR,\n" +
                "	 SUM( TOTALCOMPROBANTE * TIPOCAMBIO) AS TOTALCOMPROBANTE\n" +
                "FROM\n" +
                "	VIEW_DOCUMENTOS_ENC \n" +
                "WHERE ID_EMPRESA = ? AND EMITIDA  = 1 AND (ESTADO = 'AP' OR ESTADO = 'RE') \n" +
                "	AND DATE(FECHA_ULT_CONSULTA) >=  IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?) AND DATE(FECHA_ULT_CONSULTA) <= IF(? = 'NA', DATE(FECHA_ULT_CONSULTA), ?)\n" +
                "GROUP BY COD_CLIENTE ORDER BY TOTALCOMPROBANTE DESC LIMIT ? ";
    
    public static final String PR_ADD_USUARIO = "{CALL PR_ADD_USUARIO(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_USUARIO_RELACION = "{CALL PR_ADD_USUARIO_RELACION(?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_USUARIO = "{CALL PR_UPDATE_USUARIO(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String DELETE_USUARIO_RELACIONES = "DELETE FROM SEG_USUARIOS_EMP_GRUPOS WHERE ID_USUARIO = ? ";
    
    public static final String GET_PLANES = "SELECT\n" +
                "	ID_PLAN,\n" +
                "	DESCRIPCION,\n" +
                "	PRECIO_MENSUAL,\n" +
                "	CANTIDAD_USUARIOS,\n" +
                "	CODIGOMONEDA,\n" +
                "	(SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_PLANES.CODIGOMONEDA),\n" +
                "	(SELECT SIMBOLO FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_PLANES.CODIGOMONEDA),\n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_PLANES WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String GET_TIPOS_PLAN = "SELECT\n" +
                "	ID_TIPO_PLAN,\n" +
                "	DESCRIPCION,\n" +
                "	CANTIDAD_MESES,\n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_TIPO_PLANES WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String PR_ADD_PLAN = "{CALL PR_ADD_PLAN(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_PLAN = "{CALL PR_UPDATE_PLAN(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_ADD_TIPO_PLAN = "{CALL PR_ADD_TIPO_PLAN(?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_TIPO_PLAN = "{CALL PR_UPDATE_TIPO_PLAN(?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DISTRIBUIDORES = "SELECT \n" +
                "	EFE_EMPRESAS.RAZON_SOCIAL,\n" +
                "	(SELECT GROUP_CONCAT(TIPO_IDEN_EMPRESA) FROM EFE_TIPOS_IDEN_EMP WHERE TIPO_IDEN = EFE_EMPRESAS.TIPO_IDENTIFICACION AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	EFE_EMPRESAS.NUM_IDENTIFICACION,\n" +
                "	EFE_EMPRESAS.NOMBRE_COMERCIAL,\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_PROVINCIA AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_CANTON AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_DISTRITO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	(SELECT GROUP_CONCAT(CODIGO_LUGAR_EMP) FROM EFE_DIST_GEOGRAFICA_EMP WHERE ID_DIST_GEO = EFE_EMPRESAS.ID_BARRIO AND ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA),\n" +
                "	EFE_EMPRESAS.OTRAS_SENAS,\n" +
                "	EFE_EMPRESAS.COD_PAIS_TELEFONO,\n" +
                "	EFE_EMPRESAS.NUM_TELEFONO,\n" +
                "	EFE_EMPRESAS.COD_PAIS_FAX,\n" +
                "	EFE_EMPRESAS.NUM_FAX,\n" +
                "	EFE_EMPRESAS.CORREO_ELECTRONICO,\n" +
                "	EFE_EMPRESAS.EMPRESA,\n" +
                "	EFE_EMPRESAS.PIN_CERTIFICADO,\n" +
                "	EFE_EMPRESAS.IDEN_INGRESO,\n" +
                "	EFE_EMPRESAS.CLAVE_INGRESO,\n" +
                "	CASE  \n" +
                "                       WHEN EFE_EMPRESAS.CLIENT_ID = 'api-stag' THEN \n" +
                "                               'pruebas.p12'\n" +
                "                       ELSE\n" +
                "                               'produccion.p12'\n" +
                "                       END, \n" +
                "	CASE  \n" +
                "                       WHEN (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1) IS NOT NULL THEN \n" +
                "                               (SELECT RUTA_ARCHIVO FROM EFE_ARCHIVOS WHERE ID_EMPRESA = EFE_EMPRESAS.ID_EMPRESA AND ID_TIPO_ARCHIVO = 1 AND ESTADO = 1)\n" +
                "                       ELSE\n" +
                "                               '' \n" +
                "                       END, \n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END, \n" +
                "	ADMINISTRA_COBROS,\n" +
                "	CASE  \n" +
                "                       WHEN ADMINISTRA_COBROS = 1 THEN \n" +
                "                               'Sí' \n" +
                "                       ELSE\n" +
                "                               'No' \n" +
                "                       END \n" +
                "FROM \n" +
                "	EFE_EMPRESAS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) AND ES_DISTRIBUIDOR = 1";
    
    public static final String DELETE_GRUPO_PAGINAS = "DELETE FROM SEG_GRUPOS_PAGINAS WHERE ID_GRUPO_SEGURIDAD = ? ";
    
    public static final String PR_ADD_GRUPO_USUARIO = "{CALL PR_ADD_GRUPO_USUARIO(?, ?, ?, ?)}";
    
    public static final String PR_ADD_GRUPO_PAGINA = "{CALL PR_ADD_GRUPO_PAGINA(?, ?, ?, ?)}";
    
    public static final String PR_UPDATE_GRUPO_USUARIO = "{CALL PR_UPDATE_GRUPO_USUARIO(?, ?, ?, ?, ?)}";
    
    public static final String GET_EMPRESAS_LISTADO = "SELECT \n" +
                "	EMP.RAZON_SOCIAL,\n" +
                "	EMP.TIPO_IDENTIFICACION,\n" +
                "	EMP.NUM_IDENTIFICACION,\n" +
                "	EMP.NOMBRE_COMERCIAL,\n" +
                "	EMP.ID_PROVINCIA,\n" +
                "	EMP.ID_CANTON,\n" +
                "	EMP.ID_DISTRITO,\n" +
                "	EMP.ID_BARRIO,\n" +
                "	EMP.OTRAS_SENAS,\n" +
                "	EMP.COD_PAIS_TELEFONO,\n" +
                "	EMP.NUM_TELEFONO,\n" +
                "	EMP.COD_PAIS_FAX,\n" +
                "	EMP.NUM_FAX,\n" +
                "	EMP.CORREO_ELECTRONICO,\n" +
                "	EMP.ID_EMPRESA,\n" +
                "	EMP.PIN_CERTIFICADO,\n" +
                "	EMP.IDEN_INGRESO,\n" +
                "	EMP.CLAVE_INGRESO,\n" +
                "	EMP.ID_PLAN, \n" +
                "	(SELECT DESCRIPCION FROM SEG_PLANES WHERE ID_PLAN = EMP.ID_PLAN), \n" +
                "	EMP.ID_TIPO_PLAN, \n" +
                "	(SELECT DESCRIPCION FROM SEG_TIPO_PLANES WHERE ID_TIPO_PLAN = EMP.ID_TIPO_PLAN), \n" +
                "	EMP.ID_EMPRESA_PADRE, \n" +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE), \n" +
                "	DATE_FORMAT(EMP.FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'), \n" +
                "	EMP.ES_DISTRIBUIDOR, \n" +
                "	EMP.ADMINISTRA_COBROS, \n" +
                "	EMP.MANEJA_HACIENDA, \n" +
                "	DATE_FORMAT(EMP.FECHA_PROXIMO_PAGO, '%Y-%m-%d %H:%i:%s'), \n" +
                "	EMP.CORREO_ADM_FE \n" +
                "FROM \n" +
                "	EFE_EMPRESAS EMP WHERE EMP.ES_DISTRIBUIDOR = IF(? = 'NA', EMP.ES_DISTRIBUIDOR, ?) AND EMP.ADMINISTRA_COBROS = IF(? = 'NA', EMP.ADMINISTRA_COBROS, ?)";
    
    public static final String GET_REFERIDOS = "SELECT \n" +
                "	EMP.RAZON_SOCIAL,\n" +
                "	EMP.TIPO_IDENTIFICACION,\n" +
                "	EMP.NUM_IDENTIFICACION,\n" +
                "	EMP.NOMBRE_COMERCIAL,\n" +
                "	EMP.ID_PROVINCIA,\n" +
                "	EMP.ID_CANTON,\n" +
                "	EMP.ID_DISTRITO,\n" +
                "	EMP.ID_BARRIO,\n" +
                "	EMP.OTRAS_SENAS,\n" +
                "	EMP.COD_PAIS_TELEFONO,\n" +
                "	EMP.NUM_TELEFONO,\n" +
                "	EMP.COD_PAIS_FAX,\n" +
                "	EMP.NUM_FAX,\n" +
                "	EMP.CORREO_ELECTRONICO,\n" +
                "	EMP.ID_EMPRESA,\n" +
                "	EMP.PIN_CERTIFICADO,\n" +
                "	EMP.IDEN_INGRESO,\n" +
                "	EMP.CLAVE_INGRESO,\n" +
                "	EMP.ID_PLAN, \n" +
                "	(SELECT DESCRIPCION FROM SEG_PLANES WHERE ID_PLAN = EMP.ID_PLAN), \n" +
                "	EMP.ID_TIPO_PLAN, \n" +
                "	(SELECT DESCRIPCION FROM SEG_TIPO_PLANES WHERE ID_TIPO_PLAN = EMP.ID_TIPO_PLAN), \n" +
                "	EMP.ID_EMPRESA_PADRE, \n" +
                "	(SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE), \n" +
                "	DATE_FORMAT(EMP.FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'), \n" +
                "	EMP.ES_DISTRIBUIDOR, \n" +
                "	EMP.ADMINISTRA_COBROS, \n" +
                "	EMP.MANEJA_HACIENDA, \n" +
                "	DATE_FORMAT(EMP.FECHA_PROXIMO_PAGO, '%Y-%m-%d %H:%i:%s'), \n" +
                "	EMP.CORREO_ADM_FE \n" +
                "FROM \n" +
                "	EFE_EMPRESAS EMP WHERE EMP.ID_EMPRESA_PADRE = ? AND (SELECT ES_DISTRIBUIDOR FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE) = 1";
    
    public static final String GET_BANCOS = "SELECT\n" +
                "	ID_BANCO,\n" +
                "	DESCRIPCION,\n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_BANCOS WHERE ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String GET_CUENTAS_BANCO = "SELECT\n" +
                "	ID_CUENTA,\n" +
                "	ID_BANCO,\n" +
                "	CUENTA_CORRIENTE,\n" +
                "	CUENTA_CLIENTE,\n" +
                "	CUENTA_IBAN,\n" +
                "	CODIGOMONEDA,\n" +
                "	(SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_CUENTAS_BANCO.CODIGOMONEDA),\n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END, \n" +
                "	(SELECT SIMBOLO FROM EFE_MONEDAS WHERE CODIGOMONEDA = SEG_CUENTAS_BANCO.CODIGOMONEDA) \n" +
                "FROM\n" +
                "	SEG_CUENTAS_BANCO WHERE ID_BANCO = ? AND ESTADO = IF(? = 'NA', ESTADO, ?) ";
    
    public static final String PR_ADD_DEPOSITO_TRANSF = "{CALL PR_ADD_DEPOSITO_TRANSF(?, ?, ?, ?, ?, ?, ?, ?)}";
    
    public static final String GET_DEPOSITOS_TRANSF = "SELECT\n" +
                "	ID_CUENTA,\n" +
                "	NUMERO_COMPROBANTE,\n" +
                "	MONTO,\n" +
                "	DATE_FORMAT(FECHA_DEPOSITO, '%Y-%m-%d %H:%i:%s'),\n" +
                "	DATE_FORMAT(FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'),\n" +
                "	DATE_FORMAT(FECHA_VALIDA, '%Y-%m-%d %H:%i:%s'),\n" +
                "	ESTADO_MOVIMIENTO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO_MOVIMIENTO = 1 THEN \n" +
                "                               'Registrado' \n" +
                "                       WHEN ESTADO_MOVIMIENTO = 2 THEN \n" +
                "                               'Aprobado' \n" +
                "                       ELSE\n" +
                "                               'Rechazado' \n" +
                "                       END, \n" +
                "	(SELECT CUENTA_CORRIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT CUENTA_CLIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT CUENTA_IBAN FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT DESCRIPCION FROM SEG_BANCOS WHERE ID_BANCO = (SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)), \n" +
                "       IFNULL(OBSERVACIONES,''), \n" +
                "       ID_MOVIMIENTO, \n" +
                "       (SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = (SELECT CODIGOMONEDA FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)), \n" +
                "       (SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_EMPRESA) \n" +
                "FROM\n" +
                "	SEG_DEPOSITOS_TRANSFERENCIAS WHERE ID_EMPRESA = ? ";
    
    public static final String GET_DEPOSITOS_CHECK = "SELECT\n" +
                "	ID_CUENTA,\n" +
                "	NUMERO_COMPROBANTE,\n" +
                "	MONTO,\n" +
                "	DATE_FORMAT(FECHA_DEPOSITO, '%Y-%m-%d %H:%i:%s'),\n" +
                "	DATE_FORMAT(FECHA_REGISTRO, '%Y-%m-%d %H:%i:%s'),\n" +
                "	DATE_FORMAT(FECHA_VALIDA, '%Y-%m-%d %H:%i:%s'),\n" +
                "	ESTADO_MOVIMIENTO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO_MOVIMIENTO = 1 THEN \n" +
                "                               'Registrado' \n" +
                "                       WHEN ESTADO_MOVIMIENTO = 2 THEN \n" +
                "                               'Aprobado' \n" +
                "                       ELSE\n" +
                "                               'Rechazado' \n" +
                "                       END, \n" +
                "	(SELECT CUENTA_CORRIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT CUENTA_CLIENTE FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT CUENTA_IBAN FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA),\n" +
                "	(SELECT DESCRIPCION FROM SEG_BANCOS WHERE ID_BANCO = (SELECT ID_BANCO FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)), \n" +
                "       IFNULL(OBSERVACIONES,''), \n" +
                "       ID_MOVIMIENTO, \n" +
                "       (SELECT DESCRIPCION FROM EFE_MONEDAS WHERE CODIGOMONEDA = (SELECT CODIGOMONEDA FROM SEG_CUENTAS_BANCO WHERE ID_CUENTA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_CUENTA)), \n" +
                "       (SELECT RAZON_SOCIAL FROM EFE_EMPRESAS WHERE ID_EMPRESA = SEG_DEPOSITOS_TRANSFERENCIAS.ID_EMPRESA) \n" +
                "FROM\n" +
                "	SEG_DEPOSITOS_TRANSFERENCIAS";
    
    public static final String GET_SALDOS_EMPRESA = "SELECT\n" +
                "	EMP.ID_EMPRESA,\n" +
                "	IFNULL((SELECT SUM(MONTO) FROM SEG_DEPOSITOS_TRANSFERENCIAS WHERE ID_EMPRESA = EMP.ID_EMPRESA AND ESTADO_MOVIMIENTO = 2),0) PAGOS,\n" +
                "	IFNULL((SELECT SUM(MONTO) FROM SEG_COBROS_EMPRESA WHERE ID_EMPRESA = EMP.ID_EMPRESA),0) TOTAL_COBROS \n" +
                "FROM\n" +
                "	EFE_EMPRESAS EMP\n" +
                "WHERE\n" +
                "	EMP.MANEJA_HACIENDA = 1 AND (EMP.ID_EMPRESA_PADRE IS NULL OR 1 = (SELECT 1 FROM EFE_EMPRESAS WHERE ID_EMPRESA = EMP.ID_EMPRESA_PADRE AND (ES_DISTRIBUIDOR = 1 AND ADMINISTRA_COBROS = 0))) AND EMP.ID_EMPRESA = ? ";
    
    public static final String PR_VALIDA_DEPOSITO = "{CALL PR_VALIDA_DEPOSITO(?, ?, ?, ?, ?, ?)}";
    
    public static final String PR_GENERA_COBROS = "{CALL PR_GENERA_COBROS(?, ?)}";
    
    public static final String UPDATE_EXPIRA_TOKEN = "UPDATE EFE_EMPRESAS SET FECHA_EXPIRA_CERTIFICADO = ? WHERE ID_EMPRESA = ?";

    public static final String GET_GRUPOS_LISTA = "SELECT\n" +
                "	ID_GRUPO_SEGURIDAD, \n" +
                "	DS_GRUPO_SEGURIDAD, \n" +
                "	ESTADO,\n" +
                "	CASE  \n" +
                "                       WHEN ESTADO = 1 THEN \n" +
                "                               'Activo' \n" +
                "                       ELSE\n" +
                "                               'Inactivo' \n" +
                "                       END \n" +
                "FROM\n" +
                "	SEG_GRUPOS ";
    
    public static final String GET_RUTAS_XML = "SELECT CONCAT(RUTA_XML, CLAVENUMERICA, '.xml') archivo \n" +
                "FROM EFE_DOCUMENTOS_ENC  WHERE ID_EMPRESA = ? AND FIND_IN_SET (ID_DOCUMENTO,?) UNION \n" +
                "SELECT CONCAT(RUTA_XML, CLAVENUMERICA, 'R.xml') archivo FROM EFE_DOCUMENTOS_ENC  WHERE ID_EMPRESA = ? AND FIND_IN_SET (ID_DOCUMENTO,?) "; 
    
    public static final String ACTUALIZA_INTERESADO_NOTIF = "UPDATE EFE_INTERESADO_DOCUMENTO SET ENVIADO = 0 WHERE ID_DOCUMENTO = ? ";
    
    public static final String ACTUALIZA_PRECIO_ARTICULO = "UPDATE EFE_ARTICULOS_EMP SET PRECIO = ? WHERE ID_ARTICULO = ? AND ID_EMPRESA = ? AND 1 = (SELECT ACTUALIZAR_PRECIOS FROM EFE_PREFERENCIAS_USUARIO WHERE ID_EMPRESA = ? LIMIT 1) ";
    
    public static final String GET_EXONERACIONES_RADIUS = "SELECT Radius_FE_MaestroExoneraciones.id, EFE_CLIENTES.RAZON_SOCIAL, EFE_CLIENTES.NOMBRE_COMERCIAL,"
            + " Radius_FE_MaestroExoneraciones.CodCliente, Radius_FE_MaestroExoneraciones.CodImpuesto,"
            + " Radius_FE_MaestroExoneraciones.NumExo, Radius_FE_MaestroExoneraciones.PorcImpuesto,"
            + " Radius_FE_MaestroExoneraciones.BaseImpuesto, Radius_FE_MaestroExoneraciones.PorcExo,"
            + " Radius_FE_MaestroExoneraciones.FechaDesde, Radius_FE_MaestroExoneraciones.FechaHasta,"
            + " Radius_FE_MaestroExoneraciones.EnteEmisor, Radius_FE_MaestroExoneraciones.UltimaModificacion, Radius_FE_MaestroExoneraciones.CedCliente "
            + " FROM Radius_FE_MaestroExoneraciones , EFE_CLIENTES  "
            + " WHERE Radius_FE_MaestroExoneraciones.CedCliente = EFE_CLIENTES.NUM_IDENTIFICACION ";
    
    public static final String ADD_EXONERACION_RADIUS = "INSERT INTO Radius_FE_MaestroExoneraciones(id, CodCliente, CodImpuesto, CedCliente, NumExo, PorcImpuesto,"
            + " BaseImpuesto, PorcExo, FechaDesde, FechaHasta, EnteEmisor, UltimaModificacion) "
            + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now()); ";
    
     public static final String UPDATE_EXONERACION_RADIUS = "UPDATE Radius_FE_MaestroExoneraciones"
             + " SET CodCliente = ?, CodImpuesto = ?, CedCliente = ?, NumExo = ?, PorcImpuesto = ?, BaseImpuesto = ?, PorcExo = ?, FechaDesde = ?, FechaHasta = ?, EnteEmisor = ?"
             + " WHERE id = ?; ";

}
