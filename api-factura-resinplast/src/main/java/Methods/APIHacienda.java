/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import Constants.Constantes;
import Entities.Hacienda.Token;
import Entities.Hacienda.CheckHacienda;
import Entities.Hacienda.SendHacienda;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author msalasch
 */
public class APIHacienda {
    
    public Token getToken(String usuarioHacienda, String passwordHacienda) {
        Token token = null;
        try {
            String json = "";
            String grantType = "password";
            String username = usuarioHacienda;//"cpf-08-0114-0641@stag.comprobanteselectronicos.go.cr";
            String password = passwordHacienda;//"a*6$#{vU|Kz;%vB!0>%U";
            String clientId = Constantes.clientId;
            /*END POINT WS */
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(Constantes.apiHaciendaToken);

            /*Headers */
            post.setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");

            /*Body */
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("grant_type", grantType));
            urlParameters.add(new BasicNameValuePair("username", username));
            urlParameters.add(new BasicNameValuePair("password", password));
            urlParameters.add(new BasicNameValuePair("client_id", clientId));

            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            /*Execute */
            HttpResponse response = client.execute(post);

            /*Response */
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                json += line;
            }
            
            int status = response.getStatusLine().getStatusCode();
            
            if (status == 200) {
                Gson gson = new Gson();
                token = gson.fromJson(json, Token.class);
            }
            
            
        } catch (Exception ex) {
            return token;
        }
        return token;
    }
    
    public SendHacienda sendXML(String token, String clave, String fecha, String tipoIdEmisor, String idenEmisor, String callback, String baseXml) {
        SendHacienda sendHacienda = new SendHacienda();
        try {
            String json = "";
            
            /*END POINT WS */
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(Constantes.apiHaciendaSend);

            /*Headers */
            post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            post.setHeader("Authorization", "bearer " + token);

            /*Body */ 
            StringEntity input = new StringEntity("{\n" +
                                " 	\"clave\": \"" + clave + "\", \n" +
                                " 	\"fecha\": \"" + fecha + "\", \n" +
                                " 	\"emisor\": {\n" +
                                " 		\"tipoIdentificacion\": \"" + tipoIdEmisor + "\",\n" +
                                " 		\"numeroIdentificacion\": \"" + idenEmisor + "\" \n" +
                                " 	},\n" +
                                " 	\"callbackUrl\": \"" + callback + "\", \n" +
                                " 	\"comprobanteXml\": \"" + baseXml + "\" \n" +
                                " }");
            input.setContentType("application/json");
            post.setEntity(input);
            
            HttpResponse response = client.execute(post);

            /*Response */
            
            Header[] headers = response.getAllHeaders();
            for (Header header : headers) {
                if (header.getName().equals("Location")) {
                    sendHacienda.setMensaje(header.getValue());
                } else if (header.getName().equals("X-Error-Cause")) {
                    sendHacienda.setMensaje(header.getValue());
                }
            }
            
            int status = response.getStatusLine().getStatusCode();
            
            
            switch (status) {
                case 403:
                    //Forbidden = token invalido
                    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) {
                        json += line;
                    }  
                    
                    Gson gson = new Gson();
                    sendHacienda = gson.fromJson(json, SendHacienda.class);
                    
                    sendHacienda.setDocumentoEnviado(false);
                    break;
                case 404:
                    //Bad Request = comprobante ya ha sido enviado anteriormente
                    sendHacienda.setDocumentoEnviado(false);
                    break;
                case 202:
                    //Accepted = comprobante enviado exitosamente
                    sendHacienda.setDocumentoEnviado(true);
                    break;
                default:
                    sendHacienda.setDocumentoEnviado(false);
                    break;
            }
            
        } catch (Exception ex) {
            return sendHacienda;
        }
        return sendHacienda;
    }
    
    public CheckHacienda checkXML(String token, String apiConsulta) {
        CheckHacienda checkHacienda = null;
        try {
            String json = "";
            /*END POINT WS */
            HttpClient client = HttpClientBuilder.create().build();
            //HttpGet get = new HttpGet("https://api.comprobanteselectronicos.go.cr/recepcion-sandbox/v1/recepcion/" + idConsulta);
            HttpGet get = new HttpGet(apiConsulta);

            /*Headers */
            get.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            get.setHeader("Authorization", "bearer " + token);

            /*Execute */
            HttpResponse response = client.execute(get);

            /*Response */
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                json += line;
            }
            
            int status = response.getStatusLine().getStatusCode();
            
            if (status == 200) {
                Gson gson = new Gson();
                checkHacienda = gson.fromJson(json, CheckHacienda.class);
            }
            
            
        } catch (Exception ex) {
            return checkHacienda;
        }
        return checkHacienda;
    }
    
}
