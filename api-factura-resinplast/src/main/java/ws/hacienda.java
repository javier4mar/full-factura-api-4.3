/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import Auth.JWT;
import Constants.Constantes;
import Entities.FE.EmisorToken;
import Entities.Hacienda.Callback;
import Entities.MensajeWs.MensajeConsultaXml;
import Entities.MensajeWs.MensajeBase;
import Entities.MensajeWs.MensajeConsultaHacienda;
import Methods.IMetodos;
import Methods.Metodos;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Administrador
 *///No se usa - para eso esta la cola 
@Path("/haciendaws")
public class hacienda {
    @GET
    @Path("/enviar/{pIdDocumento}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase enviarDocumento(@HeaderParam("pToken") String pToken, @PathParam("pIdDocumento") String pIdDocumento) {
        MensajeBase mensajeEnvio = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeEnvio.setStatus(Constantes.statusError);
                mensajeEnvio.setMensaje("El token es requerido");
                return mensajeEnvio;
            }
            
            if (pIdDocumento == null || pIdDocumento.equals("")) {
                mensajeEnvio.setStatus(Constantes.statusError);
                mensajeEnvio.setMensaje("El id documento es requerido");
                return mensajeEnvio;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeEnvio.setStatus(Constantes.statusError);
                mensajeEnvio.setMensaje("Token invalido o expirado");
                return mensajeEnvio;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeEnvio = metodos.enviarDocumentoHacienda(mClaims, pIdDocumento);
            
            return mensajeEnvio;

        } catch (Exception e) {
            mensajeEnvio.setStatus(Constantes.statusError);
            mensajeEnvio.setMensaje("Error: " + e.getMessage());
            return mensajeEnvio;
        }
    }
    
    @GET
    @Path("/consultar/{pIdDocumento}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeConsultaHacienda consultarDocumento(@HeaderParam("pToken") String pToken, @PathParam("pIdDocumento") String pIdDocumento) {
        MensajeConsultaHacienda mensajeConsulta = new MensajeConsultaHacienda();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            if (pIdDocumento == null || pIdDocumento.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id documento es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.consultarDocumentoHacienda(mClaims, pIdDocumento);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje("Error: " + e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/consultarXml/{pIdDocumento}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeConsultaXml consultarXml(@HeaderParam("pToken") String pToken, @HeaderParam("pTipo") String pTipo, @PathParam("pIdDocumento") String pIdDocumento) {
        MensajeConsultaXml mensajeConsulta = new MensajeConsultaXml();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            if (pIdDocumento == null || pIdDocumento.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id documento es requerido");
                return mensajeConsulta;
            }
            
            if (pTipo == null || pTipo.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El tipo de documento es requerido");
                return mensajeConsulta;
            }
            
            if (!pTipo.equals("0") && !pTipo.equals("1")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El tipo de documento es invalido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.consultarXml(mClaims, pIdDocumento, pTipo);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje("Error: " + e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/callBack")
    @Produces(MediaType.APPLICATION_JSON)
    public void callback(Callback pRespuesta) {
        try {
            IMetodos metodos = new Metodos();
            
            metodos.callBack(pRespuesta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
