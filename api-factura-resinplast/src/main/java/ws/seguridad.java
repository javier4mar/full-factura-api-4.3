/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import Auth.JWT;
import Constants.Constantes;
import Entities.FE.ActualizaClave;
import Entities.FE.DatosOlvideClave;
import Entities.FE.DepositoTransfAdd;
import Entities.FE.EmisorToken;
import Entities.FE.FiltroKpi;
import Entities.FE.GrupoUsuarioAdd;
import Entities.FE.LoginSistema;
import Entities.FE.PlanAdd;
import Entities.FE.Preferencias;
import Entities.FE.TipoPlanAdd;
import Entities.FE.TokenCorreo;
import Entities.FE.UsrAdd;
import Entities.FE.ValidaDeposito;
import Entities.Hacienda.GeneraToken;
import Entities.MensajeWs.MensajeAddLogo;
import Entities.MensajeWs.MensajeBancos;
import Entities.MensajeWs.MensajeBase;
import Entities.MensajeWs.MensajeCatalogoFactura;
import Entities.MensajeWs.MensajeConsultaGrupos;
import Entities.MensajeWs.MensajeCuentasBanco;
import Entities.MensajeWs.MensajeDatosEmpresa;
import Entities.MensajeWs.MensajeDepositoTransf;
import Entities.MensajeWs.MensajeDistribuidores;
import Entities.MensajeWs.MensajeEmpresa;
import Entities.MensajeWs.MensajeEmpresas;
import Entities.MensajeWs.MensajeKpi;
import Entities.MensajeWs.MensajeLogin;
import Entities.MensajeWs.MensajeMenuLateral;
import Entities.MensajeWs.MensajePaginas;
import Entities.MensajeWs.MensajePags;
import Entities.MensajeWs.MensajePlanes;
import Entities.MensajeWs.MensajePreferencias;
import Entities.MensajeWs.MensajeSaldosEmp;
import Entities.MensajeWs.MensajeTiposPlan;
import Entities.MensajeWs.MensajeToken;
import Entities.MensajeWs.MensajeUsr;
import Entities.MensajeWs.MensajeUsrAdd;
import Entities.MensajeWs.MensajeUsuarios;
import Methods.IMetodos;
import Methods.Metodos;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import java.io.InputStream;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Administrador
 */
@Path("/seguridadws")
public class seguridad {
    @Context ServletContext servletContext;
    
    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeToken generarToken(@HeaderParam("pKey") String pKey, GeneraToken data) {
        MensajeToken mensajeToken = new MensajeToken();
        
        try {
            if (pKey == null || pKey.equals("")) {
                mensajeToken.setStatus(Constantes.statusError);
                mensajeToken.setMensaje("El key es requerido");
                return mensajeToken;
            }
            
            if (data.getpIdEmpresaFe() == null || data.getpIdEmpresaFe().equals("")) {
                mensajeToken.setStatus(Constantes.statusError);
                mensajeToken.setMensaje("El id empresa es requerido");
                return  mensajeToken;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeToken.setStatus(Constantes.statusError);
                mensajeToken.setMensaje("El key no es valido");
                return  mensajeToken;
            }
            
            IMetodos metodos = new Metodos();
                
            String token = metodos.getToken(data);
            
            if (token == null) {
                mensajeToken.setStatus(Constantes.statusError);
                mensajeToken.setMensaje("Error generando el token");
                return  mensajeToken;
            }
            
            mensajeToken.setStatus(Constantes.statusSuccess);
            mensajeToken.setMensaje("Token creado correctamente");
            mensajeToken.setToken(token);

            return  mensajeToken;
            
        } catch (Exception ex) {
            mensajeToken.setStatus(Constantes.statusError);
            mensajeToken.setMensaje(ex.getMessage());
            return  mensajeToken;
        }
        
    }
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeLogin login(@HeaderParam("pKey") String pKey, @HeaderParam("pIdEmpresaFe") String pIdEmpresaFe, @HeaderParam("pActualizaEmp") String pActualizaEmp, LoginSistema data) {
        MensajeLogin mensajeLogin = new MensajeLogin();
        try {
            
            if (data.getUsuario() == null || data.getUsuario().equals("")) {
                mensajeLogin.setStatus(Constantes.statusError);
                mensajeLogin.setMensaje("El usuario es requerido");
                return mensajeLogin;
            }
            
            if (data.getClave() == null || data.getClave().equals("")) {
                mensajeLogin.setStatus(Constantes.statusError);
                mensajeLogin.setMensaje("La clave es requerida");
                return mensajeLogin;
            }
            
            if (pKey == null || pKey.equals("")) {
                mensajeLogin.setStatus(Constantes.statusError);
                mensajeLogin.setMensaje("El key es requerido");
                return mensajeLogin;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeLogin.setStatus(Constantes.statusError);
                mensajeLogin.setMensaje("El key no es valido");
                return  mensajeLogin;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeLogin = metodos.login(data.getUsuario(), data.getClave(), pIdEmpresaFe, pActualizaEmp);
            
            return mensajeLogin;

        } catch (Exception e) {
            mensajeLogin.setStatus(Constantes.statusError);
            mensajeLogin.setMensaje(e.getMessage());
            return mensajeLogin;
        }
    }
    
    @GET
    @Path("/paginas/{pIdGrupo}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePaginas paginas(@HeaderParam("pToken") String pToken, @HeaderParam("pPreferencias") String pPreferencias, @PathParam("pIdGrupo") String pIdGrupo) {
        MensajePaginas mensajeConsulta = new MensajePaginas();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            if (pIdGrupo == null || pIdGrupo.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id grupo es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getPaginas(pIdGrupo, pPreferencias);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/menuLateral/{pIdGrupo}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeMenuLateral menuLateral(@HeaderParam("pToken") String pToken, @PathParam("pIdGrupo") String pIdGrupo) {
        MensajeMenuLateral mensajeConsulta = new MensajeMenuLateral();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            if (pIdGrupo == null || pIdGrupo.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id grupo es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getMenuLateral(pIdGrupo);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/usuarios/{pIdUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeUsuarios usuarios(@HeaderParam("pToken") String pToken, @PathParam("pIdUsuario") String pIdUsuario) {
        MensajeUsuarios mensajeUsuario = new MensajeUsuarios();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeUsuario.setStatus(Constantes.statusError);
                mensajeUsuario.setMensaje("El token es requerido");
                return mensajeUsuario;
            }else if (pIdUsuario == null || pIdUsuario.equals("")){
                mensajeUsuario.setStatus(Constantes.statusError);
                mensajeUsuario.setMensaje("El id usuario es requerido");
                return mensajeUsuario;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeUsuario.setStatus(Constantes.statusError);
                mensajeUsuario.setMensaje("Token invalido o expirado");
                return mensajeUsuario;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeUsuario = metodos.getUsuarios(pIdUsuario);
            
            return mensajeUsuario;

        } catch (Exception e) {
            mensajeUsuario.setStatus(Constantes.statusError);
            mensajeUsuario.setMensaje(e.getMessage());
            return mensajeUsuario;
        }
    }
    
    @GET
    @Path("/preferencias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePreferencias preferencias(@HeaderParam("pToken") String pToken) {
        MensajePreferencias mensajePreferencias = new MensajePreferencias();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajePreferencias.setStatus(Constantes.statusError);
                mensajePreferencias.setMensaje("El token es requerido");
                return mensajePreferencias;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajePreferencias.setStatus(Constantes.statusError);
                mensajePreferencias.setMensaje("Token invalido o expirado");
                return mensajePreferencias;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajePreferencias = metodos.getPreferencias(mClaims);
            
            return mensajePreferencias;

        } catch (Exception e) {
            mensajePreferencias.setStatus(Constantes.statusError);
            mensajePreferencias.setMensaje(e.getMessage());
            return mensajePreferencias;
        }
    }
    
    @POST
    @Path("/addPreferencias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addPreferencias(@HeaderParam("pToken") String pToken, Preferencias pPreferencias) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addPreferencias(mClaims, pPreferencias);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updatePreferencias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updatePreferencias(@HeaderParam("pToken") String pToken, Preferencias pPreferencias) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updatePreferencias(mClaims, pPreferencias);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/actualizaClave")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase actualizaClave(@HeaderParam("pToken") String pToken, ActualizaClave pDatos) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            if (pDatos.getpClaveAnterior() == null || pDatos.getpClaveAnterior().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("La clave anterior es requerida");
                return mensajeInsert;
            }
            
            if (pDatos.getpClaveNueva() == null || pDatos.getpClaveNueva().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar la nueva clave");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.actualizaClave(mClaims, pDatos);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/olvideClave")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase olvideClave(DatosOlvideClave pDatos) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pDatos.getpUsuario()== null || pDatos.getpUsuario().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El usuario es requerido");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.olvideClave(pDatos);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/reseteaClave")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase reseteaClave(@HeaderParam("pHashCorreo") String pHashCorreo, ActualizaClave pDatos) {
        
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pHashCorreo == null || pHashCorreo.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El hash es requerido");
                return mensajeInsert;
            }
            
            if (pDatos.getpClaveNueva() == null || pDatos.getpClaveNueva().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Debe indicar la nueva clave");
                return mensajeInsert;
            }
            
            TokenCorreo mClaims = JWT.verifyHashCorreo(pHashCorreo);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Hash invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.reseteaClave(mClaims, pDatos.getpClaveNueva());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/addLogoEmpresa")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeAddLogo addLogoEmpresa(@HeaderParam("pToken") String pToken, 
                                        @FormDataParam("pLogo") InputStream pLogo,
                                        @FormDataParam("pLogo") FormDataContentDisposition pLogoDetail) {
        
        MensajeAddLogo mensajeInsert = new MensajeAddLogo();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            if (pLogo == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El logo es requerido");
                return mensajeInsert;
            }
            
            String nombreLogo = pLogoDetail.getFileName().toUpperCase();
                
            if (!nombreLogo.endsWith(".JPG") && !nombreLogo.endsWith(".JPEG") && !nombreLogo.endsWith(".PNG")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Solo se permiten logos de tipo .jpeg, .jpg y .png");

                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            String rutaGlassfish = servletContext.getRealPath("/");
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addLogoEmpresa(mClaims, pLogo, pLogoDetail, rutaGlassfish);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/addEmpresa")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeEmpresa addEmpresa(@HeaderParam("pKey") String pKey,  
                                        @FormDataParam("pIdHomEmpresa") String pIdHomEmpresa,
                                        @FormDataParam("pRazonSocial") String pRazonSocial,
                                        @FormDataParam("pNombreComercial") String pNombreComercial,
                                        @FormDataParam("pTipoIdentificacion") String pTipoIdentificacion,
                                        @FormDataParam("pNumIdentificacion") String pNumIdentificacion,
                                        @FormDataParam("pIdProvincia") String pIdProvincia,
                                        @FormDataParam("pIdCanton") String pIdCanton,
                                        @FormDataParam("pIdDistrito") String pIdDistrito,
                                        @FormDataParam("pIdBarrio") String pIdBarrio,
                                        @FormDataParam("pOtrasSenas") String pOtrasSenas,
                                        @FormDataParam("pCodPaisTel") String pCodPaisTel,
                                        @FormDataParam("pTelefono") String pTelefono,
                                        @FormDataParam("pCodPaisFax") String pCodPaisFax,
                                        @FormDataParam("pFax") String pFax,
                                        @FormDataParam("pCorreoElectronico") String pCorreoElectronico,
                                        @FormDataParam("pCertificado") InputStream pCertificado,
                                        @FormDataParam("pCertificado") FormDataContentDisposition pCertificadoDetail,
                                        @FormDataParam("pPinCertificado") String pPinCertificado,
                                        @FormDataParam("pUsuarioHacienda") String pUsuarioHacienda,
                                        @FormDataParam("pClaveHacienda") String pClaveHacienda,
                                        @FormDataParam("pCorreoFe") String pCorreoFe,
                                        @FormDataParam("pNombreUsuario") String pNombreUsuario,
                                        @FormDataParam("pIdGrupo") String pIdGrupo,
                                        @FormDataParam("pIdPlan") String pIdPlan,
                                        @FormDataParam("pIdTipoPlan") String pIdTipoPlan,
                                        @FormDataParam("pIdPadre") String pIdPadre,
                                        @FormDataParam("pEsDistribuidor") String pEsDistribuidor,
                                        @FormDataParam("pAdministraCobros") String pAdministraCobros,
                                        @FormDataParam("pManejaHacienda") String pManejaHacienda,
                                        @FormDataParam("pLogo") InputStream pLogo,
                                        @FormDataParam("pLogo") FormDataContentDisposition pLogoDetail) {
        
        MensajeEmpresa mensajeInsert = new MensajeEmpresa();
        try {
            String rutaGlassfish = servletContext.getRealPath("/");
            
            if (pKey == null || pKey.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El key es requerido");
                return mensajeInsert;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El key no es valido");
                return  mensajeInsert;
            }
            
            if (pCertificado != null) {
                if (!pCertificadoDetail.getFileName().toUpperCase().endsWith(".P12")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El certificado debe ser de tipo p12");
                    return mensajeInsert;
                }
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addEmpresa(pIdHomEmpresa, pRazonSocial, pNombreComercial, pTipoIdentificacion, 
                    pNumIdentificacion, pIdProvincia, pIdCanton, pIdDistrito, pIdBarrio, pOtrasSenas, pCodPaisTel, 
                    pTelefono, pCodPaisFax, pFax, pCorreoElectronico, pCertificado, pCertificadoDetail, pPinCertificado, pUsuarioHacienda, 
                    pClaveHacienda, pCorreoFe, pNombreUsuario, pLogo, pLogoDetail, rutaGlassfish, pIdGrupo, pIdPlan, pIdTipoPlan, pEsDistribuidor, pIdPadre, pAdministraCobros, pManejaHacienda);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateEmpresaRef/{pIdEmpresa}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeAddLogo updateEmpresaRef(@HeaderParam("pKey") String pKey, 
                                        @PathParam("pIdEmpresa") String pIdEmpresa,
                                        @FormDataParam("pIdHomEmpresa") String pIdHomEmpresa,
                                        @FormDataParam("pRazonSocial") String pRazonSocial,
                                        @FormDataParam("pNombreComercial") String pNombreComercial,
                                        @FormDataParam("pTipoIdentificacion") String pTipoIdentificacion,
                                        @FormDataParam("pNumIdentificacion") String pNumIdentificacion,
                                        @FormDataParam("pIdProvincia") String pIdProvincia,
                                        @FormDataParam("pIdCanton") String pIdCanton,
                                        @FormDataParam("pIdDistrito") String pIdDistrito,
                                        @FormDataParam("pIdBarrio") String pIdBarrio,
                                        @FormDataParam("pOtrasSenas") String pOtrasSenas,
                                        @FormDataParam("pCodPaisTel") String pCodPaisTel,
                                        @FormDataParam("pTelefono") String pTelefono,
                                        @FormDataParam("pCodPaisFax") String pCodPaisFax,
                                        @FormDataParam("pFax") String pFax,
                                        @FormDataParam("pCorreoElectronico") String pCorreoElectronico,
                                        @FormDataParam("pCertificado") InputStream pCertificado,
                                        @FormDataParam("pCertificado") FormDataContentDisposition pCertificadoDetail,
                                        @FormDataParam("pPinCertificado") String pPinCertificado,
                                        @FormDataParam("pUsuarioHacienda") String pUsuarioHacienda,
                                        @FormDataParam("pClaveHacienda") String pClaveHacienda,
                                        @FormDataParam("pCorreoFe") String pCorreoFe,
                                        @FormDataParam("pNombreUsuario") String pNombreUsuario,
                                        @FormDataParam("pIdGrupo") String pIdGrupo,
                                        @FormDataParam("pEsDistribuidor") String pEsDistribuidor,
                                        @FormDataParam("pAdministraCobros") String pAdministraCobros,
                                        @FormDataParam("pManejaHacienda") String pManejaHacienda,
                                        @FormDataParam("pLogo") InputStream pLogo,
                                        @FormDataParam("pLogo") FormDataContentDisposition pLogoDetail) {
        
        MensajeAddLogo mensajeInsert = new MensajeAddLogo();
        try {
            String rutaGlassfish = servletContext.getRealPath("/");
            
            if (pKey == null || pKey.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El key es requerido");
                return mensajeInsert;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El key no es valido");
                return  mensajeInsert;
            }
            
            if (pCertificado != null) {
                if (!pCertificadoDetail.getFileName().toUpperCase().endsWith(".P12")) {
                    mensajeInsert.setStatus(Constantes.statusError);
                    mensajeInsert.setMensaje("El certificado debe ser de tipo p12");
                    return mensajeInsert;
                }
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updateEmpresaRef(pIdHomEmpresa, pRazonSocial, pNombreComercial, pTipoIdentificacion, 
                    pNumIdentificacion, pIdProvincia, pIdCanton, pIdDistrito, pIdBarrio, pOtrasSenas, pCodPaisTel, 
                    pTelefono, pCodPaisFax, pFax, pCorreoElectronico, pCertificado, pCertificadoDetail, pPinCertificado, pUsuarioHacienda, 
                    pClaveHacienda, pCorreoFe, pNombreUsuario, pLogo, pLogoDetail, rutaGlassfish, pIdGrupo, pEsDistribuidor, pAdministraCobros, pManejaHacienda, pIdEmpresa);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/datosEmpresa")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDatosEmpresa datosEmpresa(@HeaderParam("pToken") String pToken) {
        MensajeDatosEmpresa mensajeConsulta = new MensajeDatosEmpresa();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getDatosEmpresa(mClaims.getIdEmpresaFe());
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/updateEmpresa")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeAddLogo updateEmpresa(@HeaderParam("pToken") String pToken,  
                                        @FormDataParam("pRazonSocial") String pRazonSocial,
                                        @FormDataParam("pNombreComercial") String pNombreComercial,
                                        @FormDataParam("pTipoIdentificacionEmp") String pTipoIdentificacionEmp,
                                        @FormDataParam("pNumIdentificacion") String pNumIdentificacion,
                                        @FormDataParam("pIdProvinciaEmp") String pIdProvinciaEmp,
                                        @FormDataParam("pIdCantonEmp") String pIdCantonEmp,
                                        @FormDataParam("pIdDistritoEmp") String pIdDistritoEmp,
                                        @FormDataParam("pIdBarrioEmp") String pIdBarrioEmp,
                                        @FormDataParam("pOtrasSenas") String pOtrasSenas,
                                        @FormDataParam("pCodPaisTel") String pCodPaisTel,
                                        @FormDataParam("pTelefono") String pTelefono,
                                        @FormDataParam("pCodPaisFax") String pCodPaisFax,
                                        @FormDataParam("pFax") String pFax,
                                        @FormDataParam("pCorreoElectronico") String pCorreoElectronico,
                                        @FormDataParam("pCertificado") InputStream pCertificado,
                                        @FormDataParam("pCertificado") FormDataContentDisposition pCertificadoDetail,
                                        @FormDataParam("pPinCertificado") String pPinCertificado,
                                        @FormDataParam("pUsuarioHacienda") String pUsuarioHacienda,
                                        @FormDataParam("pClaveHacienda") String pClaveHacienda,
                                        @FormDataParam("pCorreoFe") String pCorreoFe,
                                        @FormDataParam("pNombreUsuario") String pNombreUsuario,
                                        @FormDataParam("pLogo") InputStream pLogo,
                                        @FormDataParam("pLogo") FormDataContentDisposition pLogoDetail) {
        
        MensajeAddLogo mensajeUpdate = new MensajeAddLogo();
        try {
            String rutaGlassfish = servletContext.getRealPath("/");
            
            if (pToken == null || pToken.equals("")) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("El token es requerido");
                return mensajeUpdate;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("Token invalido o expirado");
                return mensajeUpdate;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeUpdate = metodos.updateEmpresa(pRazonSocial, pNombreComercial, pTipoIdentificacionEmp, 
                    pNumIdentificacion, pIdProvinciaEmp, pIdCantonEmp, pIdDistritoEmp, pIdBarrioEmp, pOtrasSenas, pCodPaisTel, 
                    pTelefono, pCodPaisFax, pFax, pCorreoElectronico, pCertificado, pCertificadoDetail, pPinCertificado, pUsuarioHacienda, 
                    pClaveHacienda, pCorreoFe, pNombreUsuario, pLogo, pLogoDetail, rutaGlassfish, mClaims);

            return mensajeUpdate;

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());
            return mensajeUpdate;
        }
    }
    
    @GET
    @Path("/provincias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura provincias() {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            
            
            IMetodos metodos = new Metodos();
                
            mensajeConsulta = metodos.getProvinciasReg();
            
            return  mensajeConsulta;
            
        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());
                
            return  mensajeConsulta;
        }
        
    }
    
    @GET
    @Path("/cantones/{pIdProvincia}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura cantones(@PathParam("pIdProvincia") String pIdProvincia) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            
            if (pIdProvincia== null ||pIdProvincia.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getCantonesReg(pIdProvincia);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/distritos/{pIdCanton}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura distritos(@PathParam("pIdCanton") String pIdCanton) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            
            if (pIdCanton== null || pIdCanton.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del canton es requerido");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getDistritosReg(pIdCanton);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/barrios/{pIdDistrito}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura barrios(@PathParam("pIdDistrito") String pIdDistrito) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            
            if (pIdDistrito== null || pIdDistrito.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del distrito es requerido");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getBarriosReg(pIdDistrito);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/tiposIden")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura tiposIden() {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        
        try {
            IMetodos metodos = new Metodos();
                
            mensajeConsulta = metodos.getTiposIdenReg();
            
            return  mensajeConsulta;
            
        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());
                
            return  mensajeConsulta;
        }
        
    }
    
    @GET
    @Path("/pags")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePags pags(@HeaderParam("pToken") String pToken) {
        MensajePags mensajeConsulta = new MensajePags();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getPags();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/grupos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeConsultaGrupos grupos(@HeaderParam("pToken") String pToken, @HeaderParam("pIdGrupoSesion") String pIdGrupoSesion, @HeaderParam("pEstado") String pEstado) {
        MensajeConsultaGrupos mensajeConsulta = new MensajeConsultaGrupos();
        try {
            if (pIdGrupoSesion == null || pIdGrupoSesion.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El grupo del usuario en sesion es requerido");
                return mensajeConsulta;
            }
            
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getGrupos(pIdGrupoSesion, pEstado);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/usuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeUsr usuarios(@HeaderParam("pToken") String pToken) {
        MensajeUsr mensajeConsulta = new MensajeUsr();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getUsers(mClaims);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addUsuario")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeUsrAdd addUsuario(@HeaderParam("pToken") String pToken, UsrAdd pDatos) {
        MensajeUsrAdd mensajeInsert = new MensajeUsrAdd();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addUser(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateUsuario/{pIdUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateUsuario(@HeaderParam("pToken") String pToken, @PathParam("pIdUsuario") String pIdUsuario, UsrAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updateUser(mClaims, pDatos, pIdUsuario);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/kpi")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeKpi kpi(@HeaderParam("pToken") String pToken, FiltroKpi pDatos) {
        
        MensajeKpi mensajeConsulta = new MensajeKpi();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getKpis(mClaims, pDatos);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/planes")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajePlanes planes(@HeaderParam("pKey") String pKey, @HeaderParam("pEstado") String pEstado) {
        
        MensajePlanes mensajeConsulta = new MensajePlanes();
        try {
            if (pKey == null || pKey.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El key es requerido");
                return mensajeConsulta;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El key no es valido");
                return  mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getPlanes(pEstado);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addPlan")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addPlan(@HeaderParam("pToken") String pToken, PlanAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addPlan(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updatePlan/{pIdPlan}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updatePlan(@HeaderParam("pToken") String pToken, @PathParam("pIdPlan") String pIdPlan, PlanAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updatePlan(mClaims, pDatos, pIdPlan);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/tiposPlan")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTiposPlan tiposPlan(@HeaderParam("pKey") String pKey, @HeaderParam("pEstado") String pEstado) {
        
        MensajeTiposPlan mensajeConsulta = new MensajeTiposPlan();
        try {
            if (pKey == null || pKey.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El key es requerido");
                return mensajeConsulta;
            }
            
            if (!JWT.SECRET_UID_KEY.equals(pKey)) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El key no es valido");
                return  mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getTiposPlan(pEstado);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addTipoPlan")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoPlan(@HeaderParam("pToken") String pToken, TipoPlanAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addTipoPlan(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateTipoPlan/{pIdTipoPlan}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateTipoPlan(@HeaderParam("pToken") String pToken, @PathParam("pIdTipoPlan") String pIdTipoPlan, TipoPlanAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updateTipoPlan(mClaims, pDatos, pIdTipoPlan);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/distribuidores")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDistribuidores distribuidores(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado) {
        MensajeDistribuidores mensajeConsulta = new MensajeDistribuidores();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getDistribuidores(pEstado);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addGrupoUsuario")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addGrupoUsuario(@HeaderParam("pToken") String pToken, GrupoUsuarioAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addGrupoUsuario(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @POST
    @Path("/updateGrupoUsuario/{pIdGrupo}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateGrupoUsuario(@HeaderParam("pToken") String pToken, @PathParam("pIdGrupo") String pIdGrupo, GrupoUsuarioAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.updateGrupoUsuario(mClaims, pDatos, pIdGrupo);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/empresas")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeEmpresas empresas(@HeaderParam("pToken") String pToken, @HeaderParam("pEsDistribuidor") String pEsDistribuidor, @HeaderParam("pAdministraCobros") String pAdministraCobros) {
        MensajeEmpresas mensajeConsulta = new MensajeEmpresas();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getEmpresas(pEsDistribuidor, pAdministraCobros);
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/referidos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeEmpresas referidos(@HeaderParam("pToken") String pToken) {
        MensajeEmpresas mensajeConsulta = new MensajeEmpresas();
        
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getReferidos(mClaims.getIdEmpresaFe());
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/bancos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBancos bancos(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado) {
        
        MensajeBancos mensajeConsulta = new MensajeBancos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getBancos(pEstado);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/cuentasBanco/{pIdBanco}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCuentasBanco cuentasBanco(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado, @PathParam("pIdBanco") String pIdBanco) {
        
        MensajeCuentasBanco mensajeConsulta = new MensajeCuentasBanco();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            if (pIdBanco == null || pIdBanco.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El banco es requerido");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getCuentasBanco(pIdBanco, pEstado);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/addDepositoTransferencia")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addDepositoTransferencia(@HeaderParam("pToken") String pToken, DepositoTransfAdd pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.addDepositoTransf(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/depositosTransf")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDepositoTransf depositosTransf(@HeaderParam("pToken") String pToken) {
        
        MensajeDepositoTransf mensajeConsulta = new MensajeDepositoTransf();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getDepositosTransf(mClaims.getIdEmpresaFe());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/depositosCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDepositoTransf depositosCheck(@HeaderParam("pToken") String pToken) {
        
        MensajeDepositoTransf mensajeConsulta = new MensajeDepositoTransf();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getDepositosCheck();

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/saldosEmpresa")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeSaldosEmp saldosEmpresa(@HeaderParam("pToken") String pToken) {
        
        MensajeSaldosEmp mensajeConsulta = new MensajeSaldosEmp();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getSaldosEmpresa(mClaims.getIdEmpresaFe());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @POST
    @Path("/validaTransaccion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase validaTransaccion(@HeaderParam("pToken") String pToken, ValidaDeposito pDatos) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeInsert = metodos.validaTransaccion(mClaims, pDatos);
            
            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    @GET
    @Path("/generaCobros")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase generaCobros() {
        MensajeBase mensajeConsulta = new MensajeBase();
        try {
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.generaCobros();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
    
    @GET
    @Path("/gruposLista")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeConsultaGrupos gruposLista(@HeaderParam("pToken") String pToken) {
        MensajeConsultaGrupos mensajeConsulta = new MensajeConsultaGrupos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }
            
            EmisorToken mClaims = JWT.verifyToken(pToken);
            
            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }
            
            IMetodos metodos = new Metodos();
            
            mensajeConsulta = metodos.getGruposLista();
            
            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }
  
}
