/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import Auth.JWT;
import Constants.Constantes;
import Entities.Catalogos.CodDescripcion;
import Entities.Catalogos.RadiusExoneraciones;
import Entities.FE.ProcesarCatalogoEmp;
import Entities.FE.AddTipoDocEmp;
import Entities.FE.ArticuloAdd;
import Entities.FE.EmisorToken;
import Entities.FE.GetBarrios;
import Entities.FE.GetCantones;
import Entities.FE.GetDistritos;
import Entities.FE.IdDescripcion;
import Entities.MensajeWs.MensajeArticulos;
import Entities.MensajeWs.MensajeBase;
import Entities.MensajeWs.MensajeCatalogoFactura;
import Entities.MensajeWs.MensajeCondicionesVenta;
import Entities.MensajeWs.MensajeDistGeografica;
import Entities.MensajeWs.MensajeExoneraciones;
import Entities.MensajeWs.MensajeImpuestos;
import Entities.MensajeWs.MensajeInsert;
import Entities.MensajeWs.MensajeMediosPago;
import Entities.MensajeWs.MensajeMonedas;
import Entities.MensajeWs.MensajeRadiusExoneraciones;
import Entities.MensajeWs.MensajeTipoIdentificacion;
import Entities.MensajeWs.MensajeTipos;
import Entities.MensajeWs.MensajeUnidadesMedida;
import Methods.IMetodos;
import Methods.Metodos;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Administrador
 */
@Path("/catalogosws")
public class catalogos {

    @GET
    @Path("/unidadesMedida")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeUnidadesMedida unidadesMedida(@HeaderParam("pToken") String pToken) {
        MensajeUnidadesMedida mensajeConsulta = new MensajeUnidadesMedida();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getUnidadesMedida(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addUnidadMedidaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addUnidadMedidaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addUnidadMedidaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteUnidadMedidaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteUnidadMedidaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La unidad de medida es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La unidad de medida de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteUnidadMedidaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/condicionesVenta")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCondicionesVenta condicionesVenta(@HeaderParam("pToken") String pToken) {
        MensajeCondicionesVenta mensajeConsulta = new MensajeCondicionesVenta();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getCondicionesVenta(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addCondicionVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addCondicionVentaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addCondicionVentaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteCondicionVentaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteCondicionVentaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La condicion de venta es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La condicion de venta de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteCondicionVentaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/distGeografica")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeDistGeografica distGeografica(@HeaderParam("pToken") String pToken) {
        List<CodDescripcion> niveles = new ArrayList<>();
        CodDescripcion nivel;
        MensajeDistGeografica mensajeConsulta = new MensajeDistGeografica();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getDistGeografica(mClaims);

            nivel = new CodDescripcion();
            nivel.setCodigo("0");
            nivel.setDescripcion("Pais");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("1");
            nivel.setDescripcion("Provincia");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("2");
            nivel.setDescripcion("Canton");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("3");
            nivel.setDescripcion("Distrito");

            niveles.add(nivel);

            nivel = new CodDescripcion();
            nivel.setCodigo("4");
            nivel.setDescripcion("Barrio");

            niveles.add(nivel);

            mensajeConsulta.setNiveles(niveles);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addDistGeoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addDistGeoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addDistGeoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteDistGeoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteDistGeoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La distribucion geografica es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La distribucion geografica de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteDistGeoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/mediosPago")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeMediosPago mediosPago(@HeaderParam("pToken") String pToken) {
        MensajeMediosPago mensajeConsulta = new MensajeMediosPago();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getMediosPago(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addMedioPagoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addMedioPagoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addMedioPagoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteMedioPagoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteMedioPagoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El medio de pago es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El medio de pago de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteMedioPagoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/impuestos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeImpuestos impuestos(@HeaderParam("pToken") String pToken) {
        MensajeImpuestos mensajeConsulta = new MensajeImpuestos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getImpuestos(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addImpuestoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addImpuestoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addImpuestoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteImpuestoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteImpuestoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El impuesto es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El impuesto de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteImpuestoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/monedas")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeMonedas monedas(@HeaderParam("pToken") String pToken) {
        MensajeMonedas mensajeConsulta = new MensajeMonedas();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getMonedas(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addMonedaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addMonedaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addMonedaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteMonedaEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteMonedaEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La moneda es requerida");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("La moneda de la empresa es requerida");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteMonedaEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposDocumento")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposDocumento(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposDocumento(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoDocumentoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoDocumentoEmp(@HeaderParam("pToken") String pToken, AddTipoDocEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoDocumentoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp(), data.getpDescripcion());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoDocumentoEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoDocumentoEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoDocumentoEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposExoneracion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposExoneracion(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposExoneracion(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoExoneracionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoExoneracionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoExoneracionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoExoneracionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoExoneracionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de exoneracion es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de exoneracion de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoExoneracionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposIdentificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipoIdentificacion tiposIdentificacion(@HeaderParam("pToken") String pToken) {
        MensajeTipoIdentificacion mensajeConsulta = new MensajeTipoIdentificacion();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposIdentificacion(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoIdentificacionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoIdentificacionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoIdentificacionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoIdentificacionEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoIdentificacionEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de identificacion es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de identificacion de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoIdentificacionEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/tiposCodigoArt")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposCodigoArt(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTiposCodigoArt(mClaims);

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/addTipoCodigoArtEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoCodigoArtEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoCodigoArtEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoCodigoArtEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoCodigoArtEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de codigo de articulo es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de codigo de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoCodigoArtEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/articulos/{pIdArticulo}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeArticulos articulos(@HeaderParam("pToken") String pToken, @HeaderParam("pEstado") String pEstado, @PathParam("pIdArticulo") String pIdArticulo) { //@PathParam("pIdArticulo") String pIdArticulo
        MensajeArticulos mensajeArticulo = new MensajeArticulos();

        try {

            if (pToken == null || pToken.equals("")) {
                mensajeArticulo.setStatus(Constantes.statusError);
                mensajeArticulo.setMensaje("El token es requerido");
                return mensajeArticulo;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            IMetodos metodos = new Metodos();

            mensajeArticulo = metodos.getArticulos(pIdArticulo, mClaims.getIdEmpresaFe(), pEstado);

            return mensajeArticulo;

        } catch (Exception ex) {
            mensajeArticulo.setStatus(Constantes.statusError);
            mensajeArticulo.setMensaje(ex.getMessage());
            return mensajeArticulo;
        }

    }

    @POST
    @Path("/addArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInsert addArticuloEmp(@HeaderParam("pToken") String pToken, ArticuloAdd data) {
        MensajeInsert mensajeInsert = new MensajeInsert();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addArticuloEmp(mClaims.getIdEmpresaFe(), data);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/updateArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateArticuloEmp(@HeaderParam("pToken") String pToken, ArticuloAdd data) {
        MensajeBase mensajeUpdate = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("El token es requerido");
                return mensajeUpdate;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeUpdate.setStatus(Constantes.statusError);
                mensajeUpdate.setMensaje("Token invalido o expirado");
                return mensajeUpdate;
            }

            IMetodos metodos = new Metodos();

            mensajeUpdate = metodos.updateArticuloEmp(mClaims.getIdEmpresaFe(), data);

            return mensajeUpdate;

        } catch (Exception e) {
            mensajeUpdate.setStatus(Constantes.statusError);
            mensajeUpdate.setMensaje(e.getMessage());
            return mensajeUpdate;
        }
    }

    @POST
    @Path("/deleteArticuloEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteArticuloEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El ID de articulo de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            mensajeDelete = metodos.deleteArticuloEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogoEmp());

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/exoneraciones/{pIdExoneracion}")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeExoneraciones exoneraciones(@HeaderParam("pToken") String pToken,
            @PathParam("pIdExoneracion") String pIdExoneracion) {
        MensajeExoneraciones mensajeExoneracion = new MensajeExoneraciones();

        try {

            if (pToken == null || pToken.equals("")) {
                mensajeExoneracion.setStatus(Constantes.statusError);
                mensajeExoneracion.setMensaje("El token es requerido");
                return mensajeExoneracion;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            IMetodos metodos = new Metodos();

            mensajeExoneracion = metodos.getExoneraciones(pIdExoneracion, mClaims.getIdEmpresaFe());

            return mensajeExoneracion;

        } catch (Exception ex) {
            mensajeExoneracion.setStatus(Constantes.statusError);
            mensajeExoneracion.setMensaje(ex.getMessage());
            return mensajeExoneracion;
        }

    }

    @GET
    @Path("/tiposDocumentoRef")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeTipos tiposDocumentoRef(@HeaderParam("pToken") String pToken) {
        MensajeTipos mensajeConsulta = new MensajeTipos();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getTipoDocRef(mClaims);

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/addTipoDocRefEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase addTipoDocRefEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeInsert = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addTipoDocRefEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp());

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }

    @POST
    @Path("/deleteTipoDocRefEmp")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase deleteTipoDocRefEmp(@HeaderParam("pToken") String pToken, ProcesarCatalogoEmp data) {
        MensajeBase mensajeDelete = new MensajeBase();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El token es requerido");
                return mensajeDelete;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Token invalido o expirado");
                return mensajeDelete;
            }

            if (data.getpIdCatalogo() == null || data.getpIdCatalogo().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de referencia es requerido");
                return mensajeDelete;
            }

            if (data.getpIdCatalogoEmp() == null || data.getpIdCatalogoEmp().equals("")) {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("El tipo de documento de referencia de la empresa es requerido");
                return mensajeDelete;
            }

            IMetodos metodos = new Metodos();

            if (metodos.deleteTipoDocRefEmp(mClaims.getIdEmpresaFe(), data.getpIdCatalogo(), data.getpIdCatalogoEmp())) {
                mensajeDelete.setStatus(Constantes.statusSuccess);
                mensajeDelete.setMensaje("Registro borrado con exito");
            } else {
                mensajeDelete.setStatus(Constantes.statusError);
                mensajeDelete.setMensaje("Error al borrar el registro");
            }

            return mensajeDelete;

        } catch (Exception e) {
            mensajeDelete.setStatus(Constantes.statusError);
            mensajeDelete.setMensaje(e.getMessage());
            return mensajeDelete;
        }
    }

    @GET
    @Path("/provincias")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura provincias(@HeaderParam("pToken") String pToken) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getProvincias(mClaims.getIdEmpresaFe());

            return mensajeConsulta;

        } catch (Exception ex) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(ex.getMessage());

            return mensajeConsulta;
        }

    }

    @POST
    @Path("/cantones")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura cantones(@HeaderParam("pToken") String pToken, GetCantones data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getCantones(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/distritos")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura distritos(@HeaderParam("pToken") String pToken, GetDistritos data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdCantonEmp() == null || data.getpIdCantonEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del canton es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getDistritos(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp(), data.getpIdCantonEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @POST
    @Path("/barrios")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeCatalogoFactura barrios(@HeaderParam("pToken") String pToken, GetBarrios data) {
        MensajeCatalogoFactura mensajeConsulta = new MensajeCatalogoFactura();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El token es requerido");
                return mensajeConsulta;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("Token invalido o expirado");
                return mensajeConsulta;
            }

            if (data.getpIdProvinciaEmp() == null || data.getpIdProvinciaEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id de la provincia es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdCantonEmp() == null || data.getpIdCantonEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del canton es requerido");
                return mensajeConsulta;
            }

            if (data.getpIdDistritoEmp() == null || data.getpIdDistritoEmp().equals("")) {
                mensajeConsulta.setStatus(Constantes.statusError);
                mensajeConsulta.setMensaje("El id del distrito es requerido");
                return mensajeConsulta;
            }

            IMetodos metodos = new Metodos();

            mensajeConsulta = metodos.getBarrios(mClaims.getIdEmpresaFe(), data.getpIdProvinciaEmp(), data.getpIdCantonEmp(), data.getpIdDistritoEmp());

            return mensajeConsulta;

        } catch (Exception e) {
            mensajeConsulta.setStatus(Constantes.statusError);
            mensajeConsulta.setMensaje(e.getMessage());
            return mensajeConsulta;
        }
    }

    @GET
    @Path("/RadiusMaestroExoneraciones")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeRadiusExoneraciones RadiusMaestroExoneraciones(@HeaderParam("pToken") String pToken) {
        MensajeRadiusExoneraciones mensajeExoneraciones = new MensajeRadiusExoneraciones();

        try {
            if (pToken == null || pToken.equals("")) {
                mensajeExoneraciones.setStatus(Constantes.statusError);
                mensajeExoneraciones.setMensaje("El token es requerido");
                return mensajeExoneraciones;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeExoneraciones.setStatus(Constantes.statusError);
                mensajeExoneraciones.setMensaje("Token invalido o expirado");
                return mensajeExoneraciones;
            }

            IMetodos metodos = new Metodos();

            mensajeExoneraciones = metodos.getRadiusExoneraciones();

            return mensajeExoneraciones;

        } catch (Exception ex) {
            mensajeExoneraciones.setStatus(Constantes.statusError);
            mensajeExoneraciones.setMensaje(ex.getMessage());

            return mensajeExoneraciones;
        }
    }

    @POST
    @Path("/RadiusMaestroExoneraciones")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeInsert crearRadiusMaestroExoneraciones(@HeaderParam("pToken") String pToken, RadiusExoneraciones data) {
        MensajeInsert mensajeInsert = new MensajeInsert();
        try {
            if (pToken == null || pToken.equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El token es requerido");
                return mensajeInsert;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("Token invalido o expirado");
                return mensajeInsert;
            }

            if (data.getCodCliente() == null || data.getCodCliente().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El cod cliente radius es requerido");
                return mensajeInsert;
            }

            if (data.getCodImpuesto() == null || data.getCodImpuesto().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El Cod Impuesto es requerido");
                return mensajeInsert;
            }

            if (data.getNumExo() == null || data.getNumExo().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El Num Exo es requerido");
                return mensajeInsert;
            }

            if (data.getPorcImpuesto() == null || data.getPorcImpuesto().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El Porc Impuesto es requerido");
                return mensajeInsert;
            }

            if (data.getBaseImpuesto() == null || data.getBaseImpuesto().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El base impuesto es requerido");
                return mensajeInsert;
            }

            if (data.getPorcExo() == null || data.getPorcExo().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El porc exo es requerido");
                return mensajeInsert;
            }

            if (data.getFechaDesde() == null || data.getFechaDesde().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El fecha desde requerido");
                return mensajeInsert;
            }

            if (data.getFechaHasta() == null || data.getFechaHasta().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El fecha hasta es requerido");
                return mensajeInsert;
            }

            if (data.getEnteEmisor() == null || data.getEnteEmisor().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("El ente emisor es requerido");
                return mensajeInsert;
            }

           
            if (data.getCedCliente() == null || data.getCedCliente().equals("")) {
                mensajeInsert.setStatus(Constantes.statusError);
                mensajeInsert.setMensaje("La ced de cliente es requerida");
                return mensajeInsert;
            }


            IMetodos metodos = new Metodos();

            mensajeInsert = metodos.addExoneracionRadius(data);

            return mensajeInsert;

        } catch (Exception e) {
            mensajeInsert.setStatus(Constantes.statusError);
            mensajeInsert.setMensaje(e.getMessage());
            return mensajeInsert;
        }
    }
    
    
    @POST
    @Path("/UpdateRadiusMaestroExoneraciones")
    @Produces(MediaType.APPLICATION_JSON)
    public MensajeBase updateRadiusMaestroExoneraciones(@HeaderParam("pToken") String pToken, RadiusExoneraciones data) {
        MensajeBase mensajeBase = new MensajeBase();
        try {
            
            if (pToken == null || pToken.equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El token es requerido");
                return mensajeBase;
            }

            EmisorToken mClaims = JWT.verifyToken(pToken);

            if (mClaims == null) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("Token invalido o expirado");
                return mensajeBase;
            }

             if (data.getId()== null || data.getId().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El id radius es requerido");
                return mensajeBase;
            }
             
            if (data.getCodCliente() == null || data.getCodCliente().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El cod cliente radius es requerido");
                return mensajeBase;
            }

            if (data.getCodImpuesto() == null || data.getCodImpuesto().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El Cod Impuesto es requerido");
                return mensajeBase;
            }

            if (data.getNumExo() == null || data.getNumExo().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El Num Exo es requerido");
                return mensajeBase;
            }

            if (data.getPorcImpuesto() == null || data.getPorcImpuesto().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El Porc Impuesto es requerido");
                return mensajeBase;
            }

            if (data.getBaseImpuesto() == null || data.getBaseImpuesto().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El base impuesto es requerido");
                return mensajeBase;
            }

            if (data.getPorcExo() == null || data.getPorcExo().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El porc exo es requerido");
                return mensajeBase;
            }

            if (data.getFechaDesde() == null || data.getFechaDesde().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El fecha desde requerido");
                return mensajeBase;
            }

            if (data.getFechaHasta() == null || data.getFechaHasta().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El fecha hasta es requerido");
                return mensajeBase;
            }

            if (data.getEnteEmisor() == null || data.getEnteEmisor().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("El ente emisor es requerido");
                return mensajeBase;
            }

           
            if (data.getCedCliente() == null || data.getCedCliente().equals("")) {
                mensajeBase.setStatus(Constantes.statusError);
                mensajeBase.setMensaje("La ced de cliente es requerida");
                return mensajeBase;
            }


            IMetodos metodos = new Metodos();

            mensajeBase = metodos.updateExoneracionRadius(data);

            return mensajeBase;

        } catch (Exception e) {
            mensajeBase.setStatus(Constantes.statusError);
            mensajeBase.setMensaje(e.getMessage());
            return mensajeBase;
        }
    }
}
